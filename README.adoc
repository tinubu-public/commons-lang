= commons-lang
:gitlab-url: https://gitlab.com
:default-branch: master
:project-path: tinubu-public/commons-lang

image:{gitlab-url}/{project-path}/badges/{default-branch}/pipeline.svg[link="{gitlab-url}/{project-path}/commits/{default-branch}",title="pipeline status"]
image:{gitlab-url}/{project-path}/badges/{default-branch}/coverage.svg[link="{gitlab-url}/{project-path}/commits/{default-branch}",title="coverage report"]

Generic Java language functions :

* Java bean accessor alternative logic using explicit annotations
* Application clock management so that time can be fixed in tests
* Date/time conversion for all types (ZonedDataTime, OffsetDateTime, Date, etc...) based on strategies
* Inverse enum mapping to be used generically in enums
* Collection/Stream helpers
* Simple validation (ala Apache commons)

[[utils]]
== Java utils

`StreamUtils` and `CollectionUtils` are null-safe, stream and collection builders.

They are null-safe means they support `null` parameters and ignore them, so that returned collection is never `null`.
IMPORTANT: However, these builders support and preserve `null` elements if they are part of the real collection elements.

When a factory is specified, the collection is always recreated using the specified factory.
Otherwise, the original non-null collection is not recreated if possible.
Special features like filtering or map entry merging will lead to collection recreation too.

.Null handling in builders
[source,java]
----
import static com.tinubu.commons.lang.CollectionUtils.list;

list(asList(1, null, 2)); // -> original ArrayList : [1, null, 2]
list((List<Integer>)null); // -> new ArrayList : []

list(1, null, 2); // -> new ArrayList : [1, null, 2]
list((List<Integer>[])null); // -> new ArrayList : []
----

These examples use `list`, but all operations follow the same pattern, including `StreamUtils::stream` and `StreamUtils::streamConcat` :

.Null handling in stream builders
[source,java]
----
import static com.tinubu.commons.lang.StreamUtils.stream;
import static com.tinubu.commons.lang.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.StreamUtils.entryStream;

stream(Stream.of(1, null, 2)); // -> original Stream : [1, null, 2]
stream((Stream<Integer>)null); // -> new Stream : []

entryStream(entry("a", 1), null, entry("b", null)); // -> new Stream<Entry> : [a->1, b->null]

streamConcat(asList(1, 2), null, asList(3, null)); // -> new Stream : [1, 2, 3, null]
streamConcat(Stream.of(1, 2), null, Stream.of(3, null)); // -> new Stream : [1, 2, 3, null]
----

=== Usage

.Stream usage
[source,java]
----
import static com.tinubu.commons.lang.StreamUtils.stream;

stream(); // -> new Stream : []
stream(1, 2, 3); // -> new Stream : [1, 2, 3]

stream(asList(1, 2, 3)); // -> new Stream : [1, 2, 3]

stream(Paths.get("path/subpath/file")); // -> new Stream : [Path(path), Path(subpath), Path(file)] because Path implements Iterable<Path>
----

.Collection usage
[source,java]
----
import static com.tinubu.commons.lang.CollectionUtils.list;
import static com.tinubu.commons.lang.CollectionUtils.collection;

list(); // -> new ArrayList : []
list(1, 2, 3); // -> new ArrayList : [1, 2, 3]

list(asList(1, 2, 3)); // -> original ArrayList : [1, 2, 3]

collection(HashSet::new); // -> new HashSet : []
collection(HashSet::new, 1, 2, 2); // -> new HashSet : [1, 2]
----

.Map usage
[source,java]
----
import static com.tinubu.commons.lang.CollectionUtils.entry;
import static com.tinubu.commons.lang.CollectionUtils.map;

map(); // -> new HashMap : []
map(entry("a", 1), entry("b", 2)) // -> new HashMap : [b->2, a->1]
map(new HashMap<>(...)) // -> original HashMap : [...]

map(LinkedHashMap::new, entry("a", 1), entry("b", 2)) // -> new LinkedHashMap : [a->1, b->2]

map((v1, v2) -> v1, entry("a", 1), entry("a", 2), entry("b", 3)) // -> new HashMap : [a->1, b->3]
----

You must use `StreamUtils::streamConcat` to flatten several collections.
More than 2 collections are supported, the implementation protects against `StackOverflowException` that can occur when composing `Stream.concat` several times.

.Collection flattening
[source,java]
----
import static com.tinubu.commons.lang.CollectionUtils.list;
import static com.tinubu.commons.lang.CollectionUtils.collection;
import static com.tinubu.commons.lang.StreamUtils.streamConcat;

list(streamConcat(asList(1, 2), asList(3, 4), asList(5))); // -> new ArrayList : [1, 2, 3, 4, 5]

collection(HashSet::new, streamConcat(asList(1, 2), asList(2, 3))) // -> new HashSet : [1, 2, 3]

map(streamConcat(map(entry("a", 1), entry("b", 2)), map(entry("c", 3), entry("d", 4)))); // -> new HashMap : [a->1, b->2, c->3, d->4]
----

.Null-safe immutable collections
[source,java]
----
import static com.tinubu.commons.lang.CollectionUtils.immutable;
import static com.tinubu.commons.lang.CollectionUtils.entry;
import static com.tinubu.commons.lang.CollectionUtils.map;

immutable(list(1, null, 2)) // -> new UnmodifiableList(ArrayList)) : [1, null, 2]
immutable((List)null) // -> new UnmodifiableList(EmptyList)) : []

immutable(map(LinkedHashMap::new, entry("a", 1), entry("b", 2))) // -> new UnmodifiableMap(LinkedHashMap)) : [a->1, b->2]
immutable((Map)null) // -> new UnmodifiableMap(EmptyMap)) : []
----

[[variant]]
== Variant

The `Variant` type can be used to manage alternative types for a single variable.
The framework provides type checking mechanism to enforce strong typing.

[[variant-implementations]]
=== Variant implementations

The library provides the following implementations :

* `UnboundVariant` : Variant supporing any type
* `BoundVariant` : Variant supporting only subtypes of declared super type
* `NumberVariant` : Specialized variant for `Number` type.
Conversion is supported for types `Long`, `Integer`, `Short`, `Byte`, `Float`, `Double`, `BigDecimal`, `BigInteger`
* `ConvertVariant` : Variant with conversion support from/to an external representation
* `StringConvertVariant` : Simplified `ConvertVariant` for `String` external representation

[[variant-usage]]
=== Variant usage

[source,java]
.Unbounded variant usage
----
UnboundVariant variant = new UnboundVariant<>(3 /*, Integer.class */);

Object objectValue = variant.value(); // OK
Integer intValue = variant.value(Integer.class); // OK
Number numberValue = variant.value(Number.class); // OK
Long longValue = variant.value(Long.class); // throws VariantTypeException
----

[source,java]
.Bounded variant usage
----
BoundVariant<Number> variant = new BoundVariant<>(3 /*, Integer.class */);

Number numberValue = variant.value(); // OK
Integer intValue = variant.value(Integer.class); // OK
Number numberValue = variant.value(Number.class); // OK
Long longValue = variant.value(Long.class); // throws VariantTypeException
String stringValue = variant.value(String.class); // throws VariantTypeException
----

[source,java]
.`NumberVariant` usage to support `Number` types converters
----
NumberVariant variant = new NumberVariant(3 /*, Integer.class */);

Number numberValue = variant.value(); // OK

Integer intValue = variant.value(Integer.class); // OK
Integer intValue = variant.integerValue(); // Alternative operation, OK

Long longValue = variant.value(Long.class); // throws VariantTypeException

Long longValue = variant.convertValue(Long.class); // OK
Long longValue = variant.longValue(); // Alternative operation, OK
----

[source,java]
.`StringConvertVariant` usage to read from a `String` external representation
----
StringConvertVariant variant = new StringConvertVariant("3", Long.class, conversionService); // <1>

Long longValue = variant.value(); // OK
Long longValue = variant.value(Long.class); // OK

Integer intValue = variant.value(Integer.class); // throws VariantTypeException
----

<1> You must provide a valid `ConversionService` implementation supporting required conversions (`String` &#8596; `Long`)

[source,java]
.`StringConvertVariant` usage to write to a `String` external representation
----
StringConvertVariant variant = new StringConvertVariant(3L, Long.class, conversionService);

Long longValue = variant.value(); // OK
Long longValue = variant.value(Long.class); // OK

String externalValue = variant.externalValue(); // <1>
----

<1> Provided `ConversionService` implementation will be used to generate the external representation

== Build

[[build-git-flow]]
=== Git flow

https://nvie.com/posts/a-successful-git-branching-model/[Git-flow] is a standardized branch model.

We use the following Git flow configuration :

Development branches :

* Development branch : `master` (non default)
* Releases branch : `releases` (non default)

Branches prefixes :

* Feature : `feature/`
* release : `release/`
* hotfix : `hotfix/`
* support : `support/`

Release version tags : `release-` (non default)

[[build-versioning]]
=== Versioning

We use the https://semver.org[Semantic versioning] : `<major>.<minor>.<patch>`, e.g.: `1.0.2`.

When we choose the next version for the project after a release, we always start by incrementing the _<minor>_ number.
The _<patch>_ number is reserved for security/hot-fixes. +
If the development introduces some backwards incompatible changes, we must release the current version, then increment the _<major>_ number and reset the _<minor>_ number to 0 for next version.

[[build-release]]
=== Release procedure

<<build-git-flow,Git flow>> must be configured correctly.

The library release supervisor must follow the procedure :

1. Use `git-flow` to start the release : `git flow release start <release-version>`
2. Bump the current project version and external dependencies that are still in `-SNAPSHOT` on release branch :
* Set release version : `mvn versions:set -DnewVersion=<release-version>`
* External dependencies version must be manually updated
* You *must* run `mvn clean test` successfully
3. Use `git-flow` to finish the release : `git flow release finish <release-version>`
+
CAUTION: Do not push your changes now, before having bumped the development branch version, or library artifacts will be deployed to repositories with release version and will corrupt Maven repository caches on various locations
+
4. Bump the current project version on development branch following the <<build-versioning>> instructions.
+
NOTE: Only the current project version is required to be bumped to next snapshot version, external dependencies should only be bumped when required, and preferably later, in a different commit.
5. Push all your changes, *including the tags*

==== Scripted procedure

A script is available for both release and hotfix procedures, that enforces the release guidelines :

.Release script
[source,bash]
----
release.sh release <release-version> [<resume step number>]
----

.Hotfix script
[source,bash]
----
release.sh hotfix <hotfix-version> [<resume step number>]
----
