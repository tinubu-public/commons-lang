/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.libraryselector;

import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.joining;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import com.tinubu.commons.lang.libraryselector.LibrarySelector.LibraryDefinition;
import com.tinubu.commons.lang.util.GlobalState;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.SetOnceGlobalState;

/**
 * A generic selector for different implementation libraries.
 * This selector should be instantiated statically.
 *
 * @param <T> library definition type
 */
public class LibrarySelector<T extends LibraryDefinition> {

   private final GlobalState<T> defaultLibrary = new SetOnceGlobalState<>();
   private final Map<T, Boolean> libraries;

   /**
    * Creates new instance.
    *
    * @param libraries all selectable libraries by priority order (from the highest to the lower
    *       priority)
    */
   public LibrarySelector(List<T> libraries) {
      this.libraries = immutable(initializeLibraries(list(libraries)));
   }

   /**
    * Creates new instance.
    *
    * @param libraries all selectable libraries by priority order (from the highest to the lower
    *       priority)
    */
   @SafeVarargs
   public LibrarySelector(T... libraries) {
      this(list(libraries));
   }

   /**
    * Returns first detected library by priority order that matches the specified filter, if any.
    *
    * @return first detected library or {@link Optional#empty} if no library matches
    */
   public Optional<T> library(Predicate<LibraryDefinition> filter) {
      notNull(filter, "filter");

      return stream(libraries.keySet()).filter(filter).filter(libraries::get).findFirst();
   }

   /**
    * Returns first detected library by priority order that conform to global configuration, if any.
    *
    * @return first detected library or {@link Optional#empty} if no library matches
    */
   public Optional<T> library() {
      return library(lib -> nullable(defaultLibrary.state(), lib).equals(lib));
   }

   /**
    * Returns first detected library by priority order that conform to global configuration.
    *
    * @return first detected library
    *
    * @throws IllegalStateException if no library matches
    */
   public T checkLibrary() {
      return library().orElseThrow(this::noMatchingLibraryException);
   }

   /**
    * Checks if specified library is present on classpath.
    *
    * @param library library that must be present
    *
    * @return whether the library has been found or not
    */
   public boolean hasLibrary(LibraryDefinition library) {
      notNull(library, "library");

      return library(lib -> lib.equals(library)).isPresent();
   }

   /**
    * Checks if specified library is present on classpath.
    *
    * @param library library that must be present
    *
    * @throws IllegalStateException if specified library is not present
    */
   public void checkLibrary(LibraryDefinition library) {
      notNull(library, "library");

      library(lib -> lib.equals(library)).orElseThrow(this::noMatchingLibraryException);
   }

   public IllegalStateException noMatchingLibraryException() {
      return noMatchingLibraryException(this.defaultLibrary.state());
   }

   public IllegalStateException noMatchingLibraryException(LibraryDefinition defaultLibrary) {
      String displayDefaultLibrary =
            nullable(defaultLibrary).map(mt -> "'" + mt.libraryName() + "' ").orElse("");
      String libraryReferences = stream(libraries.keySet())
            .map(lib -> "'" + lib.libraryName() + "' " + (libraries.get(lib) ? "(present)" : "(not present)"))
            .collect(joining(", "));

      return new IllegalStateException("No "
                                       + displayDefaultLibrary
                                       + "implementation found, supports : "
                                       + libraryReferences);
   }

   /**
    * Accessor to the default library configuration.
    *
    * @return default library configuration
    */
   public GlobalState<T> defaultLibrary() {
      return defaultLibrary;
   }

   private static <T extends LibraryDefinition> Map<T, Boolean> initializeLibraries(List<T> libraries) {
      return map(LinkedHashMap::new,
                 stream(libraries).map(lib -> Pair.of(lib, isClassPresentOnClasspath(lib.classSample()))));
   }

   private static boolean isClassPresentOnClasspath(String className) {
      try {
         Class.forName(className);
         return true;
      } catch (Throwable ex) {
         return false;
      }
   }

   /**
    * Definition of a library with various information.
    */
   public interface LibraryDefinition {

      /**
       * Library name for reference in messages. Format is arbitrary (e.g.:
       * {@code org.springframework:spring-core}).
       *
       * @return library reference name
       */
      String libraryName();

      /**
       * A FQDN {@link Class} name belonging the library for auto-detection purpose.
       *
       * @return FQDN class name
       */
      String classSample();

      default boolean sameLibrary(LibraryDefinition definition) {
         return equals(definition);
      }
   }

}
