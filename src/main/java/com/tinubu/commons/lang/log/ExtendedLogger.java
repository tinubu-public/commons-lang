/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.log;

import static com.tinubu.commons.lang.log.ExtendedLogger.Level.DEBUG;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.ERROR;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.INFO;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.TRACE;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.WARN;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.concurrent.Callable;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extends delegated {@link Logger} with new log operations, supporting lazy evaluation of both message and
 * arguments.
 */
public class ExtendedLogger {

   protected final Logger logger;

   protected ExtendedLogger(Logger logger) {
      this.logger = notNull(logger, "logger");
   }

   public static ExtendedLogger of(Logger logger) {
      return new ExtendedLogger(logger);
   }

   public static ExtendedLogger of(Class<?> clazz) {
      return new ExtendedLogger(LoggerFactory.getLogger(clazz));
   }

   public static ExtendedLogger of(String name) {
      return new ExtendedLogger(LoggerFactory.getLogger(name));
   }

   public enum Level {
      OFF, ERROR, WARN, INFO, DEBUG, TRACE
   }

   public void log(Level level, Callable<String> message) {
      notNull(level, "level");
      notNull(message, "message");

      switch (level) {
         case OFF:
            break;
         case ERROR:
            if (logger.isErrorEnabled()) logger.error(resolve(message));
            break;
         case WARN:
            if (logger.isWarnEnabled()) logger.warn(resolve(message));
            break;
         case INFO:
            if (logger.isInfoEnabled()) logger.info(resolve(message));
            break;
         case DEBUG:
            if (logger.isDebugEnabled()) logger.debug(resolve(message));
            break;
         case TRACE:
            if (logger.isTraceEnabled()) logger.trace(resolve(message));
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported %s log level", level));
      }
   }

   public void error(Callable<String> message) {
      log(ERROR, message);
   }

   public void warn(Callable<String> message) {
      log(WARN, message);
   }

   public void info(Callable<String> message) {
      log(INFO, message);
   }

   public void debug(Callable<String> message) {
      log(DEBUG, message);
   }

   public void trace(Callable<String> message) {
      log(TRACE, message);
   }

   public void log(Level level, String message) {
      notNull(message, "message");

      log(level, () -> message);
   }

   public void error(String message) {
      log(ERROR, message);
   }

   public void warn(String message) {
      log(WARN, message);
   }

   public void info(String message) {
      log(INFO, message);
   }

   public void debug(String message) {
      log(DEBUG, message);
   }

   public void trace(String message) {
      log(TRACE, message);
   }

   public void log(Level level, Callable<String> message, Callable<?> arg) {
      notNull(level, "level");
      notNull(message, "message");
      notNull(arg, "arg");

      switch (level) {
         case OFF:
            break;
         case ERROR:
            if (logger.isErrorEnabled()) logger.error(resolve(message), resolve(arg));
            break;
         case WARN:
            if (logger.isWarnEnabled()) logger.warn(resolve(message), resolve(arg));
            break;
         case INFO:
            if (logger.isInfoEnabled()) logger.info(resolve(message), resolve(arg));
            break;
         case DEBUG:
            if (logger.isDebugEnabled()) logger.debug(resolve(message), resolve(arg));
            break;
         case TRACE:
            if (logger.isTraceEnabled()) logger.trace(resolve(message), resolve(arg));
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported %s log level", level));
      }
   }

   public void error(Callable<String> message, Callable<?> arg) {
      log(ERROR, message, arg);
   }

   public void warn(Callable<String> message, Callable<?> arg) {
      log(WARN, message, arg);
   }

   public void info(Callable<String> message, Callable<?> arg) {
      log(INFO, message, arg);
   }

   public void debug(Callable<String> message, Callable<?> arg) {
      log(DEBUG, message, arg);
   }

   public void trace(Callable<String> message, Callable<?> arg) {
      log(TRACE, message, arg);
   }

   public void log(Level level, String message, Callable<?> arg) {
      notNull(message, "message");

      log(level, () -> message, arg);
   }

   public void error(String message, Callable<?> arg) {
      log(ERROR, message, arg);
   }

   public void warn(String message, Callable<?> arg) {
      log(WARN, message, arg);
   }

   public void info(String message, Callable<?> arg) {
      log(INFO, message, arg);
   }

   public void debug(String message, Callable<?> arg) {
      log(DEBUG, message, arg);
   }

   public void trace(String message, Callable<?> arg) {
      log(TRACE, message, arg);
   }

   public void log(Level level, Callable<String> message, Callable<?> arg1, Callable<?> arg2) {
      notNull(level, "level");
      notNull(message, "message");
      notNull(arg1, "arg1");
      notNull(arg2, "arg2");

      switch (level) {
         case OFF:
            break;
         case ERROR:
            if (logger.isErrorEnabled()) logger.error(resolve(message), resolve(arg1), resolve(arg2));
            break;
         case WARN:
            if (logger.isWarnEnabled()) logger.warn(resolve(message), resolve(arg1), resolve(arg2));
            break;
         case INFO:
            if (logger.isInfoEnabled()) logger.info(resolve(message), resolve(arg1), resolve(arg2));
            break;
         case DEBUG:
            if (logger.isDebugEnabled()) logger.debug(resolve(message), resolve(arg1), resolve(arg2));
            break;
         case TRACE:
            if (logger.isTraceEnabled()) logger.trace(resolve(message), resolve(arg1), resolve(arg2));
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported %s log level", level));
      }
   }

   public void error(Callable<String> message, Callable<?> arg1, Callable<?> arg2) {
      log(ERROR, message, arg1, arg2);
   }

   public void warn(Callable<String> message, Callable<?> arg1, Callable<?> arg2) {
      log(WARN, message, arg1, arg2);
   }

   public void info(Callable<String> message, Callable<?> arg1, Callable<?> arg2) {
      log(INFO, message, arg1, arg2);
   }

   public void debug(Callable<String> message, Callable<?> arg1, Callable<?> arg2) {
      log(DEBUG, message, arg1, arg2);
   }

   public void trace(Callable<String> message, Callable<?> arg1, Callable<?> arg2) {
      log(TRACE, message, arg1, arg2);
   }

   public void log(Level level, String message, Callable<?> arg1, Callable<?> arg2) {
      notNull(message, "message");

      log(level, () -> message, arg1, arg2);
   }

   public void error(String message, Callable<?> arg1, Callable<?> arg2) {
      log(ERROR, message, arg1, arg2);
   }

   public void warn(String message, Callable<?> arg1, Callable<?> arg2) {
      log(WARN, message, arg1, arg2);
   }

   public void info(String message, Callable<?> arg1, Callable<?> arg2) {
      log(INFO, message, arg1, arg2);
   }

   public void debug(String message, Callable<?> arg1, Callable<?> arg2) {
      log(DEBUG, message, arg1, arg2);
   }

   public void trace(String message, Callable<?> arg1, Callable<?> arg2) {
      log(TRACE, message, arg1, arg2);
   }

   public void log(Level level, Callable<String> message, Callable<?>... args) {
      notNull(level, "level");
      notNull(message, "message");
      noNullElements(args, "args");

      switch (level) {
         case OFF:
            break;
         case ERROR:
            if (logger.isErrorEnabled()) logger.error(resolve(message),
                                                      stream(args)
                                                            .map(ExtendedLogger::resolve)
                                                            .toArray(Object[]::new));
            break;
         case WARN:
            if (logger.isWarnEnabled()) logger.warn(resolve(message),
                                                    stream(args)
                                                          .map(ExtendedLogger::resolve)
                                                          .toArray(Object[]::new));
            break;
         case INFO:
            if (logger.isInfoEnabled()) logger.info(resolve(message),
                                                    stream(args)
                                                          .map(ExtendedLogger::resolve)
                                                          .toArray(Object[]::new));
            break;
         case DEBUG:
            if (logger.isDebugEnabled()) logger.debug(resolve(message),
                                                      stream(args)
                                                            .map(ExtendedLogger::resolve)
                                                            .toArray(Object[]::new));
            break;
         case TRACE:
            if (logger.isTraceEnabled()) logger.trace(resolve(message),
                                                      stream(args)
                                                            .map(ExtendedLogger::resolve)
                                                            .toArray(Object[]::new));
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported %s log level", level));
      }
   }

   public void error(Callable<String> message, Callable<?>... args) {
      log(ERROR, message, args);
   }

   public void warn(Callable<String> message, Callable<?>... args) {
      log(WARN, message, args);
   }

   public void info(Callable<String> message, Callable<?>... args) {
      log(INFO, message, args);
   }

   public void debug(Callable<String> message, Callable<?>... args) {
      log(DEBUG, message, args);
   }

   public void trace(Callable<String> message, Callable<?>... args) {
      log(TRACE, message, args);
   }

   public void log(Level level, String message, Callable<?>... args) {
      notNull(message, "message");

      log(level, () -> message, args);
   }

   public void error(String message, Callable<?>... args) {
      log(ERROR, message, args);
   }

   public void warn(String message, Callable<?>... args) {
      log(WARN, message, args);
   }

   public void info(String message, Callable<?>... args) {
      log(INFO, message, args);
   }

   public void debug(String message, Callable<?>... args) {
      log(DEBUG, message, args);
   }

   public void trace(String message, Callable<?>... args) {
      log(TRACE, message, args);
   }

   public void log(Level level, Callable<String> message, Throwable throwable) {
      notNull(level, "level");
      notNull(message, "message");
      notNull(throwable, "throwable");

      switch (level) {
         case OFF:
            break;
         case ERROR:
            if (logger.isErrorEnabled()) logger.error(resolve(message), throwable);
            break;
         case WARN:
            if (logger.isWarnEnabled()) logger.warn(resolve(message), throwable);
            break;
         case INFO:
            if (logger.isInfoEnabled()) logger.info(resolve(message), throwable);
            break;
         case DEBUG:
            if (logger.isDebugEnabled()) logger.debug(resolve(message), throwable);
            break;
         case TRACE:
            if (logger.isTraceEnabled()) logger.trace(resolve(message), throwable);
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported %s log level", level));
      }
   }

   public void error(Callable<String> message, Throwable throwable) {
      log(ERROR, message, throwable);
   }

   public void warn(Callable<String> message, Throwable throwable) {
      log(WARN, message, throwable);
   }

   public void info(Callable<String> message, Throwable throwable) {
      log(INFO, message, throwable);
   }

   public void debug(Callable<String> message, Throwable throwable) {
      log(DEBUG, message, throwable);
   }

   public void trace(Callable<String> message, Throwable throwable) {
      log(TRACE, message, throwable);
   }

   public void log(Level level, String message, Throwable throwable) {
      notNull(message, "message");

      log(level, () -> message, throwable);
   }

   public void error(String message, Throwable throwable) {
      log(ERROR, message, throwable);
   }

   public void warn(String message, Throwable throwable) {
      log(WARN, message, throwable);
   }

   public void info(String message, Throwable throwable) {
      log(INFO, message, throwable);
   }

   public void debug(String message, Throwable throwable) {
      log(DEBUG, message, throwable);
   }

   public void trace(String message, Throwable throwable) {
      log(TRACE, message, throwable);
   }

   /**
    * Returns current log level.
    *
    * @return current log level
    */
   public Level logLevel() {
      if (logger.isInfoEnabled()) return INFO;
      else if (logger.isDebugEnabled()) return DEBUG;
      else if (logger.isWarnEnabled()) return WARN;
      else if (logger.isErrorEnabled()) return ERROR;
      else if (logger.isTraceEnabled()) return TRACE;
      else throw new IllegalStateException("Unknown log level");
   }

   public boolean isErrorLogLevel() {
      return logger.isErrorEnabled();
   }

   public boolean isWarnLogLevel() {
      return logger.isWarnEnabled();
   }

   public boolean isInfoLogLevel() {
      return logger.isInfoEnabled();
   }

   public boolean isDebugLogLevel() {
      return logger.isDebugEnabled();
   }

   public boolean isTraceLogLevel() {
      return logger.isTraceEnabled();
   }

   /**
    * Lazy parameter evaluation for loggers that takes any {@link Object} in parameter and calls
    * {@link Object#toString()} on it if log level is sufficient. As {@link Callable} operation will be
    * evaluated only if current log level is sufficient, it's not required to wrap the log into a condition,
    * testing the current log level, when parameter evaluation is costly.
    * <p>
    * This operation is not required when {@link ExtendedLogger} API is used, only for external, unrelated,
    * usages.
    *
    * @param callable lazy parameter evaluation
    *
    * @return parameter value lazily evaluated when {@link Object#toString} will be called
    */
   public static Object lazy(Callable<?> callable) {
      notNull(callable, "callable");

      return new Object() {
         @Override
         public String toString() {
            return ExtendedLogger.resolve(callable);
         }
      };
   }

   /**
    * Sets underlying logger default level for specified name. Underlying logger must be supported.
    *
    * @param name log prefix name, or {@link Logger#ROOT_LOGGER_NAME}
    * @param level level value
    */
   public static void setLoggerLevel(String name, ExtendedLogger.Level level) {
      notBlank(name, "name");
      notNull(level, "level");

      Logger logger = LoggerFactory.getLogger(name);

      if (logger instanceof ch.qos.logback.classic.Logger) {
         ch.qos.logback.classic.Logger loggerImpl = (ch.qos.logback.classic.Logger) logger;
         loggerImpl.setLevel(logbackLevel(level));
      } else {
         throw new IllegalStateException(String.format("Unknown '%s' logger implementation",
                                                       logger.getClass().getName()));
      }

   }

   /**
    * Maps level to Logback level.
    *
    * @param level level to map
    *
    * @return logback level
    */
   public static ch.qos.logback.classic.Level logbackLevel(Level level) {
      ch.qos.logback.classic.Level logbackLevel;
      switch (level) {
         case OFF:
            logbackLevel = ch.qos.logback.classic.Level.OFF;
            break;
         case ERROR:
            logbackLevel = ch.qos.logback.classic.Level.ERROR;
            break;
         case WARN:
            logbackLevel = ch.qos.logback.classic.Level.WARN;
            break;
         case INFO:
            logbackLevel = ch.qos.logback.classic.Level.INFO;
            break;
         case DEBUG:
            logbackLevel = ch.qos.logback.classic.Level.DEBUG;
            break;
         case TRACE:
            logbackLevel = ch.qos.logback.classic.Level.TRACE;
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported '%s' log level", level));
      }
      return logbackLevel;
   }

   /**
    * Maps Logback level to level.
    *
    * @param level Logback level to map
    *
    * @return level
    */
   public static Level level(ch.qos.logback.classic.Level level) {
      Level logbackLevel;
      switch (level.toInt()) {
         case ch.qos.logback.classic.Level.OFF_INT:
            logbackLevel = Level.OFF;
            break;
         case ch.qos.logback.classic.Level.ERROR_INT:
            logbackLevel = Level.ERROR;
            break;
         case ch.qos.logback.classic.Level.WARN_INT:
            logbackLevel = Level.WARN;
            break;
         case ch.qos.logback.classic.Level.INFO_INT:
            logbackLevel = Level.INFO;
            break;
         case ch.qos.logback.classic.Level.DEBUG_INT:
            logbackLevel = Level.DEBUG;
            break;
         case ch.qos.logback.classic.Level.TRACE_INT:
            logbackLevel = Level.TRACE;
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported '%s' log level", level));
      }
      return logbackLevel;
   }

   /**
    * Resolve argument to string for delegated logger.
    *
    * @param argument logger argument to resolve, can be any object, or a {@link Callable}, or a
    *       {@link Supplier}, in such cases, argument is lazily, and recursively evaluated
    *
    * @return resolved argument as string
    *
    * @implSpec If an exception occurs while resolving argument, the exception message is returned as
    *       the resolved value for argument
    */
   protected static String resolve(Object argument) {
      if (argument instanceof Callable || argument instanceof Supplier) {
         Object lazyArgument;
         try {
            if (argument instanceof Callable) {
               lazyArgument = ((Callable<?>) argument).call();
            } else {
               lazyArgument = ((Supplier<?>) argument).get();
            }
         } catch (Exception e) {
            lazyArgument = "[" + e.getClass().getSimpleName() + ":" + e.getMessage() + "]";
         }
         return resolve(lazyArgument);
      } else {
         return nullable(argument).map(Object::toString).orElse(null);
      }
   }

}
