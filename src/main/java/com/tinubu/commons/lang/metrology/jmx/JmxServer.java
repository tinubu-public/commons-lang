/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.metrology.jmx;

import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.StringUtils.removeEndIgnoreCase;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.lang.management.ManagementFactory;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;

import com.tinubu.commons.lang.log.ExtendedLogger;

/**
 * Generic JMX server to register MBeans.
 */
public class JmxServer {

   private static final ExtendedLogger log = ExtendedLogger.of(JmxServer.class);

   private static final Map<String, JmxServer> instances = new ConcurrentHashMap<>();

   private final String domain;
   private final MBeanServer server;

   private JmxServer(String domain) {
      this.domain = notBlank(domain, "domain");
      this.server = ManagementFactory.getPlatformMBeanServer();
   }

   public static JmxServer instance(String domain) {
      notBlank(domain, "domain");

      return instances.computeIfAbsent(domain, JmxServer::new);
   }

   public ObjectInstance registerMBean(Object mBean)
         throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException {
      notNull(mBean, "mBean");

      try {
         return registerMBean(mBean, autoName(mBean), autoName(mBean));
      } catch (MalformedObjectNameException e) {
         throw sneakyThrow(e);
      }
   }

   public ObjectInstance registerMBean(Object mBean, String name) throws
                                                                  InstanceAlreadyExistsException,
                                                                  MBeanRegistrationException,
                                                                  NotCompliantMBeanException,
                                                                  MalformedObjectNameException {
      notNull(mBean, "mBean");
      notNull(name, "name");

      return registerMBean(mBean, autoName(mBean), name);
   }

   public ObjectInstance registerMBean(Object mBean, String type, String name) throws
                                                                               InstanceAlreadyExistsException,
                                                                               MBeanRegistrationException,
                                                                               NotCompliantMBeanException,
                                                                               MalformedObjectNameException {
      notNull(mBean, "mBean");
      notNull(type, "type");
      notNull(name, "name");

      ObjectName mBeanName = mBeanName(type, name);

      log.info("Register '{}' MBean to JMX", mBeanName::toString);
      return server.registerMBean(mBean, mBeanName);
   }

   public void unregisterMBean(ObjectName mBeanName)
         throws InstanceNotFoundException, MBeanRegistrationException {
      notNull(mBeanName, "mBeanName");

      log.info("Unregister '{}' MBean to JMX", mBeanName::toString);
      server.unregisterMBean(mBeanName);
   }

   public void unregisterMBean(Class<?> objectType)
         throws InstanceNotFoundException, MBeanRegistrationException {
      notNull(objectType, "type");

      try {
         unregisterMBean(autoName(objectType), autoName(objectType));
      } catch (MalformedObjectNameException e) {
         throw sneakyThrow(e);
      }
   }

   public void unregisterMBean(Class<?> objectType, String name)
         throws InstanceNotFoundException, MBeanRegistrationException, MalformedObjectNameException {
      notNull(objectType, "type");
      notBlank(name, "name");

      unregisterMBean(autoName(objectType), name);
   }

   public void unregisterMBean(String type, String name)
         throws InstanceNotFoundException, MBeanRegistrationException, MalformedObjectNameException {
      notBlank(type, "type");
      notBlank(name, "name");

      ObjectName mBeanName = mBeanName(type, name);

      unregisterMBean(mBeanName);
   }

   private static String autoName(Object mBean) {
      return autoName(mBean.getClass());
   }

   private static String autoName(Class<?> mBeanClass) {
      String type = mBeanClass.getSimpleName();

      type = removeEndIgnoreCase(type, "MBean");
      type = removeEndIgnoreCase(type, "MXBean");

      return type;
   }

   private ObjectName mBeanName(String type, String name) throws MalformedObjectNameException {
      notBlank(type);
      notBlank(name);

      return new ObjectName(String.format("%s:type=%s,name=%s", this.domain, type, name));
   }

}
