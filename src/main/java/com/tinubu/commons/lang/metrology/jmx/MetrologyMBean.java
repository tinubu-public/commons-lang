/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.metrology.jmx;

import static com.tinubu.commons.lang.util.CheckedFunction.uncheckedFunction;
import static com.tinubu.commons.lang.util.CollectionUtils.collection;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.util.Try.handleException;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.ReflectionException;

import com.tinubu.commons.lang.metrology.Counter;
import com.tinubu.commons.lang.metrology.Gauge;
import com.tinubu.commons.lang.util.Pair;

/**
 * {@link DynamicMBean} wrapper for delegated metrology implementation. On the contrary to regular MBeans,
 * this implementation supports inherited metrics from superclass/interfaces, and arbitrary metric method
 * naming.
 * <p>
 * Exposed attributes are all {@link Counter}/{@link Gauge} annotated methods from delegate class or any
 * superclass or interface.
 * Methods do not require to be Java Bean getters, you can use {@link Counter#value()}/{@link Gauge#value()}
 * to override method name if needed.
 */
public class MetrologyMBean<T> implements DynamicMBean {

   private final String name;
   private final T metrology;
   private final Map<String, AttributeInfo> attributes;

   @SuppressWarnings("unchecked")
   public MetrologyMBean(T metrology, String name) {
      this.metrology = notNull(metrology, "metrology");
      this.name = notBlank(name, "name");
      this.attributes = metricAttributes(metricMethods((Class<? extends T>) metrology.getClass()));
   }

   @Override
   public Object getAttribute(String attribute) throws AttributeNotFoundException {
      return nullable(attributes.get(attribute))
            .map(uncheckedFunction(infos -> infos.invoke(metrology)))
            .orElseThrow(() -> new AttributeNotFoundException(attribute));
   }

   @Override
   public void setAttribute(Attribute attribute) {
      throw new UnsupportedOperationException();
   }

   @Override
   public AttributeList getAttributes(String[] attributes) {
      return collection(AttributeList::new,
                        stream(attributes).flatMap(attribute -> handleException(() -> new Attribute(attribute,
                                                                                                    getAttribute(
                                                                                                          attribute))).stream()));
   }

   @Override
   public AttributeList setAttributes(AttributeList attributes) {
      throw new UnsupportedOperationException();
   }

   @Override
   public Object invoke(String actionName, Object[] params, String[] signature) {
      throw new UnsupportedOperationException();
   }

   @Override
   public MBeanInfo getMBeanInfo() {
      Stream<MBeanAttributeInfo> mBeanAttributeInfos = attributes
            .values()
            .stream()
            .map(ai -> new MBeanAttributeInfo(ai.name(),
                                              ai.type().getName(),
                                              ai.description(),
                                              true,
                                              false,
                                              false));

      return new MBeanInfo(metrology.getClass().getName(),
                           name,
                           mBeanAttributeInfos.toArray(MBeanAttributeInfo[]::new),
                           null,
                           null,
                           null

      );
   }

   private Map<String, AttributeInfo> metricAttributes(Stream<Method> metricMethods) {
      return map(LinkedHashMap::new, metricMethods.map(this::metricAttribute).map(a -> Pair.of(a.name(), a)));
   }

   private AttributeInfo metricAttribute(Method method) {
      return stream(method.getDeclaredAnnotations()).flatMap(a -> {
         if (a.annotationType().equals(Counter.class)) {
            return stream(counterAttribute((Counter) a, method));
         } else if (a.annotationType().equals(Gauge.class)) {
            return stream(gaugeAttribute((Gauge) a, method));
         } else {
            return stream();
         }
      }).findFirst().orElseThrow();
   }

   private static AttributeInfo counterAttribute(Counter counter, Method method) {
      return new AttributeInfo(metricAttributeName(counter.value(), method),
                               counter.description(),
                               method.getReturnType(),
                               method);
   }

   private static AttributeInfo gaugeAttribute(Gauge gauge, Method method) {
      return new AttributeInfo(metricAttributeName(gauge.value(), method),
                               gauge.description(),
                               method.getReturnType(),
                               method);
   }

   private static String metricAttributeName(String[] name, Method method) {
      return nullable(name)
            .filter(names -> names.length > 0)
            .map(names -> names[0])
            .orElseGet(method::getName);
   }

   @SuppressWarnings("unchecked")
   private Stream<Method> metricMethods(Class<? extends T> metrologyClass) {
      return streamConcat(streamConcat(stream(metrologyClass.getInterfaces()),
                                       stream(nullable(metrologyClass.getSuperclass()))).flatMap(top -> metricMethods(
            (Class<T>) top)), stream(metrologyClass.getDeclaredMethods()).filter(isMetric()));
   }

   private static Predicate<Method> isMetric() {
      return method -> stream(method.getDeclaredAnnotations()).anyMatch(annotation -> annotation
                                                                                            .annotationType()
                                                                                            .equals(Counter.class)
                                                                                      || annotation
                                                                                            .annotationType()
                                                                                            .equals(Gauge.class));
   }

   public static class AttributeInfo {
      private final String name;
      private final String description;
      private final Class<?> type;
      private final Method method;

      public AttributeInfo(String name, String description, Class<?> type, Method method) {
         this.name = notBlank(name, "name");
         this.description = notNull(description, "description");
         this.type = notNull(type, "type");
         this.method = notNull(method, "method");
      }

      public String name() {
         return name;
      }

      public String description() {
         return description;
      }

      public Class<?> type() {
         return type;
      }

      public Method method() {
         return method;
      }

      public Object invoke(Object instance) throws ReflectionException {
         try {
            return method.invoke(instance);
         } catch (IllegalAccessException | InvocationTargetException e) {
            throw new ReflectionException(e);
         }
      }
   }
}
