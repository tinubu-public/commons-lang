package com.tinubu.commons.lang.beans;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Identifies an object to ignore (builder, accessor, ...).
 * <p>
 * See com.tsquare.mapstruct.accessornaming.AnnotatedAccessorNamingStrategy
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.METHOD })
public @interface Transient {}
