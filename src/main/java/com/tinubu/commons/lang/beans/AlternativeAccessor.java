/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.beans;

import static com.tinubu.commons.lang.annotation.AnnotationDefaults.isDefaultValue;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.SimpleElementVisitor8;
import javax.lang.model.util.SimpleTypeVisitor8;

import com.tinubu.commons.lang.validation.Validate;

/**
 * Support for non-standard accessors in Java classes.
 * <p>
 * Fields can be annotated with {@link Getter} or {@link Setter}.
 * <p>
 * Field name strategy can be customized in annotations or, by default, using class-level or package-level
 * {@link AccessorConfig}. If no accessor configuration is provided, the JavaBean standard applies.
 * <p>
 * Fields can be ignored using {@link Transient} annotation.
 */
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public final class AlternativeAccessor {
   private static final AccessorConfig DEFAULT_CONFIG = DefaultAccessorConfigHolder.defaultAccessorConfig();

   /**
    * {@link AccessorConfig} Cache resulting from analysis of a given package. If no annotation is found in
    * subpackages, the {@link #DEFAULT_CONFIG} is put in cache.
    */
   private final static Map<PackageElement, AccessorConfig> packageAccessorConfigCache =
         new ConcurrentHashMap<>();

   public static boolean isGetterMethod(ExecutableElement method, Elements elements) {
      if (transientAccessor(method)) return false;
      Getter getterAnnotation = getterAnnotation(method);
      if (getterAnnotation != null) return true;
      Setter setterAnnotation = setterAnnotation(method);
      if (setterAnnotation != null) return false;

      String methodName = method.getSimpleName().toString();

      String getterPrefix = AlternativeAccessor.getterPrefix(method, getterAnnotation, elements);
      if (getterPrefix.trim().isEmpty()) {
         return false;
      }
      boolean isGetter = methodName.startsWith(getterPrefix)
                         && methodName.length() > getterPrefix.length()
                         && method.getReturnType().getKind() != TypeKind.VOID;

      return isGetter || isBooleanGetterMethod(method, elements);
   }

   public static boolean isSetterMethod(ExecutableElement method, Elements elements) {
      if (transientAccessor(method)) return false;
      Setter setterAnnotation = setterAnnotation(method);
      if (setterAnnotation != null) return true;
      Getter getterAnnotation = getterAnnotation(method);
      if (getterAnnotation != null) return false;

      String methodName = method.getSimpleName().toString();

      String setterPrefix = AlternativeAccessor.setterPrefix(method, setterAnnotation, elements);
      if (setterPrefix.trim().isEmpty()) {
         return false;
      }
      return methodName.startsWith(setterPrefix) && methodName.length() > setterPrefix.length();
   }

   public static String propertyName(ExecutableElement getterOrSetterMethod, Elements elements) {
      String methodName = getterOrSetterMethod.getSimpleName().toString();

      if (isBooleanGetterMethod(getterOrSetterMethod, elements)) {
         String booleanGetterPrefix =
               AlternativeAccessor.booleanGetterPrefix(getterOrSetterMethod, null, elements);
         return uncapitalize(methodName.substring(booleanGetterPrefix.length()));
      } else {
         if (isSetterMethod(getterOrSetterMethod, elements)) {
            String setterPrefix = AlternativeAccessor.setterPrefix(getterOrSetterMethod, null, elements);
            return uncapitalize(methodName.substring(setterPrefix.length()));
         } else {
            if (isGetterMethod(getterOrSetterMethod, elements)) {
               String getterPrefix = AlternativeAccessor.getterPrefix(getterOrSetterMethod, null, elements);
               return uncapitalize(methodName.substring(getterPrefix.length()));
            }
         }
      }

      throw new IllegalStateException(String.format(
            "Internal error : Can't get property name for method '%s::%s'",
            getterOrSetterMethod.getEnclosingElement(),
            getterOrSetterMethod));
   }

   private static boolean isBooleanGetterMethod(ExecutableElement method, Elements elements) {
      if (transientAccessor(method)) return false;
      Setter setterAnnotation = setterAnnotation(method);
      if (setterAnnotation != null) return false;

      String methodName = method.getSimpleName().toString();

      String booleanGetterPrefix = AlternativeAccessor.booleanGetterPrefix(method, null, elements);
      if (booleanGetterPrefix.trim().isEmpty()) {
         return false;
      }

      boolean returnTypeIsBoolean =
            method.getReturnType().getKind() == TypeKind.BOOLEAN || "java.lang.Boolean".equals(
                  fullyQualifiedName(method.getReturnType()));

      return methodName.startsWith(booleanGetterPrefix)
             && methodName.length() > booleanGetterPrefix.length()
             && returnTypeIsBoolean;
   }

   /**
    * Returns getter prefix for element. Can't be {@code null}.
    *
    * @param method method to retrieve prefix for
    * @param getterAnnotation optional getter annotation if already retrieved (optimization)
    * @param elements elements
    *
    * @return getter prefix, never {@code null}
    */
   public static String getterPrefix(ExecutableElement method, Getter getterAnnotation, Elements elements) {
      Validate.notNull(method, "method");
      Validate.notNull(elements, "elements");

      String getterPrefix = getterAnnotation != null
                            ? getterAnnotationPrefix(getterAnnotation)
                            : getterAnnotationPrefix(method);

      if (getterPrefix == null) {
         getterPrefix = accessorConfigAnnotation(method, elements).getterPrefix();
      }

      return getterPrefix;
   }

   /**
    * Returns setter prefix for element. Can't be {@code null}.
    *
    * @param method method to retrieve prefix for
    * @param setterAnnotation optional setter annotation if already retrieved (optimization)
    * @param elements elements
    *
    * @return setter prefix, never {@code null}
    */
   public static String setterPrefix(ExecutableElement method, Setter setterAnnotation, Elements elements) {
      Validate.notNull(method, "method");
      Validate.notNull(elements, "elements");

      String setterPrefix = setterAnnotation != null
                            ? setterAnnotationPrefix(setterAnnotation)
                            : setterAnnotationPrefix(method);

      if (setterPrefix == null) {
         setterPrefix = accessorConfigAnnotation(method, elements).setterPrefix();
      }

      return setterPrefix;
   }

   /**
    * Returns boolean getter prefix for element. Can't be {@code null}.
    *
    * @param method method to retrieve prefix for
    * @param getterAnnotation optional getter annotation if already retrieved (optimization)
    * @param elements elements
    *
    * @return setter prefix, never {@code null}
    */
   public static String booleanGetterPrefix(ExecutableElement method,
                                            Getter getterAnnotation,
                                            Elements elements) {
      Validate.notNull(method, "method");
      Validate.notNull(elements, "elements");

      String getterPrefix = getterAnnotation != null
                            ? getterAnnotationPrefix(getterAnnotation)
                            : getterAnnotationPrefix(method);

      if (getterPrefix == null) {
         getterPrefix = accessorConfigAnnotation(method, elements).booleanGetterPrefix();
      }

      return getterPrefix;
   }

   /**
    * Returns {@code true} if method is annotated with {@link Transient}, and should not be considered an
    * accessor.
    */
   public static boolean transientAccessor(ExecutableElement method) {
      return method.getAnnotation(Transient.class) != null;
   }

   private static String getterAnnotationPrefix(Getter getterAnnotation) {
      return ofNullable(getterAnnotation)
            .map(Getter::value)
            .filter(prefix -> !isDefaultValue(prefix))
            .orElse(null);
   }

   private static String getterAnnotationPrefix(ExecutableElement executableElement) {
      return getterAnnotationPrefix(getterAnnotation(executableElement));
   }

   private static String setterAnnotationPrefix(Setter setterAnnotation) {
      return ofNullable(setterAnnotation)
            .map(Setter::value)
            .filter(prefix -> !isDefaultValue(prefix))
            .orElse(null);
   }

   private static String setterAnnotationPrefix(ExecutableElement executableElement) {
      return setterAnnotationPrefix(setterAnnotation(executableElement));
   }

   /**
    * Searches for specified annotation on specified element or enclosing elements (maximum 3 enclosing
    * elements).
    *
    * @param element element to search annotation on
    * @param annotationClass annotation to find
    * @param <T> annotation type
    *
    * @return annotation or {@code null}
    */
   // TODO Support meta-annotations.
   private static <T extends Annotation> T findAnnotation(Element element, Class<T> annotationClass) {
      T annotation = element.getAnnotation(annotationClass);

      Element enclosingElement = element;
      while (annotation == null && (enclosingElement instanceof TypeElement
                                    || enclosingElement instanceof PackageElement)) {
         enclosingElement = enclosingElement.getEnclosingElement();
         annotation = enclosingElement.getAnnotation(annotationClass);
      }

      return annotation;
   }

   /**
    * Returns {@link AccessorConfig} for the specified accessor method. Searches enclosing elements (maximum 3
    * enclosing elements), or method package and subpackages.
    *
    * @param element accessor method
    * @param elements Elements context
    *
    * @return {@link AccessorConfig} for the specified accessor method
    */
   private static AccessorConfig accessorConfigAnnotation(Element element, Elements elements) {
      AccessorConfig config = null;

      Element enclosingElement = element;
      while (config == null && enclosingElement.getEnclosingElement() instanceof TypeElement) {
         enclosingElement = enclosingElement.getEnclosingElement();
         config = enclosingElement.getAnnotation(AccessorConfig.class);
      }

      if (config == null) {
         config =
               packageAccessorConfigCache.computeIfAbsent(elements.getPackageOf(element), elementPackage -> {
                  AccessorConfig accessorConfig = null;
                  for (PackageElement subPackage : subPackages(elementPackage, elements)) {
                     accessorConfig = subPackage.getAnnotation(AccessorConfig.class);
                     if (accessorConfig != null) {
                        break;
                     }
                  }
                  if (accessorConfig == null) {
                     accessorConfig = DEFAULT_CONFIG;
                  }

                  return accessorConfig;
               });
      }

      return config;
   }

   /**
    * Returns an ordered list of {@link PackageElement} representing the specified element package and its
    * subpackages. The list is ordered from the most specific package to the root package.
    *
    * @param element element to retrieves subpackages from
    *
    * @return an ordered list of package elements representing the specified element package and its subpackages
    *
    * @implSpec {@link ModuleElement#getEnclosedElements()} does not return {@link PackageElement} if it
    *       only contains a {@code package-info.java}. We have to explicitly retrieves all packages by name.
    */
   private static List<PackageElement> subPackages(Element element, Elements elements) {
      //ModuleElement elementModule = elements.getModuleOf(element); // TODO optimization Java 11
      List<String> subPackages = subPackages(elements.getPackageOf(element));

      return subPackages
            .stream()
            .map(p -> elements.getPackageElement(/* elementModule, TODO optimization Java 11 */ p))
            .filter(Objects::nonNull)
            .sorted(packageComparator().reversed())
            .collect(toList());
   }

   private static List<String> subPackages(PackageElement elementPackage) {
      String[] packagesParts = elementPackage.getQualifiedName().toString().split("\\.");
      List<String> subPackages = new ArrayList<>();
      for (int i = 0; i < packagesParts.length; i++) {
         String subPackage = null;
         for (int j = 0; j < i + 1; j++) {
            subPackage = subPackage == null ? packagesParts[j] : subPackage + "." + packagesParts[j];
         }
         subPackages.add(subPackage);
      }
      return subPackages;
   }

   /**
    * Returns {@code true} if predicate input package element is a subpackage of specified package element.
    *
    * @param element leaf package element
    *
    * @return predicate matching subpackages of specified package element
    *
    * @implNote A dot "." is added to the comparison to prevent matching of partially matching package
    *       simple names, e.g. {@code com.subpackage.package} contains {@code com.sub}, but {@code
    *       com.subpackage.package.} does not contain {@code com.sub.}.
    */
   private static Predicate<PackageElement> isSubPackageOf(PackageElement element) {
      return p -> (element.getQualifiedName() + ".").contains(p.getQualifiedName() + ".");
   }

   /**
    * Returns {@link PackageElement} comparator. The comparison is based on the natural order of package fully
    * qualified names.
    *
    * @return package element comparator
    */
   private static Comparator<PackageElement> packageComparator() {
      return Comparator.comparing(p -> p.getQualifiedName().toString());
   }

   private static Getter getterAnnotation(ExecutableElement method) {
      return AlternativeAccessor.findAnnotation(method, Getter.class);
   }

   private static Setter setterAnnotation(ExecutableElement method) {
      return AlternativeAccessor.findAnnotation(method, Setter.class);
   }

   /**
    * Un-capitalizes specified {@code name} specialized for getters/setters :
    * <ul>
    * <li>All (at least 2 characters) capital names are kept as-is</li>
    * </ul>
    *
    * @param name property name to un-capitalize
    *
    * @return un-capitalized property name
    */
   private static String uncapitalize(String name) {
      if (name == null || name.length() == 0) {
         return name;
      } else if (name.length() > 1
                 && Character.isUpperCase(name.charAt(1))
                 && Character.isUpperCase(name.charAt(0))) {
         return name;
      } else {
         char[] chars = name.toCharArray();
         chars[0] = Character.toLowerCase(chars[0]);
         return new String(chars);
      }
   }

   /**
    * Returns fully qualified name of a {@link TypeMirror}.
    *
    * @param type type
    *
    * @return fully qualified name of {@link DeclaredType}, or {@code null}.
    */
   protected static String fullyQualifiedName(TypeMirror type) {
      DeclaredType declaredType = type.accept(new SimpleTypeVisitor8<DeclaredType, Void>() {
         @Override
         public DeclaredType visitDeclared(DeclaredType t, Void p) {
            return t;
         }
      }, null);

      if (declaredType == null) {
         return null;
      }

      TypeElement typeElement =
            declaredType.asElement().accept(new SimpleElementVisitor8<TypeElement, Void>() {
               @Override
               public TypeElement visitType(TypeElement e, Void p) {
                  return e;
               }
            }, null);

      return typeElement != null ? typeElement.getQualifiedName().toString() : null;
   }

   /** Holds the default {@link AccessorConfig} instance. */
   @AccessorConfig
   private static class DefaultAccessorConfigHolder {
      private static AccessorConfig defaultAccessorConfig() {
         return DefaultAccessorConfigHolder.class.getAnnotation(AccessorConfig.class);
      }
   }

}
