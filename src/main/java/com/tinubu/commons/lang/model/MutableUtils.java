/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.model;

import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toCollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.tinubu.commons.lang.util.CollectionUtils;
import com.tinubu.commons.lang.util.StreamUtils;

/**
 * Null-safe mutable collection utils.
 *
 * @deprecated Use {@link CollectionUtils} and {@link StreamUtils} instead
 */
@Deprecated
public final class MutableUtils {

   private static final Predicate<Object> ELEMENTS_FILTER = __ -> true; // Objects::nonNull;

   private MutableUtils() {
   }

   /**
    * Returns a mutable, always initialized collection, of specified streams.
    * If multiple streams are specified, they are concatenated in a single collection.
    * {@code null} streams are ignored.
    *
    * @param collectionFactory collection factory to use to generate returned collection
    * @param streams original stream or {@code null}
    * @param <T> collection items type
    * @param <C> collection factory type
    *
    * @return new mutable collection of specified streams, or new mutable empty collection
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C mutableCollection(Supplier<C> collectionFactory,
                                                                  final Stream<? extends T>... streams) {
      notNull(collectionFactory, "collectionFactory");

      return StreamUtils
            .streamConcat(streams)
            .filter(ELEMENTS_FILTER)
            .collect(toCollection(collectionFactory));
   }

   /**
    * Returns a mutable, always initialized collection, of specified collections.
    * If multiple collections are specified, they are concatenated in a single collection.
    * {@code null} collections are ignored.
    *
    * @param collectionFactory collection factory to use to generate returned collection
    * @param collections original collection or {@code null}
    * @param <T> collection items type
    * @param <C> collection factory type
    *
    * @return new mutable collection of specified collections, or new mutable empty collection
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C mutableCollection(Supplier<C> collectionFactory,
                                                                  final Collection<? extends T>... collections) {
      notNull(collectionFactory, "collectionFactory");

      return StreamUtils
            .streamConcat(collections)
            .filter(ELEMENTS_FILTER)
            .collect(toCollection(collectionFactory));
   }

   /**
    * Returns a mutable, always initialized collection, of specified elements.
    *
    * @param collectionFactory collection factory to use to generate returned collection
    * @param elements original collection of elements, {@code null} elements will be included in
    *       resulting collection
    * @param <T> collection items type
    * @param <C> collection factory type
    *
    * @return new mutable collection of specified elements, or new mutable empty collection
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C mutableCollection(Supplier<C> collectionFactory,
                                                                  final T... elements) {
      notNull(collectionFactory, "collectionFactory");

      return stream(elements).filter(ELEMENTS_FILTER).collect(toCollection(collectionFactory));
   }

   /**
    * Returns a mutable, always initialized list, of specified lists.
    * If multiple lists are specified, they are concatenated in a single list.
    * {@code null} lists are ignored.
    *
    * @param lists original list or {@code null}
    * @param <T> list items type
    *
    * @return new mutable list of specified lists, or new mutable empty list
    */
   @SafeVarargs
   public static <T> List<T> mutableList(final List<? extends T>... lists) {
      return mutableCollection(() -> new ArrayList<T>(), lists);
   }

   /**
    * Returns a mutable, always initialized list, of specified streams.
    * If multiple streams are specified, they are concatenated in a single list.
    * {@code null} streams are ignored.
    *
    * @param streams original stream or {@code null}
    * @param <T> list items type
    *
    * @return new mutable list of specified streams, or new mutable empty list
    */
   @SafeVarargs
   public static <T> List<T> mutableList(final Stream<? extends T>... streams) {
      return mutableCollection(() -> new ArrayList<T>(), streams);
   }

   /**
    * Returns a mutable, empty list.
    *
    * @param <T> list items type
    *
    * @return new mutable empty list
    */
   public static <T> List<T> mutableList() {
      return new ArrayList<>();
   }

   /**
    * Returns a mutable, always initialized list, of specified elements.
    *
    * @param elements original list of elements, {@code null} elements will be included in
    *       resulting list
    * @param <T> list items type
    *
    * @return new mutable list of specified elements, or new mutable empty list
    */
   @SafeVarargs
   public static <T> List<T> mutableList(final T... elements) {
      return mutableCollection(ArrayList::new, elements);
   }

   /**
    * Returns a mutable, always initialized map, of specified maps.
    * If multiple maps are specified, they are concatenated in a single map.
    * {@code null} maps are ignored.
    * If the same key is present in multiple maps, the specified merge function is applied.
    *
    * @param mergeFunction map merge function to apply when key duplicates found
    * @param mapFactory map factory to use to generate returned map
    * @param maps original map, or null
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new mutable map of specified maps, or new mutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> M mutableMap(BinaryOperator<V> mergeFunction,
                                                          Supplier<M> mapFactory,
                                                          final Map<? extends K, ? extends V>... maps) {
      notNull(mergeFunction, "mergeFunction");
      notNull(mapFactory, "mapFactory");

      return StreamUtils
            .streamConcat(maps)
            .collect(toMapNullSafe(Entry::getKey, Entry::getValue, mergeFunction, mapFactory));
   }

   /**
    * Returns a mutable, always initialized map, of specified maps.
    * If multiple maps are specified, they are concatenated in a single map.
    * {@code null} maps are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #mutableMap(BinaryOperator, Supplier, Map[])} to use a custom map builder.
    *
    * @param mapFactory map factory to use to generate returned map
    * @param maps original map, or null
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new mutable map of specified maps, or new mutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> M mutableMap(Supplier<M> mapFactory,
                                                          final Map<? extends K, ? extends V>... maps) {
      return mutableMap((v1, v2) -> v2, mapFactory, maps);
   }

   /**
    * Returns a mutable, always initialized map, of specified maps.
    * If multiple maps are specified, they are concatenated in a single map.
    * {@code null} maps are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #mutableMap(BinaryOperator, Supplier, Map[])} to use a custom map builder.
    *
    * @param maps original map, or null
    * @param <K> map items key type
    * @param <V> map items value type
    *
    * @return new mutable map of specified maps, or new mutable empty map
    */
   @SafeVarargs
   public static <K, V> Map<K, V> mutableMap(final Map<? extends K, ? extends V>... maps) {
      return mutableMap((v1, v2) -> v2, HashMap::new, maps);
   }

   /**
    * Returns a mutable, empty map.
    *
    * @param <K> map items key type
    * @param <V> map items value type
    *
    * @return new mutable empty map
    */
   public static <K, V> Map<K, V> mutableMap() {
      return new HashMap<>();
   }

   /**
    * Returns a mutable, always initialized map, of specified entries.
    * {@code null} entries are ignored.
    * If the same key is present in multiple maps, the specified merge function is applied.
    *
    * @param mergeFunction map merge function to apply when key duplicates found
    * @param mapFactory map factory to use to generate returned map
    * @param entries entries
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new mutable map of specified entries, or new mutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> M mutableMap(BinaryOperator<V> mergeFunction,
                                                          Supplier<M> mapFactory,
                                                          Entry<? extends K, ? extends V>... entries) {
      notNull(mergeFunction, "mergeFunction");
      notNull(mapFactory, "mapFactory");

      return stream(entries)
            .filter(Objects::nonNull)
            .collect(toMapNullSafe(Entry::getKey, Entry::getValue, mergeFunction, mapFactory));
   }

   /**
    * Returns a mutable, always initialized map, of specified entries.
    * {@code null} entries are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #mutableMap(BinaryOperator, Supplier, Entry[])} to use a custom map builder.
    *
    * @param mapFactory map factory to use to generate returned map
    * @param entries entries
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new mutable map of specified entries, or new mutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> M mutableMap(Supplier<M> mapFactory,
                                                          Entry<? extends K, ? extends V>... entries) {
      return mutableMap((v1, v2) -> v2, mapFactory, entries);
   }

   /**
    * Returns a mutable, always initialized map, of specified entries.
    * {@code null} entries are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #mutableMap(BinaryOperator, Supplier, Entry[])} to use a custom map builder.
    *
    * @param entries entries
    * @param <K> map items key type
    * @param <V> map items value type
    *
    * @return new mutable map of specified entries, or new mutable empty map
    */
   @SafeVarargs
   public static <K, V> Map<K, V> mutableMap(Entry<? extends K, ? extends V>... entries) {
      return mutableMap((v1, v2) -> v2, HashMap::new, entries);
   }

   /**
    * Workaround from https://stackoverflow.com/a/62692728 for JDK bug
    * https://bugs.openjdk.org/browse/JDK-8148463
    */
   private static <T, K, V, M extends Map<K, V>> Collector<T, ?, M> toMapNullSafe(Function<? super T, ? extends K> keyMapper,
                                                                                  Function<? super T, ? extends V> valueMapper,
                                                                                  BinaryOperator<V> mergeFunction,
                                                                                  Supplier<M> mapFactory) {
      return Collectors.collectingAndThen(Collectors.toList(), list -> {
         M result = mapFactory.get();
         for (T item : list) {
            K key = keyMapper.apply(item);
            V newValue = valueMapper.apply(item);
            V value = result.containsKey(key) ? mergeFunction.apply(result.get(key), newValue) : newValue;
            result.put(key, value);
         }
         return result;
      });
   }

}
