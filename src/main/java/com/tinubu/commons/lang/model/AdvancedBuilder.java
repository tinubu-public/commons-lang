/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.model;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import com.tinubu.commons.lang.beans.Transient;

/**
 * Represents an instance builder pattern with advanced features.
 * <p>
 * <ul>
 *    <li>Supports advanced builder operations (chain operations, ...)</li>
 *    <li>Easier support for builder inheritance with separately overridable build operations : builders
 *        implementations should only override {@link #buildObject()} and let {@link #build()} and {@link #finalizeBuild()}
 *        logic to the parent builder implementations</li>
 * </ul>
 *
 * @param <T> built instance type
 */
@FunctionalInterface
public interface AdvancedBuilder<T> extends Builder<T> {

   /**
    * Overrides this method to create an instance of the build object.
    *
    * @return build object instance
    */
   T buildObject();

   /**
    * Builds a new instance.
    * Final builder implementations should not override it, but should override {@link #buildObject()}
    * instead.
    * Abstract builders can override this method and {@link #finalizeBuild()}.
    *
    * @return new instance
    */
   @Override
   default T build() {
      return finalizeBuild().buildObject();
   }

   /**
    * Chains defined operation to current builder.
    *
    * @param builderOperation operation to apply to current builder
    *
    * @return current builder with operation applied
    *
    * @implNote Method is marked transient to prevent any confusion with a "setter" method.
    */
   @Transient
   default AdvancedBuilder<T> chain(Function<? super AdvancedBuilder<T>, ? extends AdvancedBuilder<T>> builderOperation) {
      notNull(builderOperation, "builderOperation");

      return builderOperation.apply(this);
   }

   /**
    * Chains defined operation to current builder only if {@code predicate} is true.
    *
    * @param predicate chain condition
    * @param builderOperation operation to apply to current builder.
    *
    * @return current builder with operation applied
    */
   default AdvancedBuilder<T> conditionalChain(Predicate<? super AdvancedBuilder<T>> predicate,
                                               Function<? super AdvancedBuilder<T>, ? extends AdvancedBuilder<T>> builderOperation) {
      notNull(predicate, "predicate");
      notNull(builderOperation, "builderOperation");

      if (predicate.test(this)) {
         return builderOperation.apply(this);
      } else {
         return this;
      }
   }

   /**
    * Chains defined operation to current builder only if {@code optional} value is present.
    *
    * @param <U> optional type
    * @param optional chain condition
    * @param builderOperation operation to apply to current builder. encapsulated value parameter is
    *       passed to function.
    *
    * @return current builder with operation applied
    */
   @SuppressWarnings({ "OptionalUsedAsFieldOrParameterType", "unchecked" })
   default <U> AdvancedBuilder<T> optionalChain(Optional<U> optional,
                                                BiFunction<AdvancedBuilder<? super T>, U, AdvancedBuilder<? extends T>> builderOperation) {
      notNull(optional, "optional");
      notNull(builderOperation, "builderOperation");

      return optional.map(value -> (AdvancedBuilder<T>) builderOperation.apply(this, value)).orElse(this);
   }

   /**
    * Overrides this method to finalize the builder before building it.
    * Build finalization is automatically applied in {@link #build()}, but you can have to manually apply it
    * in other cases.
    */
   default AdvancedBuilder<T> finalizeBuild() {
      return this;
   }

}
