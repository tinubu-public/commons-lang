/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.model;

import static com.tinubu.commons.lang.model.MutableUtils.mutableCollection;
import static com.tinubu.commons.lang.model.MutableUtils.mutableList;
import static com.tinubu.commons.lang.model.MutableUtils.mutableMap;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import com.tinubu.commons.lang.util.CollectionUtils;
import com.tinubu.commons.lang.util.StreamUtils;

/**
 * Null-safe immutable collection utils.
 *
 * @deprecated Use {@link CollectionUtils} and {@link StreamUtils} instead
 */
@Deprecated
public final class ImmutableUtils {

   private ImmutableUtils() {
   }

   /**
    * Returns an immutable, always initialized collection, of specified streams.
    * If multiple streams are specified, they are concatenated in a single collection.
    * {@code null} streams are ignored.
    *
    * @param collectionFactory collection factory to use to generate returned collection, before
    *       unmodifiable view wrapping
    * @param streams original stream or {@code null}
    * @param <T> collection items type
    * @param <C> collection factory type
    *
    * @return new immutable collection of specified streams, or new immutable empty collection
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> Collection<T> immutableCollection(Supplier<C> collectionFactory,
                                                                                final Stream<? extends T>... streams) {
      return unmodifiableCollection(mutableCollection(collectionFactory, streams));
   }

   /**
    * Returns an immutable, always initialized collection, of specified collections.
    * If multiple collections are specified, they are concatenated in a single collection.
    * {@code null} collections are ignored.
    *
    * @param collectionFactory collection factory to use to generate returned collection, before
    *       unmodifiable wrapping
    * @param collections original collection or {@code null}
    * @param <T> collection items type
    * @param <C> collection factory type
    *
    * @return new immutable collection of specified collections, or new immutable empty collection
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> Collection<T> immutableCollection(Supplier<C> collectionFactory,
                                                                                final Collection<? extends T>... collections) {
      return unmodifiableCollection(mutableCollection(collectionFactory, collections));
   }

   /**
    * Returns an immutable, always initialized collection, of specified elements.
    *
    * @param collectionFactory collection factory to use to generate returned collection, before
    *       unmodifiable view wrapping
    * @param elements original collection of elements, {@code null} elements will be included in
    *       resulting collection
    * @param <T> collection items type
    * @param <C> collection factory type
    *
    * @return new immutable collection of specified elements, or new immutable empty collection
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> Collection<T> immutableCollection(Supplier<C> collectionFactory,
                                                                                final T... elements) {
      notNull(collectionFactory, "collectionFactory");

      return unmodifiableCollection(mutableCollection(collectionFactory, elements));
   }

   /**
    * Returns an immutable, always initialized list, of specified streams.
    * If multiple streams are specified, they are concatenated in a single list.
    * {@code null} streams are ignored.
    *
    * @param streams original stream or {@code null}
    * @param <T> list items type
    *
    * @return new immutable list of specified streams, or new immutable empty list
    */
   @SafeVarargs
   public static <T> List<T> immutableList(final Stream<? extends T>... streams) {
      return unmodifiableList(mutableList(streams));
   }

   /**
    * Returns an immutable, always initialized list, of specified lists.
    * If multiple lists are specified, they are concatenated in a single list.
    * {@code null} lists are ignored.
    *
    * @param lists original list or {@code null}
    * @param <T> list items type
    *
    * @return new immutable list of specified lists, or new immutable empty list
    */
   @SafeVarargs
   public static <T> List<T> immutableList(final List<? extends T>... lists) {
      return unmodifiableList(mutableList(lists));
   }

   /**
    * Returns an immutable, empty list.
    *
    * @param <T> list items type
    *
    * @return new immutable empty list
    */
   public static <T> List<T> immutableList() {
      return emptyList();
   }

   /**
    * Returns an immutable, always initialized list, of specified elements.
    *
    * @param elements original list of elements, {@code null} elements will be included in
    *       resulting list
    * @param <T> list items type
    *
    * @return new immutable list of specified elements, or new immutable empty list
    */
   @SafeVarargs
   public static <T> List<T> immutableList(final T... elements) {
      return unmodifiableList(mutableList(elements));
   }

   /**
    * Returns an immutable, always initialized map, of specified maps.
    * If multiple maps are specified, they are concatenated in a single map.
    * {@code null} maps are ignored.
    * If the same key is present in multiple maps, the specified merge function is applied.
    *
    * @param mergeFunction map merge function to apply when key duplicates found
    * @param mapFactory map factory to use to generate returned map, before unmodifiable view wrapping
    * @param maps original map, or null
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new immutable map of specified maps, or new immutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> Map<K, V> immutableMap(BinaryOperator<V> mergeFunction,
                                                                    Supplier<M> mapFactory,
                                                                    final Map<? extends K, ? extends V>... maps) {
      notNull(mergeFunction, "mergeFunction");
      notNull(mapFactory, "mapFactory");

      return unmodifiableMap(mutableMap(mergeFunction, mapFactory, maps));
   }

   /**
    * Returns an immutable, always initialized map, of specified maps.
    * If multiple maps are specified, they are concatenated in a single map.
    * {@code null} maps are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #immutableMap(BinaryOperator, Supplier, Map[])} to use a custom merge function.
    *
    * @param mapFactory map factory to use to generate returned map, before unmodifiable view wrapping
    * @param maps original map, or null
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new immutable map of specified maps, or new immutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> Map<K, V> immutableMap(Supplier<M> mapFactory,
                                                                    final Map<? extends K, ? extends V>... maps) {
      return immutableMap((v1, v2) -> v2, mapFactory, maps);
   }

   /**
    * Returns an immutable, always initialized map, of specified maps.
    * If multiple maps are specified, they are concatenated in a single map.
    * {@code null} maps are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #immutableMap(BinaryOperator, Supplier, Map[])} to use a custom merge function.
    *
    * @param maps original map, or null
    * @param <K> map items key type
    * @param <V> map items value type
    *
    * @return new immutable map of specified maps, or new immutable empty map
    */
   @SafeVarargs
   public static <K, V> Map<K, V> immutableMap(final Map<? extends K, ? extends V>... maps) {
      return immutableMap((v1, v2) -> v2, HashMap::new, maps);
   }

   /**
    * Returns an immutable, empty map.
    *
    * @param <K> map items key type
    * @param <V> map items value type
    *
    * @return new immutable empty map
    */
   public static <K, V> Map<K, V> immutableMap() {
      return emptyMap();
   }

   /**
    * Returns an immutable, always initialized map, of specified entries.
    * {@code null} entries are ignored.
    * If the same key is present in multiple maps, the specified merge function is applied.
    *
    * @param mergeFunction map merge function to apply when key duplicates found
    * @param mapFactory map factory to use to generate returned map, before unmodifiable view wrapping
    * @param entries entries
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new immutable map of specified entries, or new immutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> Map<K, V> immutableMap(BinaryOperator<V> mergeFunction,
                                                                    Supplier<M> mapFactory,
                                                                    Entry<? extends K, ? extends V>... entries) {
      notNull(mergeFunction, "mergeFunction");
      notNull(mapFactory, "mapFactory");

      return unmodifiableMap(mutableMap(mergeFunction, mapFactory, entries));
   }

   /**
    * Returns an immutable, always initialized map, of specified entries.
    * {@code null} entries are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #immutableMap(BinaryOperator, Supplier, Entry[])} to use a custom map builder.
    *
    * @param mapFactory map factory to use to generate returned map, before unmodifiable view wrapping
    * @param entries entries
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new immutable map of specified entries, or new immutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> Map<K, V> immutableMap(Supplier<M> mapFactory,
                                                                    Entry<? extends K, ? extends V>... entries) {
      return immutableMap((v1, v2) -> v2, mapFactory, entries);
   }

   /**
    * Returns an immutable, always initialized map, of specified entries.
    * {@code null} entries are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #immutableMap(BinaryOperator, Supplier, Entry[])} to use a custom map builder.
    *
    * @param entries entries
    * @param <K> map items key type
    * @param <V> map items value type
    *
    * @return new immutable map of specified entries, or new immutable empty map
    */
   @SafeVarargs
   public static <K, V> Map<K, V> immutableMap(Entry<? extends K, ? extends V>... entries) {
      return immutableMap((v1, v2) -> v2, HashMap::new, entries);
   }

   /**
    * Creates a new immutable list with element matched with {@code matcher} updated by {@code updated}, or
    * with {@code element} added in last position.
    *
    * @param list list to operate on
    * @param element element to add
    * @param matcher element matching function
    * @param updater element updating function
    * @param <T> element type
    *
    * @return new immutable list with updated or added element
    */
   public static <T> List<T> listUpdateOrAdd(List<? extends T> list,
                                             T element,
                                             Predicate<T> matcher,
                                             UnaryOperator<T> updater) {
      notNull(list, "list");
      notNull(element, "element");
      notNull(matcher, "matcher");
      notNull(updater, "updater");

      Optional<? extends T> foundItem = list.stream().filter(matcher).findFirst();

      return immutableList(StreamUtils.streamConcat(list
                                                          .stream()
                                                          .filter(i -> foundItem
                                                                .map(fi -> fi != i)
                                                                .orElse(true)),
                                                    stream(foundItem.map(updater).orElse(element))));
   }

   /**
    * Creates a new immutable list with element matched with {@code matcher} updated by {@code updated}.
    * If no element match, list is returned unchanged.
    *
    * @param list list to operate on
    * @param matcher element matching function
    * @param updater element updating function
    * @param <T> element type
    *
    * @return new immutable list with updated element, or unchanged
    */
   public static <T> List<T> listUpdate(List<? extends T> list,
                                        Predicate<T> matcher,
                                        UnaryOperator<T> updater) {
      notNull(list, "list");
      notNull(matcher, "matcher");
      notNull(updater, "updater");

      return immutableList(list.stream().map(i -> matcher.test(i) ? updater.apply(i) : i));
   }

   /**
    * Creates a new immutable list with element matched with {@code matcher} updated by {@code updated}.
    *
    * @param list list to operate on
    * @param matcher element matching function
    * @param updater element updating function
    * @param <T> element type
    *
    * @return new immutable list with updated element
    *
    * @throws IllegalArgumentException if no element is matched using {@code matcher}
    */
   public static <T> List<T> listUpdateOrThrow(List<? extends T> list,
                                               Predicate<T> matcher,
                                               UnaryOperator<T> updater) {
      notNull(list, "list");
      notNull(matcher, "matcher");
      notNull(updater, "updater");

      Optional<? extends T> foundItem = list.stream().filter(matcher).findFirst();
      if (!foundItem.isPresent()) throw new IllegalArgumentException("No element found");

      return immutableList(listUpdate(list, matcher, updater));
   }

   /**
    * Creates a new immutable list with elements matched with {@code matcher} retained.
    *
    * @param list list to operate on
    * @param matcher element matching function
    * @param <T> element type
    *
    * @return new immutable list with matched elements removed
    */
   public static <T> List<T> listFilter(List<? extends T> list, Predicate<T> matcher) {
      notNull(list, "list");
      notNull(matcher, "matcher");

      return immutableList(list.stream().filter(matcher));
   }

   /**
    * Creates a new immutable list with elements matched with {@code matcher} removed.
    *
    * @param list list to operate on
    * @param matcher element matching function
    * @param <T> element type
    *
    * @return new immutable list with matched elements removed
    */
   public static <T> List<T> listRemove(List<? extends T> list, Predicate<T> matcher) {
      return listFilter(list, matcher.negate());
   }

   /**
    * Creates a new immutable map from source map where key is removed.
    *
    * @param map map to operate on
    * @param key element to add
    * @param <K> key of map
    *
    * @return new immutable map with removed element
    */
   public static <K, V> Map<K, V> mapRemove(Map<K, V> map, K key) {
      notNull(map, "map");
      notNull(key, "key");

      Map<K, V> mutableMap = new HashMap<>(map);
      mutableMap.remove(key);
      return immutableMap(mutableMap);
   }

   /**
    * Creates a new immutable map from source map where all specified keys are removed.
    *
    * @param map map to operate on
    * @param keys element to remove
    * @param <K> key of map
    *
    * @return new immutable map with removed element
    */
   public static <K, V> Map<K, V> mapRemove(Map<K, V> map, List<K> keys) {
      notNull(map, "map");
      notNull(keys, "key");

      Map<K, V> mutableMap = new HashMap<>(map);
      keys.forEach(mutableMap::remove);
      return immutableMap(mutableMap);
   }

}
