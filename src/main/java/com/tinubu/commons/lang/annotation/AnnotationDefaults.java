/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.annotation;

/**
 * Support for default values in Java annotations.
 */
public final class AnnotationDefaults {

   /**
    * Default for any {@link String} or {@link String} array value in annotations.
    */
   public static final String DEFAULT_STRING = "<dflt:\u0080\u00A0\u00DE\u013D\u017F>";

   private AnnotationDefaults() {}

   public static boolean isDefaultValue(Object object) {
      if (object instanceof String) {
         return DEFAULT_STRING.equals(object);
      } else if (object instanceof String[]) {
         return ((String[]) object).length == 1 && DEFAULT_STRING.equals(((String[]) object)[0]);
      }

      return false;
   }

}
