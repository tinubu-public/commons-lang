/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * A specialized file {@link OutputStream} implementation that provide getter to underlying {@link File}.
 * Most constructors builds a new {@link FileOutputStream}, excepting
 * {@link #ReferencedFileOutputStream(OutputStream, File)} ReferencedFileOutputStream} that support any
 * {@link OutputStream}.
 */
public class ReferencedFileOutputStream extends OutputStream {
   private final File file;
   private final OutputStream delegate;

   public ReferencedFileOutputStream(OutputStream delegate, File file) {
      this.delegate = delegate;
      this.file = file;
   }

   public ReferencedFileOutputStream(OutputStream delegate, String name) {
      this(delegate, new File(name));
   }

   public ReferencedFileOutputStream(String name) throws FileNotFoundException {
      this(new FileOutputStream(name), name);
   }

   public ReferencedFileOutputStream(String name, boolean append) throws FileNotFoundException {
      this(new FileOutputStream(name, append), name);
   }

   public ReferencedFileOutputStream(File file) throws FileNotFoundException {
      this(new FileOutputStream(file), file);
   }

   public ReferencedFileOutputStream(File file, boolean append) throws FileNotFoundException {
      this(new FileOutputStream(file, append), file);
   }

   public File file() {
      return file;
   }

   @Override
   public void write(int b) throws IOException {
      delegate.write(b);
   }

   @Override
   public void write(byte[] b, int off, int len) throws IOException {
      delegate.write(b, off, len);
   }

   @Override
   public void flush() throws IOException {
      delegate.flush();
   }

   @Override
   public void close() throws IOException {
      delegate.close();
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      ReferencedFileOutputStream that = (ReferencedFileOutputStream) o;
      return Objects.equals(file, that.file) && Objects.equals(delegate, that.delegate);
   }

   @Override
   public int hashCode() {
      return Objects.hash(file, delegate);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ReferencedFileOutputStream.class.getSimpleName() + "[", "]")
            .add("file=" + file)
            .add("delegate=" + delegate)
            .toString();
   }

}
