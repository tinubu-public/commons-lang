/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.tinubu.commons.lang.io.GeneralReplacerReader.TokenReplacer;

public class MultiReplacer implements TokenReplacer {
   private final List<TokenReplacer> replacers;

   private List<TokenReplacer> matchingReplacers;

   public MultiReplacer(List<TokenReplacer> replacers) {
      this.replacers = notNull(replacers, "replacers");
      this.matchingReplacers = new ArrayList<>(replacers);
   }

   public MultiReplacer(TokenReplacer... matchers) {
      this(list(notNull(matchers, "replacers")));
   }

   @Override
   public void reset() {
      matchingReplacers = new ArrayList<>(replacers);
      matchingReplacers.forEach(TokenReplacer::reset);
   }

   @Override
   public boolean isPremise(int c) {
      return c != -1 && matchingReplacers.stream().anyMatch(m -> m.isPremise(c));
   }

   @Override
   public boolean append(int c) {
      boolean hasStillAnyReplacerMatching = false;
      for (Iterator<TokenReplacer> replacerIterator = matchingReplacers.iterator();
           replacerIterator.hasNext(); ) {
         TokenReplacer replacer = replacerIterator.next();

         if (!replacer.append(c)) {
            replacerIterator.remove();
         } else {
            hasStillAnyReplacerMatching = true;
         }
      }

      return hasStillAnyReplacerMatching;
   }

   @Override
   public boolean matches() {
      return matchingReplacers.stream().anyMatch(TokenReplacer::matches);
   }

   @Override
   public Optional<char[]> replacement() {
      return matchingReplacers
            .stream()
            .filter(TokenReplacer::matches)
            .findFirst()
            .orElseThrow(IllegalStateException::new)
            .replacement();
   }

}
