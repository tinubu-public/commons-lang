/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.lang.Math.min;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.util.Optional;

/**
 * General replacer for reader based on {@link PushbackReader} to replace arbitrary tokens in large
 * documents.
 * Note that you should wrap the provided input reader in a {@link BufferedReader} for performance purpose.
 * You must provide a {@link TokenReplacer} implementation to implement your token matching and
 * replacement algorithm.
 * Limitation : section length and replacement text is limited to {@value #DEFAULT_BUFFER_SIZE} bytes by
 * default. You can configure an alternative buffer size depending on your requirements. In case of
 * buffer overflow, token replacement string will be ignored and original text left as-is.
 */
public class GeneralReplacerReader extends PushbackReader {

   /**
    * Default buffer size.
    */
   private static final int DEFAULT_BUFFER_SIZE = 8192;

   private final TokenReplacer tokenReplacer;

   /**
    * Maximum length of pushback buffer, constraining all theses :
    * <ul>
    *    <li>the maximum read length in {@link #read(char[], int, int)}</li>
    *    <li>the maximum length of section to replace, and</li>
    *    <li>the maximum length of replacement text</li>
    * </ul>
    */
   private final int bufferSize;

   /** Tracks last computed section remaining length to read before computing the next section. */
   private int currentSectionLen = 0;

   /**
    * {@link TokenReplacer} factory. A new instance will be generated for each new token identified.
    */
   @FunctionalInterface
   public interface TokenReplacerFactory {
      /**
       * Instantiates a new instance.
       *
       * @param bufferSize current buffer size configured in {@link GeneralReplacerReader}, can be
       *       useful to align replacer implementation with it.
       *
       * @return new token replacer instance
       */
      TokenReplacer instance(int bufferSize);
   }

   /**
    * Token matching and replacement algorithm. A new instance will be generated for each new
    * identified token.
    */
   public interface TokenReplacer {

      /**
       * Resets token replacer internal state for next token matching.
       * This operation is called before a new token is analyzed.
       */
      void reset();

      /**
       * Returns {@code true} if specified character is a token premise. Replacer state should not be modified
       * at this level but in {@link #append(int)} operation.
       *
       * @param c character to match, is never {@code -1}
       *
       * @return whether the specified character is a token premise
       */
      boolean isPremise(int c);

      /**
       * Appends the specified character to token matching. Returns {@code false} if matching fails with this
       * new character, {@code true} if matching is still successful and matching can continue.
       * Note that {@code -1} character can be received if no more characters are available, in this case you
       * can decide to return {@code true} or {@code false} depending on your matching algorithm.
       * <p>
       * Note that if you return {@code false} here, no replacement will occur. Otherwise, a call to
       * {@link #matches()} will be tried after this call, so if you consider the token as "matched" with
       * this last character append, just return {@code true} and also makes {@link #matches()} return
       * {@code true}.
       *
       * @param c new character to append to token matcher, can be -1 if no more characters are
       *       available in reader
       */
      boolean append(int c);

      /**
       * Returns {@code true} if token is considered as "matched" since the last {@link #append(int)} call.
       * The general replacer will then call {@link #replacement()} to retrieve replacement string for matched
       * token.
       *
       * @return {@code true} if token is considered as "matched"
       */
      boolean matches();

      /**
       * Returns replacement string for matched token. You can return {@link Optional#empty()} if replacement
       * string can't be internally generated for the matched token, in this case, token string won't be
       * replaced and left as-is.
       *
       * @return token replacement string or {@link Optional#empty()} if no replacement string is available
       *       for matched token
       */
      Optional<char[]> replacement();
   }

   /**
    * Creates general replacer reader instance.
    *
    * @param in reader to read from
    * @param tokenReplacerFactory token replacer factory
    * @param bufferSize internal buffer size to adapt to your requirements
    */
   public GeneralReplacerReader(Reader in, TokenReplacerFactory tokenReplacerFactory, int bufferSize) {
      super(notNull(in, "in"), satisfies(bufferSize, bs -> bs > 0, "bufferSize", "must be > 0"));

      this.tokenReplacer = notNull(tokenReplacerFactory, "tokenReplacerFactory").instance(bufferSize);
      this.bufferSize = bufferSize;
   }

   /**
    * Creates general replacer reader instance with default buffer size.
    *
    * @param in reader to read from
    * @param tokenReplacerFactory token replacer factory
    */
   public GeneralReplacerReader(Reader in, TokenReplacerFactory tokenReplacerFactory) {
      this(in, tokenReplacerFactory, DEFAULT_BUFFER_SIZE);
   }

   @Override
   public int read() throws IOException {
      computeNextSection(1);
      return super.read();
   }

   @Override
   public int read(char[] cbuf, int off, int len) throws IOException {
      return super.read(cbuf, off, computeNextSection(len));
   }

   /**
    * Main section dispatcher based on next character.
    *
    * @return the number of bytes to read for the next section
    *
    * @throws IOException if an I/O error occurs
    * @implNote Because of pushback buffer size limitation, the entire buffer size must be used for a
    *       single section at a time, so that the next section must be completely read before starting a new
    *       section.
    */
   private int computeNextSection(int len) throws IOException {
      int c = super.read();
      int sectionLen;

      if (c == -1) {
         sectionLen = len;
      } else if (currentSectionLen > 0) {
         super.unread(c);
         sectionLen = currentSectionLen;
      } else {
         tokenReplacer.reset();

         if (tokenReplacer.isPremise(c)) {
            super.unread(c);
            sectionLen = currentSectionLen = computeTokenSection(tokenReplacer);
         } else {
            super.unread(c);
            sectionLen = currentSectionLen = computeRegularTextSection(tokenReplacer);
         }
      }

      sectionLen = min(len, sectionLen);
      currentSectionLen -= sectionLen;
      return sectionLen;
   }

   /**
    * Identifies the next regular text section until a premise character is found.
    *
    * @return the number of bytes to read for this section
    *
    * @throws IOException if an I/O error occurs
    * @implNote section length is constrained by pushback buffer size. Buffer size has no impact on
    *       final result : more iterations will be just required if buffer size is too low.
    */
   private int computeRegularTextSection(TokenReplacer tokenReplacer) throws IOException {
      int c;
      boolean token = false;
      int constrainedLength = bufferSize;
      char[] textBuffer = new char[constrainedLength];
      int textBufferLength = 0;

      do {
         c = super.read();

         if (tokenReplacer.isPremise(c)) {
            token = true;
         }
         if (c != -1) {
            textBuffer[textBufferLength] = (char) c;
            textBufferLength++;
         }
      } while (c != -1 && textBufferLength < constrainedLength && !token);

      super.unread(textBuffer, 0, textBufferLength);
      if (token && c != -1) {
         return textBufferLength - 1;
      } else {
         return textBufferLength;
      }
   }

   /**
    * Identifies the next token section.
    *
    * @param tokenReplacer current token replacer instance for this section
    *
    * @return the number of bytes to read for this section
    *
    * @throws IOException if an I/O error occurs
    * @implNote If buffer size is too low to handle this section in one iteration, the section won't be
    *       replaced. The same if token replacement string is larger than buffer.
    */
   private int computeTokenSection(TokenReplacer tokenReplacer) throws IOException {
      int c;
      boolean parseFinished = false;
      char[] tokenBuffer = new char[bufferSize];
      int tokenBufferLength = 0;

      do {
         c = super.read();

         if (c != -1) {
            tokenBuffer[tokenBufferLength] = (char) c;
            tokenBufferLength++;
         }

         if (!tokenReplacer.append(c)) {
            parseFinished = true;

            if (c != -1) {
               super.unread(c);
               tokenBufferLength--;
            }
         } else if (tokenReplacer.matches()) {
            parseFinished = true;

            Optional<char[]> value = tokenReplacer.replacement();
            if (value.isPresent() && value.get().length <= bufferSize) {
               tokenBuffer = value.get();
               tokenBufferLength = value.get().length;
            }
         }

      } while (!parseFinished && tokenBufferLength < bufferSize && c != -1);

      super.unread(tokenBuffer, 0, tokenBufferLength);
      return tokenBufferLength;
   }
}
