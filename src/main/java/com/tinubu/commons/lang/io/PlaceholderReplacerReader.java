/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalPredicate;
import static com.tinubu.commons.lang.validation.Validate.notEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.lang.Math.min;
import static java.util.Objects.requireNonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.util.Map;
import java.util.function.Function;

/**
 * Optimized reader based on {@link PushbackReader} to replace placeholders in large documents.
 * Note that you should wrap the provided input reader in a {@link BufferedReader} for performance purpose.
 * Placeholders have the format {@code ${key}} but begin and end separators are configurable.
 * Replacement function whose values are object are converted to string using {@link String#valueOf(Object)}.
 * Limitation : Placeholders length and replacement text is limited to {@value #DEFAULT_BUFFER_SIZE} bytes by
 * default. You can configure an alternative buffer size depending on your requirements. In case of
 * buffer overflow, replacement string will be ignored and original text left as-is.
 */
public class PlaceholderReplacerReader extends PushbackReader {

   private static final int DEFAULT_BUFFER_SIZE = 8192;
   private static final String DEFAULT_PLACEHOLDER_BEGIN = "${";
   private static final String DEFAULT_PLACEHOLDER_END = "}";

   private final Function<String, Object> replacementFunction;
   /**
    * Maximum length of pushback buffer, constraining all theses :
    * <ul>
    *    <li>the maximum read length in {@link #read(char[], int, int)}</li>
    *    <li>the maximum length of placeholder to replace, including separator characters, and</li>
    *    <li>the maximum length of replacement text</li>
    * </ul>
    */
   private final int bufferSize;
   private final char[] placeholderBegin;
   private final char[] placeholderEnd;

   /** Tracks last computed section remaining length to read before computing the next section. */
   private int currentSectionLen = 0;

   /**
    * Creates placeholder replacer reader instance.
    *
    * @param in reader to read from
    * @param replacementFunction replacement function, taking key value in parameter
    * @param placeholderBegin placeholder begin char sequence
    * @param placeholderEnd placeholder end char sequence
    * @param bufferSize internal buffer size to adapt to your requirements
    */
   public PlaceholderReplacerReader(Reader in,
                                    Function<String, Object> replacementFunction,
                                    String placeholderBegin,
                                    String placeholderEnd,
                                    int bufferSize) {
      super(notNull(in, "in"), satisfies(bufferSize, bs -> bs > 0, "bufferSize", "must be > 0"));

      this.replacementFunction = notNull(replacementFunction, "replacementFunction");
      this.bufferSize = bufferSize;
      this.placeholderBegin = notEmpty(placeholderBegin, "placeholderBegin").toCharArray();
      this.placeholderEnd = notEmpty(placeholderEnd, "placeholderEnd").toCharArray();
   }

   /**
    * Creates placeholder replacer reader instance.
    *
    * @param in reader to read from
    * @param replacementFunction replacement function, taking key value in parameter
    */
   public PlaceholderReplacerReader(Reader in, Function<String, Object> replacementFunction) {
      this(in, replacementFunction, DEFAULT_PLACEHOLDER_BEGIN, DEFAULT_PLACEHOLDER_END, DEFAULT_BUFFER_SIZE);
   }

   /**
    * Creates placeholder replacer reader instance.
    *
    * @param in reader to read from
    * @param replacementFunction replacement function, taking key value in parameter
    * @param bufferSize internal buffer size to adapt to your requirements
    */
   public PlaceholderReplacerReader(Reader in, Function<String, Object> replacementFunction, int bufferSize) {
      this(in, replacementFunction, DEFAULT_PLACEHOLDER_BEGIN, DEFAULT_PLACEHOLDER_END, bufferSize);
   }

   /**
    * Creates placeholder replacer reader instance.
    *
    * @param in reader to read from
    * @param replacementFunction replacement function, taking key value in parameter
    * @param placeholderBegin placeholder begin char sequence
    * @param placeholderEnd placeholder end char sequence
    */
   public PlaceholderReplacerReader(Reader in,
                                    Function<String, Object> replacementFunction,
                                    String placeholderBegin,
                                    String placeholderEnd) {
      this(in, replacementFunction, placeholderBegin, placeholderEnd, DEFAULT_BUFFER_SIZE);
   }

   /**
    * Simple replacement function adapter for {@link Map} model.
    *
    * @param model map model
    *
    * @return replacement function
    */
   public static Function<String, Object> mapModelReplacementFunction(Map<String, Object> model) {
      notNull(model, "model");

      return key -> model.get(requireNonNull(key));
   }

   @Override
   public int read() throws IOException {
      computeNextSection(1);
      return super.read();
   }

   @Override
   public int read(char[] cbuf, int off, int len) throws IOException {
      return super.read(cbuf, off, computeNextSection(len));
   }

   /**
    * Main section dispatcher based on next character.
    *
    * @return the number of bytes to read for the next section
    *
    * @throws IOException if an I/O error occurs
    * @implNote Because of pushback buffer size limitation, the entire buffer size must be used for a
    *       single section at a time, so that the next section must be completely read before starting a new
    *       section.
    */
   private int computeNextSection(int len) throws IOException {
      int c = super.read();
      int sectionLen;

      if (c == -1) {
         sectionLen = len;
      } else if (currentSectionLen > 0) {
         super.unread(c);
         sectionLen = currentSectionLen;
      } else if (c == placeholderBegin[0]) {
         super.unread(c);
         sectionLen = currentSectionLen = computePlaceholderSection();
      } else {
         super.unread(c);
         sectionLen = currentSectionLen = computeRegularTextSection();
      }

      sectionLen = min(len, sectionLen);
      currentSectionLen -= sectionLen;
      return sectionLen;
   }

   /**
    * Identifies the next regular text section until a placeholder character is found.
    *
    * @return the number of bytes to read for this section
    *
    * @throws IOException if an I/O error occurs
    * @implNote section length is constrained by pushback buffer size. Buffer size has no impact on
    *       final result : more iterations will be just required if buffer size is too low.
    */
   private int computeRegularTextSection() throws IOException {
      int c;
      boolean ph = false;
      int constrainedLength = bufferSize;
      char[] textBuffer = new char[constrainedLength];
      int textBufferLength = 0;

      do {
         c = super.read();

         if (c == placeholderBegin[0]) {
            ph = true;
         }
         if (c != -1) {
            textBuffer[textBufferLength] = (char) c;
            textBufferLength++;
         }
      } while (c != -1 && textBufferLength < constrainedLength && !ph);

      super.unread(textBuffer, 0, textBufferLength);
      if (ph) {
         return textBufferLength - 1;
      } else {
         return textBufferLength;
      }
   }

   /**
    * Identifies the next placeholder section. This can be a false positive if subsequent characters are not
    * part of placeholder start string, hence, the section will be downgraded to regular text.
    *
    * @return the number of bytes to read for this section
    *
    * @throws IOException if an I/O error occurs
    * @implNote If buffer size is too low to handle this section in one iteration, the section will be
    *       downgraded to regular text, so the placeholder won't be replaced. The same if replacement string
    *       is larger than buffer.
    */
   private int computePlaceholderSection() throws IOException {
      int c;
      int parseToken = 0; // 0=parse phBegin, 1=parse ph, 2=parse phEnd
      boolean parseFinished = false;
      int phBeginIndex = 0;
      int phEndIndex = 0;

      char[] phBuffer = new char[bufferSize];
      int phBufferLength = 0;
      char[] phKeyBuffer = new char[bufferSize];
      int phKeyBufferLength = 0;

      do {
         c = super.read();

         if (c != -1) {
            phBuffer[phBufferLength] = (char) c;
            phBufferLength++;

            if (parseToken == 0) {
               if (c != placeholderBegin[phBeginIndex++]) {
                  parseFinished = true;
               }
               if (!parseFinished && phBeginIndex >= placeholderBegin.length) {
                  parseToken = 1;
               }
            } else if (parseToken == 1) {
               if (c == placeholderEnd[0]) {
                  parseToken = 2;
                  phBufferLength--;
                  super.unread(c);
               } else {
                  phKeyBuffer[phKeyBufferLength] = (char) c;
                  phKeyBufferLength++;
               }
            } else if (parseToken == 2) {

               if (c != placeholderEnd[phEndIndex++]) {
                  parseToken = 1;

                  for (int i = 0; i < phEndIndex; i++) {
                     phKeyBuffer[phKeyBufferLength] = phBuffer[phBufferLength - phEndIndex + i];
                     phKeyBufferLength++;
                  }

                  phEndIndex = 0;
               }

               if (parseToken == 2 && phEndIndex >= placeholderEnd.length) {
                  parseFinished = true;

                  String key = new String(phKeyBuffer, 0, phKeyBufferLength);
                  String value = nullable(replacementFunction.apply(key))
                        .map(String::valueOf)
                        .flatMap(replacementString -> optionalPredicate(replacementString,
                                                                        __ -> replacementString.length()
                                                                              <= bufferSize))
                        .orElse(new String(placeholderBegin) + key + new String(placeholderEnd));

                  phBuffer = value.toCharArray();
                  phBufferLength = phBuffer.length;
               }
            }
         }
      } while (!parseFinished && phBufferLength < bufferSize && c != -1);

      super.unread(phBuffer, 0, phBufferLength);
      return phBufferLength;
   }
}
