/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mapper;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.function.Function;

/**
 * Schwartzian transformer with user-specified mapper. The principle is to compute and memorize the mapped
 * result for each item, so that sorting algorithms won't have to apply mapping many times.
 * <p>
 * This transformer supports {@code null} values for both source object and mapper result.
 * Two {@code null} objects are always considered equals.
 * <p>
 * Typical usage :
 * {@code Stream.of(2, 1, 3)
 * .map(d -> SchwartzianTransformer.wrap(d, String::valueOf))
 * .sorted()
 * .map(SchwartzianTransformer::unwrap)
 * .collect(toList()))}
 */
public class SchwartzianTransformer<S, T extends Comparable<? super T>>
      implements Comparable<SchwartzianTransformer<S, T>> {
   private final S source;
   private final T target;

   protected SchwartzianTransformer(S source, Function<? super S, ? extends T> mapper) {
      this.source = source;
      this.target = nullable(source).map(s -> notNull(mapper, "mapper").apply(s)).orElse(null);
   }

   public static <S, T extends Comparable<? super T>> SchwartzianTransformer<S, T> wrap(S source,
                                                                                        Function<? super S, ? extends T> mapper) {
      return new SchwartzianTransformer<>(source, mapper);
   }

   public S unwrap() {
      return source;
   }

   @SuppressWarnings("unchecked")
   public boolean equals(Object other) {
      if (other == this) {
         return true;
      }

      if (other instanceof SchwartzianTransformer) {
         return targetEquals((SchwartzianTransformer<S, T>) other);
      } else {
         return false;
      }
   }

   private boolean targetEquals(SchwartzianTransformer<S, T> other) {
      if (other.target == target) {
         return true;
      } else {
         if (other.target == null) {
            return false;
         } else {
            return other.target.equals(target);
         }
      }
   }

   public int hashCode() {
      return nullable(target).map(Object::hashCode).orElse(0);
   }

   @Override
   public int compareTo(SchwartzianTransformer<S, T> o) {
      if (target == o.target) {
         return 0;
      } else {
         if (target == null) {
            return -1;
         } else {
            if (o.target == null) {
               return 1;
            } else {
               return target.compareTo(o.target);
            }
         }
      }

   }
}
