/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.datetime;

import static java.util.Objects.requireNonNull;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

/**
 * Global application clock holder.
 * All application code related to system clock should use this holder so that testing code have
 * the opportunity to switch global clock to fixed one.
 * <p>
 * e.g.: {@code OffsetDateTime.now(ApplicationClock.clock())}
 *
 * @implSpec This class is not thread-safe but default clock should be switched only in tests
 */
public final class ApplicationClock {
   private static final Clock DEFAULT_CLOCK = Clock.systemDefaultZone();

   private static Clock clock = DEFAULT_CLOCK;

   private ApplicationClock() {}

   public static Clock clock() {
      return clock;
   }

   public static void setClock(Clock clock) {
      ApplicationClock.clock = requireNonNull(clock, "clock must not be null");
   }

   public static void setFixedClock(ZonedDateTime fixedDate) {
      requireNonNull(fixedDate, "fixedDate must not be null");
      ApplicationClock.clock = Clock.fixed(fixedDate.toInstant(), fixedDate.getZone());
   }

   public static void resetClock() {
      ApplicationClock.clock = DEFAULT_CLOCK;
   }

   /**
    * Alias for {@link #nowAsOffsetDateTime()}.
    *
    * @return current time
    */
   public static OffsetDateTime now() {
      return nowAsOffsetDateTime();
   }

   /**
    * The only and official "now" functions that must be used everywhere.
    *
    * @return current time
    */
   public static Instant nowAsInstant() {
      return Instant.now(clock());
   }

   /**
    * The only and official "now" functions that must be used everywhere.
    *
    * @return current time
    */
   public static OffsetDateTime nowAsOffsetDateTime() {
      return OffsetDateTime.now(clock());
   }

   /**
    * The only and official "now" functions that must be used everywhere.
    *
    * @return current time
    */
   public static ZonedDateTime nowAsZonedDateTime() {
      return ZonedDateTime.now(clock());
   }

   /**
    * The only and official "now" functions that must be used everywhere.
    *
    * @return current time
    */
   public static LocalDate nowAsLocalDate() {
      return LocalDate.now(clock());
   }

   /**
    * The only and official "now" functions that must be used everywhere.
    *
    * @return current time
    */
   public static LocalDateTime nowAsLocalDateTime() {
      return LocalDateTime.now(clock());
   }
}
