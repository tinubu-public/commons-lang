/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.datetime.converter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Date time types conversion interface.
 *
 * @implSpec {@code null} input values are mapped as {@code null} return values following the "mapping"
 *       pattern.
 */
public interface DateTimeConverter {

   /**
    * Converts a {@link OffsetDateTime} into a {@link ZonedDateTime} depending on instance
    * logic.
    *
    * @param offsetDate offset date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   ZonedDateTime zonedDateTime(OffsetDateTime offsetDate);

   /**
    * Converts a {@link LocalDateTime} into a {@link ZonedDateTime} depending on instance
    * logic.
    *
    * @param localDate local date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   ZonedDateTime zonedDateTime(LocalDateTime localDate);

   /**
    * Converts a {@link Date} into a {@link ZonedDateTime} depending on instance logic.
    *
    * @param localDate date to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   ZonedDateTime zonedDateTime(Date localDate);

   /**
    * Converts a {@link Calendar} into a {@link ZonedDateTime} depending on instance logic.
    *
    * @param calendar calendar to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   ZonedDateTime zonedDateTime(Calendar calendar);

   /**
    * Converts a {@link Instant} into a {@link ZonedDateTime} depending on instance logic.
    *
    * @param instant instant to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   ZonedDateTime zonedDateTime(Instant instant);

   /**
    * Converts a {@link ZonedDateTime} into a {@link OffsetDateTime} depending on instance
    * logic.
    *
    * @param zonedDate zoned date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   OffsetDateTime offsetDateTime(ZonedDateTime zonedDate);

   /**
    * Converts a {@link LocalDateTime} into a {@link OffsetDateTime} depending on instance
    * logic.
    *
    * @param localDate local date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   OffsetDateTime offsetDateTime(LocalDateTime localDate);

   /**
    * Converts a {@link Date} into a {@link OffsetDateTime} depending on instance logic.
    *
    * @param localDate local date to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   OffsetDateTime offsetDateTime(Date localDate);

   /**
    * Converts a {@link Calendar} into a {@link OffsetDateTime} depending on instance logic.
    *
    * @param calendar calendar date to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   OffsetDateTime offsetDateTime(Calendar calendar);

   /**
    * Converts a {@link Instant} into a {@link OffsetDateTime} depending on instance logic.
    *
    * @param instant instant to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   OffsetDateTime offsetDateTime(Instant instant);

   /**
    * Converts a {@link ZonedDateTime} into a {@link LocalDateTime} depending on instance
    * logic.
    *
    * @param zonedDate zoned date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   LocalDateTime localDateTime(ZonedDateTime zonedDate);

   /**
    * Converts a {@link OffsetDateTime} into a {@link LocalDateTime} depending on instance
    * logic.
    *
    * @param offsetDate offset date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   LocalDateTime localDateTime(OffsetDateTime offsetDate);

   /**
    * Converts a {@link Date} into a {@link LocalDateTime} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param localDate local date to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   LocalDateTime localDateTime(Date localDate);

   /**
    * Converts a {@link Calendar} into a {@link LocalDateTime} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param calendar calendar date to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   LocalDateTime localDateTime(Calendar calendar);

   /**
    * Converts a {@link Instant} into a {@link LocalDateTime} depending on instance logic.
    *
    * @param instant instant to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   LocalDateTime localDateTime(Instant instant);

   /**
    * Converts a {@link ZonedDateTime} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param zonedDate zoned date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Date dateInstant(ZonedDateTime zonedDate);

   /**
    * Converts a {@link OffsetDateTime} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param offsetDate offset date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Date dateInstant(OffsetDateTime offsetDate);

   /**
    * Converts a {@link Calendar} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param calendar calendar to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Date dateInstant(Calendar calendar);

   /**
    * Converts a {@link Instant} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param instant instant to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Date dateInstant(Instant instant);

   /**
    * Converts a {@link LocalDateTime} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param localDate local date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Date dateInstant(LocalDateTime localDate);

   /**
    * Converts a {@link ZonedDateTime} into a {@link Instant} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param zonedDate zoned date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Instant instant(ZonedDateTime zonedDate);

   /**
    * Converts a {@link OffsetDateTime} into a {@link Instant} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param offsetDate offset date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Instant instant(OffsetDateTime offsetDate);

   /**
    * Converts a {@link LocalDateTime} into a {@link Instant} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param localDate local date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Instant instant(LocalDateTime localDate);

   /**
    * Converts a {@link Date} into a {@link Instant} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param instant instant to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Instant instant(Date instant);

   /**
    * Converts a {@link Calendar} into a {@link Instant} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param calendar calendar to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Instant instant(Calendar calendar);

   /**
    * Converts a {@link ZonedDateTime} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param zonedDate zoned date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Calendar calendar(ZonedDateTime zonedDate);

   /**
    * Converts a {@link OffsetDateTime} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param offsetDate offset date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Calendar calendar(OffsetDateTime offsetDate);

   /**
    * Converts a {@link Date} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param date date to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Calendar calendar(Date date);

   /**
    * Converts a {@link Instant} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param instant instant to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Calendar calendar(Instant instant);

   /**
    * Converts a {@link LocalDateTime} into a {@link Date} depending on instance logic.
    * You should not work with local date time, use {@link OffsetDateTime} instead.
    *
    * @param localDate local date-time to convert
    *
    * @return converted value or {@code null} if input is {@code null}
    */
   Calendar calendar(LocalDateTime localDate);

}
