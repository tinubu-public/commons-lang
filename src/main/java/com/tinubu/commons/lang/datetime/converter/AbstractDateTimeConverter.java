/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.datetime.converter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Abstract date time types converter.
 * <p>
 * The missing elements (ZoneId, ...) in some converters are let to implementors.
 */
public abstract class AbstractDateTimeConverter implements DateTimeConverter {

   /**
    * Zone to use when date is missing this information (e.g.: local date to zoned date conversion).
    */
   private final ZoneId missingZone;

   protected AbstractDateTimeConverter(ZoneId missingZone) {
      this.missingZone = missingZone;
   }

   @Override
   public ZonedDateTime zonedDateTime(OffsetDateTime offsetDate) {
      if (offsetDate == null) return null;

      return ZonedDateTime.ofInstant(offsetDate.toInstant(), missingZone);
   }

   @Override
   public ZonedDateTime zonedDateTime(LocalDateTime localDate) {
      if (localDate == null) return null;

      return localDate.atZone(missingZone);
   }

   @Override
   public ZonedDateTime zonedDateTime(Date localDate) {
      if (localDate == null) return null;

      return ZonedDateTime.ofInstant(localDate.toInstant(), missingZone);
   }

   @Override
   public ZonedDateTime zonedDateTime(Calendar calendar) {
      if (calendar == null) return null;

      return ZonedDateTime.ofInstant(calendar.toInstant(), calendar.getTimeZone().toZoneId());
   }

   @Override
   public ZonedDateTime zonedDateTime(Instant instant) {
      if (instant == null) return null;

      return ZonedDateTime.ofInstant(instant, missingZone);
   }

   @Override
   public OffsetDateTime offsetDateTime(ZonedDateTime zonedDate) {
      if (zonedDate == null) return null;

      return zonedDate.toOffsetDateTime();
   }

   @Override
   public OffsetDateTime offsetDateTime(LocalDateTime localDate) {
      if (localDate == null) return null;

      return OffsetDateTime.ofInstant(instant(localDate), missingZone);
   }

   @Override
   public OffsetDateTime offsetDateTime(Date localDate) {
      if (localDate == null) return null;

      return OffsetDateTime.ofInstant(localDate.toInstant(), missingZone);
   }

   @Override
   public OffsetDateTime offsetDateTime(Calendar calendar) {
      if (calendar == null) return null;

      return OffsetDateTime.ofInstant(calendar.toInstant(), missingZone);
   }

   @Override
   public OffsetDateTime offsetDateTime(Instant instant) {
      if (instant == null) return null;

      return OffsetDateTime.ofInstant(instant, missingZone);
   }

   @Override
   public LocalDateTime localDateTime(ZonedDateTime zonedDate) {
      if (zonedDate == null) return null;

      return zonedDate.toLocalDateTime();
   }

   @Override
   public LocalDateTime localDateTime(OffsetDateTime offsetDate) {
      if (offsetDate == null) return null;

      return offsetDate.toLocalDateTime();
   }

   @Override
   public LocalDateTime localDateTime(Date localDate) {
      if (localDate == null) return null;

      return LocalDateTime.ofInstant(localDate.toInstant(), missingZone);
   }

   @Override
   public LocalDateTime localDateTime(Calendar calendar) {
      if (calendar == null) return null;

      return LocalDateTime.ofInstant(calendar.toInstant(), missingZone);
   }

   @Override
   public LocalDateTime localDateTime(Instant instant) {
      if (instant == null) return null;

      return LocalDateTime.ofInstant(instant, missingZone);
   }

   @Override
   public Date dateInstant(ZonedDateTime zonedDate) {
      if (zonedDate == null) return null;

      return Date.from(zonedDate.toInstant());
   }

   @Override
   public Date dateInstant(OffsetDateTime offsetDate) {
      if (offsetDate == null) return null;

      return Date.from(offsetDate.toInstant());
   }

   @Override
   public Date dateInstant(Calendar calendar) {
      if (calendar == null) return null;

      return Date.from(calendar.toInstant());
   }

   @Override
   public Date dateInstant(Instant instant) {
      if (instant == null) return null;

      return Date.from(instant);
   }

   @Override
   public Date dateInstant(LocalDateTime localDate) {
      if (localDate == null) return null;

      return Date.from(localDate.atZone(missingZone).toInstant());
   }

   @Override
   public Calendar calendar(ZonedDateTime zonedDate) {
      if (zonedDate == null) return null;

      return GregorianCalendar.from(zonedDate);
   }

   @Override
   public Calendar calendar(OffsetDateTime offsetDate) {
      if (offsetDate == null) return null;

      return GregorianCalendar.from(zonedDateTime(offsetDate));
   }

   @Override
   public Calendar calendar(Date date) {
      if (date == null) return null;

      return GregorianCalendar.from(zonedDateTime(date));
   }

   @Override
   public Calendar calendar(Instant instant) {
      if (instant == null) return null;

      return GregorianCalendar.from(zonedDateTime(instant));
   }

   @Override
   public Calendar calendar(LocalDateTime localDate) {
      if (localDate == null) return null;

      return GregorianCalendar.from(zonedDateTime(localDate));
   }

   @Override
   public Instant instant(ZonedDateTime zonedDate) {
      if (zonedDate == null) return null;

      return zonedDate.toInstant();
   }

   @Override
   public Instant instant(OffsetDateTime offsetDate) {
      if (offsetDate == null) return null;

      return offsetDate.toInstant();
   }

   @Override
   public Instant instant(LocalDateTime localDate) {
      if (localDate == null) return null;

      return localDate.atZone(missingZone).toInstant();
   }

   @Override
   public Instant instant(Date date) {
      if (date == null) return null;

      return date.toInstant();
   }

   @Override
   public Instant instant(Calendar calendar) {
      if (calendar == null) return null;

      return calendar.toInstant();
   }

}
