/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.validation;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;

import com.tinubu.commons.lang.util.StringUtils;

/**
 * Provides a simple set validators without external dependencies.
 */
public class Validate {

   private static final String DEFAULT_OBJECT_NAME = "<object>";

   public static <T> T satisfies(T value, Predicate<T> predicate, String name, String message) {
      if (!predicate.test(value)) {
         throw new IllegalArgumentException(String.format("'%s' %s", name, message));
      }
      return value;
   }

   public static <T> T satisfies(T value, Predicate<T> predicate) {
      return satisfies(value, predicate, DEFAULT_OBJECT_NAME, "must satisfy predicate");
   }

   public static <T> T notNull(T value, String name) {
      if (value == null) {
         throw new NullPointerException(String.format("'%s' must not be null", name));
      }
      return value;
   }

   public static <T> T notNull(T value) {
      return notNull(value, DEFAULT_OBJECT_NAME);
   }

   public static <T extends String> T notEmpty(final T value, final String name) {
      if (notNull(value, name).isEmpty()) {
         throw new IllegalArgumentException(String.format("'%s' must not be empty", name));
      }
      return value;
   }

   public static <T extends String> T notEmpty(final T value) {
      return notEmpty(value, DEFAULT_OBJECT_NAME);
   }

   public static <T extends String> T notBlank(final T value, final String name) {
      if (StringUtils.isBlank(notNull(value, name))) {
         throw new IllegalArgumentException(String.format("'%s' must not be blank", name));
      }
      return value;
   }

   public static <T extends String> T notBlank(final T value) {
      return notBlank(value, DEFAULT_OBJECT_NAME);
   }

   public static <T> T[] noNullElements(final T[] array, final String name) {
      notNull(array, name);
      for (int i = 0; i < array.length; i++) {
         if (array[i] == null) {
            throw new IllegalArgumentException(String.format("'%s' must not have null elements at index : %d",
                                                             name,
                                                             i));
         }
      }
      return array;
   }

   public static <T> T[] noNullElements(final T[] array) {
      return noNullElements(array, DEFAULT_OBJECT_NAME);
   }

   public static <T extends Iterable<?>> T noNullElements(final T iterable, final String name) {
      notNull(iterable, name);
      int i = 0;
      for (final Iterator<?> it = iterable.iterator(); it.hasNext(); i++) {
         if (it.next() == null) {
            throw new IllegalArgumentException(String.format("'%s' must not have null elements at index : %d",
                                                             name,
                                                             i));
         }
      }
      return iterable;
   }

   public static <T extends Iterable<?>> T noNullElements(final T iterable) {
      return noNullElements(iterable, DEFAULT_OBJECT_NAME);
   }

   public static <T> T[] notEmpty(final T[] array, final String name) {
      if (notNull(array, name).length == 0) {
         throw new IllegalArgumentException(String.format("'%s' must not be empty", name));
      }
      return array;
   }

   public static <T> T[] notEmpty(final T[] array) {
      return notEmpty(array, DEFAULT_OBJECT_NAME);
   }

   public static byte[] notEmpty(final byte[] array, final String name) {
      if (notNull(array, name).length == 0) {
         throw new IllegalArgumentException(String.format("'%s' must not be empty", name));
      }
      return array;
   }

   public static byte[] notEmpty(final byte[] array) {
      return notEmpty(array, DEFAULT_OBJECT_NAME);
   }

   public static char[] notEmpty(final char[] array, final String name) {
      if (notNull(array, name).length == 0) {
         throw new IllegalArgumentException(String.format("'%s' must not be empty", name));
      }
      return array;
   }

   public static char[] notEmpty(final char[] array) {
      return notEmpty(array, DEFAULT_OBJECT_NAME);
   }

   public static int[] notEmpty(final int[] array, final String name) {
      if (notNull(array, name).length == 0) {
         throw new IllegalArgumentException(String.format("'%s' must not be empty", name));
      }
      return array;
   }

   public static int[] notEmpty(final int[] array) {
      return notEmpty(array, DEFAULT_OBJECT_NAME);
   }

   public static <T extends Collection<?>> T notEmpty(final T collection, final String name) {
      if (notNull(collection, name).isEmpty()) {
         throw new IllegalArgumentException(String.format("'%s' must not be empty", name));
      }
      return collection;
   }

   public static <T extends Collection<?>> T notEmpty(final T collection) {
      return notEmpty(collection, DEFAULT_OBJECT_NAME);
   }

   /**
    * Explicits the fact of ignoring specified value, returned for example by an {@link CheckReturnValue}
    * annotated method.
    *
    * @param object object to ignore
    */
   public static void ignore(final Object object) {
   }

}
