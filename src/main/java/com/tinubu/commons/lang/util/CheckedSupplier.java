/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CheckedRunnable.checkedRunnable;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static java.util.Objects.requireNonNull;

import java.util.concurrent.Callable;
import java.util.function.Supplier;

/**
 * Represents a supplier of results.
 * This operation can throw checked exceptions.
 * <p>
 * There is no requirement that a new or distinct result be returned each
 * time the supplier is invoked.
 * <p>
 * This is a functional interface whose functional method is {@link #getChecked()}.
 *
 * @param <T> the type of results supplied by this supplier
 */
@FunctionalInterface
public interface CheckedSupplier<T> extends Supplier<T>, Callable<T> {
   /**
    * Gets a result.
    *
    * @return a result
    */
   @Override
   default T get() {
      return unchecked(this);
   }

   /**
    * Computes a result, or throws an exception if unable to do so.
    *
    * @return computed result
    *
    * @throws Exception if unable to compute a result
    */
   @Override
   default T call() throws Exception {
      return getChecked();
   }

   /**
    * Gets a result.
    * This operation can throw checked exceptions.
    *
    * @return a result
    */
   T getChecked() throws Exception;

   /**
    * Returns a checked supplier from a standard supplier.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    */
   static <T> CheckedSupplier<T> checkedSupplier(Supplier<T> supplier) {
      requireNonNull(supplier);
      if (supplier instanceof CheckedSupplier) {
         return (CheckedSupplier<T>) supplier;
      } else {
         return supplier::get;
      }
   }

   /**
    * Returns a checked supplier from a standard callable.
    *
    * @param callable callable to adapt
    *
    * @return checked supplier
    */
   static <T> CheckedSupplier<T> checkedSupplier(Callable<T> callable) {
      requireNonNull(callable);
      return callable::call;
   }

   /**
    * Returns a checked supplier from a standard runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return checked supplier
    */
   static <T> CheckedSupplier<T> checkedSupplier(Runnable runnable) {
      requireNonNull(runnable);
      return checkedSupplier(checkedRunnable(runnable));
   }

   /**
    * Returns a checked supplier from a checked runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return checked supplier
    */
   static <T> CheckedSupplier<T> checkedSupplier(CheckedRunnable runnable) {
      requireNonNull(runnable);
      return () -> {
         runnable.runChecked();
         return null;
      };
   }

   /**
    * Returns an unchecked supplier from a checked supplier.
    *
    * @param supplier supplier to adapt
    *
    * @return unchecked supplier
    */
   static <T> Supplier<T> uncheckedSupplier(CheckedSupplier<T> supplier) {
      requireNonNull(supplier);
      return supplier.unchecked();
   }

   /**
    * Returns a value from unchecked supplier.
    *
    * @param supplier supplier to get
    *
    * @return supplied value
    */
   static <T> T unchecked(CheckedSupplier<T> supplier) {
      requireNonNull(supplier);
      return supplier.unchecked().get();
   }

   /**
    * Returns an unchecked supplier from this checked supplier.
    *
    * @return unchecked supplier
    */
   default Supplier<T> unchecked() {
      return () -> {
         try {
            return this.getChecked();
         } catch (Exception e) {
            throw sneakyThrow(e);
         }
      };
   }

}
