/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Objects.requireNonNull;

import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Represents either a success with a value or a failure with a reason.
 *
 * @param <V> success value
 * @param <R> failure reason
 */
public abstract class Try<V, R> {

   /**
    * Handle exceptions that can be thrown by specified callable and returns it as a {@link Try} instance.
    *
    * @param callable callable to execute
    * @param exceptionClass catch only exception that are instance of this one, letting other exception
    *       be thrown as regular exceptions
    *
    * @throws Exception if callable throw an exception that is not an instance of specified exception
    *       class.
    */
   public static <V, X extends Exception> Try<V, Exception> handleException(Callable<V> callable,
                                                                            Class<? extends X> exceptionClass)
         throws Exception {
      notNull(callable, "callable");
      notNull(exceptionClass, "exceptionClass");

      try {
         return Success.of(callable.call());
      } catch (Exception e) {
         if (exceptionClass.isAssignableFrom(e.getClass())) {
            return ExceptionFailure.of(e);
         } else {
            throw e;
         }
      }
   }

   /**
    * Handle exceptions that can be thrown by specified callable and returns it as a {@link Try} instance.
    *
    * @param callable callable to execute
    */
   public static <V> Try<V, Exception> handleException(Callable<V> callable) {
      notNull(callable, "callable");

      try {
         return Success.of(callable.call());
      } catch (Exception e) {
         return ExceptionFailure.of(e);
      }
   }

   /**
    * Handle exceptions that can be thrown by specified runnable and returns it as a {@link Try} instance.
    * In case of success, {@code true} is returned.
    *
    * @param runnable runnable to execute
    * @param exceptionClass catch only exception that are instance of this one, letting other exception
    *       be thrown as regular exceptions
    *
    * @throws Exception if runnable throw an exception that is not an instance of specified exception
    *       class.
    */
   public static <X extends Exception> Try<Boolean, Exception> handleException(Runnable runnable,
                                                                               Class<? extends X> exceptionClass)
         throws Exception {
      notNull(runnable, "runnable");
      notNull(exceptionClass, "exceptionClass");

      try {
         runnable.run();
         return Success.of(true);
      } catch (Exception e) {
         if (exceptionClass.isAssignableFrom(e.getClass())) {
            return ExceptionFailure.of(e);
         } else {
            throw e;
         }
      }
   }

   /**
    * Handle exceptions that can be thrown by specified runnable and returns it as a {@link Try} instance.
    * In case of success, {@code true} is returned.
    *
    * @param runnable runnable to execute
    */
   public static Try<Boolean, Exception> handleException(Runnable runnable) {
      notNull(runnable, "runnable");

      try {
         runnable.run();
         return Success.of(true);
      } catch (Exception e) {
         return ExceptionFailure.of(e);
      }
   }

   /**
    * Maps this {@link Try} instance using the specified mapper on success.
    *
    * @param mapper success value mapper
    * @param <T> mapped success type
    *
    * @return new try instance with mapped success, or previous failure
    */
   public abstract <T> Try<T, R> map(Function<? super V, ? extends T> mapper);

   /**
    * Maps this {@link Try} instance using the specified mapper on success.
    *
    * @param mapper success value mapper
    * @param <T> mapped success type
    *
    * @return new try instance with mapped success, or previous failure
    */
   public abstract <T> Try<T, R> flatMap(Function<? super V, ? extends Try<? extends T, ? extends R>> mapper);

   /**
    * Returns either {@link Success#value()} or failure reason mapper result.
    *
    * @param mapper failure reason mapper
    *
    * @return either success value or failure reason mapper result
    */
   public abstract V orElseGet(Function<? super R, ? extends V> mapper);

   /**
    * Returns either {@link Success#value()} or supplied value.
    *
    * @param supplier value supplier
    *
    * @return either success value or supplied value
    */
   public abstract V orElseGet(Supplier<? extends V> supplier);

   /**
    * Returns either {@link Success#value()} or specified value.
    *
    * @param value value
    *
    * @return either success value or failure reason mapper result
    */
   public abstract V orElse(V value);

   /**
    * Returns either {@link Success#value()} or throw exception.
    *
    * @return success value, or throw exception
    *
    * @throws IllegalStateException with reason on failure
    */
   public abstract V orElseThrow();

   /**
    * Returns either {@link Success#value()} or throw mapped exception.
    *
    * @return success value, or throw mapped exception
    *
    * @throws X with reason on failure
    */
   public abstract <X extends Throwable> V orElseThrow(Function<? super R, ? extends X> exceptionMapper)
         throws X;

   /**
    * Calls specified callback on success.
    *
    * @param onSuccess on success callback
    *
    * @return this instance
    */
   public abstract Try<V, R> peek(Consumer<? super V> onSuccess);

   /**
    * Calls specified callback on failure.
    *
    * @param onFailure on failure callback
    *
    * @return this instance
    */
   public abstract Try<V, R> peekFailure(Consumer<? super R> onFailure);

   /**
    * Calls specified callback on success.
    *
    * @param onSuccess on success callback
    */
   public abstract void ifSuccess(Consumer<? super V> onSuccess);

   /**
    * Calls specified callback on failure.
    *
    * @param onFailure on failure callback
    */
   public abstract void ifFailure(Consumer<? super R> onFailure);

   /**
    * Calls specified callbacks on success or on failure
    *
    * @param onSuccess on success callback
    * @param onFailure on failure callback
    */
   public abstract void ifSuccessOrElse(Consumer<? super V> onSuccess, Consumer<? super R> onFailure);

   /**
    * Returns supplied result on failure.
    *
    * @param supplier on failure supplier
    *
    * @return new try instance
    */
   public abstract Try<V, R> or(Supplier<? extends Try<? extends V, ? extends R>> supplier);

   /**
    * Returns this object as a {@link Success#value()} {@link Stream} on success, or an empty stream on
    * failure.
    *
    * @return this object as a stream
    */
   public abstract Stream<V> stream();

   /**
    * Returns this object as a {@link Success#value()} {@link Optional} on success, or an empty optional on
    * failure.
    * <p>
    * Note that if successful value is {@code null}, an empty optional will also be returned.
    *
    * @return this object as an optional
    */
   public abstract Optional<V> optional();

   public static class Success<V, R> extends Try<V, R> {
      private final V value;

      private Success(V value) {
         this.value = value;
      }

      public static <V, R> Success<V, R> of(V value) {
         return new Success<>(value);
      }

      public V value() {
         return this.value;
      }

      @Override
      public <T> Success<T, R> map(Function<? super V, ? extends T> mapper) {
         notNull(mapper, "mapper");

         return Success.of(mapper.apply(value));
      }

      @Override
      public <T> Try<T, R> flatMap(Function<? super V, ? extends Try<? extends T, ? extends R>> mapper) {
         notNull(mapper, "mapper");

         @SuppressWarnings("unchecked")
         Try<T, R> apply = (Try<T, R>) mapper.apply(value);

         return requireNonNull(apply);
      }

      @Override
      public V orElseGet(Function<? super R, ? extends V> mapper) {
         notNull(mapper, "mapper");

         return this.value;
      }

      @Override
      public V orElseGet(Supplier<? extends V> supplier) {
         notNull(supplier, "supplier");

         return this.value;
      }

      @Override
      public V orElse(V value) {
         return this.value;
      }

      @Override
      public V orElseThrow() {
         return this.value;
      }

      @Override
      public <X extends Throwable> V orElseThrow(Function<? super R, ? extends X> exceptionMapper) throws X {
         notNull(exceptionMapper, "exceptionMapper");

         return this.value;
      }

      @Override
      public Success<V, R> peek(Consumer<? super V> onSuccess) {
         notNull(onSuccess, "onSuccess");

         onSuccess.accept(value);

         return this;
      }

      @Override
      public Success<V, R> peekFailure(Consumer<? super R> onFailure) {
         notNull(onFailure, "onFailure");

         return this;
      }

      @Override
      public void ifSuccess(Consumer<? super V> onSuccess) {
         notNull(onSuccess, "onSuccess");

         onSuccess.accept(value);
      }

      @Override
      public void ifFailure(Consumer<? super R> onFailure) {
         notNull(onFailure, "onFailure");
      }

      @Override
      public void ifSuccessOrElse(Consumer<? super V> onSuccess, Consumer<? super R> onFailure) {
         notNull(onSuccess, "onSuccess");
         notNull(onFailure, "onFailure");

         onSuccess.accept(value);
      }

      @Override
      public Try<V, R> or(Supplier<? extends Try<? extends V, ? extends R>> supplier) {
         notNull(supplier, "supplier");

         return this;
      }

      public Stream<V> stream() {
         return StreamUtils.stream(value);
      }

      public Optional<V> optional() {
         return NullableUtils.nullable(value);
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Success<?, ?> success = (Success<?, ?>) o;
         return Objects.equals(value, success.value);
      }

      @Override
      public int hashCode() {
         return Objects.hash(value);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Success.class.getSimpleName() + "[", "]")
               .add("value=" + value)
               .toString();
      }
   }

   public static class Failure<V, R> extends Try<V, R> {
      protected final R reason;

      protected Failure(R reason) {
         this.reason = notNull(reason, "reason");
      }

      public static <V, R> Failure<V, R> of(R reason) {
         return new Failure<>(reason);
      }

      public R reason() {
         return this.reason;
      }

      public String reasonString() {
         return this.reason.toString();
      }

      @Override
      public <T> Failure<T, R> map(Function<? super V, ? extends T> mapper) {
         notNull(mapper, "mapper");

         return Failure.of(reason);
      }

      @Override
      public <T> Try<T, R> flatMap(Function<? super V, ? extends Try<? extends T, ? extends R>> mapper) {
         notNull(mapper, "mapper");

         return Failure.of(reason);
      }

      @Override
      public V orElseGet(Function<? super R, ? extends V> mapper) {
         notNull(mapper, "mapper");

         return mapper.apply(reason);
      }

      @Override
      public V orElseGet(Supplier<? extends V> supplier) {
         notNull(supplier, "supplier");

         return supplier.get();
      }

      @Override
      public V orElse(V value) {
         return value;
      }

      @Override
      public V orElseThrow() {
         if (this instanceof ExceptionFailure) {
            throw new IllegalStateException(reasonString(), (Exception) reason);
         } else {
            throw new IllegalStateException(reasonString());
         }
      }

      @Override
      public <X extends Throwable> V orElseThrow(Function<? super R, ? extends X> exceptionMapper) throws X {
         notNull(exceptionMapper, "exceptionMapper");

         throw requireNonNull(exceptionMapper.apply(reason));
      }

      @Override
      public Failure<V, R> peek(Consumer<? super V> onSuccess) {
         notNull(onSuccess, "onSuccess");

         return this;
      }

      @Override
      public Failure<V, R> peekFailure(Consumer<? super R> onFailure) {
         notNull(onFailure, "onFailure");

         onFailure.accept(reason);

         return this;
      }

      @Override
      public void ifSuccess(Consumer<? super V> onSuccess) {
         notNull(onSuccess, "onSuccess");
      }

      @Override
      public void ifFailure(Consumer<? super R> onFailure) {
         notNull(onFailure, "onFailure");

         onFailure.accept(reason);
      }

      @Override
      public void ifSuccessOrElse(Consumer<? super V> onSuccess, Consumer<? super R> onFailure) {
         notNull(onSuccess, "onSuccess");
         notNull(onFailure, "onFailure");

         onFailure.accept(reason);
      }

      @Override
      public Try<V, R> or(Supplier<? extends Try<? extends V, ? extends R>> supplier) {
         notNull(supplier, "supplier");

         @SuppressWarnings("unchecked")
         Try<V, R> supply = (Try<V, R>) supplier.get();

         return requireNonNull(supply);
      }

      public Stream<V> stream() {
         return StreamUtils.stream();
      }

      public Optional<V> optional() {
         return OptionalUtils.optional();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Failure<?, ?> failure = (Failure<?, ?>) o;
         return Objects.equals(reason, failure.reason);
      }

      @Override
      public int hashCode() {
         return Objects.hash(reason);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Failure.class.getSimpleName() + "[", "]")
               .add("reason=" + reason)
               .toString();
      }
   }

   public static class ExceptionFailure<V> extends Failure<V, Exception> {

      protected ExceptionFailure(Exception exception) {
         super(exception);
      }

      public static <V> ExceptionFailure<V> of(Exception exception) {
         return new ExceptionFailure<>(exception);
      }

      @Override
      public String reasonString() {
         return super.reason().getMessage();
      }
   }

}
