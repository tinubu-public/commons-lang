/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.ExceptionUtils.smartFinalize;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;

public final class CloseableUtils {

   private CloseableUtils() {
   }

   /**
    * Helper to close a closeable after function application, as in a try-with-resources. This is needed if
    * resources should be freed (IO usage, ...).
    *
    * @param object object to close
    * @param function function to apply to object before closing
    * @param <T> object type
    * @param <U> function return type
    *
    * @return function result
    *
    * @throws Exception if function evaluation throws an exception or if {@link AutoCloseable#close}
    *       throws an exception. If both function evaluation and {@link AutoCloseable#close} throw
    *       exceptions, function evaluation exception is returned with {@link AutoCloseable#close} exception
    *       {@link Throwable#addSuppressed(Throwable) suppressed}
    * @see #applyCloseable(AutoCloseable, CheckedFunction)
    */
   public static <T extends AutoCloseable, U> U finalApplyCloseable(T object,
                                                                    CheckedFunction<? super T, ? extends U> function)
         throws Exception {
      return smartFinalize(() -> function.applyChecked(object), object::close);
   }

   /**
    * Helper to close a closeable after function application, as in a try-with-resources. This is needed if
    * resources should be freed (IO usage, ...).
    * <p>
    * Throws {@link RuntimeException} if function evaluation throws an exception or if
    * {@link AutoCloseable#close} throws an exception. If both function evaluation and
    * {@link AutoCloseable#close} throw exceptions, function evaluation exception is returned with
    * {@link AutoCloseable#close} exception {@link Throwable#addSuppressed(Throwable) suppressed}
    *
    * @param object object to close
    * @param function function to apply to object before closing
    * @param <T> object type
    * @param <U> function return type
    *
    * @return function result
    *
    * @see #applyCloseableUnchecked(AutoCloseable, CheckedFunction)
    */
   public static <T extends AutoCloseable, U> U finalApplyCloseableUnchecked(T object,
                                                                             CheckedFunction<? super T, ? extends U> function) {
      try {
         return smartFinalize(() -> function.apply(object), object::close);
      } catch (Throwable t) {
         throw sneakyThrow(t);
      }
   }

   /**
    * Helper to close a closeable after consumer application, as in a try-with-resources. This is needed if
    * resources should be freed (IO usage, ...).
    *
    * @param object object to close
    * @param consumer consumer to apply to object before closing
    * @param <T> object type
    *
    * @throws Exception if consumer evaluation throws an exception or if {@link AutoCloseable#close}
    *       throws an exception. If both consumer evaluation and {@link AutoCloseable#close} throw
    *       exceptions, consumer evaluation exception is returned with {@link AutoCloseable#close} exception
    *       {@link Throwable#addSuppressed(Throwable) suppressed}
    * @see #acceptCloseable(AutoCloseable, CheckedConsumer)
    */
   public static <T extends AutoCloseable> void finalAcceptCloseable(T object,
                                                                     CheckedConsumer<? super T> consumer)
         throws Exception {
      smartFinalize(() -> consumer.acceptChecked(object), object::close);
   }

   /**
    * Helper to close a closeable after consumer application, as in a try-with-resources. This is needed if
    * resources should be freed (IO usage, ...).
    * <p>
    * Throws {@link RuntimeException} if consumer evaluation throws an exception or if
    * {@link AutoCloseable#close} throws an exception. If both consumer evaluation and
    * {@link AutoCloseable#close} throw exceptions, consumer evaluation exception is returned with
    * {@link AutoCloseable#close} exception {@link Throwable#addSuppressed(Throwable) suppressed}
    *
    * @param object object to close
    * @param consumer consumer to apply to object before closing
    * @param <T> object type
    *
    * @see #acceptCloseableUnchecked(AutoCloseable, CheckedConsumer)
    */
   public static <T extends AutoCloseable> void finalAcceptCloseableUnchecked(T object,
                                                                              CheckedConsumer<? super T> consumer) {
      try {
         smartFinalize(() -> consumer.accept(object), object::close);
      } catch (Throwable t) {
         throw sneakyThrow(t);
      }
   }

   /**
    * Helper to close a closeable only in case of exception in function application. This is needed to
    * ensure resources are freed (IO usage, ...) in case of error.
    *
    * @param object object to apply function on
    * @param function function to apply to object
    * @param <T> object type
    * @param <U> function return type
    *
    * @return function result
    *
    * @throws Exception if function evaluation throws an exception or if {@link AutoCloseable#close}
    *       throws an exception. If both function evaluation and {@link AutoCloseable#close} throw
    *       exceptions, function evaluation exception is returned with {@link AutoCloseable#close} exception
    *       {@link Throwable#addSuppressed(Throwable) suppressed}
    * @see #finalApplyCloseable(AutoCloseable, CheckedFunction)
    */
   public static <T extends AutoCloseable, U> U applyCloseable(T object,
                                                               CheckedFunction<? super T, ? extends U> function)
         throws Exception {
      try {
         return function.applyChecked(object);
      } catch (Throwable t) {
         return finalApplyCloseable(object, __ -> {
            throw t;
         });
      }
   }

   /**
    * Helper to close a closeable only in case of exception in function application. This is needed to
    * ensure resources are freed (IO usage, ...) in case of error.
    *
    * @param object object to apply function on
    * @param function function to apply to object
    * @param <T> object type
    * @param <U> function return type
    *
    * @return function result
    *
    * @throws RuntimeException if function evaluation throws an exception or if
    *       {@link AutoCloseable#close} throws an exception. If both function evaluation and
    *       {@link AutoCloseable#close} throw exceptions, function evaluation exception is returned with
    *       {@link AutoCloseable#close} exception {@link Throwable#addSuppressed(Throwable) suppressed}
    * @see #finalApplyCloseableUnchecked(AutoCloseable, CheckedFunction)
    */
   public static <T extends AutoCloseable, U> U applyCloseableUnchecked(T object,
                                                                        CheckedFunction<? super T, ? extends U> function) {
      try {
         return function.applyChecked(object);
      } catch (Throwable t) {
         return finalApplyCloseableUnchecked(object, __ -> {
            throw t;
         });
      }
   }

   /**
    * Helper to close a closeable only in case of exception in consumer application. This is needed to
    * ensure resources are freed (IO usage, ...) in case of error.
    *
    * @param object object to apply function on
    * @param consumer consumer to apply to object
    * @param <T> object type
    *
    * @throws Exception if consumer evaluation throws an exception or if {@link AutoCloseable#close}
    *       throws an exception. If both consumer evaluation and {@link AutoCloseable#close} throw
    *       exceptions, consumer evaluation exception is returned with {@link AutoCloseable#close} exception
    *       {@link Throwable#addSuppressed(Throwable) suppressed}
    * @see #finalAcceptCloseable(AutoCloseable, CheckedConsumer)
    */
   public static <T extends AutoCloseable> void acceptCloseable(T object, CheckedConsumer<? super T> consumer)
         throws Exception {
      try {
         consumer.acceptChecked(object);
      } catch (Throwable t) {
         finalApplyCloseable(object, __ -> {
            throw t;
         });
      }
   }

   /**
    * Helper to close a closeable only in case of exception in consumer application. This is needed to
    * ensure resources are freed (IO usage, ...) in case of error.
    *
    * @param object object to apply function on
    * @param consumer consumer to apply to object
    * @param <T> object type
    *
    * @throws RuntimeException if consumer evaluation throws an exception or if
    *       {@link AutoCloseable#close} throws an exception. If both consumer evaluation and
    *       {@link AutoCloseable#close} throw exceptions, consumer evaluation exception is returned with
    *       {@link AutoCloseable#close} exception {@link Throwable#addSuppressed(Throwable) suppressed}
    * @see #finalAcceptCloseableUnchecked(AutoCloseable, CheckedConsumer)
    */
   public static <T extends AutoCloseable> void acceptCloseableUnchecked(T object,
                                                                         CheckedConsumer<? super T> consumer) {
      try {
         consumer.acceptChecked(object);
      } catch (Throwable t) {
         finalApplyCloseableUnchecked(object, __ -> {
            throw t;
         });
      }
   }

}
