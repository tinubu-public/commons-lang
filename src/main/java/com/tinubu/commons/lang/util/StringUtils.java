/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

public final class StringUtils {

   private StringUtils() {
   }

   /**
    * Returns the specified char sequence, or the default one if {@link #isBlank(CharSequence)}>.
    *
    * @param <T> char sequence type
    * @param cs char sequence to check, or {@code null}
    * @param defaultCs default char sequence
    *
    * @return char sequence
    */
   public static <T extends CharSequence> T defaultIfBlank(final T cs, final T defaultCs) {
      return isBlank(cs) ? defaultCs : cs;
   }

   /**
    * Returns the specified char sequence, or the default one if {@link #isEmpty(CharSequence)}>.
    *
    * @param <T> char sequence type
    * @param cs char sequence to check, or {@code null}
    * @param defaultCs default char sequence
    *
    * @return char sequence
    */
   public static <T extends CharSequence> T defaultIfEmpty(final T cs, final T defaultCs) {
      return isEmpty(cs) ? defaultCs : cs;
   }

   /**
    * Checks if a char sequence is empty ({@code ""}) or {@code null}.
    *
    * @param cs char sequence to check, or {@code null}
    *
    * @return {@code true} if char sequence is empty
    */
   public static boolean isEmpty(CharSequence cs) {
      return cs == null || cs.length() == 0;
   }

   /**
    * Checks if a char sequence is blank or {@code null}.
    * Character is considered blank if {@link Character#isWhitespace(char)} matches.
    *
    * @param cs char sequence to check, or {@code null}
    *
    * @return {@code true} if char sequence is blank
    */
   public static boolean isBlank(CharSequence cs) {
      int strLen = cs == null ? 0 : cs.length();

      if (strLen == 0) {
         return true;
      }
      for (int i = 0; i < strLen; i++) {
         if (!Character.isWhitespace(cs.charAt(i))) {
            return false;
         }
      }
      return true;
   }

   public static String removeEnd(String string, String suffix) {
      if (isEmpty(string) || isEmpty(suffix)) {
         return string;
      }
      if (string.endsWith(suffix)) {
         return string.substring(0, string.length() - suffix.length());
      }
      return string;
   }

   public static String removeEndIgnoreCase(String string, String suffix) {
      if (isEmpty(string) || isEmpty(suffix)) {
         return string;
      }
      if (string.toLowerCase().endsWith(suffix.toLowerCase())) {
         return string.substring(0, string.length() - suffix.length());
      }
      return string;
   }

}
