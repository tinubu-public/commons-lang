/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.util.Optional;

/**
 * Provides optimal values for various usages, based on current hardware characteristics.
 * Values can be optimal values based on statistical studies, or based on real hardware.
 * All values can be overridden using configurable system properties.
 */
public class MechanicalSympathy {

   private static final String PROPERTY_PREFIX = "mechanical-sympathy";

   private static final int GENERAL_BUFFER_SIZE = intValue("general-buffer-size", 8192);
   private static final int OPTIMAL_CONCURRENCY =
         intValue("optimal-concurrency", Runtime.getRuntime().availableProcessors());

   /**
    * Optimal buffer size based on statistical analysis.
    * <p>
    * Value can be tuned using {@code mechanical-sympathy.general-buffer-size} property.
    *
    * @return optimal buffer size in bytes
    */
   public static int generalBufferSize() {
      return GENERAL_BUFFER_SIZE;
   }

   /**
    * Number of available physical processors (cores, hyperthreads).
    *
    * @return number of available physical processors.
    */
   public static int optimalConcurrency() {
      return OPTIMAL_CONCURRENCY;
   }

   private static int intValue(String type, int defaultValue) {
      return propertyValue(type).map(Integer::parseInt).orElse(defaultValue);
   }

   private static Optional<String> propertyValue(String type, String defaultValue) {
      return nullable(System.getProperty(PROPERTY_PREFIX + "." + type, defaultValue));
   }

   private static Optional<String> propertyValue(String type) {
      return nullable(System.getProperty(PROPERTY_PREFIX + "." + type));
   }

}
