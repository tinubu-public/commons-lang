/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.function.Function.identity;
import static java.util.stream.Stream.concat;
import static java.util.stream.Stream.empty;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class StreamUtils {

   private StreamUtils() {
   }

   /**
    * Returns an empty stream.
    *
    * @param <T> stream type
    *
    * @return empty stream
    */
   public static <T> Stream<T> stream() {
      return empty();
   }

   /**
    * Returns a stream, always initialized, of specified optional.
    * Can be used as Java &lt; 9 {@link Optional} value to {@link Stream} converter.
    *
    * @param optional optional
    * @param <T> value type
    *
    * @return value as stream or {@link Stream#empty} if value is {@code null} or not present
    */
   @SuppressWarnings({ "OptionalUsedAsFieldOrParameterType", "OptionalAssignedToNull", "unchecked" })
   public static <T> Stream<T> stream(Optional<? extends T> optional) {
      if (optional == null) {
         return empty();
      } else {
         return ((Optional<T>) optional).map(Stream::of).orElseGet(Stream::empty);
      }
   }

   /**
    * Returns a stream, always initialized, of specified stream.
    *
    * @param stream stream
    * @param <T> stream type
    *
    * @return specified stream or {@link Stream#empty}
    *
    * @implSpec Implementation must not recreate the collection if not necessary.
    */
   public static <T> Stream<T> stream(Stream<? extends T> stream) {
      return nullSafeStream(stream);
   }

   /**
    * Returns a stream, always initialized, of specified collection.
    *
    * @param collection collection
    * @param <T> stream type
    *
    * @return new collection stream or {@link Stream#empty}
    */
   @SuppressWarnings("unchecked")
   public static <T> Stream<T> stream(Collection<? extends T> collection) {
      return (Stream<T>) nullable(collection).map(Collection::stream).orElse(empty());
   }

   /**
    * Returns a stream, always initialized, of specified {@link Enumeration}.
    *
    * @param enumeration enumeration
    * @param <T> stream type
    *
    * @return new enumeration stream or {@link Stream#empty}
    */
   public static <T> Stream<T> stream(Enumeration<? extends T> enumeration) {
      if (enumeration == null) {
         return empty();
      }
      return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new Iterator<T>() {
         @Override
         public T next() {
            return enumeration.nextElement();
         }

         @Override
         public boolean hasNext() {
            return enumeration.hasMoreElements();
         }
      }, Spliterator.ORDERED), false);
   }

   /**
    * Returns a stream, always initialized, of specified {@link Iterable}.
    *
    * @param iterable iterable
    * @param <T> stream type
    *
    * @return new enumeration stream or {@link Stream#empty}
    */
   @SuppressWarnings("unchecked")
   public static <T> Stream<T> stream(Iterable<? extends T> iterable) {
      if (iterable == null) {
         return empty();
      }
      return (Stream<T>) StreamSupport.stream(iterable.spliterator(), false);
   }

   /**
    * Returns a stream, always initialized, of specified elements.
    *
    * @param elements elements to stream, {@code null} elements will be included in
    *       resulting stream
    * @param <T> element type
    *
    * @return new stream of elements
    */
   @SafeVarargs
   public static <T> Stream<T> stream(T... elements) {
      return nullSafeStream(elements);
   }

   /**
    * Returns a stream, always initialized, of flattened specified streams.
    * {@code null} streams are ignored.
    *
    * @param streams streams to flatten in a single stream
    * @param <T> stream type
    *
    * @return new stream of all stream elements
    *
    * @apiNote If one specified stream is parallelized, resulting stream elements order is undefined
    * @implNote {@link Stream#concat(Stream, Stream)} is more optimized for 2 streams, as it respect
    *       original streams parallelization. {@link Stream#flatMap(Function)} does not have
    *       {@link StackOverflowError} issue with many streams to concatenate, and is more performant for
    *       small streams (~ 32 elements).
    * @implSpec Implementation must not recreate the stream if not necessary.
    * @see <a
    *       href="https://www.techempower.com/blog/2016/10/19/efficient-multiple-stream-concatenation-in-java/">Efficient
    *       multiple stream concatenation in java</a>
    * @see <a href="https://bugs.openjdk.org/browse/JDK-8025523">StackOverflowError in Streams</a>
    */
   @SuppressWarnings("unchecked")
   @SafeVarargs
   public static <T> Stream<T> streamConcat(Stream<? extends T>... streams) {
      Stream<T> elementsStream;

      if (streams != null && streams.length == 1 && streams[0] != null) {
         elementsStream = (Stream<T>) streams[0];
      } else if (streams != null && streams.length == 2 && streams[0] != null && streams[1] != null) {
         elementsStream = concat(streams[0], streams[1]);
      } else {
         elementsStream = streamConcat(nullSafeStream(streams));
      }

      return elementsStream;
   }

   /**
    * Returns a stream, always initialized, of flattened specified streams.
    * {@code null} streams are ignored.
    *
    * @param streams streams to flatten in a single stream
    * @param <T> stream type
    *
    * @return new stream of all stream elements
    *
    * @apiNote If one specified stream is parallelized, resulting stream elements order is undefined
    * @implSpec Implementation must not recreate the stream if not necessary.
    */
   public static <T> Stream<T> streamConcat(Stream<? extends Stream<? extends T>> streams) {
      return nullSafeStream(streams).filter(Objects::nonNull).flatMap(identity());
   }

   /**
    * Returns a stream, always initialized, of flattened specified collections.
    * {@code null} collections are ignored.
    *
    * @param collections collections to flatten in a single stream
    * @param <T> collection type
    *
    * @return new stream of all collections elements
    *
    * @apiNote If one specified stream is parallelized, resulting stream elements order is undefined
    * @implSpec Implementation must not recreate the stream if not necessary.
    */
   public static <T> Stream<T> streamConcatOfCollection(Stream<? extends Collection<? extends T>> collections) {
      return nullSafeStream(collections).filter(Objects::nonNull).flatMap(Collection::stream);
   }

   /**
    * Returns a stream, always initialized, of flattened specified collections.
    * {@code null} collections are ignored.
    *
    * @param collections collections to flatten in a single stream
    * @param <T> collection type
    *
    * @return stream of all collections elements
    */
   @SafeVarargs
   public static <T> Stream<T> streamConcat(Collection<? extends T>... collections) {
      return streamConcatOfCollection(nullSafeStream(collections));

   }

   /**
    * Returns a stream, always initialized, of flattened specified collections.
    * {@code null} collections are ignored.
    *
    * @param collections collections to flatten in a single stream
    * @param <T> collection type
    *
    * @return stream of all collections elements
    */
   public static <T> Stream<T> streamConcat(Collection<? extends Collection<? extends T>> collections) {
      return streamConcatOfCollection(stream(collections));
   }

   /**
    * Returns an entry stream, always initialized, of flattened specified maps.
    * {@code null} maps are ignored.
    * There is no merging operation executed, so multiple entries with the same key can be present in result
    * stream.
    *
    * @param maps maps to flatten in a single entry stream
    * @param <K> map key type
    * @param <V> map value type
    *
    * @return stream of all maps entries
    */
   @SafeVarargs
   @SuppressWarnings("unchecked")
   public static <K, V> Stream<Entry<K, V>> streamConcat(Map<? extends K, ? extends V>... maps) {
      return nullSafeStream(maps)
            .filter(Objects::nonNull)
            .map(Map::entrySet)
            .flatMap(Collection::stream)
            .map(e -> (Entry<K, V>) e);
   }

   /**
    * Returns an entry stream, always initialized, of specified entry stream.
    *
    * @param entries entries to stream. {@code null} entries are ignored
    * @param <K> entry key type
    * @param <V> entry value type
    *
    * @return stream of entries
    */
   @SuppressWarnings("unchecked")
   public static <K, V> Stream<Entry<K, V>> entryStream(Stream<? extends Entry<? extends K, ? extends V>> entries) {
      return stream(entries).filter(Objects::nonNull).map(e -> (Entry<K, V>) e);
   }

   /**
    * Returns an entry stream, always initialized, of specified entry map.
    *
    * @param map map to stream. {@code null} entries are ignored
    * @param <K> entry key type
    * @param <V> entry value type
    *
    * @return stream of map entries
    */
   @SuppressWarnings("unchecked")
   public static <K, V> Stream<Entry<K, V>> entryStream(Map<? extends K, ? extends V> map) {
      return entryStream(nullable(map)
                               .map(Map::entrySet)
                               .map(Collection::stream)
                               .orElse(empty())
                               .map(e -> (Entry<K, V>) e));
   }

   /**
    * Returns an entry stream, always initialized, of specified entries.
    *
    * @param entries entries to stream, {@code null} entries will be ignored
    * @param <K> entry key type
    * @param <V> entry value type
    *
    * @return stream of entries
    */
   @SafeVarargs
   public static <K, V> Stream<Entry<K, V>> entryStream(Entry<? extends K, ? extends V>... entries) {
      return entryStream(nullSafeStream(entries));
   }

   /**
    * Returns an always initialized stream of specified list in reverse order.
    *
    * @param list list to stream in reverse order
    * @param <T> stream type
    *
    * @return reversed list stream
    */
   @SuppressWarnings("unchecked")
   public static <T> Stream<T> reversedStream(List<? extends T> list) {
      return nullable(list).map(l -> {
         final int size = list.size();
         return (Stream<T>) IntStream.range(0, size).mapToObj(i -> list.get(size - 1 - i));
      }).orElse(stream());
   }

   /**
    * Creates a new stream, with element updated, if matched with {@code matcher}, or
    * with {@code element} added in last position. If element is updated, its order is preserved.
    * First matcher parameter is current stream element, or {@code null} if the element is {@code null},
    * and the second one is the new element.
    * The mapper will be applied both to updated matching element, or the added element. In this last case,
    * the first parameter will be {@code null}.
    * First mapper parameter is existing value, or {@code null} if the element is {@code null}, and the
    * second one is the new element.
    * If multiple elements matches the matcher, they'll be all updated and mapped.
    * If element is finally added to stream, it will be lazily mapped.
    *
    * @param stream stream to operate on
    * @param element element to add
    * @param matcher function matching the element to update
    * @param mapper updated element mapping function. First parameter is existing element or
    *       {@code null}. Second parameter is new element.
    * @param <T> element type
    *
    * @return new stream with updated or added element
    *
    * @implNote streams are lazily loaded, so the updated flag is read using a stream filter that'll be
    *       executed after the flag is updated.
    */
   public static <T> Stream<T> streamUpdateOrAdd(Stream<? extends T> stream,
                                                 T element,
                                                 BiPredicate<T, T> matcher,
                                                 BinaryOperator<T> mapper) {
      notNull(stream, "stream");
      notNull(matcher, "matcher");
      notNull(mapper, "mapper");

      AtomicBoolean updated = new AtomicBoolean(false);
      Stream<T> updatedStream = stream.map(e -> {
         if (matcher.test(e, element)) {
            updated.set(true);
            return mapper.apply(e, element);
         } else {
            return e;
         }
      });

      return streamConcat(updatedStream,
                          stream((T) null).filter(__ -> !updated.get()).map(e -> mapper.apply(e, element)));
   }

   /**
    * Creates a new stream, with element replaced, if matched with {@code matcher}, or
    * with {@code element} added in last position. If element is replaced, its order is preserved.
    * First matcher parameter is current stream element, or {@code null} if the element is {@code null},
    * and the second one is the new element.
    * If multiple elements matches the matcher, they'll be all replaced.
    *
    * @param stream stream to operate on
    * @param element element to add
    * @param matcher function matching the element to update
    * @param <T> element type
    *
    * @return new stream with replaced or added element
    */
   public static <T> Stream<T> streamUpdateOrAdd(Stream<? extends T> stream,
                                                 T element,
                                                 BiPredicate<T, T> matcher) {
      return streamUpdateOrAdd(stream, element, matcher, (pv, nv) -> nv);
   }

   /**
    * Creates a new stream, with element replaced, if they are {@link Object#equals(Object)}, or
    * with {@code element} added in last position. If element is replaced, its order is preserved.
    * If multiple elements matches, they'll be all replaced.
    *
    * @param stream stream to operate on
    * @param element element to add
    * @param <T> element type
    *
    * @return new stream with replaced or added element
    */
   public static <T> Stream<T> streamUpdateOrAdd(Stream<? extends T> stream, T element) {
      return streamUpdateOrAdd(stream, element, Objects::equals, (pv, nv) -> nv);
   }

   /**
    * Creates a new stream, with element mapped, if matched with {@code matcher}, or
    * with mapper result added in last position. If element is updated, its order is preserved.
    * Matcher parameter is current stream element, or {@code null} if the element is {@code null}.
    * The mapper will be applied both to updated matching element, or the added element. In this last case,
    * the mapper parameter will be {@code null}.
    * Mapper parameter is existing value, or {@code null} if the element is {@code null}.
    * If multiple elements matches the matcher, they'll be all mapped.
    *
    * @param stream stream to operate on
    * @param matcher function matching the element to update
    * @param mapper updated element mapping function. Parameter is existing element or
    *       {@code null}
    * @param <T> element type
    *
    * @return new stream with mapped elements
    *
    * @implNote streams are lazily loaded, so the updated flag is read using a stream filter that'll be
    *       executed after the flag is updated.
    */
   public static <T> Stream<T> streamUpdateOrAdd(Stream<? extends T> stream,
                                                 Predicate<T> matcher,
                                                 UnaryOperator<T> mapper) {
      notNull(stream, "stream");
      notNull(matcher, "matcher");
      notNull(mapper, "mapper");

      AtomicBoolean updated = new AtomicBoolean(false);
      Stream<T> updatedStream = stream.map(e -> {
         if (matcher.test(e)) {
            updated.set(true);
            return mapper.apply(e);
         } else {
            return e;
         }
      });

      return streamConcat(updatedStream, stream((T) null).filter(__ -> !updated.get()).map(mapper));
   }

   /**
    * Returns first stream element.
    * Specified stream element must have at most one element, otherwise an exception is throw.
    *
    * @param stream stream
    * @param <T> stream type
    *
    * @return stream first element, or {@link Optional#empty} if stream is empty
    *
    * @throws IllegalArgumentException if stream has more than one element available
    * @implSpec specified stream is not closed by this function, it remains the responsibility of the
    *       caller
    */
   public static <T> Optional<T> findOne(Stream<? extends T> stream) {
      return findOneOrElse(stream, () -> {
         throw new IllegalArgumentException("'stream' must have at most one element");
      });
   }

   /**
    * Returns first stream element.
    * Specified stream element must have at most one element, otherwise the fallback value is evaluated and
    * returned.
    *
    * @param stream stream
    * @param fallback fallback provider if stream has more than one element. If fallback is evaluated to
    *       {@code null}, an empty optional is returned
    * @param <T> stream type
    *
    * @return stream first element, or {@link Optional#empty} if stream is empty, or fallback value
    *
    * @implSpec specified stream is not closed by this function, it remains the responsibility of the
    *       caller
    */
   public static <T> Optional<T> findOneOrElse(Stream<? extends T> stream, Supplier<Optional<T>> fallback) {
      notNull(stream, "stream");
      notNull(fallback, "fallback");

      Iterator<? extends T> iterator = stream.iterator();

      Optional<T> ret = optional(iterator).filter(Iterator::hasNext).map(Iterator::next);

      if (iterator.hasNext()) {
         return nullable(fallback.get()).orElse(optional());
      }

      return ret;
   }

   /**
    * Returns first stream element.
    * Specified stream element must have at most one element, otherwise {@link Optional#empty} is returned.
    *
    * @param stream stream
    * @param <T> stream type
    *
    * @return stream first element, or {@link Optional#empty}
    */
   public static <T> Optional<T> findOneOrElseEmpty(Stream<? extends T> stream) {
      return findOneOrElse(stream, OptionalUtils::optional);
   }

   /**
    * Predefined filter that always matches.
    *
    * @param <T> filter type
    *
    * @return predicate
    */
   public static <T> Predicate<? super T> alwaysTrue() {
      return __ -> true;
   }

   /**
    * Predefined filter that never matches.
    *
    * @param <T> filter type
    *
    * @return predicate
    */
   public static <T> Predicate<? super T> alwaysFalse() {
      return __ -> false;
   }

   /**
    * Predefined element filter to filter non-{@code null} elements.
    *
    * @param <T> filter type
    *
    * @return predicate
    */
   public static <T> Predicate<? super T> nonNull() {
      return Objects::nonNull;
   }

   /**
    * (key, value) to entry predicate adapter.
    *
    * @param entryPredicate entry bi-predicate
    * @param <K> entry key type
    * @param <V> entry value type
    *
    * @return entry predicate
    */
   public static <K, V> Predicate<Entry<K, V>> entryFilter(BiPredicate<? super K, ? super V> entryPredicate) {
      return entry -> entryPredicate.test(entry.getKey(), entry.getValue());
   }

   /**
    * Predefined entry filter to filter map entries with non-{@code null} keys.
    *
    * @param <K> entry key type
    * @param <V> entry value type
    *
    * @return entry bi-predicate
    */
   public static <K, V> BiPredicate<? super K, ? super V> nonNullEntryKey() {
      return (k, v) -> Objects.nonNull(k);
   }

   /**
    * Predefined entry filter to filter map entries with non-{@code null} values.
    *
    * @param <K> entry key type
    * @param <V> entry value type
    *
    * @return entry bi-predicate
    */
   public static <K, V> BiPredicate<? super K, ? super V> nonNullEntryValue() {
      return (k, v) -> Objects.nonNull(v);
   }

   /**
    * Predefined entry filter to filter map entries with non-{@code null} keys and non-{@code null} values.
    *
    * @param <K> entry key type
    * @param <V> entry value type
    *
    * @return entry bi-predicate
    */
   public static <K, V> BiPredicate<? super K, ? super V> nonNullEntry() {
      return nonNullEntryValue().and(nonNullEntryKey());
   }

   /**
    * Generates a {@link Reader} from specified character stream. Reader content size will depend on input
    * stream size, and can be infinite. Specified stream can't be consumed after this call.
    *
    * @param charStream input character stream, possibly infinite
    *
    * @return new reader from character stream
    */
   public static Reader reader(Stream<Character> charStream) {
      notNull(charStream, "charStream");

      return new Reader() {
         private boolean closed = false;
         private final Iterator<Character> cIt = charStream.iterator();

         @Override
         public int read(char[] cbuf, int off, int len) throws IOException {
            synchronized (lock) {
               notNull(cbuf, "cbuf");
               //Objects.checkFromIndexSize(off, len, cbuf.length);
               if (off < 0 || len < 0 || len > cbuf.length + off) {
                  throw new IndexOutOfBoundsException();
               }

               ensureOpen();

               if (!cIt.hasNext()) {
                  return -1;
               }

               int i = 0;
               while (i < len) {
                  if (!cIt.hasNext()) {
                     return i;
                  }
                  cbuf[off + i] = cIt.next();
                  i++;
               }
               return i;
            }
         }

         private void ensureOpen() throws IOException {
            if (closed) {
               throw new IOException("Reader is closed");
            }
         }

         @Override
         public void close() {
            synchronized (lock) {
               closed = true;
            }
         }
      };
   }

   /**
    * Generates a {@link InputStream} from specified byte stream. Input stream content size will depend on
    * input stream size, and can be infinite. Specified stream can't be consumed after this call.
    *
    * @param inputStream input byte stream, possibly infinite
    *
    * @return new input stream from byte stream
    */
   public static InputStream inputStream(Stream<Byte> inputStream) {
      notNull(inputStream, "inputStream");

      return new InputStream() {
         private Object lock = this;
         private boolean closed = false;
         private final Iterator<Byte> cIt = inputStream.iterator();

         @Override
         public int read() throws IOException {
            synchronized (lock) {
               ensureOpen();
               if (!cIt.hasNext()) {
                  return -1;
               } else {
                  return cIt.next();
               }
            }
         }

         private void ensureOpen() throws IOException {
            if (closed) {
               throw new IOException("Stream closed");
            }
         }

         @Override
         public void close() {
            synchronized (lock) {
               this.closed = true;
            }
         }
      };
   }

   private static <T> Stream<T> nullSafeStream(T[] elements) {
      return nullable(elements).map(Stream::of).orElse(empty());
   }

   @SuppressWarnings("unchecked")
   private static <T> Stream<T> nullSafeStream(Stream<? extends T> stream) {
      return (Stream<T>) nullable(stream).orElse(empty());
   }

}
