/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static java.util.Objects.requireNonNull;

import java.util.function.Function;

/**
 * Represents a function that accepts one argument and produces a result.
 * This operation can throw checked exceptions.
 * <p>
 * This is a functional interface whose functional method is {@link #applyChecked(Object)}.
 *
 * @param <T> the type of the input to the function
 * @param <R> the type of the result of the function
 */
@FunctionalInterface
public interface CheckedFunction<T, R> extends Function<T, R> {
   /**
    * Applies this function to the given argument.
    *
    * @param t the function argument
    *
    * @return the function result
    */
   @Override
   default R apply(T t) {
      return uncheckedFunction(this).apply(t);
   }

   /**
    * Applies this function to the given argument.
    * This operation can throw checked exceptions.
    *
    * @param t the function argument
    *
    * @return the function result
    */
   R applyChecked(T t) throws Exception;

   /**
    * Returns a function that always returns its input argument.
    *
    * @param <T> the type of the input and output objects to the function
    *
    * @return a function that always returns its input argument
    */
   static <T> CheckedFunction<T, T> identity() {
      return t -> t;
   }

   /**
    * Returns a checked function from a standard function.
    *
    * @param function function to adapt
    *
    * @return checked function
    */
   static <T, R> CheckedFunction<T, R> checkedFunction(Function<T, R> function) {
      requireNonNull(function);
      if (function instanceof CheckedFunction) {
         return (CheckedFunction<T, R>) function;
      } else {
         return function::apply;
      }
   }

   /**
    * Returns an unchecked function from a checked function.
    *
    * @param function function to adapt
    *
    * @return unchecked function
    */
   static <T, R> Function<T, R> uncheckedFunction(CheckedFunction<T, R> function) {
      requireNonNull(function);
      return function.unchecked();
   }

   /**
    * Returns an unchecked function from this checked function.
    *
    * @return unchecked function
    */
   default Function<T, R> unchecked() {
      return t -> {
         try {
            return this.applyChecked(t);
         } catch (Exception e) {
            throw sneakyThrow(e);
         }
      };
   }

   @Override
   default <V> CheckedFunction<V, R> compose(Function<? super V, ? extends T> before) {
      requireNonNull(before);
      return (V v) -> applyChecked(before.apply(v));
   }

   @Override
   default <V> CheckedFunction<T, V> andThen(Function<? super R, ? extends V> after) {
      requireNonNull(after);
      return (T t) -> after.apply(applyChecked(t));
   }

}
