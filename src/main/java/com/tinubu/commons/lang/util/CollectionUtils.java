/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.entryStream;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcatOfCollection;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Stream;

import com.tinubu.commons.lang.util.typefactory.DefaultTypeFactoryRegistry;
import com.tinubu.commons.lang.util.typefactory.TypeFactory;
import com.tinubu.commons.lang.util.typefactory.TypeFactoryRegistry;

/**
 * Null-safe collection utils.
 */
public final class CollectionUtils {

   /** java.util known immutable list classes names. */
   private static final HashSet<String> IMMUTABLE_LIST_CLASSES = new HashSet<>() {{
      add("java.util.Collections$UnmodifiableList");
      add("java.util.Collections$UnmodifiableRandomAccessList");
      add("java.util.Collections$SingletonList");
      add("java.util.Collections$EmptyList");
      add("java.util.Arrays$ArrayList");
      add("java.util.ImmutableCollections$List12");
      add("java.util.ImmutableCollections$ListN");
      add("java.util.ImmutableCollections$SubList");
   }};

   /** java.util known immutable collection classes names. */
   private static final HashSet<String> IMMUTABLE_COLLECTION_CLASSES = new HashSet<>() {{
      add("java.util.Collections$UnmodifiableCollection");
      add("java.util.Collections$UnmodifiableNavigableSet");
      add("java.util.Collections$UnmodifiableNavigableSet$EmptyNavigableSet");
      add("java.util.Collections$UnmodifiableSet");
      add("java.util.Collections$UnmodifiableSortedSet");
      add("java.util.Collections$SingletonSet");
      add("java.util.Collections$EmptySet");
      add("java.util.ImmutableCollections$Set12");
      add("java.util.ImmutableCollections$SetN");
      addAll(IMMUTABLE_LIST_CLASSES);
   }};

   /** java.util known immutable map classes names. */
   private static final HashSet<String> IMMUTABLE_MAP_CLASSES = new HashSet<>() {{
      add("java.util.Collections$UnmodifiableMap");
      add("java.util.Collections$UnmodifiableNavigableMap");
      add("java.util.Collections$UnmodifiableNavigableMap$EmptyNavigableMap");
      add("java.util.Collections$UnmodifiableSortedMap");
      add("java.util.Collections$SingletonMap");
      add("java.util.Collections$EmptyMap");
      add("java.util.ImmutableCollections$Map1");
      add("java.util.ImmutableCollections$MapN");
   }};

   private CollectionUtils() {
   }

   /**
    * Returns a new mutable, always initialized, collection of specified stream.
    *
    * @param <T> collection items type
    * @param <C> collection factory type
    * @param collectionFactory collection factory to use to generate returned collection
    * @param stream stream or {@code null}
    *
    * @return new mutable collection of specified stream, or new mutable empty collection
    */
   public static <T, C extends Collection<T>> C collection(TypeFactory<C> collectionFactory,
                                                           Stream<? extends T> stream) {
      notNull(collectionFactory, "collectionFactory");

      return stream(stream).collect(toCollection(collectionFactory));
   }

   /**
    * Returns a new mutable, always initialized, collection of concatenated streams.
    * If no stream is provided, concatenation will contain no element.
    * If only one stream is provided, concatenation will contain only its elements.
    * <p>
    * Resulting collection is not de-duplicated.
    *
    * @param <T> collection items type
    * @param <C> collection factory type
    * @param collectionFactory collection factory to use to generate returned collection
    * @param streams streams to concatenate, or {@code null}. {@code null} streams are ignored
    *
    * @return new mutable collection of concatenated streams
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C collectionConcat(TypeFactory<C> collectionFactory,
                                                                 Stream<? extends T>... streams) {
      return collectionConcat(collectionFactory, stream(streams));
   }

   public static <T, C extends Collection<T>> C collectionConcat(TypeFactory<C> collectionFactory,
                                                                 Stream<? extends Stream<? extends T>> streams) {
      notNull(collectionFactory, "collectionFactory");

      return collection(collectionFactory, streamConcat(streams));
   }

   /**
    * Returns a new mutable, always initialized, collection of specified collection.
    *
    * @param <T> collection items type
    * @param <C> collection factory type
    * @param collectionFactory collection factory to use to generate returned collection
    * @param collection collection or {@code null}
    *
    * @return new mutable collection of specified collection, or new mutable empty collection
    */
   public static <T, C extends Collection<T>> C collection(TypeFactory<C> collectionFactory,
                                                           Collection<? extends T> collection) {
      return collection(collectionFactory, stream(collection));
   }

   /**
    * Returns a new mutable, always initialized, collection of concatenated collections.
    * If no collection is provided, concatenation will contain no element.
    * If only one collection is provided, concatenation will contain only its elements.
    * <p>
    * Resulting collection is not de-duplicated.
    *
    * @param <T> collection items type
    * @param <C> collection factory type
    * @param collectionFactory collection factory to use to generate returned collection
    * @param collections collections to concatenate, or {@code null}. {@code null} collections
    *       are ignored
    *
    * @return new mutable collection of concatenated collections
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C collectionConcat(TypeFactory<C> collectionFactory,
                                                                 Collection<? extends T>... collections) {
      return collection(collectionFactory, streamConcat(collections));
   }

   /**
    * Returns a new mutable, always initialized, collection of collections union.
    * If no collection is provided, union will contain no element.
    * If only one collection is provided, union will contain only its elements.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param <T> collection items type
    * @param <C> collection factory type
    * @param collectionFactory collection factory to use to generate returned collection
    * @param collections collections to union, or {@code null}. {@code null} collections
    *       are ignored
    *
    * @return new mutable collection of collections union
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C collectionUnion(TypeFactory<C> collectionFactory,
                                                                Collection<? extends T>... collections) {
      return collection(collectionFactory, streamConcat(collections).distinct());
   }

   public static <T, C extends Collection<T>> C collectionUnion(TypeFactory<C> collectionFactory,
                                                                Stream<? extends Collection<? extends T>> collections) {
      return collection(collectionFactory, streamConcatOfCollection(collections).distinct());
   }

   /**
    * Returns a new mutable, always initialized, collection of collections intersection.
    * If no collection is provided, intersection will contain no element.
    * If only one collection is provided, intersection will contain no element.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param <T> collection items type
    * @param <C> collection factory type
    * @param collectionFactory collection factory to use to generate returned collection
    * @param collections collections to intersect, or {@code null}. {@code null} collections
    *       are ignored
    *
    * @return new mutable collection of collections intersection
    *
    * @implNote Use {@link LinkedHashSet} internally to remove duplicates and keep the maximum
    *       characteristics from provided collections (order).
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C collectionIntersection(TypeFactory<C> collectionFactory,
                                                                       Collection<? extends T>... collections) {
      return collectionIntersection(collectionFactory, stream(collections));
   }

   public static <T, C extends Collection<T>> C collectionIntersection(TypeFactory<C> collectionFactory,
                                                                       Stream<? extends Collection<? extends T>> collections) {
      notNull(collectionFactory, "collectionFactory");

      Set<T> intersection = new LinkedHashSet<>();

      if (collections != null) {
         AtomicInteger count = new AtomicInteger(0);

         collections.forEach(collection -> {
            if (collection != null) {
               if (count.getAndIncrement() == 0) {
                  intersection.addAll(collection);
               } else {
                  intersection.retainAll(collection);
               }
            }
         });
         if (count.get() < 2) {
            return collection(collectionFactory);
         }
      }

      return collection(collectionFactory, intersection);
   }

   /**
    * Returns a mutable, always initialized, collection of collections subtraction (collection -
    * {subtractCollections}).
    * If only the first collection is provided, subtraction will only contain its elements.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param <T> collection items type
    * @param <C> collection factory type
    * @param collectionFactory collection factory to use to generate returned collection
    * @param collection collection
    * @param subtractCollections collections to subtract to collection, or {@code null}. {@code null}
    *       collections are ignored
    *
    * @return new mutable collection of collections subtraction
    *
    * @implNote Use {@link LinkedHashSet} internally to remove duplicates, and keep the maximum
    *       (in best-effort) characteristics from provided collections (order).
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C collectionSubtraction(TypeFactory<C> collectionFactory,
                                                                      Collection<? extends T> collection,
                                                                      Collection<? extends T>... subtractCollections) {
      return collectionSubtraction(collectionFactory, collection, stream(subtractCollections));
   }

   public static <T, C extends Collection<T>> C collectionSubtraction(TypeFactory<C> collectionFactory,
                                                                      Collection<? extends T> collection,
                                                                      Stream<? extends Collection<? extends T>> subtractCollections) {
      notNull(collectionFactory, "collectionFactory");
      notNull(collection, "collection");

      Set<T> subtraction = new LinkedHashSet<>(collection);

      if (subtractCollections != null) {
         subtractCollections.forEach(subtractCollection -> {
            if (subtractCollection != null) {
               subtraction.removeAll(subtractCollection);
            }
         });
      }

      return collection(collectionFactory, subtraction);
   }

   /**
    * Returns a new mutable, always initialized, collection of collections disjunction.
    * It's equivalent to
    * {@code collectionSubtract(collectionConcat(collections), collectionIntersect(collections))}.
    * If no collection is provided, disjunction will contain no element.
    * If only one collection is provided, disjunction will only contain its elements.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param <T> collection items type
    * @param <C> collection factory type
    * @param collectionFactory collection factory to use to generate returned collection
    * @param collections collections to apply disjunction to, or {@code null}. {@code null} collections
    *       are ignored
    *
    * @return new mutable collection of collections disjunction
    *
    * @implNote Use {@link LinkedHashSet} internally to remove duplicates and keep the maximum
    *       characteristics from provided collections (order).
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C collectionDisjunction(TypeFactory<C> collectionFactory,
                                                                      Collection<? extends T>... collections) {
      return collectionDisjunction(collectionFactory, stream(collections));
   }

   public static <T, C extends Collection<T>> C collectionDisjunction(TypeFactory<C> collectionFactory,
                                                                      Stream<? extends Collection<? extends T>> collections) {
      notNull(collectionFactory, "collectionFactory");

      Set<T> disjunction = new LinkedHashSet<>();
      Set<T> intersection = new LinkedHashSet<>();

      if (collections != null) {
         AtomicInteger count = new AtomicInteger(0);

         collections.forEach(collection -> {
            if (collection != null) {
               disjunction.addAll(collection);
               if (count.getAndIncrement() == 0) {
                  intersection.addAll(collection);
               } else {
                  intersection.retainAll(collection);
               }
            }
         });
         if (count.get() > 1) {
            disjunction.removeAll(intersection);
         }
      }

      return collection(collectionFactory, disjunction);
   }

   /**
    * Returns a new mutable, always initialized, collection of specified elements.
    *
    * @param collectionFactory collection factory to use to generate returned collection
    * @param elements collection of elements, {@code null} elements will be included in
    *       resulting collection
    * @param <T> collection items type
    * @param <C> collection factory type
    *
    * @return new mutable collection of specified elements, or new mutable empty collection
    */
   @SafeVarargs
   public static <T, C extends Collection<T>> C collection(TypeFactory<C> collectionFactory, T... elements) {
      return collection(collectionFactory, stream(elements));
   }

   /**
    * Checks for collection equality using a custom item comparator.
    *
    * @param c1 first collection, or {@code null}
    * @param c2 second collection, or {@code null}
    * @param equator item equator
    * @param <T> collection items type
    *
    * @return whether collections are equals
    */
   public static <T> boolean collectionEquals(Collection<T> c1, Collection<T> c2, Equator<T> equator) {
      notNull(equator, "equator");

      if (c1 == null) return c2 == null;
      if (c2 == null) return false;

      if (c1.size() != c2.size()) return false;

      Iterator<T> i1 = c1.iterator(), i2 = c2.iterator();

      while (i1.hasNext() && i2.hasNext()) {
         if (!equator.equate(i1.next(), i2.next())) {
            return false;
         }
      }

      return true;
   }

   /**
    * Returns a new mutable, always initialized, list of specified stream.
    *
    * @param stream stream or {@code null}
    * @param <T> list items type
    *
    * @return new mutable list of specified stream, or new mutable empty list
    */
   public static <T> List<T> list(Stream<? extends T> stream) {
      return collection(ArrayList::new, stream);
   }

   /**
    * Returns a new mutable, always initialized, list of concatenated streams.
    * <p>
    * Resulting collection is not de-duplicated.
    *
    * @param streams streams to concatenate. {@code null} streams are ignored
    * @param <T> list items type
    *
    * @return new mutable list of concatenated stream
    */
   @SafeVarargs
   public static <T> List<T> listConcat(Stream<? extends T>... streams) {
      return collectionConcat(ArrayList::new, streams);
   }

   /**
    * Returns a new mutable, always initialized, list of specified collection.
    *
    * @param collection collection or {@code null}
    * @param <T> list items type
    *
    * @return new mutable list of specified collection, or new mutable empty list
    */
   public static <T> List<T> list(Collection<? extends T> collection) {
      return collection == null ? new ArrayList<>() : new ArrayList<>(collection);
   }

   /**
    * Returns a new mutable, always initialized, list of concatenated collections.
    *
    * @param collections collections to concatenate. {@code null} collections are ignored
    * @param <T> list items type
    *
    * @return new mutable list of concatenated collections
    */
   @SafeVarargs
   public static <T> List<T> listConcat(Collection<? extends T>... collections) {
      return collectionConcat(ArrayList::new, collections);
   }

   /**
    * Returns a new mutable, always initialized, list of collections union.
    * If no collection is provided, union will contain no element.
    * If only one collection is provided, union will contain only its elements.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param collections collections to union. {@code null} collections are ignored
    * @param <T> list items type
    *
    * @return new mutable list of collection union
    */
   @SafeVarargs
   public static <T> List<T> listUnion(Collection<? extends T>... collections) {
      return collectionUnion(ArrayList::new, collections);
   }

   /**
    * Returns a new mutable, always initialized, list of collections union.
    * If no collection is provided, union will contain no element.
    * If only one collection is provided, union will contain only its elements.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param collections collections to union. {@code null} collections are ignored
    * @param <T> list items type
    *
    * @return new mutable list of collection union
    */
   public static <T> List<T> listUnion(Stream<? extends Collection<? extends T>> collections) {
      return collectionUnion(ArrayList::new, collections);
   }

   /**
    * Returns a new mutable, always initialized, list of collections intersection.
    * If no collection is provided, intersection will contain no element.
    * If only one collection is provided, intersection will contain no element.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param collections collections to intersect. {@code null} collections are ignored
    * @param <T> list items type
    *
    * @return new mutable list of collection intersection
    */
   @SafeVarargs
   public static <T> List<T> listIntersection(Collection<? extends T>... collections) {
      return collectionIntersection(ArrayList::new, collections);
   }

   /**
    * Returns a new mutable, always initialized, list of collections intersection.
    * If no collection is provided, intersection will contain no element.
    * If only one collection is provided, intersection will contain no element.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param collections collections to intersect. {@code null} collections are ignored
    * @param <T> list items type
    *
    * @return new mutable list of collection intersection
    */
   public static <T> List<T> listIntersection(Stream<? extends Collection<? extends T>> collections) {
      return collectionIntersection(ArrayList::new, collections);
   }

   /**
    * Returns a new mutable, always initialized, list of collections subtraction.
    * If only the first collection is provided, subtraction will only contain its elements.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param collection collection
    * @param subtractCollections collections to subtract to collection, or {@code null}. {@code null}
    *       collections are ignored
    * @param <T> list items type
    *
    * @return new mutable list of collection subtraction
    */
   @SafeVarargs
   public static <T> List<T> listSubtraction(Collection<? extends T> collection,
                                             Collection<? extends T>... subtractCollections) {
      return collectionSubtraction(ArrayList::new, collection, subtractCollections);
   }

   /**
    * Returns a new mutable, always initialized, list of collections subtraction.
    * If only the first collection is provided, subtraction will only contain its elements.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param collection collection
    * @param subtractCollections collections to subtract to collection, or {@code null}. {@code null}
    *       collections are ignored
    * @param <T> list items type
    *
    * @return new mutable list of collection subtraction
    */
   public static <T> List<T> listSubtraction(Collection<? extends T> collection,
                                             Stream<? extends Collection<? extends T>> subtractCollections) {
      return collectionSubtraction(ArrayList::new, collection, subtractCollections);
   }

   /**
    * Returns a new mutable, always initialized, list of collections disjunction.
    * If no collection is provided, disjunction will contain no element.
    * If only one collection is provided, disjunction will only contain its elements.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param collections collections to apply disjunction to, or {@code null}. {@code null} collections
    *       are ignored
    * @param <T> list items type
    *
    * @return new mutable list of collection disjunction
    */
   @SafeVarargs
   public static <T> List<T> listDisjunction(Collection<? extends T>... collections) {
      return collectionDisjunction(ArrayList::new, collections);
   }

   /**
    * Returns a new mutable, always initialized, list of collections disjunction.
    * If no collection is provided, disjunction will contain no element.
    * If only one collection is provided, disjunction will only contain its elements.
    * <p>
    * Resulting collection is de-duplicated.
    *
    * @param collections collections to apply disjunction to, or {@code null}. {@code null} collections
    *       are ignored
    * @param <T> list items type
    *
    * @return new mutable list of collection disjunction
    */
   public static <T> List<T> listDisjunction(Stream<? extends Collection<? extends T>> collections) {
      return collectionDisjunction(ArrayList::new, collections);
   }

   /**
    * Returns a new mutable, always initialized, list of specified {@link Enumeration}.
    *
    * @param enumeration enumeration or {@code null}
    * @param <T> list items type
    *
    * @return new mutable list of specified enumeration, or new mutable empty list
    */
   @SuppressWarnings("unchecked")
   public static <T> List<T> list(Enumeration<? extends T> enumeration) {
      return Collections.list((Enumeration<T>) enumeration);
   }

   /**
    * Returns a new mutable, always initialized, list of specified elements.
    *
    * @param elements list of elements, {@code null} elements will be included in
    *       resulting list
    * @param <T> list items type
    *
    * @return new mutable list of specified elements, or new mutable empty list
    */
   @SafeVarargs
   public static <T> List<T> list(T... elements) {
      return list(stream(elements));
   }

   /**
    * Returns a new mutable, empty {@link ArrayList}.
    *
    * @param <T> list items type
    *
    * @return new mutable empty list
    */
   public static <T> List<T> list() {
      return new ArrayList<>();
   }

   /**
    * Returns an immutable map entry.
    *
    * @param key entry key
    * @param value entry value
    * @param <K> entry key type
    * @param <V> entry value type
    *
    * @return an immutable map entry
    */
   public static <K, V> Entry<K, V> entry(K key, V value) {
      return Pair.of(key, value);
   }

   /**
    * Returns a new mutable, always initialized, map of specified entry stream.
    *
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    * @param mergeFunction optional merge function to apply when key duplicates found. Default merge
    *       function is {@code (v1, v2) -> v2}
    * @param mapFactory map factory to use to generate returned map
    * @param entries entry stream, or {@code null}
    *
    * @return new mutable map of specified map, or new mutable empty map
    */
   public static <K, V, M extends Map<K, V>> M map(BinaryOperator<V> mergeFunction, TypeFactory<M> mapFactory,
                                                   Stream<? extends Entry<? extends K, ? extends V>> entries) {
      notNull(mapFactory, "mapFactory");

      return entryStream(entries).collect(toNullSafeMap(Entry::getKey,
                                                        Entry::getValue,
                                                        nullable(mergeFunction, (v1, v2) -> v2),
                                                        mapFactory));
   }

   /**
    * Returns a new mutable, always initialized, map of specified entry stream.
    *
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    * @param mapFactory map factory to use to generate returned map
    * @param entries entry stream, or {@code null}
    *
    * @return new mutable map of specified map, or new mutable empty map
    */
   public static <K, V, M extends Map<K, V>> M map(TypeFactory<M> mapFactory,
                                                   Stream<? extends Entry<? extends K, ? extends V>> entries) {
      return map(null, mapFactory, entries);
   }

   /**
    * Returns a new mutable, always initialized, map of specified entry stream.
    *
    * @param <K> map items key type
    * @param <V> map items value type
    * @param entries entry stream, or {@code null}
    *
    * @return new mutable map of specified map, or new mutable empty map
    */
   public static <K, V> Map<K, V> map(Stream<? extends Entry<? extends K, ? extends V>> entries) {
      return map(null, HashMap::new, entries);
   }

   /**
    * Returns a new mutable, always initialized, map of specified map.
    *
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    * @param mergeFunction optional merge function to apply when key duplicates found. Default merge
    *       function is {@code (v1, v2) -> v2}
    * @param mapFactory map factory to use to generate returned map
    * @param map map, or {@code null}
    *
    * @return new mutable map of specified map, or new mutable empty map
    */
   public static <K, V, M extends Map<K, V>> M map(BinaryOperator<V> mergeFunction, TypeFactory<M> mapFactory,
                                                   Map<? extends K, ? extends V> map) {
      return map(mergeFunction, mapFactory, entryStream(map));
   }

   /**
    * Returns a new mutable, always initialized, map of specified map.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #map(BinaryOperator, TypeFactory, Map)} to use a custom map builder.
    *
    * @param mapFactory map factory to use to generate returned map
    * @param map map, or null
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new mutable map of specified map, or new mutable empty map
    */
   public static <K, V, M extends Map<K, V>> M map(TypeFactory<M> mapFactory,
                                                   Map<? extends K, ? extends V> map) {
      return map(null, mapFactory, map);
   }

   /**
    * Returns a new mutable, always initialized map, of specified map.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #map(BinaryOperator, TypeFactory, Map)} to use a custom map builder.
    *
    * @param map map, or null
    * @param <K> map items key type
    * @param <V> map items value type
    *
    * @return new mutable map of specified map, or new mutable empty map
    */
   public static <K, V> Map<K, V> map(Map<? extends K, ? extends V> map) {
      return map == null ? new HashMap<>() : new HashMap<>(map);
   }

   /**
    * Returns a new mutable, always initialized, map of specified entries.
    * {@code null} entries are ignored.
    * If the same key is present in multiple maps, the specified merge function is applied.
    *
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    * @param mergeFunction map merge function to apply when key duplicates found
    * @param mapFactory map factory to use to generate returned map
    * @param entries entries, {@code null} entries will be ignored
    *
    * @return new mutable map of specified entries, or new mutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> M map(BinaryOperator<V> mergeFunction, TypeFactory<M> mapFactory,
                                                   Entry<? extends K, ? extends V>... entries) {
      return map(mergeFunction, mapFactory, entryStream(entries));
   }

   /**
    * Returns a new mutable, always initialized, map of specified entries.
    * {@code null} entries are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #map(BinaryOperator, TypeFactory, Entry[])} to use a custom map builder.
    *
    * @param mapFactory map factory to use to generate returned map
    * @param entries entries, {@code null} entries will be ignored
    * @param <K> map items key type
    * @param <V> map items value type
    * @param <M> map factory type
    *
    * @return new mutable map of specified entries, or new mutable empty map
    */
   @SafeVarargs
   public static <K, V, M extends Map<K, V>> M map(TypeFactory<M> mapFactory,
                                                   Entry<? extends K, ? extends V>... entries) {
      return map(null, mapFactory, entryStream(entries));
   }

   /**
    * Returns a new mutable, always initialized, map of specified entries.
    * {@code null} entries are ignored.
    * The merge function applied when keys duplicated found is to keep the last value, see
    * {@link #map(BinaryOperator, TypeFactory, Entry[])} to use a custom map builder.
    *
    * @param entries entries
    * @param <K> map items key type
    * @param <V> map items value type
    *
    * @return new mutable map of specified entries, or new mutable empty map
    */
   @SafeVarargs
   public static <K, V> Map<K, V> map(Entry<? extends K, ? extends V>... entries) {
      return map(entryStream(entries));
   }

   /**
    * Returns a new mutable, empty map.
    *
    * @param <K> map items key type
    * @param <V> map items value type
    *
    * @return new mutable empty map
    */
   public static <K, V> Map<K, V> map() {
      return new HashMap<>();
   }

   /**
    * Returns an immutable collection, always initialized.
    * <ul>
    *    <li>returns an empty collection if specified collection is {@code null}</li>
    *    <li>returns specified collection if it is already immutable</li>
    *    <li>otherwise, returns an immutable view of specified collection. Collection original type is lost and can't be
    *        down-casted, but its characteristics are preserved</li>
    * </ul>
    *
    * @param collection collection
    * @param <T> collection element type
    *
    * @return collection immutable view
    *
    * @apiNote This function does not enforce the copy of specified collection because most of the time
    *       immutable collections does not need this protection against external changes.
    */
   @SuppressWarnings("unchecked")
   public static <T> Collection<T> immutable(Collection<? extends T> collection) {
      if (collection == null) {
         return emptyList();
      } else if (IMMUTABLE_COLLECTION_CLASSES.contains(collection.getClass().getName())) {
         return (Collection<T>) collection;
      } else {
         return Collections.unmodifiableCollection(collection);
      }
   }

   /**
    * Returns an immutable list, always initialized.
    * <ul>
    *    <li>returns an empty list if specified list is {@code null}</li>
    *    <li>returns specified list if it is already immutable</li>
    *    <li>otherwise, returns an immutable view of specified list. List original type is lost and can't be
    *        down-casted, but its characteristics are preserved</li>
    * </ul>
    *
    * @param list list
    * @param <T> list element type
    *
    * @return list immutable view
    *
    * @apiNote This function does not enforce the copy of specified collection because most of the time
    *       immutable collections does not need this protection against external changes.
    */
   @SuppressWarnings("unchecked")
   public static <T> List<T> immutable(List<? extends T> list) {
      if (list == null) {
         return emptyList();
      } else if (IMMUTABLE_LIST_CLASSES.contains(list.getClass().getName())) {
         return (List<T>) list;
      } else {
         return Collections.unmodifiableList(list);
      }
   }

   /**
    * Returns an immutable map, always initialized.
    * <ul>
    *    <li>returns an empty map if specified map is {@code null}</li>
    *    <li>returns specified map if it is already immutable</li>
    *    <li>otherwise, returns an immutable view of specified map. Map original type is lost and can't be
    *        down-casted, but its characteristics are preserved</li>
    * </ul>
    *
    * @param map map
    * @param <K> map key type
    * @param <V> map value type
    *
    * @return map immutable view
    *
    * @apiNote This function does not enforce the copy of specified collection because most of the time
    *       immutable collections does not need this protection against external changes.
    */
   @SuppressWarnings("unchecked")
   public static <K, V> Map<K, V> immutable(Map<? extends K, ? extends V> map) {
      if (map == null) {
         return emptyMap();
      } else if (IMMUTABLE_MAP_CLASSES.contains(map.getClass().getName())) {
         return (Map<K, V>) map;
      } else {
         return Collections.unmodifiableMap(map);
      }
   }

   /**
    * Returns a synchronized view, always initialized, of specified collection. Collection original type is
    * lost
    * in favor of synchronized view type, and can't be down-casted, but its characteristics are preserved.
    *
    * @param collection collection
    * @param <T> collection element type
    *
    * @return collection synchronized view
    */
   @SuppressWarnings("unchecked")
   public static <T> Collection<T> synchronizedCollection(Collection<? extends T> collection) {
      return Collections.synchronizedCollection(collection != null
                                                ? (Collection<T>) collection
                                                : collection(ArrayList::new, collection));
   }

   /**
    * Returns a synchronized view, always initialized, of specified list. List original type is lost
    * in favor of synchronized view type, and can't be down-casted, but its characteristics are preserved.
    *
    * @param list list
    * @param <T> list element type
    *
    * @return list synchronized view
    */
   @SuppressWarnings("unchecked")
   public static <T> List<T> synchronizedList(List<? extends T> list) {
      return Collections.synchronizedList(list != null ? (List<T>) list : list(list));
   }

   /**
    * Returns a synchronized view, always initialized, of specified map. Map original type is lost in favor
    * of synchronized view type, and can't be down-casted, but its characteristics are preserved.
    *
    * @param map map
    * @param <K> map key type
    * @param <V> map value type
    *
    * @return list synchronized view
    */
   @SuppressWarnings("unchecked")
   public static <K, V> Map<K, V> synchronizedMap(Map<? extends K, ? extends V> map) {
      return Collections.synchronizedMap(map != null ? (Map<K, V>) map : map(map));
   }

   /**
    * Workaround from <a href="https://stackoverflow.com/a/62692728">StackOverflow</a> for JDK bug
    * <a href="https://bugs.openjdk.org/browse/JDK-8148463">JDK-8148463</a>
    */
   private static <T, K, V, M extends Map<K, V>> Collector<T, ?, M> toNullSafeMap(Function<? super T, ? extends K> keyMapper,
                                                                                  Function<? super T, ? extends V> valueMapper,
                                                                                  BinaryOperator<V> mergeFunction,
                                                                                  TypeFactory<M> mapFactory) {
      return collectingAndThen(toList(), list -> {
         M result = mapFactory.get();
         for (T item : list) {
            K key = keyMapper.apply(item);
            V newValue = valueMapper.apply(item);
            V value = result.containsKey(key) ? mergeFunction.apply(result.get(key), newValue) : newValue;
            result.put(key, value);
         }
         return result;
      });
   }

   /**
    * Best-compatible automatic type factory for known {@link Collection} implementation types from
    * {@code java.util} and {@code java.util.concurrent} packages.
    * You can {@link TypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own factories.
    *
    * @param type class to create a factory for
    * @param defaultFactory default factory if type is not known in registered factories
    *
    * @return factory for the same type
    *
    * @implSpec Resulting factory type must be modifiable.
    */
   @SuppressWarnings("unchecked")
   public static <T extends Collection<U>, U> TypeFactory<? extends Collection<U>> compatibleCollectionFactory(
         Class<? extends T> type,
         TypeFactory<? extends Collection<U>> defaultFactory) {
      return ((Optional<TypeFactory<? extends Collection<U>>>) DefaultTypeFactoryRegistry
            .instance().compatibleTypeFactory(Collection.class, type)).orElse(defaultFactory);
   }

   /**
    * Best-compatible automatic type factory for known {@link List} implementation types from
    * {@code java.util} and {@code java.util.concurrent} packages.
    * You can {@link TypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own factories.
    *
    * @param type class to create a factory for
    * @param defaultFactory default factory if type is not known in registered factories
    *
    * @return factory for the same type
    *
    * @implSpec Resulting factory type must be modifiable.
    */
   @SuppressWarnings("unchecked")
   public static <T extends List<U>, U> TypeFactory<? extends List<U>> compatibleListFactory(Class<? extends T> type,
                                                                                             TypeFactory<? extends List<U>> defaultFactory) {
      return ((Optional<TypeFactory<? extends List<U>>>) DefaultTypeFactoryRegistry
            .instance().compatibleTypeFactory(List.class, type)).orElse(defaultFactory);
   }

   public static <T extends List<U>, U> TypeFactory<? extends List<U>> compatibleListFactory(Class<? extends T> type) {
      return compatibleListFactory(type, ArrayList::new);
   }

   /**
    * Best-compatible automatic type factory for known {@link Map} implementation types from
    * {@code java.util} and {@code java.util.concurrent} packages.
    * You can {@link TypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own factories.
    *
    * @param type class to create a factory for
    * @param defaultFactory default factory if type is not known in registered factories
    *
    * @return factory for the same type
    *
    * @implSpec Resulting factory type must be modifiable.
    */
   @SuppressWarnings("unchecked")
   public static <T extends Map<K, V>, K, V> TypeFactory<? extends Map<K, V>> compatibleMapFactory(Class<? extends T> type,
                                                                                                   TypeFactory<? extends Map<K, V>> defaultFactory) {
      return ((Optional<TypeFactory<? extends Map<K, V>>>) DefaultTypeFactoryRegistry
            .instance().compatibleTypeFactory(Map.class, type)).orElse(defaultFactory);
   }

   public static <T extends Map<K, V>, K, V> TypeFactory<? extends Map<K, V>> compatibleMapFactory(Class<? extends T> type) {
      return compatibleMapFactory(type, HashMap::new);
   }

   /**
    * Best-compatible automatic type factory for known {@link Collection} implementation types from
    * {@code java.util} and {@code java.util.concurrent} packages.
    * You can {@link TypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own factories.
    *
    * @param instance instance to create a factory for. Content is not copied, only characteristics if
    *       it's possible
    * @param defaultFactory default factory if type is not known in registered factories
    * @param <T> instance type to select factory type
    *
    * @return factory for the same instance type
    *
    * @implSpec Resulting factory type must be modifiable.
    */
   @SuppressWarnings("unchecked")
   public static <T extends Collection<U>, U> TypeFactory<? extends Collection<U>> compatibleCollectionFactory(
         T instance,
         TypeFactory<? extends Collection<U>> defaultFactory) {
      return ((Optional<TypeFactory<? extends Collection<U>>>) DefaultTypeFactoryRegistry
            .instance().compatibleTypeFactory(Collection.class, instance)).orElse(defaultFactory);
   }

   /**
    * Best-compatible automatic type factory for known {@link List} implementation types from
    * {@code java.util} and {@code java.util.concurrent} packages.
    * You can {@link TypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own factories.
    *
    * @param instance instance to create a factory for. Content is not copied, only characteristics if
    *       it's possible
    * @param defaultFactory default factory if type is not known in registered factories
    * @param <T> instance type to select factory type
    *
    * @return factory for the same instance type
    *
    * @implSpec Resulting factory type must be modifiable.
    */
   @SuppressWarnings("unchecked")
   public static <T extends List<U>, U> TypeFactory<? extends List<U>> compatibleListFactory(T instance,
                                                                                             TypeFactory<? extends List<U>> defaultFactory) {
      return ((Optional<TypeFactory<? extends List<U>>>) DefaultTypeFactoryRegistry
            .instance().compatibleTypeFactory(Collection.class, instance)).orElse(defaultFactory);
   }

   public static <T extends List<U>, U> TypeFactory<? extends List<U>> compatibleListFactory(T instance) {
      return compatibleListFactory(instance, ArrayList::new);
   }

   /**
    * Best-compatible automatic type factory for known {@link Map} implementation types from
    * {@code java.util} and {@code java.util.concurrent} packages.
    * You can {@link TypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own factories.
    *
    * @param instance instance to create a factory for. Content is not copied, only characteristics if
    *       it's possible
    * @param defaultFactory default factory if type is not known in registered factories
    * @param <T> instance type to select factory type
    *
    * @return factory for the same instance type
    *
    * @implSpec Resulting factory type must be modifiable.
    */
   @SuppressWarnings("unchecked")
   public static <T extends Map<K, V>, K, V> TypeFactory<? extends Map<K, V>> compatibleMapFactory(T instance,
                                                                                                   TypeFactory<? extends Map<K, V>> defaultFactory) {
      return ((Optional<TypeFactory<? extends Map<K, V>>>) DefaultTypeFactoryRegistry
            .instance().compatibleTypeFactory(Map.class, instance)).orElse(defaultFactory);
   }

   public static <T extends Map<K, V>, K, V> TypeFactory<? extends Map<K, V>> compatibleMapFactory(T instance) {
      return compatibleMapFactory(instance, HashMap::new);
   }

}
