/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.or;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Conveys the nullability concept of an object.
 * This class aim is transpose object nullity to something else, e.g. {@link Optional}, or default value, ...
 * so that resulting object is not {@code null}. The only exception is {@link #nullable(Object)} that can
 * return {@code null}, and can be used for internal fields.
 */
public final class NullableUtils {

   private NullableUtils() {
   }

   /**
    * Converts an object that can be nullable to an {@link Optional} so that the result is never {@code null}.
    *
    * @param object nullable object
    * @param <T> object type
    *
    * @return {@link Optional} of object, never {@code null}
    */
   public static <T> Optional<T> nullable(T object) {
      return Optional.ofNullable(object);
   }

   /**
    * Casts an object to the specified class if it is an instance of it, otherwise, returns
    * {@link Optional#empty}.
    * Object can be nullable, in such case the method returns an empty optional so that the result is never
    * {@code null}.
    *
    * @param object nullable object
    * @param subclass subclass class
    * @param <T> object type
    * @param <U> subclass type
    *
    * @return {@link Optional} of subclassed object, or {@link Optional#empty()}
    *
    * @see OptionalUtils#optionalInstanceOf(Object, Class) if object is not nullable
    */
   public static <T, U extends T> Optional<U> nullableInstanceOf(T object, Class<U> subclass) {
      notNull(subclass, "subclass");

      return nullable(object)
            .filter(o -> notNull(subclass, "subclass").isAssignableFrom(o.getClass()))
            .map(subclass::cast);
   }

   /**
    * Returns an object wrapped in {@link Optional} if it is an instance of one of specified classes,
    * otherwise, returns {@link Optional#empty}.
    * Object can be nullable, in such case the method returns an empty optional so that the result is never
    * {@code null}.
    *
    * @param object nullable object
    * @param subclasses subclasses class
    * @param <T> object type
    *
    * @return {@link Optional} of object, or {@link Optional#empty()}
    *
    * @see OptionalUtils#optionalInstanceOf(Object, Class...) if object is not nullable
    */
   @SafeVarargs
   @SuppressWarnings("unchecked")
   public static <T> Optional<Object> nullableInstanceOf(T object, Class<? extends T>... subclasses) {
      noNullElements(subclasses, "subclasses");

      return stream(subclasses).reduce(Optional.empty(),
                                       (o, targetClass) -> or(o,
                                                              () -> nullableInstanceOf(object,
                                                                                       (Class<Object>) targetClass)),
                                       (o1, o2) -> or(o1, () -> o2));
   }

   /**
    * Provide a default value for a nullable object so that the result should not be {@code null}.
    *
    * @param object nullable object
    * @param defaultValue default value
    * @param <T> object type
    *
    * @return object value or default value
    */
   public static <T> T nullable(T object, T defaultValue) {
      return object != null ? object : defaultValue;
   }

   /**
    * Provide a lazy default value for a nullable object so that the result should not be {@code null}.
    *
    * @param object nullable object
    * @param defaultValue default value supplier
    * @param <T> object type
    *
    * @return object value or default value
    */
   public static <T> T nullable(T object, Supplier<T> defaultValue) {
      return object != null ? object : notNull(defaultValue, "defaultValue").get();
   }

   /**
    * Apply the mapping function on nullable object only if present, or return the default value, so that the
    * result should not be {@code null}.
    *
    * @param <T> object type
    * @param object nullable object
    * @param map mapping function to apply to non-null object
    * @param defaultValue default value
    *
    * @return mapped object value or default value
    */
   public static <T> T nullable(T object, Function<? super T, ? extends T> map, T defaultValue) {
      return nullable(object == null ? object : notNull(map, "map").apply(object), defaultValue);
   }

   /**
    * Apply the mapping function on nullable object only if present, or return the lazy default value, so that
    * the result should not be {@code null}.
    *
    * @param <T> object type
    * @param object nullable object
    * @param map mapping function to apply to non-null object
    * @param defaultValue default value supplier
    *
    * @return mapped object value or default value
    */
   public static <T> T nullable(T object, Function<? super T, ? extends T> map, Supplier<T> defaultValue) {
      return nullable(object == null ? object : notNull(map, "map").apply(object), defaultValue);
   }

   /**
    * Identity function making explicit the nullable nature of {@code object}.
    *
    * @param object nullable object
    * @param map mapping function to apply to non-null object
    * @param <T> object type
    *
    * @return mapped object value or {@code null}
    */
   public static <T> T nullable(T object, Function<? super T, ? extends T> map) {
      return nullable(object, map, (T) null);
   }

   /**
    * Bridge from {@link Predicate} to {@link Optional}.
    * Predicate is not called if object is {@code null} and an empty optional is returned.
    *
    * @param object object to bridge
    * @param predicate predicate to apply to object
    *
    * @return object as an optional if predicate is satisfied, otherwise, an empty optional
    *
    * @see OptionalUtils#optionalPredicate(Object, Predicate) if object is non-nullable
    */
   public static <T> Optional<T> nullablePredicate(T object, Predicate<T> predicate) {
      notNull(predicate, "predicate");

      return nullable(object).filter(predicate);
   }

   /**
    * Bridge from {@link Boolean} to {@link Optional}.
    *
    * @param object object to bridge
    *
    * @return object as an optional if object is not {@code null} and {@code true}, otherwise, an empty optional
    *
    * @see OptionalUtils#optionalPredicate(Object, Predicate) if object is non-nullable
    */
   public static Optional<Boolean> nullableBoolean(Boolean object) {
      return Boolean.TRUE.equals(object) ? nullable(object) : optional();
   }

}
