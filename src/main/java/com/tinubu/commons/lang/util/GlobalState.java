/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

/**
 * Manages a global state that can be set externally to configure some internal behavior in classes that use
 * it, with :
 * <ul>
 *    <li>an optional, non thread-safe default state to set at start</li>
 *    <li>an optional, thread-safe state that overrides default state in current-thread</li>
 * </ul>
 * Default state and thread-local state are nullable, the default state will be returned if local-thread state is {@code null}.
 * <p>
 * This global state class should be instantiated statically for each state it represents.
 *
 * @param <S> state type
 *
 * @see SetOnceGlobalState
 */
public class GlobalState<S> {

   protected S defaultState;
   protected final ThreadLocal<S> state;

   public GlobalState(S initial) {
      this.defaultState = initial;
      this.state = new ThreadLocal<>();
   }

   public GlobalState() {
      this(null);
   }

   public void defaultState(S defaultState) {
      this.defaultState = defaultState;
   }

   public void state(S state) {
      this.state.set(state);
   }

   public S state() {
      return nullable(state.get(), defaultState);
   }

}
