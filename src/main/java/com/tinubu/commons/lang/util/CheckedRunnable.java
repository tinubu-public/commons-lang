/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static java.util.Objects.requireNonNull;

/**
 * Represents a checked {@link Runnable}.
 * This operation can throw checked exceptions.
 * <p>
 * This is a functional interface whose functional method is {@link #runChecked()}.
 */
@FunctionalInterface
public interface CheckedRunnable extends Runnable {
   /**
    * Run operation.
    */
   @Override
   default void run() {
      uncheckedRunnable(this).run();
   }

   /**
    * Run operation.
    * This operation can throw checked exceptions.
    */
   void runChecked() throws Exception;

   /**
    * Returns a checked runnable from a standard runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return checked runnable
    */
   static <T> CheckedRunnable checkedRunnable(Runnable runnable) {
      requireNonNull(runnable);
      if (runnable instanceof CheckedRunnable) {
         return (CheckedRunnable) runnable;
      } else {
         return runnable::run;
      }
   }

   /**
    * Returns an unchecked runnable from a checked runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return unchecked runnable
    */
   static <T> Runnable uncheckedRunnable(CheckedRunnable runnable) {
      requireNonNull(runnable);
      return runnable.unchecked();
   }

   /**
    * Returns a value from unchecked runnable.
    *
    * @param runnable runnable to run
    */
   static void unchecked(CheckedRunnable runnable) {
      requireNonNull(runnable);
      runnable.unchecked().run();
   }

   /**
    * Returns an unchecked runnable from this checked runnable.
    *
    * @return unchecked runnable
    */
   default Runnable unchecked() {
      return () -> {
         try {
            this.runChecked();
         } catch (Exception e) {
            ExceptionUtils.sneakyThrow(e);
         }
      };
   }

}
