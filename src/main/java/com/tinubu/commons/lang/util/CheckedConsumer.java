/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static java.util.Objects.requireNonNull;

import java.util.function.Consumer;

/**
 * Represents an operation that accepts a single input argument and returns no
 * result. Unlike most other functional interfaces, {@code CheckedConsumer} is expected
 * to operate via side-effects.
 * This operation can throw checked exceptions.
 * <p>
 * This is a functional interface whose functional method is {@link #acceptChecked(Object)}.
 *
 * @param <T> the type of the input to the operation
 */
@FunctionalInterface
public interface CheckedConsumer<T> extends Consumer<T> {
   /**
    * Performs this operation on the given argument.
    *
    * @param t the input argument
    */
   @Override
   default void accept(T t) {
      uncheckedConsumer(this).accept(t);
   }

   /**
    * Performs this operation on the given argument.
    * This operation can throw checked exceptions.
    *
    * @param t the input argument
    */
   void acceptChecked(T t) throws Exception;

   /**
    * Returns a checked consumer from a standard consumer.
    *
    * @param consumer consumer to adapt
    *
    * @return checked consumer
    */
   static <T> CheckedConsumer<T> checkedConsumer(Consumer<T> consumer) {
      requireNonNull(consumer);
      if (consumer instanceof CheckedConsumer) {
         return (CheckedConsumer<T>) consumer;
      } else {
         return consumer::accept;
      }
   }

   /**
    * Returns an unchecked consumer from a checked consumer.
    *
    * @param consumer consumer to adapt
    *
    * @return unchecked consumer
    */
   static <T> Consumer<T> uncheckedConsumer(CheckedConsumer<T> consumer) {
      requireNonNull(consumer);
      return consumer.unchecked();
   }

   /**
    * Returns an unchecked consumer from this checked consumer.
    *
    * @return unchecked consumer
    */
   default Consumer<T> unchecked() {
      return t -> {
         try {
            this.acceptChecked(t);
         } catch (Exception e) {
            ExceptionUtils.sneakyThrow(e);
         }
      };
   }

   @Override
   default CheckedConsumer<T> andThen(Consumer<? super T> after) {
      requireNonNull(after);
      return (T t) -> {
         acceptChecked(t);
         after.accept(t);
      };
   }

}
