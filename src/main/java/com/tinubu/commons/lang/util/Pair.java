/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

/**
 * A pair consisting of two elements.
 * <p>
 * This class is an abstract implementation defining the basic API.
 * It refers to the elements as 'left' and 'right'. It also implements the
 * {@code Map.Entry} interface where the key is 'left' and the value is 'right'.</p>
 * <p>
 * This container supports {@link #map(BiFunction)} and {@link #forEach(BiConsumer)} operations.
 * <p>
 * This pair can store {@code null} values for left and right elements. {@code null} values are supported in
 * {@link #equals(Object)} and {@link #compareTo(Pair)} implementations.
 *
 * @param <L> the left element type
 * @param <R> the right element type
 */
public class Pair<L, R> implements Map.Entry<L, R>, Comparable<Pair<L, R>> {

   /** Left object */
   public final L left;
   /** Right object */
   public final R right;

   /**
    * Create a new pair instance.
    *
    * @param left the left value, may be null
    * @param right the right value, may be null
    */
   protected Pair(L left, R right) {
      this.left = left;
      this.right = right;
   }

   /**
    * Obtains an immutable pair of two objects inferring the generic types.
    *
    * @param <L> the left element type
    * @param <R> the right element type
    * @param left the left element, may be null
    * @param right the right element, may be null
    *
    * @return a pair formed from the two parameters, not null
    */
   public static <L, R> Pair<L, R> of(L left, R right) {
      return new Pair<>(left, right);
   }

   /**
    * Copy factory method.
    *
    * @param pair pair to copy
    * @param <L> the left element type
    * @param <R> the right element type
    *
    * @return new pair instance with copied data
    */
   public static <L, R> Pair<L, R> from(Pair<L, R> pair) {
      return new Pair<>(pair.left, pair.right);
   }

   /**
    * forEach implementation for pair.
    *
    * @param consumer consumer for pair's left and right values
    */
   public void forEach(BiConsumer<? super L, ? super R> consumer) {
      consumer.accept(left, right);
   }

   /**
    * Map implementation for pair.
    *
    * @param mapper function to apply to pair components.
    * @param <T> mapping returning type
    *
    * @return mapping result
    */
   public <T> T map(BiFunction<? super L, ? super R, ? extends T> mapper) {
      notNull(mapper, "mapper");

      return mapper.apply(left, right);
   }

   /**
    * Filter implementation for pair.
    *
    * @param filter predicate to apply to pair components.
    *
    * @return this pair or {@link Optional#empty} if predicate is not satisfied
    */
   public Optional<Pair<L, R>> filter(BiPredicate<? super L, ? super R> filter) {
      notNull(filter, "filter");

      Predicate<Pair<L, R>> predicate = p -> filter.test(p.left, p.right);
      notNull(predicate, "predicate");

      return OptionalUtils.optional(this).filter(predicate);
   }

   /**
    * Gets the left element from this pair.
    * <p>
    * When treated as a key-value pair, this is the key.
    *
    * @return the left element, may be null
    */
   public L getLeft() {
      return left;
   }

   /**
    * Gets the right element from this pair.
    * <p>
    * When treated as a key-value pair, this is the value.
    *
    * @return the right element, may be null
    */
   public R getRight() {
      return right;
   }

   /**
    * Gets the key from this pair.
    * <p>
    * This method implements the {@code Map.Entry} interface returning the
    * left element as the key.
    *
    * @return the left element as the key, may be null
    */
   @Override
   public L getKey() {
      return getLeft();
   }

   /**
    * Gets the value from this pair.
    * <p>
    * This method implements the {@code Map.Entry} interface returning the
    * right element as the value.
    *
    * @return the right element as the value, may be null
    */
   @Override
   public R getValue() {
      return getRight();
   }

   /**
    * Throws {@code UnsupportedOperationException}.
    * <p>
    * This pair is immutable, so this operation is not supported.
    *
    * @param value the value to set
    *
    * @return never
    *
    * @throws UnsupportedOperationException as this operation is not supported
    */
   @Override
   public R setValue(R value) {
      throw new UnsupportedOperationException("Immutable implementation");
   }

   /**
    * Flips left and right elements.
    *
    * @return new pair
    */
   public Pair<R, L> flip() {
      return map((left, right) -> of(right, left));
   }

   /**
    * Compares the pair based on the left element followed by the right element.
    * The types must be {@code Comparable}.
    * <p>
    * {@code null} values for left and right elements are supported :
    * <ul>
    *    <li>this=null, other=null -> 0</li>
    *    <li>this=null, other!=null -> -1</li>
    *    <li>this!=null, other=null -> 1</li>
    * </ul>
    *
    * @param other the other pair, not null
    *
    * @return negative if this is less, zero if equal, positive if greater
    *
    * @throws ClassCastException if left and right elements are not {@link Comparable}
    */
   @Override
   @SuppressWarnings("unchecked")
   public int compareTo(Pair<L, R> other) {
      Comparator<Comparable<Object>> comparator = Comparator.nullsFirst(naturalOrder());

      int compareTo = comparator.compare((Comparable<Object>) this.left, (Comparable<Object>) other.left);
      if (compareTo == 0) {
         compareTo = comparator.compare((Comparable<Object>) this.right, (Comparable<Object>) other.right);
      }
      return compareTo;
   }

   /**
    * Returns a comparator that compares {@link Pair} in natural order on left, then right elements.
    * <p>
    * {@code null} values for left and right elements are supported :
    * <ul>
    *    <li>left=null, right=null -> 0</li>
    *    <li>left=null, right!=null -> -1</li>
    *    <li>left!=null, right=null -> 1</li>
    * </ul>
    *
    * @param <L> the {@link Comparable} type of left element
    * @param <R> the {@link Comparable} type of right element
    *
    * @return a comparator that compares {@link Pair} in natural order on left, then right elements.
    *
    * @see Comparable
    */
   public static <L extends Comparable<? super L>, R extends Comparable<? super R>> Comparator<Pair<L, R>> comparing() {
      return Pair.<L, R>comparingByLeft().thenComparing(comparingByRight());
   }

   /**
    * Returns a comparator that compares {@link Pair} on left, then right elements, using the given
    * comparators.
    *
    * @param <L> the {@link Comparable} type of left element
    * @param <R> the {@link Comparable} type of right element
    *
    * @return a comparator that compares {@link Pair} on left, then right elements.
    *
    * @see Comparable
    */
   public static <L, R> Comparator<Pair<L, R>> comparing(Comparator<? super L> leftCmp,
                                                         Comparator<? super R> rightCmp) {
      return Pair.<L, R>comparingByLeft(leftCmp).thenComparing(comparingByRight(rightCmp));
   }

   /**
    * Returns a comparator that compares {@link Pair} in natural order on left element.
    * <p>
    * {@code null} values for left elements are supported :
    * <ul>
    *    <li>e1=null, e2=null -> 0</li>
    *    <li>e1=null, e2!=null -> -1</li>
    *    <li>e1!=null, e2=null -> 1</li>
    * </ul>
    *
    * @param <L> the {@link Comparable} type of left element
    * @param <R> the type of right element
    *
    * @return a comparator that compares {@link Pair} in natural order on left element.
    *
    * @see Comparable
    * @see #comparingByLeft(Comparator) to provide your customized comparator
    */
   public static <L extends Comparable<? super L>, R> Comparator<Pair<L, R>> comparingByLeft() {
      return comparingByLeft(nullsFirst(naturalOrder()));
   }

   /**
    * Returns a comparator that compares {@link Pair} in natural order on right element.
    * <p>
    * {@code null} values for right elements are supported :
    * <ul>
    *    <li>e1=null, e2=null -> 0</li>
    *    <li>e1=null, e2!=null -> -1</li>
    *    <li>e1!=null, e2=null -> 1</li>
    * </ul>
    *
    * @param <L> the type of left element
    * @param <R> the {@link Comparable} type of right element
    *
    * @return a comparator that compares {@link Pair} in natural order on right element.
    *
    * @see Comparable
    * @see #comparingByRight(Comparator) to provide your customized comparator
    */
   public static <L, R extends Comparable<? super R>> Comparator<Pair<L, R>> comparingByRight() {
      return comparingByRight(nullsFirst(naturalOrder()));
   }

   /**
    * Returns a comparator that compares {@link Pair} by left element using the given
    * {@link Comparator}.
    *
    * @param <L> the type of the left element
    * @param <R> the type of the right element
    * @param cmp the key {@link Comparator}
    *
    * @return a comparator that compares {@link Pair} by left element.
    */
   public static <L, R> Comparator<Pair<L, R>> comparingByLeft(Comparator<? super L> cmp) {
      return Comparator.comparing(Pair::getLeft, cmp);
   }

   /**
    * Returns a comparator that compares {@link Pair} by right element using the given
    * {@link Comparator}.
    *
    * @param <L> the type of left element
    * @param <R> the type of right element
    * @param cmp the value {@link Comparator}
    *
    * @return a comparator that compares {@link Pair} by right element.
    */
   public static <L, R> Comparator<Pair<L, R>> comparingByRight(Comparator<? super R> cmp) {
      return Comparator.comparing(Pair::getRight, cmp);
   }

   /**
    * Compares this pair to another based on the two elements.
    *
    * @param obj the object to compare to, null returns false
    *
    * @return true if the elements of the pair are equal
    */
   @Override
   public boolean equals(Object obj) {
      if (obj == this) {
         return true;
      }
      if (obj instanceof Map.Entry<?, ?>) {
         Map.Entry<?, ?> other = (Map.Entry<?, ?>) obj;
         return Objects.equals(getKey(), other.getKey()) && Objects.equals(getValue(), other.getValue());
      }
      return false;
   }

   /**
    * Returns a suitable hash code.
    * The hash code follows the definition in {@code Map.Entry}.
    *
    * @return the hash code
    *
    * @see Entry#hashCode() Entry#hashcode() API specification
    */
   @Override
   public int hashCode() {
      return (getKey() == null ? 0 : getKey().hashCode()) ^ (getValue() == null ? 0 : getValue().hashCode());
   }

   /**
    * Returns a String representation of this pair using the format {@code ($left,$right)}.
    *
    * @return a string describing this object, not null
    */
   @Override
   public String toString() {
      return "(" + getLeft() + ',' + getRight() + ')';
   }

   /**
    * Formats the receiver using the given format.
    * <p>
    * This uses {@link java.util.Formattable} to perform the formatting. Two variables may
    * be used to embed the left and right elements. Use {@code %1$s} for the left
    * element (key) and {@code %2$s} for the right element (value).
    * The default format used by {@code toString()} is {@code (%1$s,%2$s)}.
    *
    * @param format the format string, optionally containing {@code %1$s} and {@code %2$s}, not null
    *
    * @return the formatted string, not null
    */
   public String toString(String format) {
      return String.format(format, getLeft(), getRight());
   }

}
