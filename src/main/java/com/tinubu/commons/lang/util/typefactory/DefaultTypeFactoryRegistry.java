/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util.typefactory;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.or;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;
import java.util.TreeMap;
import java.util.Vector;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.function.Supplier;

public class DefaultTypeFactoryRegistry extends ConcurrentHashMap<Class<?>, TypeFactory<?>>
      implements TypeFactoryRegistry {

   private static class SingletonHolder {
      private static final DefaultTypeFactoryRegistry INSTANCE = new DefaultTypeFactoryRegistry() {
         {
            registerTypeFactory(LinkedHashMap::new);
            registerTypeFactory(ConcurrentHashMap::new);
            registerTypeFactory(WeakHashMap::new);
            registerTypeFactory(new TypeFactory<TreeMap<?, ?>>() {
               @Override
               public TreeMap<?, ?> get() {
                  return new TreeMap<>();
               }

               @Override
               public TreeMap<?, ?> get(TreeMap<?, ?> instance) {
                  return new TreeMap<>(instance.comparator());
               }
            });
            registerTypeFactory(new TypeFactory<ConcurrentSkipListMap<?, ?>>() {
               @Override
               public ConcurrentSkipListMap<?, ?> get() {
                  return new ConcurrentSkipListMap<>();
               }

               @Override
               public ConcurrentSkipListMap<?, ?> get(ConcurrentSkipListMap<?, ?> instance) {
                  return new ConcurrentSkipListMap<>(instance.comparator());
               }
            });
            registerTypeFactory(Hashtable::new);
            registerTypeFactory(HashMap::new);

            registerTypeFactory(ArrayList::new);
            registerTypeFactory(LinkedList::new);
            registerTypeFactory(Stack::new);
            registerTypeFactory(Vector::new);
            registerTypeFactory(CopyOnWriteArrayList::new);
            registerTypeFactory(CopyOnWriteArraySet::new);
            registerTypeFactory(ArrayDeque::new);
            registerTypeFactory(HashSet::new);
            registerTypeFactory(LinkedHashSet::new);
            registerTypeFactory(ConcurrentLinkedDeque::new);
            registerTypeFactory(ConcurrentLinkedQueue::new);
            registerTypeFactory(ConcurrentSkipListSet::new);
            registerTypeFactory(DelayQueue::new);
            registerTypeFactory(LinkedBlockingQueue::new);
            registerTypeFactory(LinkedBlockingDeque::new);
            registerTypeFactory(LinkedTransferQueue::new);
            registerTypeFactory(PriorityBlockingQueue::new);
            registerTypeFactory(SynchronousQueue::new);
         }
      };
   }

   public static DefaultTypeFactoryRegistry instance() {
      return SingletonHolder.INSTANCE;
   }

   @Override
   public void resetRegistry() {
      clear();
   }

   @Override
   public <T> void registerTypeFactory(TypeFactory<T> factory) {
      notNull(factory, "factory");

      put(factory.factoryType(), factory);
   }

   @Override
   public <T> void registerTypeFactory(Supplier<T> factory) {
      notNull(factory, "factory");

      registerTypeFactory(TypeFactory.typeFactory(factory));
   }

   @Override
   @SuppressWarnings("unchecked")
   public <T> Optional<? extends TypeFactory<T>> typeFactory(Class<? extends T> factoryType) {
      notNull(factoryType, "factoryType");

      return nullable((TypeFactory<T>) get(factoryType));
   }

   /**
    * Best-compatible type factory for specified type.
    * Returned type must be registered into {@link DefaultTypeFactoryRegistry}. It can be a super-class of
    * specified
    * type, that is also registered into registry, but is always upper-bounded by specified upper bound type.
    * If no registered type is found for any type superclass, {@link Optional#empty} is returned.
    * You can {@link DefaultTypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own
    * factories.
    *
    * @param type class to create a factory for
    * @param <UB> upper bound type (e.g. {@link Collection} or {@link Map}) used for generic
    *       consistency across parameters
    *
    * @return factory for the same type
    */
   @SuppressWarnings("unchecked")
   public <UB> Optional<? extends TypeFactory<? extends UB>> compatibleTypeFactory(Class<UB> upperBound,
                                                                                   Class<? extends UB> type) {
      notNull(upperBound, "upperBound");
      notNull(type, "type");

      Optional<TypeFactory<? extends UB>> factory = (Optional<TypeFactory<? extends UB>>) typeFactory(type);

      return or(factory, () -> {

         Class<UB> superclass = (Class<UB>) type.getSuperclass();

         if (superclass == null || superclass.equals(upperBound)) {
            return optional();
         }

         return compatibleTypeFactory(upperBound, superclass);
      });
   }

   /**
    * Best-compatible type factory for specified type.
    * Returned type must be registered into {@link DefaultTypeFactoryRegistry}. It can be a super-class of
    * specified
    * type, that is also registered into registry.
    * If no registered type is found for any type superclass, {@link Optional#empty} is returned.
    * You can {@link DefaultTypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own
    * factories.
    *
    * @param type class to create a factory for
    *
    * @return factory for the same type
    */
   public Optional<? extends TypeFactory<?>> compatibleTypeFactory(Class<?> type) {
      return DefaultTypeFactoryRegistry.this.<Object>compatibleTypeFactory(Object.class, type);
   }

   /**
    * Best-compatible type factory for specified instance type. Specified instance content is not copied into
    * new
    * instance, only its characteristics.
    * Returned type must be registered into {@link DefaultTypeFactoryRegistry}. It can be a super-class of
    * specified
    * type, that is also registered into registry, but is always upper-bounded by specified upper bound type.
    * If no registered type is found for any type superclass, {@link Optional#empty} is returned.
    * You can {@link DefaultTypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own
    * factories.
    *
    * @param instance instance to create a factory for. Content is not copied, only characteristics if
    *       it's possible
    * @param <T> instance type to select factory type
    * @param <UB> upper bound type (e.g. {@link Collection} or {@link Map}) used for generic
    *       consistency across parameters
    *
    * @return factory for the same instance type
    */
   @SuppressWarnings("unchecked")
   public <UB, T extends UB> Optional<? extends TypeFactory<? extends UB>> compatibleTypeFactory(Class<UB> upperBound,
                                                                                                 T instance) {
      notNull(upperBound, "upperBound");
      notNull(instance, "instance");

      Optional<TypeFactory<UB>> factory = (Optional<TypeFactory<UB>>) typeFactory(instance.getClass());

      return or(factory.<TypeFactory<? extends UB>>map(f -> () -> f.get(instance)), () -> {

         Class<UB> superclass = (Class<UB>) instance.getClass().getSuperclass();

         if (superclass == null || superclass.equals(upperBound)) {
            return optional();
         }

         return compatibleTypeFactory(upperBound, superclass);
      });
   }

   /**
    * Best-compatible type factory for specified instance type. Specified instance content is not copied into
    * new instance, only its characteristics.
    * Returned type must be registered into {@link DefaultTypeFactoryRegistry}. It can be a super-class of
    * specified
    * type, that is also registered into registry.
    * If no registered type is found for any type superclass, {@link Optional#empty} is returned.
    * You can {@link DefaultTypeFactoryRegistry#registerTypeFactory(TypeFactory)} register} your own
    * factories.
    *
    * @param instance instance to create a factory for. Content is not copied, only characteristics if
    *       it's possible
    * @param <T> instance type to select factory type
    *
    * @return factory for the same instance type
    */
   public <T> Optional<? extends TypeFactory<?>> compatibleTypeFactory(T instance) {
      return compatibleTypeFactory(Object.class, instance);
   }

}
