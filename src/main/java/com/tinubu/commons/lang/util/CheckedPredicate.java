/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static java.util.Objects.requireNonNull;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Represents a predicate (boolean-valued function) of one argument.
 * This operation can throw checked exceptions.
 * <p>
 * This is a functional interface whose functional method is {@link #testChecked(Object)}.
 *
 * @param <T> the type of the input to the operation
 */
@FunctionalInterface
public interface CheckedPredicate<T> extends Predicate<T> {
   /**
    * Performs this operation on the given argument.
    *
    * @param t the input argument
    */
   @Override
   default boolean test(T t) {
      return uncheckedPredicate(this).test(t);
   }

   /**
    * Performs this operation on the given argument.
    * This operation can throw checked exceptions.
    *
    * @param t the input argument
    */
   boolean testChecked(T t) throws Exception;

   /**
    * Returns a checked predicate from a standard predicate.
    *
    * @param predicate predicate to adapt
    *
    * @return checked predicate
    */
   static <T> CheckedPredicate<T> checkedPredicate(Predicate<T> predicate) {
      requireNonNull(predicate);
      if (predicate instanceof CheckedPredicate) {
         return (CheckedPredicate<T>) predicate;
      } else {
         return predicate::test;
      }
   }

   /**
    * Returns an unchecked predicate from a checked predicate.
    *
    * @param predicate predicate to adapt
    *
    * @return unchecked predicate
    */
   static <T> Predicate<T> uncheckedPredicate(CheckedPredicate<T> predicate) {
      requireNonNull(predicate);
      return predicate.unchecked();
   }

   /**
    * Returns an unchecked predicate from this checked predicate.
    *
    * @return unchecked predicate
    */
   default Predicate<T> unchecked() {
      return t -> {
         try {
            return this.testChecked(t);
         } catch (Exception e) {
            throw ExceptionUtils.sneakyThrow(e);
         }
      };
   }

   @Override
   default CheckedPredicate<T> and(Predicate<? super T> other) {
      Objects.requireNonNull(other);
      return (t) -> test(t) && other.test(t);
   }

   @Override
   default CheckedPredicate<T> negate() {
      return (t) -> !test(t);
   }

   @Override
   default CheckedPredicate<T> or(Predicate<? super T> other) {
      Objects.requireNonNull(other);
      return (t) -> test(t) || other.test(t);
   }
}
