/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util.typefactory;

import java.util.Optional;
import java.util.function.Supplier;

public interface TypeFactoryRegistry {

   /**
    * Clears registry.
    */
   void resetRegistry();

   /**
    * Registers a new type factory.
    *
    * @param factory type factory
    */
   <T> void registerTypeFactory(TypeFactory<T> factory);

   /**
    * Registers a new type factory from a simple supplier.
    * Factory type is automatically generated once from supplied type.
    *
    * @param factory type factory as a supplier
    */
   <T> void registerTypeFactory(Supplier<T> factory);

   /**
    * Returns registered type factory for specified type.
    *
    * @param factoryType type factory registered class
    * @param <T> type factory type
    *
    * @return registered type factory for specified type or {@link Optional#empty} if no registered type
    *       factory found
    */
   <T> Optional<? extends TypeFactory<T>> typeFactory(Class<? extends T> factoryType);

}