/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CheckedSupplier.checkedSupplier;
import static com.tinubu.commons.lang.util.CollectionUtils.list;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

public final class ExceptionUtils {

   private ExceptionUtils() {
   }

   /**
    * Java 8 hack to disguise any checked exception into an unchecked exception. Note that {@link Exception}
    * are <em>not</em> transformed to {@link RuntimeException}.
    *
    * @param throwable throwable to disguise
    * @param <T> exception type
    *
    * @return (never return)
    *
    * @throws T as original exception
    */
   @SuppressWarnings("unchecked")
   public static <T extends Throwable> T sneakyThrow(Throwable throwable) throws T {
      throw (T) throwable;
   }

   /**
    * Rethrow any checked exception as an unchecked exception. {@link Exception}
    * are transformed to {@link RuntimeException} or {@link UncheckedIOException}; {@link Error} are left
    * unchanged.
    *
    * @param throwable throwable to wrap
    * @param <T> original exception type
    *
    * @return (never return)
    *
    * @throws T as best-fitting unchecked exception
    */
   public static <T extends Throwable> T runtimeThrow(Throwable throwable) throws T {
      if (throwable instanceof Error) {
         throw (Error) throwable;
      } else if (throwable instanceof RuntimeException) {
         throw (RuntimeException) throwable;
      } else if (throwable instanceof IOException) {
         UncheckedIOException uncheckedIOException =
               new UncheckedIOException(throwable.getMessage(), (IOException) throwable);

         for (Throwable suppressed : throwable.getSuppressed()) {
            uncheckedIOException.addSuppressed(suppressed);
         }

         throw uncheckedIOException;
      } else {
         RuntimeException runtimeException = new RuntimeException(throwable.getMessage(), throwable);

         for (Throwable suppressed : throwable.getSuppressed()) {
            runtimeException.addSuppressed(suppressed);
         }

         throw runtimeException;
      }
   }

   /**
    * Call a function, then ensure all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param supplier operation to execute
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws Exception is the first thrown exception, either by specified function, or one of the
    *       suppliers
    * @implNote The second catch when applying finalizers is not a {@link Throwable} because we should
    *       not ignore an {@link Error} and add it as suppressed.
    */
   public static <T> T smartFinalize(CheckedSupplier<T> supplier, CheckedRunnable... finalizers)
         throws Exception {
      return smartFinalize(supplier, list(finalizers));
   }

   /**
    * Call a function, then ensure all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param supplier operation to execute
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws Exception is the first thrown exception, either by specified function, or one of the
    *       suppliers
    * @implNote The second catch when applying finalizers is not a {@link Throwable} because we should
    *       not ignore an {@link Error} and add it as suppressed.
    */
   public static <T> T smartFinalize(CheckedSupplier<T> supplier, List<CheckedRunnable> finalizers)
         throws Exception {
      Throwable throwable = null;
      try {
         return supplier.getChecked();
      } catch (Throwable t) {
         throwable = t;
         throw t;
      } finally {
         for (CheckedRunnable r : finalizers) {
            try {
               r.runChecked();
            } catch (Throwable t) {
               if (throwable != null) {
                  throwable.addSuppressed(t);
               } else {
                  throwable = t;
               }
            }
         }
         if (throwable != null) {
            sneakyThrow(throwable);
         }
      }
   }

   /**
    * Call a function, then ensure all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param runnable operation to execute
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws Exception is the first thrown exception, either by specified function, or one of the
    *       suppliers
    * @implNote The second catch when applying finalizers is not a {@link Throwable} because we should
    *       not ignore an {@link Error} and add it as suppressed.
    */
   public static void smartFinalize(CheckedRunnable runnable, CheckedRunnable... finalizers)
         throws Exception {
      smartFinalize(checkedSupplier(runnable), finalizers);
   }

   /**
    * Call a function, then ensure all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param runnable operation to execute
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws Exception is the first thrown exception, either by specified function, or one of the
    *       suppliers
    * @implNote The second catch when applying finalizers is not a {@link Throwable} because we should
    *       not ignore an {@link Error} and add it as suppressed.
    */
   public static void smartFinalize(CheckedRunnable runnable, List<CheckedRunnable> finalizers)
         throws Exception {
      smartFinalize(checkedSupplier(runnable), finalizers);
   }

   /**
    * Rethrows an exception, ensuring all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param exception exception to rethrow
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws Exception is the first thrown exception, either by specified function, or one of the
    *       suppliers
    * @implNote The second catch when applying finalizers is not a {@link Throwable} because we should
    *       not ignore an {@link Error} and add it as suppressed.
    */
   public static <T> T smartFinalize(Exception exception, CheckedRunnable... finalizers) throws Exception {
      return smartFinalize(() -> {
         throw exception;
      }, list(finalizers));
   }

   /**
    * Rethrows an exception, ensuring all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param exception exception to rethrow
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws Exception is the first thrown exception, either by specified function, or one of the
    *       suppliers
    * @implNote The second catch when applying finalizers is not a {@link Throwable} because we should
    *       not ignore an {@link Error} and add it as suppressed.
    */
   public static <T> T smartFinalize(Exception exception, List<CheckedRunnable> finalizers) throws Exception {
      return smartFinalize(() -> {
         throw exception;
      }, finalizers);
   }

}
