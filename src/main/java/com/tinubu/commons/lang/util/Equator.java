/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.Serializable;
import java.util.Comparator;
import java.util.function.Function;

@FunctionalInterface
public interface Equator<T> {

   boolean equate(T o1, T o2);

   default Equator<T> thenEqualing(Equator<? super T> other) {
      notNull(other, "other");
      return (Equator<T> & Serializable) (c1, c2) -> {
         boolean res = equate(c1, c2);
         return !res ? res : other.equate(c1, c2);
      };
   }

   default <U> Equator<T> thenEqualing(Function<? super T, ? extends U> keyExtractor,
                                       Equator<? super U> keyEquator) {
      return thenEqualing(equaling(keyExtractor, keyEquator));
   }

   default <U> Equator<T> thenEqualing(Function<? super T, ? extends U> keyExtractor) {
      return thenEqualing(equaling(keyExtractor));
   }

   static <T> Equator<T> equaling(Comparator<T> comparator) {
      notNull(comparator, "comparator");
      return (o1, o2) -> comparator.compare(o1, o2) == 0;
   }

   static <T, U> Equator<T> equaling(Function<? super T, ? extends U> keyExtractor,
                                     Equator<? super U> keyEquator) {
      notNull(keyExtractor, "keyExtractor");
      notNull(keyEquator, "keyEquator");
      return (Equator<T> & Serializable) (c1, c2) -> keyEquator.equate(keyExtractor.apply(c1),
                                                                       keyExtractor.apply(c2));
   }

   static <T, U> Equator<T> equaling(Function<? super T, ? extends U> keyExtractor) {
      notNull(keyExtractor, "keyExtractor");
      return (Equator<T> & Serializable) (c1, c2) -> keyExtractor.apply(c1).equals(keyExtractor.apply(c2));
   }

}
