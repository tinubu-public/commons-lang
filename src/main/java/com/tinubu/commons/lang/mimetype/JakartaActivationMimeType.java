/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.activation.MimeTypeParameterList;
import javax.activation.MimeTypeParseException;

class JakartaActivationMimeType extends AbstractMimeType {

   private final javax.activation.MimeType delegate;
   private final Charset charset;
   private final Map<String, String> delegateParameters;

   public JakartaActivationMimeType(javax.activation.MimeType mimeType) {
      notNull(mimeType, "mimeType");

      this.delegate = mimeType;
      this.delegateParameters = delegateParameters(mimeType);
      this.charset = validateParameters(delegateParameters, "parameters");
   }

   public JakartaActivationMimeType(String type, String subtype, Map<String, String> parameters) {
      notBlank(type, "type");
      notBlank(subtype, "subtype");
      this.charset = validateParameters(parameters, "parameters");

      try {
         this.delegate = new javax.activation.MimeType(type, subtype);
         parameters.forEach(this.delegate::setParameter);

         this.delegateParameters = delegateParameters(this.delegate);
      } catch (MimeTypeParseException e) {
         throw new IllegalArgumentException(e);
      }

   }

   public JakartaActivationMimeType(MimeType mimeType, Map<String, String> parameters) {
      this(notNull(mimeType, "mimeType").type(), mimeType.subtype(), parameters);
   }

   public JakartaActivationMimeType(MimeType mimeType) {
      this(mimeType, notNull(mimeType, "mimeType").parameters());
   }

   public JakartaActivationMimeType(MimeType mimeType, Charset charset) {
      this(mimeType, map(entry(CHARSET_PARAMETER, notNull(charset, "charset").name())));
   }

   public JakartaActivationMimeType(String type) {
      this(type, WILDCARD);
   }

   public JakartaActivationMimeType(String type, String subtype) {
      this(type, subtype, map());
   }

   public JakartaActivationMimeType(String type, String subtype, Charset charset) {
      this(type, subtype, map(entry(CHARSET_PARAMETER, notNull(charset, "charset").name())));
   }

   public static MimeType parseMimeType(String mimeType) {
      notBlank(mimeType, "mimeType");
      try {
         return new JakartaActivationMimeType(new javax.activation.MimeType(mimeType));
      } catch (MimeTypeParseException e) {
         throw new IllegalArgumentException(e);
      }
   }

   @Override
   public String type() {
      return delegate.getPrimaryType();
   }

   @Override
   public String subtype() {
      return delegate.getSubType();
   }

   @Override
   public MimeType strippedParameters() {
      return new JakartaActivationMimeType(this, map());
   }

   @Override
   public MimeType strippedExperimental() {
      return new JakartaActivationMimeType(type(), removeExperimentalPrefix(subtype()), parameters());
   }

   @Override
   public Map<String, String> parameters() {
      return delegateParameters;
   }

   @Override
   public Optional<String> parameter(String parameter) {
      notBlank(parameter, "parameter");

      return nullable(delegateParameters.get(caseInsensitive(parameter)));
   }

   @Override
   public Optional<Charset> charset() {
      return nullable(charset);
   }

   @Override
   public boolean includes(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      return (wildcardType() || type().equalsIgnoreCase(mimeType.type())) && (wildcardSubtype()
                                                                              || subtype().equalsIgnoreCase(
            mimeType.subtype()));
   }

   @Override
   public boolean matches(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      return (wildcardType() || mimeType.wildcardType() || type().equalsIgnoreCase(mimeType.type())) && (
            wildcardSubtype()
            || mimeType.wildcardSubtype()
            || subtype().equalsIgnoreCase(mimeType.subtype()));
   }

   @Override
   public boolean equalsTypeAndSubtype(MimeType mimeType) {
      if (mimeType == null) {
         return false;
      }
      return this.type().equalsIgnoreCase(mimeType.type()) && this
            .subtype()
            .equalsIgnoreCase(mimeType.subtype());
   }

   @Override
   public boolean wildcardType() {
      return type().equals(WILDCARD);
   }

   @Override
   public boolean wildcardSubtype() {
      return subtype().equals(WILDCARD);
   }

   @Override
   public String toString() {
      return super.toString();
   }

   private static String caseInsensitive(String parameter) {
      return parameter.toLowerCase(Locale.ENGLISH);
   }

   @SuppressWarnings("unchecked")
   private static Map<String, String> delegateParameters(javax.activation.MimeType mimeType) {
      MimeTypeParameterList parameters = mimeType.getParameters();

      return immutable(map(() -> new LinkedHashMap<>(),
                           stream((Enumeration<String>) parameters.getNames()).map(name -> entry(
                                 caseInsensitive(name),
                                 parameters.get(name)))));
   }

}
