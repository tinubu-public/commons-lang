/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype.registry;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import com.tinubu.commons.lang.mimetype.MimeType;

/**
 * Registry of MIME types with associated document extensions.
 */
public interface MimeTypeRegistry {

   /**
    * Detects MIME type for specified document path.
    * Implementation should not rely on a physical storage to detect the MIME type, such as read
    * the document content from filesystem, because a document is an abstract representation.
    *
    * @param documentPath document full path
    *
    * @return document MIME type or {@link Optional#empty} if detection failed
    *
    * @apiNote Detection is case-insensitive.
    */
   Optional<MimeType> mimeType(Path documentPath);

   /**
    * Detects MIME type for specified document name.
    * Implementation should not rely on a physical storage to detect the MIME type, such as read
    * the document content from filesystem, because a document is an abstract representation.
    *
    * @param documentName document name, without path
    *
    * @return document MIME type or {@link Optional#empty} if detection failed
    *
    * @apiNote Detection is case-insensitive.
    */
   default Optional<MimeType> mimeType(String documentName) {
      return mimeType(Paths.get(notNull(documentName, "documentName")));
   }

   /**
    * Returns all MIME types associated to specified extension if any.
    * Primary MIME type is returned first in list.
    *
    * @param documentPath document full path
    *
    * @return MIME types, or empty list
    */
   List<MimeType> mimeTypes(Path documentPath);

   /**
    * Returns all MIME types associated to specified extension if any.
    * Primary MIME type is returned first in list.
    *
    * @param documentName document name, without path
    *
    * @return MIME types, or empty list
    */
   default List<MimeType> mimeTypes(String documentName) {
      return mimeTypes(Paths.get(notNull(documentName, "documentName")));
   }

   /**
    * Returns "primary" extension associated to specified MIME type if any.
    *
    * @param mimeType MIME type to match
    *
    * @return extension, in lower-case, without '.', or {@link Optional#empty}
    *
    * @implSpec specified MIME type parameter should not be taken into account
    */
   Optional<String> extension(MimeType mimeType);

   /**
    * Returns all extensions associated to specified MIME type if any.
    * Primary extension is returned first in list.
    *
    * @param mimeType MIME type to match
    *
    * @return extensions, in lower-case, without '.', or empty list
    *
    * @implSpec specified MIME type parameter should not be taken into account
    */
   List<String> extensions(MimeType mimeType);

}
