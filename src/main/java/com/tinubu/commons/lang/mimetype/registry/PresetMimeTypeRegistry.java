/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype.registry;

import static com.tinubu.commons.lang.mimetype.registry.ConfigurableMimeTypeRegistry.MimeTypeEntry.of;

import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.MimeTypeFactory;

/**
 * A preset library of mime-types.
 *
 * @implSpec Experimental subtypes are disabled, and automatically resolved with {@code x-} prefix
 *       stripped.
 */
public class PresetMimeTypeRegistry extends ConfigurableMimeTypeRegistry {

   public static final MimeType ANY = MimeTypeFactory.mimeType("*", "*");

   public static final MimeType MULTIPART_ANY = MimeTypeFactory.mimeType("multipart", "*");
   public static final MimeType MULTIPART_FORM_DATA = MimeTypeFactory.mimeType("multipart", "form-data");
   public static final MimeType MULTIPART_MIXED = MimeTypeFactory.mimeType("multipart", "mixed");
   public static final MimeType MULTIPART_RELATED = MimeTypeFactory.mimeType("multipart", "related");
   public static final MimeType MULTIPART_ALTERNATIVE = MimeTypeFactory.mimeType("multipart", "alternative");

   public static final MimeType TEXT_ANY = MimeTypeFactory.mimeType("text", "*");
   public static final MimeType TEXT_PLAIN = MimeTypeFactory.mimeType("text", "plain");
   public static final MimeType TEXT_CSS = MimeTypeFactory.mimeType("text", "css");
   public static final MimeType TEXT_CSV = MimeTypeFactory.mimeType("text", "csv");
   public static final MimeType TEXT_HTML = MimeTypeFactory.mimeType("text", "html");
   public static final MimeType TEXT_ASCIIDOC = MimeTypeFactory.mimeType("text", "asciidoc");
   public static final MimeType TEXT_MARKDOWN = MimeTypeFactory.mimeType("text", "markdown");
   public static final MimeType TEXT_FTL = MimeTypeFactory.mimeType("text", "ftl");
   public static final MimeType TEXT_SHELLSCRIPT = MimeTypeFactory.mimeType("text", "shellscript");

   public static final MimeType IMAGE_ANY = MimeTypeFactory.mimeType("image", "*");
   public static final MimeType IMAGE_JPEG = MimeTypeFactory.mimeType("image", "jpeg");
   public static final MimeType IMAGE_PNG = MimeTypeFactory.mimeType("image", "png");
   public static final MimeType IMAGE_SVG = MimeTypeFactory.mimeType("image", "svg+xml");
   public static final MimeType IMAGE_WMF = MimeTypeFactory.mimeType("image", "wmf");

   public static final MimeType APPLICATION_ANY = MimeTypeFactory.mimeType("application", "*");
   public static final MimeType APPLICATION_OCTET_STREAM =
         MimeTypeFactory.mimeType("application", "octet-stream");
   public static final MimeType APPLICATION_XHTML = MimeTypeFactory.mimeType("application", "xhtml+xml");
   public static final MimeType APPLICATION_PDF = MimeTypeFactory.mimeType("application", "pdf");
   public static final MimeType APPLICATION_RTF = MimeTypeFactory.mimeType("application", "rtf");
   public static final MimeType APPLICATION_JSON = MimeTypeFactory.mimeType("application", "json");
   public static final MimeType APPLICATION_XML = MimeTypeFactory.mimeType("application", "xml");
   public static final MimeType APPLICATION_ZIP = MimeTypeFactory.mimeType("application", "zip");
   public static final MimeType APPLICATION_GZIP = MimeTypeFactory.mimeType("application", "gzip");
   public static final MimeType APPLICATION_DOCBOOK = MimeTypeFactory.mimeType("application", "docbook+xml");
   public static final MimeType APPLICATION_JAVASCRIPT =
         MimeTypeFactory.mimeType("application", "javascript");
   public static final MimeType APPLICATION_YAML = MimeTypeFactory.mimeType("application", "yaml");
   public static final MimeType APPLICATION_BZIP = MimeTypeFactory.mimeType("application", "bzip");
   public static final MimeType APPLICATION_BZIP2 = MimeTypeFactory.mimeType("application", "bzip2");
   public static final MimeType APPLICATION_TAR = MimeTypeFactory.mimeType("application", "tar");
   public static final MimeType APPLICATION_JAVA_ARCHIVE =
         MimeTypeFactory.mimeType("application", "java-archive");

   public static final MimeType APPLICATION_ODT =
         MimeTypeFactory.mimeType("application", "vnd.oasis.opendocument.text");
   public static final MimeType APPLICATION_ODS =
         MimeTypeFactory.mimeType("application", "vnd.oasis.opendocument.spreadsheet");

   /** Microsoft Visio */
   public static final MimeType APPLICATION_VISIO_VSD = MimeTypeFactory.mimeType("application", "vnd.visio");

   /** Microsoft Word */
   public static final MimeType APPLICATION_MSWORD_DOC = MimeTypeFactory.mimeType("application", "msword");
   /** Microsoft Word (with macros) */
   public static final MimeType APPLICATION_MSWORD_DOCM =
         MimeTypeFactory.mimeType("application", "vnd.ms-word.document.macroenabled.12");
   /** Microsoft Word template (with macros) */
   public static final MimeType APPLICATION_MSWORD_DOTM =
         MimeTypeFactory.mimeType("application", "vnd.ms-word.template.macroenabled.12");

   /** OOXML Document */
   public static final MimeType APPLICATION_OOXML_DOCX = MimeTypeFactory.mimeType("application",
                                                                                  "vnd.openxmlformats-officedocument.wordprocessingml.document");
   /** OOXML Document template */
   public static final MimeType APPLICATION_OOXML_DOTX = MimeTypeFactory.mimeType("application",
                                                                                  "vnd.openxmlformats-officedocument.wordprocessingml.template");

   /** Microsoft Powerpoint */
   public static final MimeType APPLICATION_POWERPOINT_PPT =
         MimeTypeFactory.mimeType("application", "vnd.ms-powerpoint");
   /** Microsoft Powerpoint (with macros) */
   public static final MimeType APPLICATION_POWERPOINT_PPTM =
         MimeTypeFactory.mimeType("application", "vnd.ms-powerpoint.presentation.macroenabled.12");
   /** Microsoft Powerpoint template (with macros) */
   public static final MimeType APPLICATION_POWERPOINT_POTM =
         MimeTypeFactory.mimeType("application", "vnd.ms-powerpoint.template.macroEnabled.12");

   /** OOXML Presentation */
   public static final MimeType APPLICATION_OOXML_PPTX = MimeTypeFactory.mimeType("application",
                                                                                  "vnd.openxmlformats-officedocument.presentationml.presentation");
   /** OOXML Presentation template */
   public static final MimeType APPLICATION_OOXML_POTX =
         MimeTypeFactory.mimeType("application", "vnd.openxmlformats-officedocument.presentationml.template");

   /** Microsoft Excel */
   public static final MimeType APPLICATION_EXCEL_XLS =
         MimeTypeFactory.mimeType("application", "vnd.ms-excel");
   /** Microsoft Excel (with macros) */
   public static final MimeType APPLICATION_EXCEL_XLSM =
         MimeTypeFactory.mimeType("application", "vnd.ms-excel.sheet.macroenabled.12");
   /** Microsoft Excel template (with macros) */
   public static final MimeType APPLICATION_EXCEL_XLTM =
         MimeTypeFactory.mimeType("application", "vnd.ms-excel.template.macroEnabled.12");

   /** OOXML Spreadsheet */
   public static final MimeType APPLICATION_OOXML_XLSX =
         MimeTypeFactory.mimeType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet");
   /** OOXML Spreadsheet template */
   public static final MimeType APPLICATION_OOXML_XLTX =
         MimeTypeFactory.mimeType("application", "vnd.openxmlformats-officedocument.spreadsheetml.template");

   private PresetMimeTypeRegistry() {
      super(false);
   }

   public static PresetMimeTypeRegistry ofPresets() {
      return SingletonHolder.PRESET_REGISTRY;
   }

   private static class SingletonHolder {
      final static PresetMimeTypeRegistry PRESET_REGISTRY = new PresetMimeTypeRegistry() {{

         entry(of(MULTIPART_FORM_DATA));
         entry(of(MULTIPART_MIXED));
         entry(of(MULTIPART_RELATED));
         entry(of(MULTIPART_ALTERNATIVE));

         entry(of(TEXT_PLAIN, "txt"));
         entry(of(TEXT_CSS, "css"));
         entry(of(TEXT_CSV, "csv"));
         entry(of(TEXT_HTML, "html", "htm"));
         entry(of(TEXT_ASCIIDOC, "adoc", "asc", "asciidoc"));
         entry(of(TEXT_MARKDOWN, "md", "markdown"));
         entry(of(TEXT_FTL, "ftl", "ftlh", "ftlx"));
         entry(of(TEXT_SHELLSCRIPT, "sh"));

         entry(of(IMAGE_JPEG, "jpg", "jpeg"));
         entry(of(IMAGE_PNG, "png"));
         entry(of(IMAGE_SVG, "svg"));
         entry(of(IMAGE_WMF, "wmf"));

         entry(of(APPLICATION_OCTET_STREAM));
         entry(of(APPLICATION_XHTML, "xhtml", "xht"));
         entry(of(APPLICATION_PDF, "pdf"));
         entry(of(APPLICATION_RTF, "rtf"));
         entry(of(APPLICATION_JSON, "json"));
         entry(of(APPLICATION_XML, "xml"));
         entry(of(APPLICATION_ZIP, "zip"));
         entry(of(APPLICATION_GZIP, "gz", "gzip"));
         entry(of(APPLICATION_DOCBOOK, "dbk", "docbook"));
         entry(of(APPLICATION_JAVASCRIPT, "js"));
         entry(of(APPLICATION_YAML, "yml", "yaml"));
         entry(of(APPLICATION_BZIP, "bz"));
         entry(of(APPLICATION_BZIP2, "bz2"));
         entry(of(APPLICATION_TAR, "tar"));
         entry(of(APPLICATION_JAVA_ARCHIVE, "jar", "war", "ear"));

         entry(of(APPLICATION_ODT, "odt"));
         entry(of(APPLICATION_ODS, "ods"));

         entry(of(APPLICATION_VISIO_VSD, "vsd"));

         entry(of(APPLICATION_MSWORD_DOC, "doc", "dot"));
         entry(of(APPLICATION_MSWORD_DOCM, "docm"));
         entry(of(APPLICATION_MSWORD_DOTM, "dotm"));
         entry(of(APPLICATION_OOXML_DOCX, "docx"));
         entry(of(APPLICATION_OOXML_DOTX, "dotx"));

         entry(of(APPLICATION_POWERPOINT_PPT, "ppt", "pot"));
         entry(of(APPLICATION_POWERPOINT_PPTM, "pptm"));
         entry(of(APPLICATION_POWERPOINT_POTM, "potm"));
         entry(of(APPLICATION_OOXML_PPTX, "pptx"));
         entry(of(APPLICATION_OOXML_POTX, "potx"));

         entry(of(APPLICATION_EXCEL_XLS, "xls", "xlt"));
         entry(of(APPLICATION_EXCEL_XLSM, "xlsm"));
         entry(of(APPLICATION_EXCEL_XLTM, "xltm"));
         entry(of(APPLICATION_OOXML_XLSX, "xlsx"));
         entry(of(APPLICATION_OOXML_XLTX, "xltx"));

         indexEntries();
      }};
   }
}
