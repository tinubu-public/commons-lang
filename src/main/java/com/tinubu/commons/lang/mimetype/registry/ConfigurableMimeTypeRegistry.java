/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype.registry;

import static com.tinubu.commons.lang.io.FileUtils.fileExtension;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalPredicate;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;

import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.MimeTypeFactory;

/**
 * Configurable MIME type registry.
 * Each registry entry should contain all MIME types and associated extensions. No overrides are accepted
 * between entries for both MIME types and extensions.
 * When all entries are set, you should call {@link #indexEntries()} before calling any operation, otherwise
 * entries will be indexed at first call.
 */
public class ConfigurableMimeTypeRegistry implements MimeTypeRegistry {

   protected final List<MimeTypeEntry> entries;
   protected boolean indexed = false;
   protected final boolean supportExperimental;

   protected final Map<String, List<MimeType>> mimeTypes = new ConcurrentHashMap<>();
   protected final Map<MimeType, List<String>> extensions = new ConcurrentHashMap<>();

   protected ConfigurableMimeTypeRegistry(List<MimeTypeEntry> entries, boolean supportExperimental) {
      this.entries = notNull(entries, "entries");
      this.supportExperimental = supportExperimental;

      indexEntries();
   }

   protected ConfigurableMimeTypeRegistry(boolean supportExperimental) {
      this(list(), supportExperimental);
   }

   public static ConfigurableMimeTypeRegistry ofEmpty(boolean supportExperimental) {
      return new ConfigurableMimeTypeRegistry(list(), supportExperimental);
   }

   public static ConfigurableMimeTypeRegistry ofEntries(List<MimeTypeEntry> entries,
                                                        boolean supportExperimental) {
      return new ConfigurableMimeTypeRegistry(entries, supportExperimental);
   }

   public static ConfigurableMimeTypeRegistry ofEmpty() {
      return ofEmpty(false);
   }

   public static ConfigurableMimeTypeRegistry ofEntries(List<MimeTypeEntry> entries) {
      return ofEntries(entries, false);
   }

   public ConfigurableMimeTypeRegistry entry(MimeTypeEntry entry) {
      notNull(entry, "entry");

      this.entries.add(entry);
      indexed = false;

      return this;
   }

   public ConfigurableMimeTypeRegistry indexEntries() {
      extensions.clear();
      mimeTypes.clear();
      entries.forEach(entry -> {
         List<String> entryExtensions = list(entry.extensions());
         List<MimeType> entryMimeTypes = list(entry.mimeTypes());

         indexExtensions(entryExtensions, entryMimeTypes);
         indexMimeTypes(entryExtensions, entryMimeTypes);
      });

      indexed = true;

      return this;
   }

   @Override
   public Optional<MimeType> mimeType(Path documentPath) {
      notNull(documentPath, "documentPath");
      ensureIndexed();

      String extension = fileExtension(documentPath.toString());

      return optionalPredicate(extension, ext -> !ext.isEmpty())
            .flatMap(ext -> nullable(mimeTypes.get(normalizeExtension(ext))))
            .map(this::primaryMimeType);
   }

   @Override
   public List<MimeType> mimeTypes(Path documentPath) {
      notNull(documentPath, "documentPath");
      ensureIndexed();

      String extension = fileExtension(documentPath.toString());

      return optionalPredicate(extension, ext -> !ext.isEmpty())
            .flatMap(ext -> nullable(mimeTypes.get(normalizeExtension(ext))))
            .orElse(emptyList());
   }

   @Override
   public Optional<String> extension(MimeType mimeType) {
      notNull(mimeType, "mimeType");
      ensureIndexed();

      return nullable(extensions.get(normalizeExperimental(normalizeMimeType(mimeType)))).flatMap(extensions -> optionalPredicate(
            extensions,
            hasExtensions()).map(this::primaryExtension));
   }

   @Override
   public List<String> extensions(MimeType mimeType) {
      notNull(mimeType, "mimeType");
      ensureIndexed();

      return nullable(extensions.get(normalizeExperimental(normalizeMimeType(mimeType)))).orElse(emptyList());
   }

   protected void indexMimeTypes(List<String> entryExtensions, List<MimeType> entryMimeTypes) {
      Map<String, List<MimeType>> mimeTypes = map(LinkedHashMap::new);
      stream(entryExtensions).forEach(extension -> {
         if (this.mimeTypes.containsKey(extension)) {
            throw new IllegalStateException(String.format("'%s' extension is already registered", extension));
         }

         mimeTypes.put(extension, list(stream(entryMimeTypes).map(this::normalizeExperimental).distinct()));
      });
      this.mimeTypes.putAll(mimeTypes);
   }

   protected void indexExtensions(List<String> entryExtensions, List<MimeType> entryMimeTypes) {
      Map<MimeType, List<String>> extensions = map(LinkedHashMap::new);
      stream(entryMimeTypes).forEach(mimeType -> {
         mimeType = normalizeExperimental(mimeType);

         if (this.extensions.containsKey(mimeType)) {
            throw new IllegalStateException(String.format("'%s' MIME type is already registered", mimeType));
         }
         extensions.put(mimeType, entryExtensions);
      });
      this.extensions.putAll(extensions);
   }

   protected String primaryExtension(List<String> extensions) {
      return extensions.get(0);
   }

   protected MimeType primaryMimeType(List<MimeType> mimeTypes) {
      return mimeTypes.get(0);
   }

   protected Predicate<List<String>> hasExtensions() {
      return extensions -> extensions.size() > 0;
   }

   protected MimeType normalizeExperimental(MimeType mimeType) {
      return supportExperimental ? mimeType : mimeType.strippedExperimental();
   }

   /**
    * Ensures entries are indexed.
    */
   protected void ensureIndexed() {
      if (!indexed) {
         indexEntries();
      }
   }

   protected static String normalizeExtension(String mimeType) {
      if (mimeType == null) {
         return null;
      }
      return mimeType.trim().toUpperCase().toLowerCase();
   }

   protected static MimeType normalizeMimeType(MimeType mimeType) {
      if (mimeType == null) {
         return null;
      }
      return mimeType.strippedParameters();
   }

   public static class MimeTypeEntry {
      private final MimeType primaryMimeType;
      private final List<MimeType> aliasMimeTypes;
      private final String primaryExtension;
      private final List<String> aliasExtensions;

      private MimeTypeEntry(MimeType primaryMimeType, String primaryExtension) {
         this.primaryMimeType = normalizeMimeType(notNull(primaryMimeType, "primaryMimeType"));
         this.primaryExtension =
               normalizeExtension(nullable(primaryExtension, ext -> notBlank(ext, "primaryExtension")));
         this.aliasMimeTypes = list();
         this.aliasExtensions = list();
      }

      public static MimeTypeEntry of(MimeType mimeType) {
         return new MimeTypeEntry(mimeType, null);
      }

      public static MimeTypeEntry of(MimeType mimeType, String primaryExtension) {
         return new MimeTypeEntry(mimeType, primaryExtension);
      }

      public static MimeTypeEntry of(MimeType mimeType, String primaryExtension, String... aliasExtensions) {
         return new MimeTypeEntry(mimeType, primaryExtension).aliasExtensions(aliasExtensions);
      }

      public static MimeTypeEntry of(String type, String subtype) {
         return of(MimeTypeFactory.mimeType(type, subtype));
      }

      public static MimeTypeEntry of(String type, String subtype, String primaryExtension) {
         return of(MimeTypeFactory.mimeType(type, subtype), primaryExtension);
      }

      public static MimeTypeEntry of(String type,
                                     String subtype,
                                     String primaryExtension,
                                     String... aliasExtensions) {
         return of(MimeTypeFactory.mimeType(type, subtype), primaryExtension, aliasExtensions);
      }

      public static MimeTypeEntry application(String subtype,
                                              String primaryExtension,
                                              String... aliasExtensions) {
         return of(MimeTypeFactory.mimeType("application", subtype), primaryExtension, aliasExtensions);
      }

      public static MimeTypeEntry application(String subtype) {
         return application(subtype, null);
      }

      public static MimeTypeEntry image(String subtype, String primaryExtension, String... aliasExtensions) {
         return of(MimeTypeFactory.mimeType("image", subtype), primaryExtension, aliasExtensions);
      }

      public static MimeTypeEntry image(String subtype) {
         return image(subtype, null);
      }

      public static MimeTypeEntry text(String subtype, String primaryExtension, String... aliasExtensions) {
         return of(MimeTypeFactory.mimeType("text", subtype), primaryExtension, aliasExtensions);
      }

      public static MimeTypeEntry text(String subtype) {
         return text(subtype, null);
      }

      public MimeType primaryMimeType() {
         return primaryMimeType;
      }

      public List<MimeType> aliasMimeTypes() {
         return aliasMimeTypes;
      }

      public Stream<MimeType> mimeTypes() {
         return streamConcat(stream(primaryMimeType), stream(aliasMimeTypes));
      }

      public Optional<String> primaryExtension() {
         return nullable(primaryExtension);
      }

      public List<String> aliasExtensions() {
         return aliasExtensions;
      }

      public Stream<String> extensions() {
         return streamConcat(stream(nullable(primaryExtension)), stream(aliasExtensions));
      }

      public MimeTypeEntry aliasMimeTypes(MimeType... mimeTypes) {
         this.aliasMimeTypes.addAll(list(stream(mimeTypes).map(ConfigurableMimeTypeRegistry::normalizeMimeType)));
         return this;
      }

      public MimeTypeEntry aliasMimeTypes(List<MimeType> mimeTypes) {
         this.aliasMimeTypes.addAll(list(stream(mimeTypes).map(ConfigurableMimeTypeRegistry::normalizeMimeType)));
         return this;
      }

      public MimeTypeEntry aliasExtensions(String... extensions) {
         this.aliasExtensions.addAll(list(stream(extensions).map(ConfigurableMimeTypeRegistry::normalizeExtension)));
         return this;
      }

      public MimeTypeEntry aliasExtensions(List<String> extensions) {
         this.aliasExtensions.addAll(list(stream(extensions).map(ConfigurableMimeTypeRegistry::normalizeExtension)));
         return this;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (!(o instanceof MimeTypeEntry)) return false;
         MimeTypeEntry that = (MimeTypeEntry) o;
         return Objects.equals(primaryMimeType, that.primaryMimeType)
                && Objects.equals(aliasMimeTypes,
                                  that.aliasMimeTypes)
                && Objects.equals(primaryExtension, that.primaryExtension)
                && Objects.equals(aliasExtensions, that.aliasExtensions);
      }

      @Override
      public int hashCode() {
         return Objects.hash(primaryMimeType, aliasMimeTypes, primaryExtension, aliasExtensions);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", MimeTypeEntry.class.getSimpleName() + "[", "]")
               .add("primaryMimeType=" + primaryMimeType)
               .add("aliasMimeTypes=" + aliasMimeTypes)
               .add("primaryExtension='" + primaryExtension + "'")
               .add("aliasExtensions=" + aliasExtensions)
               .toString();
      }

   }
}
