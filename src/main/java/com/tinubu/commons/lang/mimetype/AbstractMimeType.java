/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicReference;

abstract class AbstractMimeType implements MimeType {
   protected static final String CHARSET_PARAMETER = "charset";
   protected static final String WILDCARD = "*";
   protected static final String EXPERIMENTAL_PREFIX = "x-";

   private String toString;

   protected Charset validateParameters(Map<String, String> parameters, String name) {
      notNull(parameters, name);

      AtomicReference<Charset> charset = new AtomicReference<>();
      parameters.forEach((parameter, value) -> {
         notBlank(parameter, name + "{" + parameter + "}");
         notBlank(value, name + "{" + parameter + "}.value");
         if (parameter.equalsIgnoreCase(CHARSET_PARAMETER)) {
            charset.set(Charset.forName(value));
         }
      });

      return charset.get();
   }

   /**
    * @implNote We assume mime-type characters are alphanumerical.
    */
   protected static String removeExperimentalPrefix(String str) {
      return str != null && str
            .toLowerCase(Locale.ENGLISH)
            .startsWith(EXPERIMENTAL_PREFIX.toLowerCase(Locale.ENGLISH))
             ? str.substring(EXPERIMENTAL_PREFIX.length())
             : str;
   }

   @Override
   public int hashCode() {
      return Objects.hash(type(), subtype(), parameters());
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) return true;
      if (!(other instanceof MimeType)) return false;
      MimeType that = (MimeType) other;
      return type().equalsIgnoreCase(that.type())
             && subtype().equalsIgnoreCase(that.subtype())
             && Objects.equals(parameters(), that.parameters());
   }

   @Override
   public String toString() {
      if (toString == null) {
         StringBuilder sb = new StringBuilder();
         sb.append(type());
         sb.append("/");
         sb.append(subtype());
         parameters().forEach((key, val) -> {
            sb.append(';');
            sb.append(key);
            sb.append('=');
            if (key.equals(CHARSET_PARAMETER)) {
               sb.append(charset().map(Charset::name).orElseThrow(IllegalStateException::new));
            } else {
               sb.append(val);
            }
         });
         toString = sb.toString();
      }
      return toString;
   }

   @Override
   public int compareTo(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      int comp = type().compareToIgnoreCase(mimeType.type());
      if (comp != 0) {
         return comp;
      }
      comp = subtype().compareToIgnoreCase(mimeType.subtype());
      if (comp != 0) {
         return comp;
      }
      Map<String, String> parameters = parameters();
      Map<String, String> otherParameters = mimeType.parameters();

      comp = parameters.size() - otherParameters.size();
      if (comp != 0) {
         return comp;
      }

      TreeSet<String> thisAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
      thisAttributes.addAll(parameters.keySet());
      TreeSet<String> otherAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
      otherAttributes.addAll(otherParameters.keySet());
      Iterator<String> thisAttributesIterator = thisAttributes.iterator();
      Iterator<String> otherAttributesIterator = otherAttributes.iterator();

      while (thisAttributesIterator.hasNext()) {
         String thisAttribute = thisAttributesIterator.next();
         String otherAttribute = otherAttributesIterator.next();
         comp = thisAttribute.compareToIgnoreCase(otherAttribute);
         if (comp != 0) {
            return comp;
         }
         if (CHARSET_PARAMETER.equals(thisAttribute)) {
            Charset thisCharset = charset().orElse(null);
            Charset otherCharset = mimeType.charset().orElse(null);
            if (thisCharset != otherCharset) {
               if (thisCharset == null) {
                  return -1;
               }
               if (otherCharset == null) {
                  return 1;
               }
               comp = thisCharset.compareTo(otherCharset);
               if (comp != 0) {
                  return comp;
               }
            }
         } else {
            String thisValue = parameters.get(thisAttribute);
            String otherValue = otherParameters.get(otherAttribute);
            if (otherValue == null) {
               otherValue = "";
            }
            comp = thisValue.compareTo(otherValue);
            if (comp != 0) {
               return comp;
            }
         }
      }

      return 0;
   }

   private boolean quotedString(String s) {
      if (s.length() < 2) {
         return false;
      } else {
         return ((s.startsWith("\"") && s.endsWith("\"")) || (s.startsWith("'") && s.endsWith("'")));
      }
   }

   protected String unquote(String s) {
      return (quotedString(s) ? s.substring(1, s.length() - 1) : s);
   }

}
