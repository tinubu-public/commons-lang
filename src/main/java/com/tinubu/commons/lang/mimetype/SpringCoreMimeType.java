/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.entryStream;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.springframework.util.InvalidMimeTypeException;

class SpringCoreMimeType extends AbstractMimeType {

   private final org.springframework.util.MimeType delegate;

   public SpringCoreMimeType(org.springframework.util.MimeType mimeType) {
      notNull(mimeType, "mimeType");
      validateParameters(mimeType.getParameters(), "parameters");

      this.delegate =
            new org.springframework.util.MimeType(mimeType, normalizeParameters(mimeType.getParameters()));
   }

   public SpringCoreMimeType(String type, String subtype, Map<String, String> parameters) {
      notBlank(type, "type");
      notBlank(subtype, "subtype");
      validateParameters(parameters, "parameters");

      this.delegate = new org.springframework.util.MimeType(type, subtype, normalizeParameters(parameters));
   }

   public SpringCoreMimeType(String type, String subtype, Charset charset) {
      notBlank(type, "type");
      notBlank(subtype, "subtype");
      notNull(charset, "charset");

      this.delegate = new org.springframework.util.MimeType(type, subtype, charset);
   }

   public SpringCoreMimeType(MimeType mimeType, Map<String, String> parameters) {
      this(notNull(mimeType, "mimeType").type(), mimeType.subtype(), parameters);
   }

   public SpringCoreMimeType(MimeType mimeType, Charset charset) {
      this(notNull(mimeType, "mimeType").type(), mimeType.subtype(), charset);
   }

   public SpringCoreMimeType(MimeType mimeType) {
      this(mimeType, notNull(mimeType, "mimeType").parameters());
   }

   public SpringCoreMimeType(String type) {
      this(type, WILDCARD);
   }

   public SpringCoreMimeType(String type, String subtype) {
      this(type, subtype, map());
   }

   public static MimeType parseMimeType(String mimeType) {
      notBlank(mimeType, "mimeType");
      try {
         return new SpringCoreMimeType(org.springframework.util.MimeType.valueOf(mimeType));
      } catch (InvalidMimeTypeException e) {
         throw new IllegalArgumentException(e);
      }
   }

   @Override
   public String type() {
      return delegate.getType();
   }

   @Override
   public String subtype() {
      return delegate.getSubtype();
   }

   @Override
   public MimeType strippedParameters() {
      return new SpringCoreMimeType(this, map());
   }

   @Override
   public MimeType strippedExperimental() {
      return new SpringCoreMimeType(type(), removeExperimentalPrefix(subtype()), parameters());
   }

   @Override
   public Map<String, String> parameters() {
      return immutable(delegate.getParameters());
   }

   @Override
   public Optional<String> parameter(String parameter) {
      notBlank(parameter, "parameter");

      return nullable(delegate.getParameter(parameter));
   }

   @Override
   public Optional<Charset> charset() {
      return nullable(delegate.getCharset());
   }

   @Override
   public boolean includes(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      org.springframework.util.MimeType mt =
            new org.springframework.util.MimeType(mimeType.type(), mimeType.subtype());

      return delegate.includes(mt);
   }

   @Override
   public boolean matches(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      org.springframework.util.MimeType mt =
            new org.springframework.util.MimeType(mimeType.type(), mimeType.subtype());

      return delegate.isCompatibleWith(mt);
   }

   @Override
   public boolean equalsTypeAndSubtype(MimeType mimeType) {
      if (mimeType == null) {
         return false;
      }

      org.springframework.util.MimeType mt =
            new org.springframework.util.MimeType(mimeType.type(), mimeType.subtype());

      return delegate.equalsTypeAndSubtype(mt);
   }

   @Override
   public boolean wildcardType() {
      return delegate.isWildcardType();
   }

   @Override
   public boolean wildcardSubtype() {
      return delegate.isWildcardSubtype();
   }

   @Override
   public int compareTo(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      org.springframework.util.MimeType mt =
            new org.springframework.util.MimeType(mimeType.type(), mimeType.subtype(), mimeType.parameters());

      return delegate.compareTo(mt);
   }

   @Override
   public String toString() {
      return super.toString();
   }

   /**
    * Lowercase parameters keys because delegate does not manage correctly parameter key case-insensitivity
    * for regular parameters and charset.
    *
    * @param parameters parameters to normalize
    *
    * @return normalized parameters
    */
   private Map<String, String> normalizeParameters(Map<String, String> parameters) {
      if (parameters == null) {
         return null;
      }
      return map(LinkedHashMap::new,
                 entryStream(parameters).map(e -> entry(e.getKey().toLowerCase(Locale.ENGLISH),
                                                        e.getValue())));
   }

}
