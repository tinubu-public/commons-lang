/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.MimeTypeLibrary.JAKARTA;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.MimeTypeLibrary.SPRING;
import static com.tinubu.commons.lang.validation.Validate.notBlank;

import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Map;

import com.tinubu.commons.lang.libraryselector.LibrarySelector;
import com.tinubu.commons.lang.libraryselector.LibrarySelector.LibraryDefinition;
import com.tinubu.commons.lang.util.GlobalState;

/**
 * Creates an instance of {@link MimeType}.
 * Multiple implementations are supported if the following dependencies are on the classpath, and in this
 * order :
 * <ul>
 *    <li>org.springframework:spring-core</li>
 *    <li>jakarta.activation:jakarta.activation-api</li>
 * </ul>
 * <p>
 * To enforce implementation, use the {@link #defaultLibrary()} to access to the default library configuration.
 */
public class MimeTypeFactory {

   private static final LibrarySelector<MimeTypeLibrary> librarySelector = MimeTypeLibrary.librarySelector();

   private static final MimeType ANY = mimeType("*", "*");

   public static MimeType any() {
      return ANY;
   }

   /**
    * Creates a MIME type from an existing {@link javax.activation.MimeType} instance.
    *
    * @param mimeType Jakarta activation MIME type
    *
    * @return new {@link MimeType} instance
    *
    * @throws IllegalArgumentException if a bad argument is specified
    * @throws IllegalCharsetNameException if the given charset name is illegal
    * @throws UnsupportedCharsetException if no support for the named charset is unavailable
    */
   public static MimeType mimeType(javax.activation.MimeType mimeType) {
      librarySelector.checkLibrary(JAKARTA);
      return new JakartaActivationMimeType(mimeType);
   }

   /**
    * Creates a MIME type from an existing {@link org.springframework.util.MimeType} instance.
    *
    * @param mimeType Spring core MIME type
    *
    * @return new {@link MimeType} instance
    *
    * @throws IllegalArgumentException if a bad argument is specified
    * @throws IllegalCharsetNameException if the given charset name is illegal
    * @throws UnsupportedCharsetException if no support for the named charset is unavailable
    */
   public static MimeType mimeType(org.springframework.util.MimeType mimeType) {
      librarySelector.checkLibrary(SPRING);
      return new SpringCoreMimeType(mimeType);
   }

   /**
    * Parses a MIME type from a string representation.
    *
    * @param mimeType MIME type string representation
    *
    * @return new {@link MimeType} instance
    *
    * @throws IllegalArgumentException if a bad argument is specified
    */
   public static MimeType parseMimeType(String mimeType) {
      switch (librarySelector.checkLibrary()) {
         case SPRING:
            return SpringCoreMimeType.parseMimeType(mimeType);
         case JAKARTA:
            return JakartaActivationMimeType.parseMimeType(mimeType);
         default:
            throw new IllegalStateException();
      }
   }

   /**
    * Creates a MIME type from an existing instance, with alternative parameters.
    *
    * @param mimeType MIME type
    * @param parameters parameters to set
    *
    * @return new {@link MimeType} instance
    *
    * @throws IllegalArgumentException if a bad argument is specified
    * @throws IllegalCharsetNameException if the given charset name is illegal
    * @throws UnsupportedCharsetException if no support for the named charset is unavailable
    */
   public static MimeType mimeType(MimeType mimeType, Map<String, String> parameters) {
      switch (librarySelector.checkLibrary()) {
         case SPRING:
            return new SpringCoreMimeType(mimeType, parameters);
         case JAKARTA:
            return new JakartaActivationMimeType(mimeType, parameters);
         default:
            throw new IllegalStateException();
      }
   }

   /**
    * Creates a MIME type from an existing instance, with alternative charset.
    *
    * @param mimeType MIME type
    * @param charset parameters to set, specified as a single {@code charset} parameter
    *
    * @return new {@link MimeType} instance
    *
    * @throws IllegalArgumentException if a bad argument is specified
    */
   public static MimeType mimeType(MimeType mimeType, Charset charset) {
      switch (librarySelector.checkLibrary()) {
         case SPRING:
            return new SpringCoreMimeType(mimeType, charset);
         case JAKARTA:
            return new JakartaActivationMimeType(mimeType, charset);
         default:
            throw new IllegalStateException();
      }
   }

   /**
    * Creates a MIME type from specified type. Subtype will be set to wildcard.
    *
    * @param type type
    *
    * @return new {@link MimeType} instance
    *
    * @throws IllegalArgumentException if a bad argument is specified
    */
   public static MimeType mimeType(String type) {
      switch (librarySelector.checkLibrary()) {
         case SPRING:
            return new SpringCoreMimeType(type);
         case JAKARTA:
            return new JakartaActivationMimeType(type);
         default:
            throw new IllegalStateException();
      }
   }

   /**
    * Creates a MIME type from specified type and subtype.
    *
    * @param type type
    * @param subtype subtype
    *
    * @return new {@link MimeType} instance
    *
    * @throws IllegalArgumentException if a bad argument is specified
    */
   public static MimeType mimeType(String type, String subtype) {
      switch (librarySelector.checkLibrary()) {
         case SPRING:
            return new SpringCoreMimeType(type, subtype);
         case JAKARTA:
            return new JakartaActivationMimeType(type, subtype);
         default:
            throw new IllegalStateException();
      }
   }

   /**
    * Creates a MIME type from specified type, subtype and parameters.
    *
    * @param type type
    * @param subtype subtype
    * @param parameters parameters
    *
    * @return new {@link MimeType} instance
    *
    * @throws IllegalArgumentException if a bad argument is specified
    * @throws IllegalCharsetNameException if the given charset name is illegal
    * @throws UnsupportedCharsetException if no support for the named charset is unavailable
    */
   public static MimeType mimeType(String type, String subtype, Map<String, String> parameters) {
      switch (librarySelector.checkLibrary()) {
         case SPRING:
            return new SpringCoreMimeType(type, subtype, parameters);
         case JAKARTA:
            return new JakartaActivationMimeType(type, subtype, parameters);
         default:
            throw new IllegalStateException();
      }
   }

   /**
    * Creates a MIME type from specified type, subtype and charset.
    *
    * @param type type
    * @param subtype subtype
    * @param charset charset parameter
    *
    * @return new {@link MimeType} instance
    *
    * @throws IllegalArgumentException if a bad argument is specified
    */
   public static MimeType mimeType(String type, String subtype, Charset charset) {
      switch (librarySelector.checkLibrary()) {
         case SPRING:
            return new SpringCoreMimeType(type, subtype, charset);
         case JAKARTA:
            return new JakartaActivationMimeType(type, subtype, charset);
         default:
            throw new IllegalStateException();
      }
   }

   public static GlobalState<MimeTypeLibrary> defaultLibrary() {
      return librarySelector.defaultLibrary();
   }

   public enum MimeTypeLibrary implements LibraryDefinition {
      SPRING("org.springframework:spring-core", "org.springframework.util.MimeType"),
      JAKARTA("jakarta.activation:jakarta.activation-api", "javax.activation.MimeType");

      private final String libraryName;
      private final String classSample;

      MimeTypeLibrary(String libraryName, String classSample) {
         this.libraryName = notBlank(libraryName, "libraryName");
         this.classSample = notBlank(classSample, "classSample");
      }

      @Override
      public String libraryName() {
         return libraryName;
      }

      @Override
      public String classSample() {
         return classSample;
      }

      public static LibrarySelector<MimeTypeLibrary> librarySelector() {
         return new LibrarySelector<>(values());
      }
   }

}
