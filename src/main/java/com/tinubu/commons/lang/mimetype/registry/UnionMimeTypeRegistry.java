/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype.registry;

import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.reversedStream;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import com.tinubu.commons.lang.mimetype.MimeType;

/**
 * A meta-registry that manage on a list of registry with a layer logic.
 */
public class UnionMimeTypeRegistry implements MimeTypeRegistry {

   /**
    * Mime-type registries, ordered from the upper to the lower layer. Lower registries have lesser
    * priority.
    */
   protected final List<MimeTypeRegistry> registries;

   protected UnionMimeTypeRegistry(List<MimeTypeRegistry> registries) {
      registries = noNullElements(list(registries), "registries");

      this.registries = immutable(list(reversedStream(registries)));
   }

   /**
    * Creates an empty union registry.
    *
    * @return empty registry
    */
   public static UnionMimeTypeRegistry ofEmpty() {
      return new UnionMimeTypeRegistry(list());
   }

   /**
    * Creates a union registry from specified registries.
    *
    * @param registries registries ordered from the lower to the upper layers. Lower registries have
    *       lesser priority.
    *
    * @return registry with specified registries
    */
   public static UnionMimeTypeRegistry ofRegistries(MimeTypeRegistry... registries) {
      return new UnionMimeTypeRegistry(list(registries));
   }

   /**
    * Creates a union registry from specified registries.
    *
    * @param registries registries ordered from the lower to the upper layers. Lower registries have
    *       lesser priority.
    *
    * @return registry with specified registries
    */
   public static UnionMimeTypeRegistry ofRegistries(List<MimeTypeRegistry> registries) {
      return new UnionMimeTypeRegistry(registries);
   }

   @Override
   public Optional<MimeType> mimeType(Path documentPath) {
      notNull(documentPath, "documentPath");

      return stream(registries).flatMap(registry -> stream(registry.mimeType(documentPath))).findFirst();
   }

   @Override
   public List<MimeType> mimeTypes(Path documentPath) {
      notNull(documentPath, "documentPath");

      return list(stream(registries)
                        .flatMap(registry -> stream(registry.mimeTypes(documentPath)))
                        .distinct());
   }

   @Override
   public Optional<String> extension(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      return stream(registries).flatMap(registry -> stream(registry.extension(mimeType))).findFirst();
   }

   @Override
   public List<String> extensions(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      return list(stream(registries).flatMap(registry -> stream(registry.extensions(mimeType))).distinct());
   }
}
