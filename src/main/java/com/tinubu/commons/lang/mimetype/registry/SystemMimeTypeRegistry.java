/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype.registry;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.MimeTypeFactory;

/**
 * Java system MIME type registry using {@link Files#probeContentType(Path)}.
 */
public class SystemMimeTypeRegistry implements MimeTypeRegistry {
   protected SystemMimeTypeRegistry() {
   }

   public static SystemMimeTypeRegistry ofSystem() {
      return new SystemMimeTypeRegistry();
   }

   @Override
   public Optional<MimeType> mimeType(Path documentPath) {
      notNull(documentPath, "documentPath");

      try {
         return nullable(Files.probeContentType(documentPath)).map(MimeTypeFactory::parseMimeType);
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @Override
   public List<MimeType> mimeTypes(Path documentPath) {
      return mimeType(documentPath).map(Collections::singletonList).orElse(emptyList());
   }

   @Override
   public Optional<String> extension(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      return optional();
   }

   @Override
   public List<String> extensions(MimeType mimeType) {
      notNull(mimeType, "mimeType");

      return list();
   }

}
