/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

/**
 * Variant with bounded internal representation.
 *
 * @param <B> variant upper bound type
 */
public class BoundVariant<B> extends UnboundVariant {

   /**
    * Creates a variant from value and explicit value class.
    *
    * @param value value
    * @param type value class
    * @param <T> value type
    */
   public <T extends B> BoundVariant(T value, Class<T> type) {
      super(value, type);
   }

   /**
    * Creates a variant from value with type auto-detection.
    * <p>
    * Note that {@code null} values are not supported using this constructor, use
    * {@link #BoundVariant(Object, Class)} instead.
    *
    * @param value value
    * @param <T> value type
    */
   public <T extends B> BoundVariant(T value) {
      super(value);
   }

   @Override
   @SuppressWarnings("unchecked")
   public B value() {
      return (B) super.value();
   }

   @Override
   @SuppressWarnings("unchecked")
   public Class<? extends B> type() {
      return (Class<? extends B>) super.type();
   }
}
