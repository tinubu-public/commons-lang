/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

import static com.tinubu.commons.lang.validation.Validate.notNull;

/**
 * Variant with {@link Object} internal representation.
 */
public class UnboundVariant implements Variant {

   protected final Object value;
   protected final Class<?> type;

   /**
    * Creates a variant from value and explicit value class.
    *
    * @param value value
    * @param type value class
    * @param <T> value type
    */
   public <T> UnboundVariant(T value, Class<T> type) {
      this.value = value;
      this.type = notNull(type, "type");
   }

   /**
    * Creates a variant from value with automatic variant type inference.
    * <p>
    * Note that {@code null} values are not supported using this constructor, use
    * {@link #UnboundVariant(Object, Class)} instead.
    *
    * @param value value
    * @param <T> value type
    */
   public <T> UnboundVariant(T value) {
      this.value = notNull(value, "value");
      this.type = value.getClass();
   }

   @Override
   public <T> T value(Class<T> type) {
      validateType(notNull(type, "type"));

      return type.cast(value);
   }

   @Override
   public Object value() {
      return value;
   }

   @Override
   public Class<?> type() {
      return type;
   }
}
