/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.util.stream.Collectors.joining;

import java.util.stream.Stream;

public class VariantTypeException extends RuntimeException {

   private static final String DEFAULT_MESSAGE_SINGLE = "'%s' type is incompatible with '%s' variant type";
   private static final String DEFAULT_MESSAGE_MULTI = "'%s' types are incompatible with '%s' variant type";

   public VariantTypeException(Variant variant, Class<?> targetType) {
      super(message(variant, targetType));
   }

   public VariantTypeException(Variant variant, Class<?> targetType, Throwable cause) {
      super(message(variant, targetType), cause);
   }

   public VariantTypeException(Variant variant, Class<?>... targetTypes) {
      super(message(variant, targetTypes));
   }

   private static String message(Variant variant, Class<?>... targetTypes) {
      notNull(variant, "variant");
      satisfies(noNullElements(targetTypes, "targetTypes"),
                types -> types.length > 0,
                "targetTypes",
                "length must be > 0");

      if (targetTypes.length == 1) {
         return String.format(DEFAULT_MESSAGE_SINGLE, targetTypes[0].getName(), variant.type().getName());
      } else {
         return String.format(DEFAULT_MESSAGE_MULTI,
                              Stream.of(targetTypes).map(Class::getName).collect(joining(",")),
                              variant.type().getName());
      }
   }

}
