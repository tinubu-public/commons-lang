/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.lang.convert.ConversionService;

/**
 * Variant with conversion support from/to an external representation.
 *
 * @param <S> external representation type
 */
public class ConverterVariant<S> extends UnboundVariant {

   protected final ConversionService conversionService;
   protected final S externalValue;

   /**
    * Creates variant from an external representation.
    *
    * @param externalValue variant value using the external representation
    * @param type variant type class
    * @param conversionService conversion service to convert from/to external representation
    * @param <T> variant type
    */
   public <T> ConverterVariant(S externalValue, Class<T> type, ConversionService conversionService) {
      super(convert(externalValue, type, conversionService), type);
      this.conversionService = notNull(conversionService, "conversionService");
      this.externalValue = externalValue;
   }

   /**
    * Creates variant from typed value.
    *
    * @param externalType external representation type to convert to
    * @param value variant value
    * @param type variant type class
    * @param conversionService conversion service to convert from/to external representation
    * @param <T> variant type
    */
   public <T> ConverterVariant(Class<S> externalType,
                               T value,
                               Class<T> type,
                               ConversionService conversionService) {
      super(value, type);
      this.conversionService = notNull(conversionService, "conversionService");
      this.externalValue = conversionService.convert(value, notNull(externalType, "externalType"));
   }

   /**
    * Creates variant from typed value with automatic variant type inference.
    * Note that {@code null} values are not supported using this constructor, use
    * {@link #ConverterVariant(Class, Object, Class, ConversionService)} instead.
    *
    * @param externalType external representation type to convert to
    * @param value variant value
    * @param conversionService conversion service to convert from/to external representation
    * @param <T> variant type
    */
   public <T> ConverterVariant(Class<S> externalType, T value, ConversionService conversionService) {
      super(value);
      this.conversionService = notNull(conversionService, "conversionService");
      this.externalValue = conversionService.convert(value, notNull(externalType, "externalType"));
   }

   protected static <T> T convert(Object value, Class<T> type, ConversionService conversionService) {
      notNull(type, "type");
      notNull(conversionService, "conversionService");

      return conversionService.convert(value, type);
   }

   /**
    * Returns external value representation for this variant.
    *
    * @return external value representation for this variant
    */
   public S externalValue() {
      return this.externalValue;
   }

}