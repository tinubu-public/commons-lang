/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

import java.util.stream.Stream;

/**
 * Variant value holder.
 */
public interface Variant {

   /**
    * Returns variant value as specified type.
    * Specified type must be assignable from real value type.
    *
    * @return variant value or {@code null}
    *
    * @throws VariantTypeException if requested type is not compatible with variant type
    */
   <T> T value(Class<T> type);

   /**
    * Returns variant value.
    * Think about using {@link #value(Class)} to retrieve a more precisely typed value.
    *
    * @return variant value or {@code null}
    */
   Object value();

   /**
    * Returns stored value real type
    *
    * @return stored value real type or {@code null} if variant were created with no explicit value class and
    *       value is {@code null}
    */
   Class<?> type();

   /**
    * Validates that variant type is in specified list
    *
    * @param types supported types or super types
    *
    * @throws VariantTypeException if none types is compatible with variant type
    */
   default void validateType(Class<?>... types) {
      Stream
            .of(types)
            .filter(type -> type.isAssignableFrom(type()))
            .findAny()
            .orElseThrow(() -> new VariantTypeException(this, types));
   }
}
