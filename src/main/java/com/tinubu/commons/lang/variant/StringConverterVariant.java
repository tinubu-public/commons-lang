/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

import com.tinubu.commons.lang.convert.ConversionService;

/**
 * Simplified {@link ConverterVariant} for {@link String} external representation.
 */
public class StringConverterVariant extends ConverterVariant<String> {

   public <T> StringConverterVariant(String sourceValue, Class<T> type, ConversionService conversionService) {
      super(sourceValue, type, conversionService);
   }

   public <T> StringConverterVariant(T value, Class<T> type, ConversionService conversionService) {
      super(String.class, value, type, conversionService);
   }

   public <T> StringConverterVariant(T value, ConversionService conversionService) {
      super(String.class, value, conversionService);
   }
}