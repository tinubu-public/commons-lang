
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert;

/**
 * Conversion failed for source object value.
 */
public class ConversionFailedException extends ConversionException {

   private static final String DEFAULT_MESSAGE = "Conversion from '%s' to '%s' failed for '%s' value";

   private final Object value;
   private final Class<?> sourceType;
   private final Class<?> targetType;

   /**
    * Creates a new conversion exception.
    *
    * @param sourceType the value's original type
    * @param targetType the value's target type
    * @param value the value we tried to convert
    */
   public ConversionFailedException(Class<?> sourceType, Class<?> targetType, Object value) {
      super(String.format(DEFAULT_MESSAGE, sourceType.getName(), targetType.getName(), value));
      this.value = value;
      this.sourceType = sourceType;
      this.targetType = targetType;
   }

   /**
    * Creates a new conversion exception.
    *
    * @param sourceType the value's original type
    * @param targetType the value's target type
    * @param value the value we tried to convert
    * @param cause the cause of the conversion failure
    */
   public ConversionFailedException(Class<?> sourceType, Class<?> targetType, Object value, Throwable cause) {
      super(String.format(DEFAULT_MESSAGE, sourceType.getName(), targetType.getName(), value), cause);
      this.value = value;
      this.sourceType = sourceType;
      this.targetType = targetType;
   }

   /**
    * Returns the source type we tried to convert the value from.
    */
   public Class<?> sourceType() {
      return this.sourceType;
   }

   /**
    * Returns the target type we tried to convert the value to.
    */
   public Class<?> targetType() {
      return this.targetType;
   }

   /**
    * Returns the offending value.
    */
   public Object value() {
      return this.value;
   }

}
