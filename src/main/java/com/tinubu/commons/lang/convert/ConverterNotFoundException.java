
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert;

/**
 * No converter found for requested conversion source and target types.
 */
public class ConverterNotFoundException extends ConversionException {

   private static final String DEFAULT_MESSAGE = "No converter found for conversion from '%s' to '%s'";

   private final Class<?> sourceType;
   private final Class<?> targetType;

   /**
    * Creates a new conversion executor not found exception.
    *
    * @param sourceType the source type requested to convert from
    * @param targetType the target type requested to convert to
    */
   public ConverterNotFoundException(Class<?> sourceType, Class<?> targetType) {
      super(String.format(DEFAULT_MESSAGE, sourceType.getName(), targetType.getName()));
      this.sourceType = sourceType;
      this.targetType = targetType;
   }

   /**
    * Returns the source type that was requested to convert from.
    */
   public Class<?> sourceType() {
      return this.sourceType;
   }

   /**
    * Returns the target type that was requested to convert to.
    */
   public Class<?> targetType() {
      return this.targetType;
   }

}
