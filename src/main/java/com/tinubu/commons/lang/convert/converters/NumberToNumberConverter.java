/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert.converters;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

import com.tinubu.commons.lang.convert.Converter;
import com.tinubu.commons.lang.convert.ConverterNotFoundException;

public class NumberToNumberConverter<T extends Number> implements Converter<Number, T> {

   /** Default {@link MathContext} for {@link BigDecimal} implementation. */
   private static final MathContext DEFAULT_MATH_CONTEXT = MathContext.UNLIMITED;

   private final Class<T> targetType;
   private final MathContext mathContext;

   public NumberToNumberConverter(Class<T> targetType, MathContext mathContext) {
      this.targetType = notNull(targetType, "targetType");
      this.mathContext = notNull(mathContext, "mathContext");
   }

   public NumberToNumberConverter(Class<T> targetType) {
      this(targetType, DEFAULT_MATH_CONTEXT);
   }

   private Long longValue(Number source) {
      return nullable(source).map(Number::longValue).orElse(null);
   }

   private Integer integerValue(Number source) {
      return nullable(source).map(Number::intValue).orElse(null);
   }

   private Float floatValue(Number source) {
      return nullable(source).map(Number::floatValue).orElse(null);
   }

   private Double doubleValue(Number source) {
      return nullable(source).map(Number::doubleValue).orElse(null);
   }

   private Short shortValue(Number source) {
      return nullable(source).map(Number::shortValue).orElse(null);
   }

   private Byte byteValue(Number source) {
      return nullable(source).map(Number::byteValue).orElse(null);
   }

   private BigDecimal bigDecimalValue(Number source) {
      return nullable(source).map(v -> new BigDecimal(v.toString(), mathContext)).orElse(null);
   }

   private BigInteger bigIntegerValue(Number source) {
      return nullable(source).map(v -> BigInteger.valueOf(longValue(source))).orElse(null);
   }

   @Override
   @SuppressWarnings("unchecked")
   public T convert(Number source) {
      if (targetType.equals(Long.class)) {
         return (T) longValue(source);
      } else if (targetType.equals(Integer.class)) {
         return (T) integerValue(source);
      } else if (targetType.equals(Float.class)) {
         return (T) floatValue(source);
      } else if (targetType.equals(Double.class)) {
         return (T) doubleValue(source);
      } else if (targetType.equals(Short.class)) {
         return (T) shortValue(source);
      } else if (targetType.equals(Byte.class)) {
         return (T) byteValue(source);
      } else if (targetType.equals(BigDecimal.class)) {
         return (T) bigDecimalValue(source);
      } else if (targetType.equals(BigInteger.class)) {
         return (T) bigIntegerValue(source);
      } else {
         throw new ConverterNotFoundException(String.class, targetType);
      }
   }
}
