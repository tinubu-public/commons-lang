/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert.converters;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.tinubu.commons.lang.convert.ConversionService;
import com.tinubu.commons.lang.convert.Converter;
import com.tinubu.commons.lang.convert.ConverterFactory;
import com.tinubu.commons.lang.convert.ConverterNotFoundException;
import com.tinubu.commons.lang.util.Pair;

public class DefaultConversionService implements ConversionService {

   @SuppressWarnings("rawtypes")
   private static final Map<Pair<Class<?>, Class<?>>, Converter> CONVERTERS;

   static {
      CONVERTERS = new HashMap<>();

      ConverterFactory<String, Number> stringToNumberConverters = new StringToNumberConverterFactory();
      ConverterFactory<Number, Number> numberToNumberConverters = new NumberToNumberConverterFactory();

      for (Class<? extends Number> nc : Arrays.asList(Long.class,
                                                      Integer.class,
                                                      Byte.class,
                                                      Short.class,
                                                      Float.class,
                                                      Double.class,
                                                      BigDecimal.class,
                                                      BigInteger.class)) {
         CONVERTERS.put(Pair.of(String.class, nc), stringToNumberConverters.instance(nc));
         CONVERTERS.put(Pair.of(nc, String.class), new NumberToStringConverter<>());

         for (Class<? extends Number> nc2 : Arrays.asList(Long.class,
                                                          Integer.class,
                                                          Byte.class,
                                                          Short.class,
                                                          Float.class,
                                                          Double.class,
                                                          BigDecimal.class,
                                                          BigInteger.class)) {
            CONVERTERS.put(Pair.of(nc, nc2), numberToNumberConverters.instance(nc2));
         }
      }
   }

   @Override
   public boolean canConvert(Class<?> sourceType, Class<?> targetType) {
      return CONVERTERS.containsKey(Pair.of(sourceType, targetType));
   }

   @Override
   @SuppressWarnings("unchecked")
   public <T> T convert(Object source, Class<T> targetType) {
      if (source == null) {
         return null;
      }

      Converter<Object, T> converter = CONVERTERS.get(Pair.of(source.getClass(), targetType));

      if (converter == null) {
         throw new ConverterNotFoundException(source.getClass(), targetType);
      }
      return converter.convert(source);
   }
}
