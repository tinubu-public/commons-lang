/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert.converters;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.convert.Converter;
import com.tinubu.commons.lang.convert.ConverterNotFoundException;

public class StringToNumberConverter<T extends Number> implements Converter<String, T> {

   /** Default {@link MathContext} for {@link BigDecimal} implementation. */
   private static final MathContext DEFAULT_MATH_CONTEXT = MathContext.UNLIMITED;

   private final Class<T> targetType;
   private final MathContext mathContext;

   public StringToNumberConverter(Class<T> targetType, MathContext mathContext) {
      this.targetType = notNull(targetType, "targetType");
      this.mathContext = notNull(mathContext, "mathContext");
   }

   public StringToNumberConverter(Class<T> targetType) {
      this(targetType, DEFAULT_MATH_CONTEXT);
   }

   private Long longValue(String source) {
      try {
         return nullable(source).map(Long::parseLong).orElse(null);
      } catch (NumberFormatException e) {
         throw new ConversionFailedException(String.class, Long.class, e);
      }
   }

   private Integer integerValue(String source) {
      try {
         return nullable(source).map(Integer::parseInt).orElse(null);
      } catch (NumberFormatException e) {
         throw new ConversionFailedException(String.class, Integer.class, e);
      }
   }

   private Float floatValue(String source) {
      try {
         return nullable(source).map(Float::parseFloat).orElse(null);
      } catch (NumberFormatException e) {
         throw new ConversionFailedException(String.class, Float.class, e);
      }
   }

   private Double doubleValue(String source) {
      try {
         return nullable(source).map(Double::parseDouble).orElse(null);
      } catch (NumberFormatException e) {
         throw new ConversionFailedException(String.class, Double.class, e);
      }
   }

   private Short shortValue(String source) {
      try {
         return nullable(source).map(Short::parseShort).orElse(null);
      } catch (NumberFormatException e) {
         throw new ConversionFailedException(String.class, Short.class, e);
      }
   }

   private Byte byteValue(String source) {
      try {
         return nullable(source).map(Byte::parseByte).orElse(null);
      } catch (NumberFormatException e) {
         throw new ConversionFailedException(String.class, Byte.class, e);
      }
   }

   private BigDecimal bigDecimalValue(String source) {
      try {
         return nullable(source).map(v -> new BigDecimal(v, mathContext)).orElse(null);
      } catch (NumberFormatException e) {
         throw new ConversionFailedException(String.class, BigDecimal.class, e);
      }
   }

   private BigInteger bigIntegerValue(String source) {
      try {
         return nullable(source).map(BigInteger::new).orElse(null);
      } catch (NumberFormatException e) {
         throw new ConversionFailedException(String.class, BigInteger.class, e);
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   public T convert(String source) {
      if (targetType.equals(Long.class)) {
         return (T) longValue(source);
      } else if (targetType.equals(Integer.class)) {
         return (T) integerValue(source);
      } else if (targetType.equals(Float.class)) {
         return (T) floatValue(source);
      } else if (targetType.equals(Double.class)) {
         return (T) doubleValue(source);
      } else if (targetType.equals(Short.class)) {
         return (T) shortValue(source);
      } else if (targetType.equals(Byte.class)) {
         return (T) byteValue(source);
      } else if (targetType.equals(BigDecimal.class)) {
         return (T) bigDecimalValue(source);
      } else if (targetType.equals(BigInteger.class)) {
         return (T) bigIntegerValue(source);
      } else {
         throw new ConverterNotFoundException(String.class, targetType);
      }
   }
}
