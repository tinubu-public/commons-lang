/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.datetime;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ApplicationClockTest {

   @BeforeEach
   public void resetClock() {
      ApplicationClock.resetClock();
   }

   @Test
   void testSetClock() {
      ZonedDateTime fixedDate =
            ZonedDateTime.of(LocalDateTime.of(2019, 8, 20, 0, 0, 0), ZoneId.systemDefault());
      ApplicationClock.setClock(Clock.fixed(fixedDate.toInstant(), fixedDate.getZone()));
      assertThat(ApplicationClock.clock()).isEqualTo(Clock.fixed(fixedDate.toInstant(), fixedDate.getZone()));
      ApplicationClock.setFixedClock(fixedDate);
      assertThat(ApplicationClock.clock()).isEqualTo(Clock.fixed(fixedDate.toInstant(), fixedDate.getZone()));
   }

   @Test
   void nowAsOffsetDateTime() {
      ZonedDateTime fixedDate =
            ZonedDateTime.of(LocalDateTime.of(2019, 8, 20, 0, 0, 0), ZoneId.systemDefault());
      ApplicationClock.setFixedClock(fixedDate);
      assertThat(ApplicationClock.nowAsOffsetDateTime()).isEqualTo(ApplicationClock.now()).isEqualTo(fixedDate.toOffsetDateTime());
   }

   @Test
   void nowAsZonedDateTime() {
      ZonedDateTime fixedDate =
            ZonedDateTime.of(LocalDateTime.of(2019, 8, 20, 0, 0, 0), ZoneId.systemDefault());
      ApplicationClock.setFixedClock(fixedDate);
      assertThat(ApplicationClock.nowAsZonedDateTime()).isEqualTo(fixedDate);
   }

   @Test
   void nowAsLocalDate() {
      ZonedDateTime fixedDate =
            ZonedDateTime.of(LocalDateTime.of(2019, 8, 20, 0, 0, 0), ZoneId.systemDefault());
      ApplicationClock.setFixedClock(fixedDate);
      assertThat(ApplicationClock.nowAsLocalDate()).isEqualTo(fixedDate.toLocalDate());
   }

   @Test
   void nowAsLocalDateTime() {
      ZonedDateTime fixedDate =
            ZonedDateTime.of(LocalDateTime.of(2019, 8, 20, 0, 0, 0), ZoneId.systemDefault());
      ApplicationClock.setFixedClock(fixedDate);
      assertThat(ApplicationClock.nowAsLocalDateTime()).isEqualTo(fixedDate.toLocalDateTime());
   }
}