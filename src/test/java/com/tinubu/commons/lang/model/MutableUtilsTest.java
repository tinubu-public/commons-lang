/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.model;

import static com.tinubu.commons.lang.model.MutableUtils.mutableCollection;
import static com.tinubu.commons.lang.model.MutableUtils.mutableList;
import static com.tinubu.commons.lang.model.MutableUtils.mutableMap;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.util.Pair;

class MutableUtilsTest {

   @Test
   public void testMutableCollectionWithStreamWhenNominal() {
      Collection<Integer> collection = mutableCollection(() -> new ArrayList<Integer>(), Stream.of(1, 2));

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(1, 2);
   }

   @Test
   public void testMutableCollectionWithMultiStreamsWhenNominal() {
      Collection<Integer> collection =
            mutableCollection(() -> new ArrayList<Integer>(), Stream.of(3, 4), Stream.of(1, 2));

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testMutableCollectionWithMultiStreamsWhenOptimized() {
      assertThat(mutableCollection(() -> new ArrayList<Integer>(),
                                   Stream.of(1, 2),
                                   Stream.of(3, 4))).containsExactly(1, 2, 3, 4);
      assertThat(mutableCollection(() -> new ArrayList<Integer>(), Stream.of(1, 2), null)).containsExactly(1,
                                                                                                           2);
      assertThat(mutableCollection(() -> new ArrayList<Integer>(),
                                   (Stream<Integer>) null,
                                   null)).containsExactly();
   }

   @Test
   public void testMutableCollectionWithMultiStreamsWhenNull() {
      Collection<Integer> collection =
            mutableCollection(() -> new ArrayList<Integer>(), Stream.of(1, 2), null);

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(1, 2);
   }

   @Test
   public void testMutableCollectionWithMultiStreamsWhenNullElement() {
      Collection<Integer> collection =
            mutableCollection(() -> new ArrayList<Integer>(), Stream.of(1, null), null);

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(1, null);
   }

   @Test
   public void testMutableCollectionWhenNominal() {
      Collection<Integer> collection = mutableCollection(() -> new ArrayList<Integer>(), Arrays.asList(1, 2));

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(1, 2);
   }

   @Test
   public void testMutableCollectionWithMultiCollectionsWhenNominal() {
      Collection<Integer> collection =
            mutableCollection(() -> new ArrayList<Integer>(), Arrays.asList(3, 4), Arrays.asList(1, 2));

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testMutableCollectionWithMultiCollectionsWhenNull() {
      Collection<Integer> collection =
            mutableCollection(() -> new ArrayList<Integer>(), Arrays.asList(1, 2), null);

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(1, 2);
   }

   @Test
   public void testMutableCollectionWithMultiCollectionsWhenNullElement() {
      Collection<Integer> collection =
            mutableCollection(() -> new ArrayList<Integer>(), Arrays.asList(1, null), null);

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(1, null);
   }

   @Test
   public void testMutableCollectionWithMultiElementsWhenNominal() {
      Collection<Integer> collection = mutableCollection(ArrayList::new, 3, 4, 1, 2);

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testMutableCollectionWithMultiElementsWhenNull() {
      Collection<Integer> collection = mutableCollection(ArrayList::new, null, 1, 2, null);

      assertThat(collection).isInstanceOf(ArrayList.class);
      assertThat(collection).containsExactly(null, 1, 2, null);
   }

   @Test
   public void testMutableListWhenEmpty() {
      List<Integer> list = mutableList();

      assertThat(list).isInstanceOf(ArrayList.class);
      assertThat(list).isEmpty();
   }

   @Test
   public void testMutableListWhenNominal() {
      List<Integer> list = mutableList(Arrays.asList(1, 2));

      assertThat(list).isInstanceOf(ArrayList.class);
      assertThat(list).containsExactly(1, 2);
   }

   @Test
   public void testMutableListWhenNull() {
      List<Integer> list = mutableList((List<Integer>) null);

      assertThat(list).isInstanceOf(ArrayList.class);
      assertThat(list).isEmpty();
   }

   @Test
   public void testMutableListWithMultiListsWhenNominal() {
      List<Integer> list = mutableList(Arrays.asList(3, 4), Arrays.asList(1, 2));

      assertThat(list).isInstanceOf(ArrayList.class);
      assertThat(list).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testMutableListWithMultiListsWhenNull() {
      List<Integer> list = mutableList(null, Arrays.asList(1, 2), null);

      assertThat(list).isInstanceOf(ArrayList.class);
      assertThat(list).containsExactly(1, 2);
   }

   @Test
   public void testMutableListWithMultiListsWhenNullElement() {
      List<Integer> list = mutableList(null, Arrays.asList(1, null), null);

      assertThat(list).isInstanceOf(ArrayList.class);
      assertThat(list).containsExactly(1, null);
   }

   @Test
   public void testMutableListWithMultiElementsWhenNominal() {
      List<Integer> list = mutableList(3, 4, 1, 2);

      assertThat(list).isInstanceOf(ArrayList.class);
      assertThat(list).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testMutableListWithMultiElementsWhenNull() {
      List<Integer> list = mutableList(null, 1, 2, null);

      assertThat(list).isInstanceOf(ArrayList.class);
      assertThat(list).containsExactly(null, 1, 2, null);
   }

   @Test
   public void testMutableMapWhenNominal() {
      Map<String, Integer> map = mutableMap(new HashMap<String, Integer>() {{
         put("a", 1);
         put("b", 2);
      }});

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of("a", 1), Pair.of("b", 2));
   }

   @Test
   public void testMutableMapWhenMapFactory() {
      Map<String, Integer> map = mutableMap(LinkedHashMap::new, new HashMap<String, Integer>() {{
         put("a", 1);
         put("b", 2);
         put("c", 3);
      }});

      assertThat(map).isInstanceOf(LinkedHashMap.class);
      assertThat(map).containsExactly(Pair.of("a", 1), Pair.of("b", 2), Pair.of("c", 3));
   }

   @Test
   public void testMutableMapWhenMergeFunction() {
      assertThat(mutableMap((v1, v2) -> v2,
                            () -> new HashMap<>(),
                            Pair.of("a", 1),
                            Pair.of("a", 2))).containsOnly(Pair.of("a", 2));
      assertThat(mutableMap((v1, v2) -> v1,
                            () -> new HashMap<>(),
                            Pair.of("a", 1),
                            Pair.of("a", 2))).containsOnly(Pair.of("a", 1));
      assertThat(mutableMap((v1, v2) -> v2,
                            () -> new HashMap<>(),
                            Pair.of("a", 1),
                            Pair.of("a", null))).containsOnly(Pair.of("a", null));
      assertThat(mutableMap((v1, v2) -> v1,
                            () -> new HashMap<>(),
                            Pair.of("a", 1),
                            Pair.of("a", null))).containsOnly(Pair.of("a", 1));
      assertThat(mutableMap((v1, v2) -> v2,
                            () -> new HashMap<>(),
                            Pair.of("a", null),
                            Pair.of("a", 2))).containsOnly(Pair.of("a", 2));
      assertThat(mutableMap((v1, v2) -> v1,
                            () -> new HashMap<>(),
                            Pair.of("a", null),
                            Pair.of("a", 2))).containsOnly(Pair.of("a", null));
   }

   @Test
   public void testMutableMapWhenNull() {
      Map<String, Integer> map = mutableMap((Map<String, Integer>) null);

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).isEmpty();
   }

   @Test
   public void testMutableMapWhenEntryNullKey() {
      Map<String, Integer> map = mutableMap(Pair.of(null, 1), Pair.of("b", 2));

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of(null, 1), Pair.of("b", 2));
   }

   @Test
   public void testMutableMapWhenOverrideEntryWithNullKey() {
      Map<String, Integer> map = mutableMap(Pair.of(null, 1), Pair.of(null, 2));

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of(null, 2));
   }

   @Test
   public void testMutableMapWhenEntryNullValue() {
      Map<String, Integer> map = mutableMap(Pair.of("a", null), Pair.of("b", 2));

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of("a", null), Pair.of("b", 2));
   }

   @Test
   public void testMutableMapWhenOverrideEntryWithNullValue() {
      Map<String, Integer> map = mutableMap(Pair.of("a", 1), Pair.of("a", null));

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of("a", null));
   }

   @Test
   public void testMutableMapWhenOverrideNullValueEntryWithValue() {
      Map<String, Integer> map = mutableMap(Pair.of("a", null), Pair.of("a", 1));

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of("a", 1));
   }

   @Test
   public void testMutableMapWithMultiMapsWhenNominal() {
      Map<String, Integer> map = mutableMap(new HashMap<String, Integer>() {{
         put("c", 3);
         put("d", 4);
      }}, new HashMap<String, Integer>() {{
         put("a", 1);
         put("b", 2);
      }});

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of("c", 3), Pair.of("d", 4), Pair.of("a", 1), Pair.of("b", 2));
   }

   @Test
   public void testMutableMapWithMultiMapsWhenNull() {
      Map<String, Integer> map = mutableMap((Map<String, Integer>) null, new HashMap<String, Integer>() {{
         put("c", 3);
         put("d", 4);
      }}, new HashMap<String, Integer>() {{
         put("a", 1);
         put("b", 2);
      }}, null);

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of("c", 3), Pair.of("d", 4), Pair.of("a", 1), Pair.of("b", 2));
   }

   @Test
   public void testMutableMapWhenEmpty() {
      Map<String, Integer> map = mutableMap();

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).isEmpty();
   }

   @Test
   public void testMutableMapWithMultiEntriesWhenNominal() {
      Map<String, Integer> map = mutableMap(Pair.of("a", 1), Pair.of("b", 2));

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of("a", 1), Pair.of("b", 2));
   }

   @Test
   public void testMutableMapWithMultiEntriesWhenNull() {
      Map<String, Integer> map =
            mutableMap((Entry<String, Integer>) null, Pair.of("a", 1), Pair.of("b", 2), null);

      assertThat(map).isInstanceOf(HashMap.class);
      assertThat(map).containsOnly(Pair.of("a", 1), Pair.of("b", 2));
   }

}