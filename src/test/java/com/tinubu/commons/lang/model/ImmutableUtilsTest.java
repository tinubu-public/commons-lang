/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.model;

import static com.tinubu.commons.lang.model.ImmutableUtils.immutableCollection;
import static com.tinubu.commons.lang.model.ImmutableUtils.immutableList;
import static com.tinubu.commons.lang.model.ImmutableUtils.immutableMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.util.Pair;

class ImmutableUtilsTest {

   @Test
   public void testImmutableCollectionWithStreamWhenNominal() {
      Collection<Integer> collection = immutableCollection(() -> new ArrayList<Integer>(), Stream.of(1, 2));

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(1, 2);
   }

   @Test
   public void testImmutableCollectionWithMultiStreamsWhenNominal() {
      Collection<Integer> collection =
            immutableCollection(() -> new ArrayList<Integer>(), Stream.of(3, 4), Stream.of(1, 2));

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testMutableCollectionWithMultiStreamsWhenOptimized() {
      assertThat(immutableCollection(() -> new ArrayList<Integer>(),
                                     Stream.of(1, 2),
                                     Stream.of(3, 4))).containsExactly(1, 2, 3, 4);
      assertThat(immutableCollection(() -> new ArrayList<Integer>(),
                                     Stream.of(1, 2),
                                     null)).containsExactly(1, 2);
      assertThat(immutableCollection(() -> new ArrayList<Integer>(),
                                     (Stream<Integer>) null,
                                     null)).containsExactly();
   }

   @Test
   public void testImmutableCollectionWithMultiStreamsWhenNull() {
      Collection<Integer> collection =
            immutableCollection(() -> new ArrayList<Integer>(), Stream.of(1, 2), null);

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(1, 2);
   }

   @Test
   public void testImmutableCollectionWithMultiStreamsWhenNullElement() {
      Collection<Integer> collection =
            immutableCollection(() -> new ArrayList<Integer>(), Stream.of(1, null), null);

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(1, null);
   }

   @Test
   public void testImmutableCollectionWhenNominal() {
      Collection<Integer> collection =
            immutableCollection(() -> new ArrayList<Integer>(), Arrays.asList(1, 2));

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(1, 2);
   }

   @Test
   public void testImmutableCollectionWithMultiCollectionsWhenNominal() {
      Collection<Integer> collection =
            immutableCollection(() -> new ArrayList<Integer>(), Arrays.asList(3, 4), Arrays.asList(1, 2));

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testImmutableCollectionWithMultiCollectionsWhenNull() {
      Collection<Integer> collection =
            immutableCollection(() -> new ArrayList<Integer>(), Arrays.asList(1, 2), null);

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(1, 2);
   }

   @Test
   public void testImmutableCollectionWithMultiCollectionsWhenNullElement() {
      Collection<Integer> collection =
            immutableCollection(() -> new ArrayList<Integer>(), Arrays.asList(1, null), null);

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(1, null);
   }

   @Test
   public void testImmutableCollectionWithMultiElementsWhenNominal() {
      Collection<Integer> collection = immutableCollection(ArrayList::new, 3, 4, 1, 2);

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testImmutableCollectionWithMultiElementsWhenNull() {
      Collection<Integer> collection = immutableCollection(ArrayList::new, null, 1, 2, null);

      assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(collection).containsExactly(null, 1, 2, null);
   }

   @Test
   public void testImmutableListWhenEmpty() {
      List<Integer> list = immutableList();

      assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(list).isEmpty();
   }

   @Test
   public void testImmutableListWhenNominal() {
      List<Integer> list = immutableList(Arrays.asList(1, 2));

      assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(list).containsExactly(1, 2);
   }

   @Test
   public void testImmutableListWhenNull() {
      List<Integer> list = immutableList((List<Integer>) null);

      assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(list).isEmpty();
   }

   @Test
   public void testImmutableListWithMultiListsWhenNominal() {
      List<Integer> list = immutableList(Arrays.asList(3, 4), Arrays.asList(1, 2));

      assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(list).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testImmutableListWithMultiListsWhenNull() {
      List<Integer> list = immutableList(null, Arrays.asList(1, 2), null);

      assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(list).containsExactly(1, 2);
   }

   @Test
   public void testImmutableListWithMultiListsWhenNullElement() {
      List<Integer> list = immutableList(null, Arrays.asList(1, null), null);

      assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(list).containsExactly(1, null);
   }

   @Test
   public void testImmutableListWithMultiElementsWhenNominal() {
      List<Integer> list = immutableList(3, 4, 1, 2);

      assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(list).containsExactly(3, 4, 1, 2);
   }

   @Test
   public void testImmutableListWithMultiElementsWhenNull() {
      List<Integer> list = immutableList(null, 1, 2, null);

      assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(list).containsExactly(null, 1, 2, null);
   }

   @Test
   public void testImmutableMapWhenNominal() {
      Map<String, Integer> map = immutableMap(new HashMap<String, Integer>() {{
         put("a", 1);
         put("b", 2);
      }});

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of("a", 1), Pair.of("b", 2));
   }

   @Test
   public void testImmutableMapWhenMapFactory() {
      Map<String, Integer> map = immutableMap(LinkedHashMap::new, new HashMap<String, Integer>() {{
         put("a", 1);
         put("b", 2);
         put("c", 3);
      }});

      assertThat(map).isNotInstanceOf(LinkedHashMap.class);
      assertThat(map).containsExactly(Pair.of("a", 1), Pair.of("b", 2), Pair.of("c", 3));
   }

   @Test
   public void testImmutableMapWhenMergeFunction() {
      assertThat(immutableMap((v1, v2) -> v2,
                              () -> new HashMap<>(),
                              Pair.of("a", 1),
                              Pair.of("a", 2))).containsOnly(Pair.of("a", 2));
      assertThat(immutableMap((v1, v2) -> v1,
                              () -> new HashMap<>(),
                              Pair.of("a", 1),
                              Pair.of("a", 2))).containsOnly(Pair.of("a", 1));
      assertThat(immutableMap((v1, v2) -> v2,
                              () -> new HashMap<>(),
                              Pair.of("a", 1),
                              Pair.of("a", null))).containsOnly(Pair.of("a", null));
      assertThat(immutableMap((v1, v2) -> v1,
                              () -> new HashMap<>(),
                              Pair.of("a", 1),
                              Pair.of("a", null))).containsOnly(Pair.of("a", 1));
      assertThat(immutableMap((v1, v2) -> v2,
                              () -> new HashMap<>(),
                              Pair.of("a", null),
                              Pair.of("a", 2))).containsOnly(Pair.of("a", 2));
      assertThat(immutableMap((v1, v2) -> v1,
                              () -> new HashMap<>(),
                              Pair.of("a", null),
                              Pair.of("a", 2))).containsOnly(Pair.of("a", null));
   }

   @Test
   public void testImmutableMapWhenNull() {
      Map<String, Integer> map = immutableMap((Map<String, Integer>) null);

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).isEmpty();
   }

   @Test
   public void testImmutableMapWhenEntryNullKey() {
      Map<String, Integer> map = immutableMap(Pair.of(null, 1), Pair.of("b", 2));

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of(null, 1), Pair.of("b", 2));
   }

   @Test
   public void testImmutableMapWhenOverrideEntryWithNullKey() {
      Map<String, Integer> map = immutableMap(Pair.of(null, 1), Pair.of(null, 2));

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of(null, 2));
   }

   @Test
   public void testImmutableMapWhenEntryNullValue() {
      Map<String, Integer> map = immutableMap(Pair.of("a", null), Pair.of("b", 2));

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of("a", null), Pair.of("b", 2));
   }

   @Test
   public void testImmutableMapWhenOverrideEntryWithNullValue() {
      Map<String, Integer> map = immutableMap(Pair.of("a", 1), Pair.of("a", null));

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of("a", null));
   }

   @Test
   public void testImmutableMapWhenOverrideNullValueEntryWithValue() {
      Map<String, Integer> map = immutableMap(Pair.of("a", null), Pair.of("a", 1));

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of("a", 1));
   }

   @Test
   public void testImmutableMapWithMultiMapsWhenNominal() {
      Map<String, Integer> map = immutableMap(new HashMap<String, Integer>() {{
         put("c", 3);
         put("d", 4);
      }}, new HashMap<String, Integer>() {{
         put("a", 1);
         put("b", 2);
      }});

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of("c", 3), Pair.of("d", 4), Pair.of("a", 1), Pair.of("b", 2));
   }

   @Test
   public void testImmutableMapWithMultiMapsWhenNull() {
      Map<String, Integer> map = immutableMap((Map<String, Integer>) null, new HashMap<String, Integer>() {{
         put("c", 3);
         put("d", 4);
      }}, new HashMap<String, Integer>() {{
         put("a", 1);
         put("b", 2);
      }}, null);

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of("c", 3), Pair.of("d", 4), Pair.of("a", 1), Pair.of("b", 2));
   }

   @Test
   public void testImmutableMapWhenEmpty() {
      Map<String, Integer> map = immutableMap();

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).isEmpty();
   }

   @Test
   public void testImmutableMapWithMultiEntriesWhenNominal() {
      Map<String, Integer> map = immutableMap(Pair.of("a", 1), Pair.of("b", 2));

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of("a", 1), Pair.of("b", 2));
   }

   @Test
   public void testImmutableMapWithMultiEntriesWhenNull() {
      Map<String, Integer> map =
            immutableMap((Entry<String, Integer>) null, Pair.of("a", 1), Pair.of("b", 2), null);

      assertThatThrownBy(() -> map.put("a", 1)).isInstanceOf(UnsupportedOperationException.class);
      assertThat(map).containsOnly(Pair.of("a", 1), Pair.of("b", 2));
   }

}