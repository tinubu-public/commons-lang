/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.log;

import static com.tinubu.commons.lang.log.ExtendedLogger.Level.INFO;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.concurrent.Callable;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExtendedLoggerTest {

   private Logger onSlf4jLog;
   private ExtendedLogger onExtendedLog;
   private Logger offSlf4jLog;
   private ExtendedLogger offExtendedLog;

   @BeforeEach
   public void beforeEach() {
      onSlf4jLog = spy(LoggerFactory.getLogger(getClass().getName() + "-on"));
      onExtendedLog = ExtendedLogger.of(onSlf4jLog);
      offSlf4jLog = spy(LoggerFactory.getLogger(getClass().getName() + "-off"));
      offExtendedLog = ExtendedLogger.of(offSlf4jLog);
   }

   @Nested
   class LogWithMessage {

      @Test
      public void logWithMessage() {
         onExtendedLog.log(INFO, "message");
         offExtendedLog.log(INFO, "message");

         verify(onSlf4jLog).info("message");
         verify(offSlf4jLog, never()).info("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null, "message"))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, (String) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void errorWithMessage() {
         onExtendedLog.error("message");
         offExtendedLog.error("message");

         verify(onSlf4jLog).error("message");
         verify(offSlf4jLog, never()).error("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error((String) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void warnWithMessage() {
         onExtendedLog.warn("message");
         offExtendedLog.warn("message");

         verify(onSlf4jLog).warn("message");
         verify(offSlf4jLog, never()).warn("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn((String) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void infoWithMessage() {
         onExtendedLog.info("message");
         offExtendedLog.info("message");

         verify(onSlf4jLog).info("message");
         verify(offSlf4jLog, never()).info("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info((String) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void debugWithMessage() {
         onExtendedLog.debug("message");
         offExtendedLog.debug("message");

         verify(onSlf4jLog).debug("message");
         verify(offSlf4jLog, never()).debug("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug((String) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void traceWithMessage() {
         onExtendedLog.trace("message");
         offExtendedLog.trace("message");

         verify(onSlf4jLog).trace("message");
         verify(offSlf4jLog, never()).trace("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace((String) null))
               .withMessage("'message' must not be null");
      }
   }

   @Nested
   class LogWithCallableMessage {

      @Test
      public void logWithCallableMessage() {
         onExtendedLog.log(INFO, () -> "message");
         offExtendedLog.log(INFO, verifyNeverCall(() -> "message"));

         verify(onSlf4jLog).info("message");
         verify(offSlf4jLog, never()).info("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null, () -> "message"))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, (Callable<String>) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void errorWithCallableMessage() {
         onExtendedLog.error(() -> "message");
         offExtendedLog.error(verifyNeverCall(() -> "message"));

         verify(onSlf4jLog).error("message");
         verify(offSlf4jLog, never()).error("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error((Callable<String>) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void warnWithCallableMessage() {
         onExtendedLog.warn(() -> "message");
         offExtendedLog.warn(verifyNeverCall(() -> "message"));

         verify(onSlf4jLog).warn("message");
         verify(offSlf4jLog, never()).warn("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn((Callable<String>) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void infoWithCallableMessage() {
         onExtendedLog.info(() -> "message");
         offExtendedLog.info(verifyNeverCall(() -> "message"));

         verify(onSlf4jLog).info("message");
         verify(offSlf4jLog, never()).info("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info((Callable<String>) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void debugWithCallableMessage() {
         onExtendedLog.debug(() -> "message");
         offExtendedLog.debug(verifyNeverCall(() -> "message"));

         verify(onSlf4jLog).debug("message");
         verify(offSlf4jLog, never()).debug("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug((Callable<String>) null))
               .withMessage("'message' must not be null");
      }

      @Test
      public void traceWithCallableMessage() {
         onExtendedLog.trace(() -> "message");
         offExtendedLog.trace(verifyNeverCall(() -> "message"));

         verify(onSlf4jLog).trace("message");
         verify(offSlf4jLog, never()).trace("message");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace((Callable<String>) null))
               .withMessage("'message' must not be null");
      }
   }

   @Nested
   class LogWithMessageAnd1Arg {

      @Test
      public void logWithMessageAnd1Arg() {
         onExtendedLog.log(INFO, "message {}", () -> "arg1");
         offExtendedLog.log(INFO, "message {}", verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).info("message {}", "arg1");
         verify(offSlf4jLog, never()).info("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null, "message {}", () -> "arg1"))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, (String) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, "message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.log(INFO, "message {}", () -> null);
         verify(onSlf4jLog).info("message {}", (Object) null);
      }

      @Test
      public void errorWithMessageAnd1Arg() {
         onExtendedLog.error("message {}", () -> "arg1");
         offExtendedLog.error("message {}", verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).error("message {}", "arg1");
         verify(offSlf4jLog, never()).error("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error((String) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error("message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.error("message {}", () -> null);
         verify(onSlf4jLog).error("message {}", (Object) null);
      }

      @Test
      public void warnWithMessageAnd1Arg() {
         onExtendedLog.warn("message {}", () -> "arg1");
         offExtendedLog.warn("message {}", verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).warn("message {}", "arg1");
         verify(offSlf4jLog, never()).warn("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn((String) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn("message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.warn("message {}", () -> null);
         verify(onSlf4jLog).warn("message {}", (Object) null);
      }

      @Test
      public void infoWithMessageAnd1Arg() {
         onExtendedLog.info("message {}", () -> "arg1");
         offExtendedLog.info("message {}", verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).info("message {}", "arg1");
         verify(offSlf4jLog, never()).info("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info((String) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info("message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.info("message {}", () -> null);
         verify(onSlf4jLog).info("message {}", (Object) null);
      }

      @Test
      public void debugWithMessageAnd1Arg() {
         onExtendedLog.debug("message {}", () -> "arg1");
         offExtendedLog.debug("message {}", verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).debug("message {}", "arg1");
         verify(offSlf4jLog, never()).debug("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug((String) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug("message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.debug("message {}", () -> null);
         verify(onSlf4jLog).debug("message {}", (Object) null);
      }

      @Test
      public void traceWithMessageAnd1Arg() {
         onExtendedLog.trace("message {}", () -> "arg1");
         offExtendedLog.trace("message {}", verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).trace("message {}", "arg1");
         verify(offSlf4jLog, never()).trace("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace((String) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace("message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.trace("message {}", () -> null);
         verify(onSlf4jLog).trace("message {}", (Object) null);
      }
   }

   @Nested
   class LogWithCallableMessageAnd1Arg {

      @Test
      public void logWithCallableMessageAnd1Arg() {
         onExtendedLog.log(INFO, () -> "message {}", () -> "arg1");
         offExtendedLog.log(INFO, verifyNeverCall(() -> "message {}"), verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).info("message {}", "arg1");
         verify(offSlf4jLog, never()).info("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null, () -> "message {}", () -> "arg1"))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, (Callable<String>) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, () -> "message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.log(INFO, () -> "message {}", () -> null);
         verify(onSlf4jLog).info("message {}", (Object) null);
      }

      @Test
      public void errorWithCallableMessageAnd1Arg() {
         onExtendedLog.error(() -> "message {}", () -> "arg1");
         offExtendedLog.error(verifyNeverCall(() -> "message {}"), verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).error("message {}", "arg1");
         verify(offSlf4jLog, never()).error("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error((Callable<String>) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error(() -> "message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.error(() -> "message {}", () -> null);
         verify(onSlf4jLog).error("message {}", (Object) null);
      }

      @Test
      public void warnWithCallableMessageAnd1Arg() {
         onExtendedLog.warn(() -> "message {}", () -> "arg1");
         offExtendedLog.warn(verifyNeverCall(() -> "message {}"), verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).warn("message {}", "arg1");
         verify(offSlf4jLog, never()).warn("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn((Callable<String>) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn(() -> "message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.warn(() -> "message {}", () -> null);
         verify(onSlf4jLog).warn("message {}", (Object) null);
      }

      @Test
      public void infoWithCallableMessageAnd1Arg() {
         onExtendedLog.info(() -> "message {}", () -> "arg1");
         offExtendedLog.info(verifyNeverCall(() -> "message {}"), verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).info("message {}", "arg1");
         verify(offSlf4jLog, never()).info("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info((Callable<String>) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info(() -> "message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.info(() -> "message {}", () -> null);
         verify(onSlf4jLog).info("message {}", (Object) null);
      }

      @Test
      public void debugWithCallableMessageAnd1Arg() {
         onExtendedLog.debug(() -> "message {}", () -> "arg1");
         offExtendedLog.debug(verifyNeverCall(() -> "message {}"), verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).debug("message {}", "arg1");
         verify(offSlf4jLog, never()).debug("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug((Callable<String>) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug(() -> "message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.debug(() -> "message {}", () -> null);
         verify(onSlf4jLog).debug("message {}", (Object) null);
      }

      @Test
      public void traceWithCallableMessageAnd1Arg() {
         onExtendedLog.trace(() -> "message {}", () -> "arg1");
         offExtendedLog.trace(verifyNeverCall(() -> "message {}"), verifyNeverCall(() -> "arg1"));

         verify(onSlf4jLog).trace("message {}", "arg1");
         verify(offSlf4jLog, never()).trace("message {}", "arg1");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace((Callable<String>) null, () -> "arg1"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace(() -> "message {}", (Callable<?>) null))
               .withMessage("'arg' must not be null");

         onExtendedLog.trace(() -> "message {}", () -> null);
         verify(onSlf4jLog).trace("message {}", (Object) null);
      }
   }

   @Nested
   class LogWithMessageAnd2Args {

      @Test
      public void logWithMessageAnd2Args() {
         onExtendedLog.log(INFO, "message {} {}", () -> "arg1", () -> "arg2");
         offExtendedLog.log(INFO,
                            "message {} {}",
                            verifyNeverCall(() -> "arg1"),
                            verifyNeverCall(() -> "arg2"));

         verify(onSlf4jLog).info("message {} {}", "arg1", "arg2");
         verify(offSlf4jLog, never()).info("message {} {}", "arg1", "arg2");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null, "message {} {}", () -> "arg1", () -> "arg2"))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, (String) null, () -> "arg1", () -> "arg2"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, "message {} {}", (Callable<?>) null, () -> "arg2"))
               .withMessage("'arg1' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, "message {} {}", () -> "arg1", (Callable<?>) null))
               .withMessage("'arg2' must not be null");

         onExtendedLog.log(INFO, "message {} {}", () -> null, () -> null);
         verify(onSlf4jLog).info("message {} {}", (Object) null, (Object) null);
      }

      @Test
      public void errorWithMessageAnd2Args() {
         onExtendedLog.error("message {} {}", () -> "arg1", () -> "arg2");
         offExtendedLog.error("message {} {}", verifyNeverCall(() -> "arg1"), verifyNeverCall(() -> "arg2"));

         verify(onSlf4jLog).error("message {} {}", "arg1", "arg2");
         verify(offSlf4jLog, never()).error("message {} {}", "arg1", "arg2");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error((String) null, () -> "arg1", () -> "arg2"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error("message {} {}", (Callable<?>) null, () -> "arg2"))
               .withMessage("'arg1' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error("message {} {}", () -> "arg1", (Callable<?>) null))
               .withMessage("'arg2' must not be null");

         onExtendedLog.error("message {} {}", () -> null, () -> null);
         verify(onSlf4jLog).error("message {} {}", (Object) null, (Object) null);
      }

      @Test
      public void warnWithMessageAnd2Args() {
         onExtendedLog.warn("message {} {}", () -> "arg1", () -> "arg2");
         offExtendedLog.warn("message {} {}", verifyNeverCall(() -> "arg1"), verifyNeverCall(() -> "arg2"));

         verify(onSlf4jLog).warn("message {} {}", "arg1", "arg2");
         verify(offSlf4jLog, never()).warn("message {} {}", "arg1", "arg2");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn((String) null, () -> "arg1", () -> "arg2"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn("message {} {}", (Callable<?>) null, () -> "arg2"))
               .withMessage("'arg1' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn("message {} {}", () -> "arg1", (Callable<?>) null))
               .withMessage("'arg2' must not be null");

         onExtendedLog.warn("message {} {}", () -> null, () -> null);
         verify(onSlf4jLog).warn("message {} {}", (Object) null, (Object) null);
      }

      @Test
      public void infoWithMessageAnd2Args() {
         onExtendedLog.info("message {} {}", () -> "arg1", () -> "arg2");
         offExtendedLog.info("message {} {}", verifyNeverCall(() -> "arg1"), verifyNeverCall(() -> "arg2"));

         verify(onSlf4jLog).info("message {} {}", "arg1", "arg2");
         verify(offSlf4jLog, never()).info("message {} {}", "arg1", "arg2");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info((String) null, () -> "arg1", () -> "arg2"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info("message {} {}", (Callable<?>) null, () -> "arg2"))
               .withMessage("'arg1' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info("message {} {}", () -> "arg1", (Callable<?>) null))
               .withMessage("'arg2' must not be null");

         onExtendedLog.info("message {} {}", () -> null, () -> null);
         verify(onSlf4jLog).info("message {} {}", (Object) null, (Object) null);
      }

      @Test
      public void debugWithMessageAnd2Args() {
         onExtendedLog.debug("message {} {}", () -> "arg1", () -> "arg2");
         offExtendedLog.debug("message {} {}", verifyNeverCall(() -> "arg1"), verifyNeverCall(() -> "arg2"));

         verify(onSlf4jLog).debug("message {} {}", "arg1", "arg2");
         verify(offSlf4jLog, never()).debug("message {} {}", "arg1", "arg2");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug((String) null, () -> "arg1", () -> "arg2"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug("message {} {}", (Callable<?>) null, () -> "arg2"))
               .withMessage("'arg1' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug("message {} {}", () -> "arg1", (Callable<?>) null))
               .withMessage("'arg2' must not be null");

         onExtendedLog.debug("message {} {}", () -> null, () -> null);
         verify(onSlf4jLog).debug("message {} {}", (Object) null, (Object) null);
      }

      @Test
      public void traceWithMessageAnd2Args() {
         onExtendedLog.trace("message {} {}", () -> "arg1", () -> "arg2");
         offExtendedLog.trace("message {} {}", verifyNeverCall(() -> "arg1"), verifyNeverCall(() -> "arg2"));

         verify(onSlf4jLog).trace("message {} {}", "arg1", "arg2");
         verify(offSlf4jLog, never()).trace("message {} {}", "arg1", "arg2");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace((String) null, () -> "arg1", () -> "arg2"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace("message {} {}", (Callable<?>) null, () -> "arg2"))
               .withMessage("'arg1' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace("message {} {}", () -> "arg1", (Callable<?>) null))
               .withMessage("'arg2' must not be null");

         onExtendedLog.trace("message {} {}", () -> null, () -> null);
         verify(onSlf4jLog).trace("message {} {}", (Object) null, (Object) null);
      }
   }

   @Nested
   class LogWithCallableMessageAnd2Args {

      @Test
      public void logWithCallableMessageAnd2Args() {
         onExtendedLog.log(INFO, () -> "message {} {}", () -> "arg1", () -> "arg2");
         offExtendedLog.log(INFO,
                            verifyNeverCall(() -> "message {} {}"),
                            verifyNeverCall(() -> "arg1"),
                            verifyNeverCall(() -> "arg2"));

         verify(onSlf4jLog).info("message {} {}", "arg1", "arg2");
         verify(offSlf4jLog, never()).info("message {} {}", "arg1", "arg2");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null, () -> "message {} {}", () -> "arg1", () -> "arg2"))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    (Callable<String>) null,
                                                    () -> "arg1",
                                                    () -> "arg2"))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    () -> "message {}",
                                                    (Callable<?>) null,
                                                    () -> "arg2"))
               .withMessage("'arg1' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    () -> "message {}",
                                                    () -> "arg1",
                                                    (Callable<?>) null))
               .withMessage("'arg2' must not be null");

         onExtendedLog.log(INFO, () -> "message {} {}", () -> null, () -> null);
         verify(onSlf4jLog).info("message {} {}", (Object) null, (Object) null);
      }
   }

   @Nested
   class LogWithMessageAndNArgs {

      @Test
      public void logWithMessageAndNArgs() {
         onExtendedLog.log(INFO, "message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.log(INFO,
                            "message {} {} {}",
                            verifyNeverCall(() -> "arg1"),
                            verifyNeverCall(() -> "arg2"),
                            verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).info("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).info("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null,
                                                    "message {} {} {}",
                                                    () -> "arg1",
                                                    () -> "arg2",
                                                    () -> "arg3"))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    (String) null,
                                                    () -> "arg1",
                                                    () -> "arg2",
                                                    () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    "message {} {} {}",
                                                    (Callable<?>) null,
                                                    () -> "arg2",
                                                    () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    "message {} {} {}",
                                                    () -> "arg1",
                                                    (Callable<?>) null,
                                                    () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    "message {} {} {}",
                                                    () -> "arg1",
                                                    () -> "arg2",
                                                    (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.log(INFO, "message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).info("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void errorWithMessageAndNArgs() {
         onExtendedLog.error("message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.error("message {} {} {}",
                              verifyNeverCall(() -> "arg1"),
                              verifyNeverCall(() -> "arg2"),
                              verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).error("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).error("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error((String) null,
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.error("message {} {} {}",
                                                      (Callable<?>) null,
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.error("message {} {} {}",
                                                      () -> "arg1",
                                                      (Callable<?>) null,
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.error("message {} {} {}",
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.error("message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).error("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void warnWithMessageAndNArgs() {
         onExtendedLog.warn("message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.warn("message {} {} {}",
                             verifyNeverCall(() -> "arg1"),
                             verifyNeverCall(() -> "arg2"),
                             verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).warn("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).warn("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn((String) null, () -> "arg1", () -> "arg2", () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.warn("message {} {} {}",
                                                     (Callable<?>) null,
                                                     () -> "arg2",
                                                     () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.warn("message {} {} {}",
                                                     () -> "arg1",
                                                     (Callable<?>) null,
                                                     () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.warn("message {} {} {}",
                                                     () -> "arg1",
                                                     () -> "arg2",
                                                     (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.warn("message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).warn("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void infoWithMessageAndNArgs() {
         onExtendedLog.info("message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.info("message {} {} {}",
                             verifyNeverCall(() -> "arg1"),
                             verifyNeverCall(() -> "arg2"),
                             verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).info("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).info("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info((String) null, () -> "arg1", () -> "arg2", () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.info("message {} {} {}",
                                                     (Callable<?>) null,
                                                     () -> "arg2",
                                                     () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.info("message {} {} {}",
                                                     () -> "arg1",
                                                     (Callable<?>) null,
                                                     () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.info("message {} {} {}",
                                                     () -> "arg1",
                                                     () -> "arg2",
                                                     (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.info("message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).info("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void debugWithMessageAndNArgs() {
         onExtendedLog.debug("message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.debug("message {} {} {}",
                              verifyNeverCall(() -> "arg1"),
                              verifyNeverCall(() -> "arg2"),
                              verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).debug("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).debug("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug((String) null,
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.debug("message {} {} {}",
                                                      (Callable<?>) null,
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.debug("message {} {} {}",
                                                      () -> "arg1",
                                                      (Callable<?>) null,
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.debug("message {} {} {}",
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.debug("message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).debug("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void traceWithMessageAndNArgs() {
         onExtendedLog.trace("message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.trace("message {} {} {}",
                              verifyNeverCall(() -> "arg1"),
                              verifyNeverCall(() -> "arg2"),
                              verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).trace("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).trace("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace((String) null,
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.trace("message {} {} {}",
                                                      (Callable<?>) null,
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.trace("message {} {} {}",
                                                      () -> "arg1",
                                                      (Callable<?>) null,
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.trace("message {} {} {}",
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.trace("message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).trace("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }
   }

   @Nested
   class LogWithCallableMessageAndNArgs {

      @Test
      public void logWithCallableMessageAndNArgs() {
         onExtendedLog.log(INFO, () -> "message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.log(INFO,
                            verifyNeverCall(() -> "message {} {} {}"),
                            verifyNeverCall(() -> "arg1"),
                            verifyNeverCall(() -> "arg2"),
                            verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).info("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).info("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null,
                                                    () -> "message {} {} {}",
                                                    () -> "arg1",
                                                    () -> "arg2",
                                                    () -> "arg3"))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    (Callable<String>) null,
                                                    () -> "arg1",
                                                    () -> "arg2",
                                                    () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    () -> "message {} {} {}",
                                                    (Callable<?>) null,
                                                    () -> "arg2",
                                                    () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    () -> "message {} {} {}",
                                                    () -> "arg1",
                                                    (Callable<?>) null,
                                                    () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    () -> "message {} {} {}",
                                                    () -> "arg1",
                                                    () -> "arg2",
                                                    (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.log(INFO, () -> "message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).info("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void errorWithCallableMessageAndNArgs() {
         onExtendedLog.error(() -> "message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.error(verifyNeverCall(() -> "message {} {} {}"),
                              verifyNeverCall(() -> "arg1"),
                              verifyNeverCall(() -> "arg2"),
                              verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).error("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).error("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error((Callable<String>) null,
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.error(() -> "message {} {} {}",
                                                      (Callable<?>) null,
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.error(() -> "message {} {} {}",
                                                      () -> "arg1",
                                                      (Callable<?>) null,
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.error(() -> "message {} {} {}",
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.error(() -> "message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).error("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void warnWithCallableMessageAndNArgs() {
         onExtendedLog.warn(() -> "message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.warn(verifyNeverCall(() -> "message {} {} {}"),
                             verifyNeverCall(() -> "arg1"),
                             verifyNeverCall(() -> "arg2"),
                             verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).warn("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).warn("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn((Callable<String>) null,
                                                     () -> "arg1",
                                                     () -> "arg2",
                                                     () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.warn(() -> "message {} {} {}",
                                                     (Callable<?>) null,
                                                     () -> "arg2",
                                                     () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.warn(() -> "message {} {} {}",
                                                     () -> "arg1",
                                                     (Callable<?>) null,
                                                     () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.warn(() -> "message {} {} {}",
                                                     () -> "arg1",
                                                     () -> "arg2",
                                                     (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.warn(() -> "message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).warn("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void infoWithCallableMessageAndNArgs() {
         onExtendedLog.info(() -> "message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.info(verifyNeverCall(() -> "message {} {} {}"),
                             verifyNeverCall(() -> "arg1"),
                             verifyNeverCall(() -> "arg2"),
                             verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).info("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).info("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info((Callable<String>) null,
                                                     () -> "arg1",
                                                     () -> "arg2",
                                                     () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.info(() -> "message {} {} {}",
                                                     (Callable<?>) null,
                                                     () -> "arg2",
                                                     () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.info(() -> "message {} {} {}",
                                                     () -> "arg1",
                                                     (Callable<?>) null,
                                                     () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.info(() -> "message {} {} {}",
                                                     () -> "arg1",
                                                     () -> "arg2",
                                                     (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.info(() -> "message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).info("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void debugWithCallableMessageAndNArgs() {
         onExtendedLog.debug(() -> "message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.debug(verifyNeverCall(() -> "message {} {} {}"),
                              verifyNeverCall(() -> "arg1"),
                              verifyNeverCall(() -> "arg2"),
                              verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).debug("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).debug("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug((Callable<String>) null,
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.debug(() -> "message {} {} {}",
                                                      (Callable<?>) null,
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.debug(() -> "message {} {} {}",
                                                      () -> "arg1",
                                                      (Callable<?>) null,
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.debug(() -> "message {} {} {}",
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.debug(() -> "message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).debug("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }

      @Test
      public void traceWithCallableMessageAndNArgs() {
         onExtendedLog.trace(() -> "message {} {} {}", () -> "arg1", () -> "arg2", () -> "arg3");
         offExtendedLog.trace(verifyNeverCall(() -> "message {} {} {}"),
                              verifyNeverCall(() -> "arg1"),
                              verifyNeverCall(() -> "arg2"),
                              verifyNeverCall(() -> "arg3"));

         verify(onSlf4jLog).trace("message {} {} {}", "arg1", "arg2", "arg3");
         verify(offSlf4jLog, never()).trace("message {} {} {}", "arg1", "arg2", "arg3");

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace((Callable<String>) null,
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'message' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.trace(() -> "message {} {} {}",
                                                      (Callable<?>) null,
                                                      () -> "arg2",
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 0");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.trace(() -> "message {} {} {}",
                                                      () -> "arg1",
                                                      (Callable<?>) null,
                                                      () -> "arg3"))
               .withMessage("'args' must not have null elements at index : 1");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> offExtendedLog.trace(() -> "message {} {} {}",
                                                      () -> "arg1",
                                                      () -> "arg2",
                                                      (Callable<?>) null))
               .withMessage("'args' must not have null elements at index : 2");

         onExtendedLog.trace(() -> "message {} {} {}", () -> null, () -> null, () -> null);
         verify(onSlf4jLog).trace("message {} {} {}", (Object) null, (Object) null, (Object) null);
      }
   }

   @Nested
   class LogWithMessageAndThrowable {

      @Test
      public void logWithMessageAndThrowable() {
         onExtendedLog.log(INFO, "message", new IllegalStateException("Error"));
         offExtendedLog.log(INFO, "message", new IllegalStateException("Error"));

         verify(onSlf4jLog).info(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).info(eq("message"),
                                           argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null, "message", new IllegalStateException("Error")))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, (String) null, new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, "message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void errorWithMessageAndThrowable() {
         onExtendedLog.error("message", new IllegalStateException("Error"));
         offExtendedLog.error("message", new IllegalStateException("Error"));

         verify(onSlf4jLog).error(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).error(eq("message"),
                                            argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error((String) null, new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error("message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void warnWithMessageAndThrowable() {
         onExtendedLog.warn("message", new IllegalStateException("Error"));
         offExtendedLog.warn("message", new IllegalStateException("Error"));

         verify(onSlf4jLog).warn(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).warn(eq("message"),
                                           argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn((String) null, new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn("message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void infoWithMessageAndThrowable() {
         onExtendedLog.info("message", new IllegalStateException("Error"));
         offExtendedLog.info("message", new IllegalStateException("Error"));

         verify(onSlf4jLog).info(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).info(eq("message"),
                                           argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info((String) null, new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info("message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void debugWithMessageAndThrowable() {
         onExtendedLog.debug("message", new IllegalStateException("Error"));
         offExtendedLog.debug("message", new IllegalStateException("Error"));

         verify(onSlf4jLog).debug(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).debug(eq("message"),
                                            argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug((String) null, new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug("message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void traceWithMessageAndThrowable() {
         onExtendedLog.trace("message", new IllegalStateException("Error"));
         offExtendedLog.trace("message", new IllegalStateException("Error"));

         verify(onSlf4jLog).trace(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).trace(eq("message"),
                                            argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace((String) null, new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace("message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }
   }

   @Nested
   class LogWithCallableMessageAndThrowable {

      @Test
      public void logWithCallableMessageAndThrowable() {
         onExtendedLog.log(INFO, () -> "message", new IllegalStateException("Error"));
         offExtendedLog.log(INFO, verifyNeverCall(() -> "message"), new IllegalStateException("Error"));

         verify(onSlf4jLog).info(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).info(eq("message"),
                                           argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(null,
                                                    () -> "message",
                                                    new IllegalStateException("Error")))
               .withMessage("'level' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO,
                                                    (Callable<String>) null,
                                                    new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.log(INFO, () -> "message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void errorWithCallableMessageAndThrowable() {
         onExtendedLog.error(() -> "message", new IllegalStateException("Error"));
         offExtendedLog.error(verifyNeverCall(() -> "message"), new IllegalStateException("Error"));

         verify(onSlf4jLog).error(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).error(eq("message"),
                                            argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error((Callable<String>) null,
                                                      new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.error(() -> "message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void warnWithCallableMessageAndThrowable() {
         onExtendedLog.warn(() -> "message", new IllegalStateException("Error"));
         offExtendedLog.warn(verifyNeverCall(() -> "message"), new IllegalStateException("Error"));

         verify(onSlf4jLog).warn(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).warn(eq("message"),
                                           argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn((Callable<String>) null,
                                                     new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.warn(() -> "message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void infoWithCallableMessageAndThrowable() {
         onExtendedLog.info(() -> "message", new IllegalStateException("Error"));
         offExtendedLog.info(verifyNeverCall(() -> "message"), new IllegalStateException("Error"));

         verify(onSlf4jLog).info(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).info(eq("message"),
                                           argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info((Callable<String>) null,
                                                     new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.info(() -> "message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void debugWithCallableMessageAndThrowable() {
         onExtendedLog.debug(() -> "message", new IllegalStateException("Error"));
         offExtendedLog.debug(verifyNeverCall(() -> "message"), new IllegalStateException("Error"));

         verify(onSlf4jLog).debug(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).debug(eq("message"),
                                            argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug((Callable<String>) null,
                                                      new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.debug(() -> "message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }

      @Test
      public void traceWithCallableMessageAndThrowable() {
         onExtendedLog.trace(() -> "message", new IllegalStateException("Error"));
         offExtendedLog.trace(verifyNeverCall(() -> "message"), new IllegalStateException("Error"));

         verify(onSlf4jLog).trace(eq("message"), argThat(eqThrowable(new IllegalStateException("Error"))));
         verify(offSlf4jLog, never()).trace(eq("message"),
                                            argThat(eqThrowable(new IllegalStateException("Error"))));

         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace((Callable<String>) null,
                                                      new IllegalStateException("Error")))
               .withMessage("'message' must not be null");
         assertThatNullPointerException()
               .isThrownBy(() -> offExtendedLog.trace(() -> "message", (Throwable) null))
               .withMessage("'throwable' must not be null");
      }
   }

   private <T extends Throwable> ArgumentMatcher<T> eqThrowable(Throwable exception) {
      return argument -> {
         if (argument == null || exception == null) {
            return false;
         }
         return argument.getClass().equals(exception.getClass()) && argument
               .getMessage()
               .equals(exception.getMessage());
      };
   }

   private <T> Callable<T> verifyNeverCall(Callable<T> ignored) {
      return () -> {
         throw new IllegalStateException("Callable must not be called");
      };
   }

}