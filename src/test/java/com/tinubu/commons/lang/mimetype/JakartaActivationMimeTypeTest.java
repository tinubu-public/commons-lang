/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.StandardCharsets;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Map;

import org.junit.jupiter.api.Test;

class JakartaActivationMimeTypeTest extends CommonMimeTypeTest {

   @Override
   protected MimeType newMimeType(String type, String subtype, Map<String, String> parameters) {
      return new JakartaActivationMimeType(type, subtype, parameters);
   }

   @Override
   protected MimeType newMimeType(String type, String subtype) {
      return newMimeType(type, subtype, map());
   }

   @Test
   public void testJakartaActivationMimeTypeFromType() {
      MimeType mimeType = new JakartaActivationMimeType("application");

      assertThat(mimeType.type()).isEqualTo("application");
      assertThat(mimeType.subtype()).isEqualTo("*");
      assertThat(mimeType.parameters()).isEmpty();

      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType((String) null))
            .withMessage("'type' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new JakartaActivationMimeType(""))
            .withMessage("'type' must not be blank");
   }

   @Test
   public void testJakartaActivationMimeTypeFromTypeSubtype() {
      MimeType mimeType = new JakartaActivationMimeType("application", "pdf");

      assertThat(mimeType.type()).isEqualTo("application");
      assertThat(mimeType.subtype()).isEqualTo("pdf");
      assertThat(mimeType.parameters()).isEmpty();

      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType(null, "pdf"))
            .withMessage("'type' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new JakartaActivationMimeType("", "pdf"))
            .withMessage("'type' must not be blank");
      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", null))
            .withMessage("'subtype' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", ""))
            .withMessage("'subtype' must not be blank");
   }

   @Test
   public void testJakartaActivationMimeTypeFromTypeSubtypeParameters() {
      MimeType mimeTypeWithEmptyParameters = new JakartaActivationMimeType("application", "pdf", map());

      assertThat(mimeTypeWithEmptyParameters.type()).isEqualTo("application");
      assertThat(mimeTypeWithEmptyParameters.subtype()).isEqualTo("pdf");
      assertThat(mimeTypeWithEmptyParameters.parameters()).isEmpty();

      MimeType mimeType = new JakartaActivationMimeType("application",
                                                        "pdf",
                                                        map(entry("key", "value"),
                                                            entry("charset", "UTF-8")));

      assertThat(mimeType.type()).isEqualTo("application");
      assertThat(mimeType.subtype()).isEqualTo("pdf");
      assertThat(mimeType.parameters()).containsExactly(entry("key", "value"), entry("charset", "UTF-8"));
      assertThat(mimeType.charset()).hasValue(StandardCharsets.UTF_8);

      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType(null, "pdf", map(entry("key", "value"))))
            .withMessage("'type' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new JakartaActivationMimeType("", "pdf", map(entry("key", "value"))))
            .withMessage("'type' must not be blank");
      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", null, map(entry("key", "value"))))
            .withMessage("'subtype' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", "", map(entry("key", "value"))))
            .withMessage("'subtype' must not be blank");
      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", "pdf", (Map<String, String>) null))
            .withMessage("'parameters' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", "pdf", map(entry(null, "value"))))
            .withMessage("'parameters{null}' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", "pdf", map(entry("", "value"))))
            .withMessage("'parameters{}' must not be blank");
      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", "pdf", map(entry("key", null))))
            .withMessage("'parameters{key}.value' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", "pdf", map(entry("key", ""))))
            .withMessage("'parameters{key}.value' must not be blank");

      assertThatExceptionOfType(UnsupportedCharsetException.class).isThrownBy(() -> new JakartaActivationMimeType(
            "application",
            "pdf",
            map(entry("charset", "NOT-EXISTS"))));
      assertThatExceptionOfType(IllegalCharsetNameException.class).isThrownBy(() -> new JakartaActivationMimeType(
            "application",
            "pdf",
            map(entry("charset", "UTF@8"))));
   }

   @Test
   public void testJakartaActivationMimeTypeFromTypeSubtypeCharset() {
      MimeType mimeType = new JakartaActivationMimeType("application", "pdf", StandardCharsets.UTF_8);

      assertThat(mimeType.type()).isEqualTo("application");
      assertThat(mimeType.subtype()).isEqualTo("pdf");
      assertThat(mimeType.parameters()).containsExactly(entry("charset", "UTF-8"));
      assertThat(mimeType.charset()).hasValue(StandardCharsets.UTF_8);

      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType(null, "pdf", map(entry("key", "value"))))
            .withMessage("'type' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new JakartaActivationMimeType("", "pdf", map(entry("key", "value"))))
            .withMessage("'type' must not be blank");
      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", null, map(entry("key", "value"))))
            .withMessage("'subtype' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", "", map(entry("key", "value"))))
            .withMessage("'subtype' must not be blank");
      assertThatNullPointerException()
            .isThrownBy(() -> new JakartaActivationMimeType("application", "pdf", (Charset) null))
            .withMessage("'charset' must not be null");
   }

}