/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

// FIXME quotes management testsuite
public abstract class CommonMimeTypeTest {

   protected abstract MimeType newMimeType(String type, String subtype, Map<String, String> parameters);

   protected abstract MimeType newMimeType(String type, String subtype);

   @Test
   public void testTypeWhenNominal() {
      assertThat(newMimeType("application", "pdf").type()).isEqualTo("application");
   }

   @Test
   public void testTypeWhenCaseInsensitive() {
      assertThat(newMimeType("APPLICATION", "pdf").type()).isEqualTo("application");
   }

   @Test
   public void testTypeWhenWildcard() {
      assertThat(newMimeType("*", "pdf").type()).isEqualTo("*");
   }

   @Test
   public void testSubtypeWhenNominal() {
      assertThat(newMimeType("application", "pdf").subtype()).isEqualTo("pdf");
   }

   @Test
   public void testSubtypeWhenCaseInsensitive() {
      assertThat(newMimeType("APPLICATION", "pdf").subtype()).isEqualTo("pdf");
   }

   @Test
   public void testSubtypeWhenWildcard() {
      assertThat(newMimeType("application", "*").subtype()).isEqualTo("*");
   }

   @Test
   public void testStrippedParametersWhenNominal() {
      MimeType stripped = newMimeType("application", "pdf", map(entry("key", "value"))).strippedParameters();

      assertThat(stripped.type()).isEqualTo("application");
      assertThat(stripped.subtype()).isEqualTo("pdf");
      assertThat(stripped.parameters()).isEmpty();
      assertThat(stripped.charset()).isEmpty();
   }

   @Test
   public void testStrippedParametersWhenNoParameters() {
      MimeType stripped = newMimeType("application", "pdf").strippedParameters();

      assertThat(stripped.type()).isEqualTo("application");
      assertThat(stripped.subtype()).isEqualTo("pdf");
      assertThat(stripped.parameters()).isEmpty();
      assertThat(stripped.charset()).isEmpty();
   }

   @Test
   public void testStrippedExperimentalWhenNominal() {
      MimeType stripped =
            newMimeType("application", "x-experimental", map(entry("key", "value"))).strippedExperimental();

      assertThat(stripped.type()).isEqualTo("application");
      assertThat(stripped.subtype()).isEqualTo("experimental");
      assertThat(stripped.parameters()).containsExactly(entry("key", "value"));
      assertThat(stripped.charset()).isEmpty();
   }

   @Test
   public void testStrippedExperimentalWhenNotExperimental() {
      MimeType stripped =
            newMimeType("application", "experimental", map(entry("key", "value"))).strippedExperimental();

      assertThat(stripped.type()).isEqualTo("application");
      assertThat(stripped.subtype()).isEqualTo("experimental");
      assertThat(stripped.parameters()).containsExactly(entry("key", "value"));
      assertThat(stripped.charset()).isEmpty();
   }

   @Test
   public void testWildcardTypeWhenNominal() {
      assertThat(newMimeType("*", "pdf").wildcardType()).isTrue();
      assertThat(newMimeType("*", "*").wildcardType()).isTrue();
      assertThat(newMimeType("application", "pdf").wildcardType()).isFalse();
      assertThat(newMimeType("application", "*").wildcardType()).isFalse();
   }

   @Test
   public void testWildcardSubtypeWhenNominal() {
      assertThat(newMimeType("application", "*").wildcardSubtype()).isTrue();
      assertThat(newMimeType("*", "*").wildcardSubtype()).isTrue();
      assertThat(newMimeType("application", "pdf").wildcardSubtype()).isFalse();
      assertThat(newMimeType("*", "pdf").wildcardSubtype()).isFalse();
   }

   @Test
   public void testConcreteWhenNominal() {
      assertThat(newMimeType("application", "pdf").concrete()).isTrue();
      assertThat(newMimeType("application", "*").concrete()).isFalse();
      assertThat(newMimeType("*", "pdf").concrete()).isFalse();
      assertThat(newMimeType("*", "*").concrete()).isFalse();
   }

   @Test
   public void testParametersWhenNominal() {
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).parameters()).containsExactly(
            entry("key", "value"));
   }

   @Test
   public void testParametersWhenCaseInsensitive() {
      assertThat(newMimeType("application", "pdf", map(entry("KEY", "VALUE"))).parameters()).containsExactly(
            entry("key", "VALUE"));
   }

   @Test
   public void testParametersWhenEmpty() {
      assertThat(newMimeType("application", "pdf").parameters()).isEmpty();
   }

   @Test
   public void testParameterWhenNominal() {
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).parameter("key")).hasValue(
            "value");
      assertThat(newMimeType("application",
                             "pdf",
                             map(entry("key", "value"))).parameter("notexists")).isEmpty();
      assertThat(newMimeType("application", "pdf", map()).parameter("key")).isEmpty();
   }

   @Test
   public void testParameterWhenCaseInsensitive() {
      assertThat(newMimeType("application", "pdf", map(entry("KEY", "VALUE"))).parameter("key")).hasValue(
            "VALUE");
      assertThat(newMimeType("application", "pdf", map(entry("KEY", "VALUE"))).parameter("KEY")).hasValue(
            "VALUE");
      assertThat(newMimeType("application",
                             "pdf",
                             map(entry("CHARSET", "UTF-8"))).parameter("charset")).hasValue("UTF-8");
      assertThat(newMimeType("application",
                             "pdf",
                             map(entry("charset", "UTF-8"))).parameter("CHARSET")).hasValue("UTF-8");
   }

   @Test
   public void testCharsetWhenNominal() {
      assertThat(newMimeType("application", "pdf", map(entry("charset", "UTF-8"))).charset()).hasValue(UTF_8);
   }

   @Test
   public void testCharsetWhenUnset() {
      assertThat(newMimeType("application", "pdf").charset()).isEmpty();
   }

   @Test
   public void testCharsetWhenCaseInsensitive() {
      assertThat(newMimeType("application", "pdf", map(entry("charset", "uTf-8"))).charset()).hasValue(UTF_8);
      assertThat(newMimeType("application", "pdf", map(entry("CHARSET", "UTF-8"))).charset()).hasValue(UTF_8);
   }

   @Test
   public void testTypeWildcardSetterWhenNominal() {
      assertThat(newMimeType("application", "pdf").typeWildcard()).isEqualTo(newMimeType("*", "pdf"));
   }

   @Test
   public void testTypeWildcardSetterWhenParameters() {
      assertThat(newMimeType("application", "pdf", map(entry("charset", "UTF-8"))).typeWildcard()).isEqualTo(
            newMimeType("*", "pdf", map(entry("charset", "UTF-8"))));
   }

   @Test
   public void testSubtypeWildcardSetterWhenNominal() {
      assertThat(newMimeType("application", "pdf").subtypeWildcard()).isEqualTo(newMimeType("application",
                                                                                            "*"));
   }

   @Test
   public void testSubtypeWildcardSetterWhenParameters() {
      assertThat(newMimeType("application",
                             "pdf",
                             map(entry("charset", "UTF-8"))).subtypeWildcard()).isEqualTo(newMimeType(
            "application",
            "*",
            map(entry("charset", "UTF-8"))));
   }

   @Test
   public void testCharsetSetterWhenNominal() {
      assertThat(newMimeType("application", "pdf").charset(US_ASCII)).isEqualTo(newMimeType("application",
                                                                                            "pdf",
                                                                                            map(entry(
                                                                                                  "charset",
                                                                                                  "US-ASCII"))));
   }

   @Test
   public void testCharsetSetterWhenExistingCharsetParameter() {
      assertThat(newMimeType("application",
                             "pdf",
                             map(entry("charset", "UTF-8"))).charset(US_ASCII)).isEqualTo(newMimeType(
            "application",
            "pdf",
            map(entry("charset", "US-ASCII"))));
   }

   @Test
   public void testCharsetSetterWhenExistingParameters() {
      assertThat(newMimeType("application",
                             "pdf",
                             map(entry("key", "value"),
                                 entry("charset", "UTF-8"))).charset(US_ASCII)).isEqualTo(newMimeType(
            "application",
            "pdf",
            map(entry("key", "value"), entry("charset", "US-ASCII"))));
   }

   @Test
   public void testEqualsTypeAndSubtypeWhenNominal() {
      assertThat(newMimeType("application", "pdf", map()).equalsTypeAndSubtype(newMimeType("application",
                                                                                           "pdf",
                                                                                           map()))).isTrue();
      assertThat(newMimeType("application", "json", map()).equalsTypeAndSubtype(newMimeType("application",
                                                                                            "pdf",
                                                                                            map()))).isFalse();
      assertThat(newMimeType("text", "pdf", map()).equalsTypeAndSubtype(newMimeType("application",
                                                                                    "pdf",
                                                                                    map()))).isFalse();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).equalsTypeAndSubtype(
            newMimeType("application", "pdf", map()))).isTrue();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).equalsTypeAndSubtype(
            newMimeType("application", "pdf", map(entry("otherkey", "otherValue"))))).isTrue();

   }

   @Test
   public void testEqualsTypeAndSubtypeWhenCaseInsensitive() {
      assertThat(newMimeType("application", "pdf").equalsTypeAndSubtype(newMimeType("APPLICATION",
                                                                                    "pdf"))).isTrue();
      assertThat(newMimeType("application", "pdf").equalsTypeAndSubtype(newMimeType("application",
                                                                                    "PDF"))).isTrue();
   }

   @Test
   public void testEqualsTypeAndSubtypeWhenWildcard() {
      assertThat(newMimeType("*", "pdf").equalsTypeAndSubtype(newMimeType("*", "pdf"))).isTrue();
      assertThat(newMimeType("application", "*").equalsTypeAndSubtype(newMimeType("application",
                                                                                  "*"))).isTrue();
      assertThat(newMimeType("*", "*").equalsTypeAndSubtype(newMimeType("*", "*"))).isTrue();
      assertThat(newMimeType("*", "pdf").equalsTypeAndSubtype(newMimeType("application", "pdf"))).isFalse();
      assertThat(newMimeType("application", "pdf").equalsTypeAndSubtype(newMimeType("*", "pdf"))).isFalse();
      assertThat(newMimeType("application", "*").equalsTypeAndSubtype(newMimeType("application",
                                                                                  "pdf"))).isFalse();
      assertThat(newMimeType("application", "pdf").equalsTypeAndSubtype(newMimeType("application",
                                                                                    "*"))).isFalse();
      assertThat(newMimeType("application", "*", map(entry("key", "value"))).equalsTypeAndSubtype(newMimeType(
            "application",
            "*",
            map()))).isTrue();
      assertThat(newMimeType("application", "*", map(entry("key", "value"))).equalsTypeAndSubtype(newMimeType(
            "application",
            "*",
            map(entry("otherkey", "otherValue"))))).isTrue();
   }

   @Test
   public void testIncludesWhenNominal() {
      assertThat(newMimeType("application", "pdf").includes(newMimeType("application", "pdf"))).isTrue();
      assertThat(newMimeType("application", "pdf").includes(newMimeType("application", "json"))).isFalse();
      assertThat(newMimeType("application", "pdf").includes(newMimeType("text", "pdf"))).isFalse();

      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).includes(newMimeType(
            "application",
            "pdf",
            map(entry("key", "value"))))).isTrue();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).includes(newMimeType(
            "application",
            "pdf",
            map(entry("otherkey", "othervalue"))))).isTrue();
      assertThat(newMimeType("application", "pdf", map(entry("samekey", "value"))).includes(newMimeType(
            "application",
            "pdf",
            map(entry("samekey", "othervalue"))))).isTrue();
   }

   @Test
   public void testIncludesWhenWildcard() {
      assertThat(newMimeType("application", "pdf").includes(newMimeType("*", "pdf"))).isFalse();
      assertThat(newMimeType("*", "pdf").includes(newMimeType("application", "pdf"))).isTrue();
      assertThat(newMimeType("*", "pdf").includes(newMimeType("*", "pdf"))).isTrue();
      assertThat(newMimeType("application", "pdf").includes(newMimeType("application", "*"))).isFalse();
      assertThat(newMimeType("application", "*").includes(newMimeType("application", "pdf"))).isTrue();
      assertThat(newMimeType("application", "*").includes(newMimeType("application", "*"))).isTrue();
      assertThat(newMimeType("*", "*").includes(newMimeType("*", "*"))).isTrue();

      assertThat(newMimeType("application", "*", map(entry("key", "value"))).includes(newMimeType(
            "application",
            "pdf",
            map(entry("otherkey", "othervalue"))))).isTrue();
      assertThat(newMimeType("application", "*", map(entry("key", "value"))).includes(newMimeType(
            "application",
            "*",
            map(entry("otherkey", "othervalue"))))).isTrue();
      assertThat(newMimeType("application", "*", map(entry("samekey", "value"))).includes(newMimeType(
            "application",
            "*",
            map(entry("samekey", "othervalue"))))).isTrue();
   }

   @Test
   public void testIncludesWhenCaseInsensitive() {
      assertThat(newMimeType("application", "pdf").includes(newMimeType("APPLICATION", "pdf"))).isTrue();
      assertThat(newMimeType("application", "pdf").includes(newMimeType("application", "PDF"))).isTrue();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).includes(newMimeType(
            "application",
            "pdf",
            map(entry("KEY", "value"))))).isTrue();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).includes(newMimeType(
            "application",
            "pdf",
            map(entry("key", "VALUE"))))).isTrue();
   }

   @Test
   public void testMatchesWhenNominal() {
      assertThat(newMimeType("application", "pdf").matches(newMimeType("application", "pdf"))).isTrue();
      assertThat(newMimeType("application", "pdf").matches(newMimeType("application", "json"))).isFalse();
      assertThat(newMimeType("application", "pdf").matches(newMimeType("text", "pdf"))).isFalse();

      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).matches(newMimeType(
            "application",
            "pdf",
            map(entry("key", "value"))))).isTrue();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).matches(newMimeType(
            "application",
            "pdf",
            map(entry("otherkey", "othervalue"))))).isTrue();
      assertThat(newMimeType("application", "pdf", map(entry("samekey", "value"))).matches(newMimeType(
            "application",
            "pdf",
            map(entry("samekey", "othervalue"))))).isTrue();
   }

   @Test
   public void testMatchesWhenWildcard() {
      assertThat(newMimeType("application", "pdf").matches(newMimeType("*", "pdf"))).isTrue();
      assertThat(newMimeType("*", "pdf").matches(newMimeType("application", "pdf"))).isTrue();
      assertThat(newMimeType("*", "pdf").matches(newMimeType("*", "pdf"))).isTrue();
      assertThat(newMimeType("application", "pdf").matches(newMimeType("application", "*"))).isTrue();
      assertThat(newMimeType("application", "*").matches(newMimeType("application", "pdf"))).isTrue();
      assertThat(newMimeType("application", "*").matches(newMimeType("application", "*"))).isTrue();
      assertThat(newMimeType("*", "*").matches(newMimeType("*", "*"))).isTrue();

      assertThat(newMimeType("application",
                             "*",
                             map(entry("key", "value"))).matches(newMimeType("application",
                                                                             "pdf",
                                                                             map(entry("otherkey",
                                                                                       "othervalue"))))).isTrue();
      assertThat(newMimeType("application",
                             "*",
                             map(entry("key", "value"))).matches(newMimeType("application",
                                                                             "*",
                                                                             map(entry("otherkey",
                                                                                       "othervalue"))))).isTrue();
      assertThat(newMimeType("application", "*", map(entry("samekey", "value"))).matches(newMimeType(
            "application",
            "*",
            map(entry("samekey", "othervalue"))))).isTrue();
   }

   @Test
   public void testMatchesWhenCaseInsensitive() {
      assertThat(newMimeType("application", "pdf").matches(newMimeType("APPLICATION", "pdf"))).isTrue();
      assertThat(newMimeType("application", "pdf").matches(newMimeType("application", "PDF"))).isTrue();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).matches(newMimeType(
            "application",
            "pdf",
            map(entry("KEY", "value"))))).isTrue();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).matches(newMimeType(
            "application",
            "pdf",
            map(entry("key", "VALUE"))))).isTrue();
   }

   @Test
   public void testCompareToWhenNominal() {
   }

   @Test
   public void testEqualsWhenNominal() {
      assertThat(newMimeType("application", "pdf", map()).equals(newMimeType("application",
                                                                             "pdf",
                                                                             map()))).isTrue();
      assertThat(newMimeType("application", "json", map()).equals(newMimeType("application",
                                                                              "pdf",
                                                                              map()))).isFalse();
      assertThat(newMimeType("text", "pdf", map()).equals(newMimeType("application",
                                                                      "pdf",
                                                                      map()))).isFalse();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).equals(newMimeType(
            "application",
            "pdf",
            map()))).isFalse();
      assertThat(newMimeType("application", "pdf", map(entry("key", "value"))).equals(newMimeType(
            "application",
            "pdf",
            map(entry("otherkey", "otherValue"))))).isFalse();

   }

   @Test
   public void testEqualsWhenCaseInsensitive() {
      assertThat(newMimeType("application", "pdf").equals(newMimeType("APPLICATION", "pdf"))).isTrue();
      assertThat(newMimeType("application", "pdf").equals(newMimeType("application", "PDF"))).isTrue();
   }

   @Test
   public void testEqualsWhenWildcard() {
      assertThat(newMimeType("*", "pdf").equals(newMimeType("*", "pdf"))).isTrue();
      assertThat(newMimeType("application", "*").equals(newMimeType("application", "*"))).isTrue();
      assertThat(newMimeType("*", "*").equals(newMimeType("*", "*"))).isTrue();
      assertThat(newMimeType("*", "pdf").equals(newMimeType("application", "pdf"))).isFalse();
      assertThat(newMimeType("application", "pdf").equals(newMimeType("*", "pdf"))).isFalse();
      assertThat(newMimeType("application", "*").equals(newMimeType("application", "pdf"))).isFalse();
      assertThat(newMimeType("application", "pdf").equals(newMimeType("application", "*"))).isFalse();
      assertThat(newMimeType("application", "*", map(entry("key", "value"))).equals(newMimeType("application",
                                                                                                "*",
                                                                                                map()))).isFalse();
      assertThat(newMimeType("application", "*", map(entry("key", "value"))).equals(newMimeType("application",
                                                                                                "*",
                                                                                                map(entry(
                                                                                                      "otherkey",
                                                                                                      "otherValue"))))).isFalse();
   }

   @Test
   public void testHashCodeWhenNominal() {
      assertThat(newMimeType("application", "pdf")).hasSameHashCodeAs(newMimeType("application", "pdf"));
      assertThat(newMimeType("*", "pdf")).hasSameHashCodeAs(newMimeType("*", "pdf"));
      assertThat(newMimeType("application", "*")).hasSameHashCodeAs(newMimeType("application", "*"));
      assertThat(newMimeType("*", "*")).hasSameHashCodeAs(newMimeType("*", "*"));
      assertThat(newMimeType("application", "pdf", map())).hasSameHashCodeAs(newMimeType("application",
                                                                                         "pdf",
                                                                                         map()));
      assertThat(newMimeType("application", "pdf", map(entry("key", "value")))).hasSameHashCodeAs(newMimeType(
            "application",
            "pdf",
            map(entry("key", "value"))));

      assertThat(newMimeType("application", "pdf")).doesNotHaveSameHashCodeAs(newMimeType("application",
                                                                                          "json"));
      assertThat(newMimeType("*", "json")).doesNotHaveSameHashCodeAs(newMimeType("*", "pdf"));
      assertThat(newMimeType("application", "*")).doesNotHaveSameHashCodeAs(newMimeType("text", "*"));
      assertThat(newMimeType("*", "*")).doesNotHaveSameHashCodeAs(newMimeType("application", "pdf"));
      assertThat(newMimeType("application", "pdf", map(entry("key", "value")))).doesNotHaveSameHashCodeAs(
            newMimeType("application", "pdf", map()));
      assertThat(newMimeType("application", "pdf", map(entry("key", "value")))).doesNotHaveSameHashCodeAs(
            newMimeType("application", "pdf", map(entry("otherkey", "othervalue"))));
   }

   @Test
   public void testHashCodeWhenCaseInsensitive() {
      assertThat(newMimeType("application", "pdf")).hasSameHashCodeAs(newMimeType("APPLICATION", "pdf"));
      assertThat(newMimeType("application", "pdf")).hasSameHashCodeAs(newMimeType("application", "PDF"));
      assertThat(newMimeType("application", "pdf", map(entry("key", "value")))).hasSameHashCodeAs(newMimeType(
            "application",
            "pdf",
            map(entry("KEY", "value"))));

      assertThat(newMimeType("application", "pdf", map(entry("key", "value")))).doesNotHaveSameHashCodeAs(
            newMimeType("application", "pdf", map(entry("key", "VALUE"))));
   }

   @Test
   public void testToStringWhenNominal() {
      assertThat(newMimeType("application", "pdf")).hasToString("application/pdf");
      assertThat(newMimeType("application", "*")).hasToString("application/*");
      assertThat(newMimeType("*", "pdf")).hasToString("*/pdf");
      assertThat(newMimeType("*", "*")).hasToString("*/*");
      assertThat(newMimeType("application", "pdf", map())).hasToString("application/pdf");
      assertThat(newMimeType("application", "pdf", map(entry("key", "value")))).hasToString(
            "application/pdf;key=value");
      assertThat(newMimeType("application", "pdf", map(entry("charset", "UTF-8")))).hasToString(
            "application/pdf;charset=UTF-8");
   }

   @Test
   public void testToStringWhenCaseInsensitive() {
      assertThat(newMimeType("APPLICATION", "pdf")).hasToString("application/pdf");
      assertThat(newMimeType("application", "PDF")).hasToString("application/pdf");
      assertThat(newMimeType("application", "pdf", map(entry("KEY", "value")))).hasToString(
            "application/pdf;key=value");
      assertThat(newMimeType("application", "pdf", map(entry("CHARSET", "uTf-8")))).hasToString(
            "application/pdf;charset=UTF-8");
      assertThat(newMimeType("application", "pdf", map(entry("key", "VALUE")))).hasToString(
            "application/pdf;key=VALUE");
   }

   @Test
   @Disabled
   public void testToStringWhenQuoted() {
      assertThat(newMimeType("application", "pdf", map(entry("key", "\"va;lue\"")))).hasToString(
            "application/pdf;key='va;lue'");
   }

}
