/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype.registry;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.assertj.core.api.OptionalAssert;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.mimetype.MimeType;

public abstract class CommonMimeTypeRegistryTest {

   protected abstract MimeTypeRegistry registry();

   @Test
   public void testMimeTypeWhenNominal() {
      assertThatMimeType(registry(), "application.pdf", "application/pdf");
      assertThat(registry().mimeType(path("file.xyz"))).isEmpty();
   }

   @Test
   public void testMimeTypeWhenBadParameters() {
      assertThatNullPointerException()
            .isThrownBy(() -> registry().mimeType((Path) null))
            .withMessage("'documentPath' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> registry().mimeType((String) null))
            .withMessage("'documentName' must not be null");
   }

   @Test
   public void testMimeTypeWhenNoExtension() {
      assertThat(registry().mimeType(path("file"))).isEmpty();
      assertThat(registry().mimeType(path(""))).isEmpty();

      assertThat(registry().mimeType("file")).isEmpty();
      assertThat(registry().mimeType("")).isEmpty();
   }

   @Test
   public void testMimeTypeWhenCaseInsensitive() {
      assertThatMimeType(registry(), "file.txt", "text/plain");
      assertThatMimeType(registry(), "FILE.TXT", "text/plain");
      assertThatMimeType(registry(), "FILE.pdf", "application/pdf");
      assertThatMimeType(registry(), "FILE.PDF", "application/pdf");
   }

   @Test
   public void testExtensionWhenNominal() {
      assertThat(registry().extension(parseMimeType("application/pdf"))).hasValue("pdf");
      assertThat(registry().extension(parseMimeType("audio/ogg"))).isEmpty();
   }

   @Test
   public void testExtensionWhenCharset() {
      assertThat(registry().extension(parseMimeType("application/pdf;charset=UTF-8"))).hasValue("pdf");
      assertThat(registry().extension(parseMimeType("audio/ogg;charset=UTF-8"))).isEmpty();
   }

   @Test
   public void testExtensionWhenMultipleExtensions() {
      assertThat(registry().extension(parseMimeType("text/html"))).hasValue("html");
   }

   protected Path path(String path) {
      return Paths.get(path);
   }

   protected OptionalAssert<MimeType> assertThatMimeType(MimeTypeRegistry detector,
                                                         String documentPath,
                                                         String mimeType) {
      return assertThat(detector.mimeType(Paths.get(documentPath))).hasValue(parseMimeType(mimeType));
   }

}
