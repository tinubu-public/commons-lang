/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Map;

import org.junit.jupiter.api.Test;

class SpringCoreMimeTypeFromImplementationTest extends CommonMimeTypeTest {

   @Override
   protected MimeType newMimeType(String type, String subtype, Map<String, String> parameters) {
      return new SpringCoreMimeType(new org.springframework.util.MimeType(type, subtype, parameters));
   }

   @Override
   protected MimeType newMimeType(String type, String subtype) {
      return newMimeType(type, subtype, map());
   }

   @Test
   public void testSpringCoreMimeTypeWhenNominal() {
      org.springframework.util.MimeType mt =
            new org.springframework.util.MimeType("application", "pdf", map(entry("key", "value")));

      assertThatNoException().isThrownBy(() -> new SpringCoreMimeType(mt));
   }

   @Test
   public void testSpringCoreMimeTypeWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> new SpringCoreMimeType((org.springframework.util.MimeType) null));
   }

   @Test
   public void testSpringCoreMimeTypeWhenBadCharset() {
      assertThatExceptionOfType(UnsupportedCharsetException.class).isThrownBy(() -> {
         org.springframework.util.MimeType mt1 = new org.springframework.util.MimeType("application",
                                                                                       "pdf",
                                                                                       map(entry("charset",
                                                                                                 "NOT-EXISTS")));
         new SpringCoreMimeType(mt1);
      });

      assertThatExceptionOfType(IllegalCharsetNameException.class).isThrownBy(() -> {
         org.springframework.util.MimeType mt2 =
               new org.springframework.util.MimeType("application", "pdf", map(entry("charset", "UTF@8")));

         new SpringCoreMimeType(mt2);
      });
   }

}