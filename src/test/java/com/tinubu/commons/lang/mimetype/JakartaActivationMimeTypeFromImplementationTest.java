/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Map;

import javax.activation.MimeTypeParseException;

import org.junit.jupiter.api.Test;

class JakartaActivationMimeTypeFromImplementationTest extends CommonMimeTypeTest {

   @Override
   protected MimeType newMimeType(String type, String subtype, Map<String, String> parameters) {
      try {
         javax.activation.MimeType mt = new javax.activation.MimeType(type, subtype);
         parameters.forEach(mt::setParameter);
         return new JakartaActivationMimeType(mt);
      } catch (MimeTypeParseException e) {
         throw new IllegalArgumentException(e);
      }
   }

   @Override
   protected MimeType newMimeType(String type, String subtype) {
      return newMimeType(type, subtype, map());
   }

   @Test
   public void testJakartaActivationMimeTypeWhenNominal() throws MimeTypeParseException {
      javax.activation.MimeType mt = new javax.activation.MimeType("application", "pdf");
      map(entry("key", "value")).forEach(mt::setParameter);

      assertThatNoException().isThrownBy(() -> new JakartaActivationMimeType(mt));
   }

   @Test
   public void testJakartaActivationMimeTypeWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> new JakartaActivationMimeType((javax.activation.MimeType) null));
   }

   @Test
   public void testJakartaActivationMimeTypeWhenBadCharset() throws MimeTypeParseException {
      assertThatExceptionOfType(UnsupportedCharsetException.class).isThrownBy(() -> {
         javax.activation.MimeType mt1 = new javax.activation.MimeType("application", "pdf");
         map(entry("charset", "NOT-EXISTS")).forEach(mt1::setParameter);
         new JakartaActivationMimeType(mt1);
      });

      assertThatExceptionOfType(IllegalCharsetNameException.class).isThrownBy(() -> {
         javax.activation.MimeType mt2 = new javax.activation.MimeType("application", "pdf");
         map(entry("charset", "UTF@8")).forEach(mt2::setParameter);
         new JakartaActivationMimeType(mt2);
      });
   }
}