/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

class SpringCoreMimeTypeFromCopyTest extends CommonMimeTypeTest {

   @Override
   protected MimeType newMimeType(String type, String subtype, Map<String, String> parameters) {
      return new SpringCoreMimeType(mockMimeType(type, subtype, parameters));
   }

   @Override
   protected MimeType newMimeType(String type, String subtype) {
      return newMimeType(type, subtype, map());
   }

   @Test
   public void testSpringCoreMimeTypeFromMimeType() {
      MimeType originalMimeType = mockMimeType("application",
                                               "pdf",
                                               map(LinkedHashMap::new,
                                                   entry("key", "value"),
                                                   entry("charset", "UTF-8")));

      MimeType mimeType = new SpringCoreMimeType(originalMimeType);

      assertThat(mimeType.type()).isEqualTo("application");
      assertThat(mimeType.subtype()).isEqualTo("pdf");
      assertThat(mimeType.parameters()).containsExactly(entry("key", "value"), entry("charset", "UTF-8"));
      assertThat(mimeType.charset()).hasValue(StandardCharsets.UTF_8);

      assertThatNullPointerException()
            .isThrownBy(() -> new SpringCoreMimeType((MimeType) null))
            .withMessage("'mimeType' must not be null");
   }

   @Test
   public void testSpringCoreMimeTypeFromMimeTypeParameters() {
      MimeType originalMimeType = mockMimeType("application",
                                               "pdf",
                                               map(LinkedHashMap::new,
                                                   entry("key", "value"),
                                                   entry("charset", "UTF-8")));

      MimeType mimeTypeWithCharsetOverride =
            new SpringCoreMimeType(originalMimeType, map(entry("charset", "ISO-8859-1")));

      assertThat(mimeTypeWithCharsetOverride.type()).isEqualTo("application");
      assertThat(mimeTypeWithCharsetOverride.subtype()).isEqualTo("pdf");
      assertThat(mimeTypeWithCharsetOverride.parameters()).containsExactly(entry("charset", "ISO-8859-1"));
      assertThat(mimeTypeWithCharsetOverride.charset()).hasValue(StandardCharsets.ISO_8859_1);

      MimeType mimeType = new SpringCoreMimeType(originalMimeType, map(entry("otherkey", "othervalue")));

      assertThat(mimeType.type()).isEqualTo("application");
      assertThat(mimeType.subtype()).isEqualTo("pdf");
      assertThat(mimeType.parameters()).containsExactly(entry("otherkey", "othervalue"));
      assertThat(mimeType.charset()).isEmpty();

      assertThatNullPointerException()
            .isThrownBy(() -> new SpringCoreMimeType((MimeType) null))
            .withMessage("'mimeType' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new SpringCoreMimeType(originalMimeType, (Map<String, String>) null))
            .withMessage("'parameters' must not be null");
   }

   @Test
   public void testSpringCoreMimeTypeFromMimeTypeCharset() {
      MimeType originalMimeType = mockMimeType("application",
                                               "pdf",
                                               map(LinkedHashMap::new,
                                                   entry("key", "value"),
                                                   entry("charset", "UTF-8")));

      MimeType mimeType = new SpringCoreMimeType(originalMimeType, StandardCharsets.ISO_8859_1);

      assertThat(mimeType.type()).isEqualTo("application");
      assertThat(mimeType.subtype()).isEqualTo("pdf");
      assertThat(mimeType.parameters()).containsExactly(entry("charset", "ISO-8859-1"));
      assertThat(mimeType.charset()).hasValue(StandardCharsets.ISO_8859_1);

      assertThatNullPointerException()
            .isThrownBy(() -> new SpringCoreMimeType((MimeType) null))
            .withMessage("'mimeType' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new SpringCoreMimeType(originalMimeType, (Charset) null))
            .withMessage("'charset' must not be null");
   }

   private MimeType mockMimeType(String type, String subtype, Map<String, String> parameters) {
      MimeType mockMimeType = mock(MimeType.class);
      when(mockMimeType.type()).thenReturn(type);
      when(mockMimeType.subtype()).thenReturn(subtype);
      when(mockMimeType.parameters()).thenReturn(parameters);

      return mockMimeType;
   }

}