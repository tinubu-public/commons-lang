/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype.registry;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.mimetype.registry.ConfigurableMimeTypeRegistry.MimeTypeEntry.of;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;

import org.junit.jupiter.api.Test;

class ConfigurableMimeTypeRegistryTest extends CommonMimeTypeRegistryTest {

   @Override
   protected ConfigurableMimeTypeRegistry registry() {
      return ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("TEXT/PLAIN"), "TXT"),
                                                         of(parseMimeType("application/pdf"), "pdf"),
                                                         of(parseMimeType("image/png;charset=UTF-8"), "png"),
                                                         of(parseMimeType("text/html"),
                                                            "html",
                                                            "htm").aliasMimeTypes(parseMimeType(
                                                               "text/vnd.html"))));
   }

   @Test
   public void testMapMimeTypeRegistryWhenOverrideMimeType() {
      assertThatIllegalStateException()
            .isThrownBy(() -> ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("text/plain"),
                                                                             "txt"),
                                                                          of(parseMimeType("text/plain"),
                                                                             "txt"))))
            .withMessage("'text/plain' MIME type is already registered");
   }

   @Test
   public void testMapMimeTypeRegistryWhenOverrideExtension() {
      assertThatIllegalStateException()
            .isThrownBy(() -> ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("text/plain"),
                                                                             "txt"),
                                                                          of(parseMimeType("text/html"),
                                                                             "txt"))))
            .withMessage("'txt' extension is already registered");
   }

   @Test
   public void testOfEntriesWhenNormalize() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("text/plain;charset=UTF-8"), "txt"),
                                                        of(parseMimeType("application/pdf"), " pdf  ")));

      assertThatMimeType(registry, "file.txt", "text/plain");
      assertThatMimeType(registry, "file.pdf", "application/pdf");
   }

   @Test
   public void testEntryWhenNominal() {
      ConfigurableMimeTypeRegistry updatedRegistry =
            registry().entry(of(parseMimeType("text/new"), "new", "newww")).indexEntries();

      assertThat(updatedRegistry.mimeType("file.new")).hasValue(parseMimeType("text/new"));
      assertThat(updatedRegistry.mimeType("file.newww")).hasValue(parseMimeType("text/new"));
      assertThat(updatedRegistry.extension(parseMimeType("text/new"))).hasValue("new");
   }

   @Test
   public void testEntryWhenOverrideMimeType() {
      assertThatIllegalStateException()
            .isThrownBy(() -> registry().entry(of(parseMimeType("text/plain"), "txt")).indexEntries())
            .withMessage("'text/plain' MIME type is already registered");
   }

   @Test
   public void testEntryWhenOverrideExtension() {
      assertThatIllegalStateException()
            .isThrownBy(() -> registry().entry(of(parseMimeType("new/new"), "txt")).indexEntries())
            .withMessage("'txt' extension is already registered");
   }

   @Test
   public void testExtensionWhenMultipleMimeTypes() {
      assertThat(registry().extension(parseMimeType("text/vnd.html"))).hasValue("html");
   }

   @Test
   public void testMimeTypeWhenNoExperimentalSupport() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("application/x-experimental"),
                                                           "exp"),
                                                        of(parseMimeType("application/x-experimental2"),
                                                           "exp2").aliasMimeTypes(parseMimeType(
                                                              "application/experimental2"))), false);

      assertThatMimeType(registry, "file.exp", "application/experimental");
      assertThatMimeType(registry, "file.exp2", "application/experimental2");
   }

   @Test
   public void testMimeTypeWhenExperimentalSupport() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("application/x-experimental"),
                                                           "exp"),
                                                        of(parseMimeType("application/x-experimental2"),
                                                           "exp2").aliasMimeTypes(parseMimeType(
                                                              "application/experimental2"))), true);

      assertThatMimeType(registry, "file.exp", "application/x-experimental");
      assertThatMimeType(registry, "file.exp2", "application/x-experimental2");
   }

   @Test
   public void testMimeTypesWhenNoExperimentalSupport() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("application/x-experimental"),
                                                           "exp"),
                                                        of(parseMimeType("application/x-experimental2"),
                                                           "exp2").aliasMimeTypes(parseMimeType(
                                                              "application/experimental2"))), false);

      assertThat(registry.mimeTypes("file.exp")).containsExactly(parseMimeType("application/experimental"));
      assertThat(registry.mimeTypes("file.exp2")).containsExactly(parseMimeType("application/experimental2"));
   }

   @Test
   public void testMimeTypesWhenExperimentalSupport() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("application/x-experimental"),
                                                           "exp"),
                                                        of(parseMimeType("application/x-experimental2"),
                                                           "exp2").aliasMimeTypes(parseMimeType(
                                                              "application/experimental2"))), true);

      assertThat(registry.mimeTypes("file.exp")).containsExactly(parseMimeType("application/x-experimental"));
      assertThat(registry.mimeTypes("file.exp2")).containsExactly(parseMimeType("application/x-experimental2"),
                                                                  parseMimeType("application/experimental2"));
   }

   @Test
   public void testExtensionWhenNoExperimentalSupport() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("application/x-experimental"),
                                                           "exp"),
                                                        of(parseMimeType("application/x-experimental2"),
                                                           "exp2").aliasMimeTypes(parseMimeType(
                                                              "application/experimental2"))), false);

      assertThat(registry.extension(parseMimeType("application/x-experimental"))).hasValue("exp");
      assertThat(registry.extension(parseMimeType("application/experimental"))).hasValue("exp");

      assertThat(registry.extension(parseMimeType("application/x-experimental2"))).hasValue("exp2");
      assertThat(registry.extension(parseMimeType("application/experimental2"))).hasValue("exp2");
   }

   @Test
   public void testExtensionWhenExperimentalSupport() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("application/x-experimental"),
                                                           "exp"),
                                                        of(parseMimeType("application/x-experimental2"),
                                                           "exp2").aliasMimeTypes(parseMimeType(
                                                              "application/experimental2"))), true);

      assertThat(registry.extension(parseMimeType("application/x-experimental"))).hasValue("exp");
      assertThat(registry.extension(parseMimeType("application/experimental"))).isEmpty();

      assertThat(registry.extension(parseMimeType("application/x-experimental2"))).hasValue("exp2");
      assertThat(registry.extension(parseMimeType("application/experimental2"))).hasValue("exp2");
   }

   @Test
   public void testExtensionsWhenNoExperimentalSupport() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("application/x-experimental"),
                                                           "exp"),
                                                        of(parseMimeType("application/x-experimental2"),
                                                           "exp2").aliasMimeTypes(parseMimeType(
                                                              "application/experimental2"))), false);

      assertThat(registry.extensions(parseMimeType("application/x-experimental"))).containsExactly("exp");
      assertThat(registry.extensions(parseMimeType("application/experimental"))).containsExactly("exp");

      assertThat(registry.extensions(parseMimeType("application/x-experimental2"))).containsExactly("exp2");
      assertThat(registry.extensions(parseMimeType("application/experimental2"))).containsExactly("exp2");
   }

   @Test
   public void testExtensionsWhenExperimentalSupport() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("application/x-experimental"),
                                                           "exp"),
                                                        of(parseMimeType("application/x-experimental2"),
                                                           "exp2").aliasMimeTypes(parseMimeType(
                                                              "application/experimental2"))), true);

      assertThat(registry.extensions(parseMimeType("application/x-experimental"))).containsExactly("exp");
      assertThat(registry.extensions(parseMimeType("application/experimental"))).isEmpty();

      assertThat(registry.extensions(parseMimeType("application/x-experimental2"))).containsExactly("exp2");
      assertThat(registry.extensions(parseMimeType("application/experimental2"))).containsExactly("exp2");
   }

   @Test
   public void testExtensionWhenExtensionNotApplicable() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("multipart/mixed"))));

      assertThat(registry.extension(parseMimeType("multipart/mixed"))).isEmpty();
   }

   @Test
   public void testExtensionsWhenExtensionNotApplicable() {
      ConfigurableMimeTypeRegistry registry =
            ConfigurableMimeTypeRegistry.ofEntries(list(of(parseMimeType("multipart/mixed"))));

      assertThat(registry.extensions(parseMimeType("multipart/mixed"))).isEmpty();
   }

}