/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert.converters;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.jupiter.api.Test;

class DefaultConversionServiceTest {

   @Test
   public void testCanConvert() {
      DefaultConversionService service = new DefaultConversionService();

      assertThat(service.canConvert(String.class, Long.class)).isTrue();
      assertThat(service.canConvert(String.class, Integer.class)).isTrue();
      assertThat(service.canConvert(String.class, Byte.class)).isTrue();
      assertThat(service.canConvert(String.class, Short.class)).isTrue();
      assertThat(service.canConvert(String.class, Float.class)).isTrue();
      assertThat(service.canConvert(String.class, Double.class)).isTrue();
      assertThat(service.canConvert(String.class, BigDecimal.class)).isTrue();
      assertThat(service.canConvert(String.class, BigInteger.class)).isTrue();

      assertThat(service.canConvert(Long.class, String.class)).isTrue();
      assertThat(service.canConvert(Integer.class, String.class)).isTrue();
      assertThat(service.canConvert(Byte.class, String.class)).isTrue();
      assertThat(service.canConvert(Short.class, String.class)).isTrue();
      assertThat(service.canConvert(Float.class, String.class)).isTrue();
      assertThat(service.canConvert(Double.class, String.class)).isTrue();
      assertThat(service.canConvert(BigDecimal.class, String.class)).isTrue();
      assertThat(service.canConvert(BigInteger.class, String.class)).isTrue();

      assertThat(service.canConvert(Long.class, Long.class)).isTrue();
      assertThat(service.canConvert(Long.class, Integer.class)).isTrue();
      assertThat(service.canConvert(Long.class, Byte.class)).isTrue();
      assertThat(service.canConvert(Long.class, Short.class)).isTrue();
      assertThat(service.canConvert(Long.class, Float.class)).isTrue();
      assertThat(service.canConvert(Long.class, Double.class)).isTrue();
      assertThat(service.canConvert(Long.class, BigDecimal.class)).isTrue();
      assertThat(service.canConvert(Long.class, BigInteger.class)).isTrue();
   }

   @Test
   public void testConvert() {
      DefaultConversionService service = new DefaultConversionService();

      assertThat(service.convert("3", Long.class)).isEqualTo(3L);
      assertThat(service.convert("3", Integer.class)).isEqualTo(3);
      assertThat(service.convert("3", Byte.class)).isEqualTo((byte) 3);
      assertThat(service.convert("3", Short.class)).isEqualTo((short) 3);
      assertThat(service.convert("3", Float.class)).isEqualTo(3f);
      assertThat(service.convert("3", Double.class)).isEqualTo(3d);
      assertThat(service.convert("3", BigDecimal.class)).isEqualTo(new BigDecimal("3"));
      assertThat(service.convert("3", BigInteger.class)).isEqualTo(new BigInteger("3"));

      assertThat(service.convert(3L, String.class)).isEqualTo("3");
      assertThat(service.convert(3, String.class)).isEqualTo("3");
      assertThat(service.convert((byte) 3, String.class)).isEqualTo("3");
      assertThat(service.convert((short) 3, String.class)).isEqualTo("3");
      assertThat(service.convert(3f, String.class)).isEqualTo("3.0");
      assertThat(service.convert(3d, String.class)).isEqualTo("3.0");
      assertThat(service.convert(new BigDecimal("3.00"), String.class)).isEqualTo("3");
      assertThat(service.convert(new BigInteger("3"), String.class)).isEqualTo("3");

      assertThat(service.convert(3L, Long.class)).isEqualTo(3L);
      assertThat(service.convert(3L, Integer.class)).isEqualTo(3);
      assertThat(service.convert(3L, Byte.class)).isEqualTo((byte) 3);
      assertThat(service.convert(3L, Short.class)).isEqualTo((short) 3);
      assertThat(service.convert(3L, Float.class)).isEqualTo(3f);
      assertThat(service.convert(3L, Double.class)).isEqualTo(3d);
      assertThat(service.convert(3L, BigDecimal.class)).isEqualTo(new BigDecimal("3"));
      assertThat(service.convert(3L, BigInteger.class)).isEqualTo(new BigInteger("3"));
   }

   @Test
   public void testConvertWhenNullValue() {
      DefaultConversionService service = new DefaultConversionService();

      assertThat(service.convert(null, Long.class)).isNull();
   }

}