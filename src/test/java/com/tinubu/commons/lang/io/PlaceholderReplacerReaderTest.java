/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io;

import static com.tinubu.commons.lang.io.PlaceholderReplacerReader.mapModelReplacementFunction;
import static com.tinubu.commons.lang.io.ReplacerTestUtils.DEFAULT_BUFFER_SIZE;
import static com.tinubu.commons.lang.io.ReplacerTestUtils.readerToString;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.io.IOException;
import java.io.StringReader;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PlaceholderReplacerReaderTest {

   @Test
   public void testReplacerWhenNominal() throws IOException {
      assertThatEquals("Some text", map(), "Some text");
      assertThatEquals("", map(), "");
   }

   @Test
   @SuppressWarnings("resource")
   public void testReplacerWhenBadParameters() throws IOException {
      assertThatNullPointerException()
            .isThrownBy(() -> new PlaceholderReplacerReader(null, __ -> null))
            .withMessage("'in' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new PlaceholderReplacerReader(new StringReader("test"), null))
            .withMessage("'replacementFunction' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new PlaceholderReplacerReader(new StringReader("test"), __ -> null, -5))
            .withMessage("'bufferSize' must be > 0");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new PlaceholderReplacerReader(new StringReader("test"), __ -> null, "", "}"))
            .withMessage("'placeholderBegin' must not be empty");
      assertThatNullPointerException()
            .isThrownBy(() -> new PlaceholderReplacerReader(new StringReader("test"), __ -> null, null, "}"))
            .withMessage("'placeholderBegin' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new PlaceholderReplacerReader(new StringReader("test"), __ -> null, "${", ""))
            .withMessage("'placeholderEnd' must not be empty");
      assertThatNullPointerException()
            .isThrownBy(() -> new PlaceholderReplacerReader(new StringReader("test"), __ -> null, "${", null))
            .withMessage("'placeholderEnd' must not be null");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenPlaceholder(String phBegin, String phEnd, int readLength) throws IOException {
      assertThatEquals("<key>", map(entry("key", "value")), phBegin, phEnd, readLength, "value");
      assertThatEquals("Some <key> text",
                       map(entry("key", "value")),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some value text");
      assertThatEquals("<key> Some text",
                       map(entry("key", "value")),
                       phBegin,
                       phEnd,
                       readLength,
                       "value Some text");
      assertThatEquals("Some text <key>",
                       map(entry("key", "value")),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some text value");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenReplacementFunctionNullValue(String phBegin, String phEnd, int readLength)
         throws IOException {
      assertThatEquals("Some <key> text",
                       map(entry("key", null)),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some <key> text");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenPlaceholderValueLargerThanInitialSource(String phBegin,
                                                                       String phEnd,
                                                                       int readLength) throws IOException {
      assertThatEquals("Some <key> text",
                       map(entry("key", "01234567890123456789")),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some 01234567890123456789 text");
      assertThatEquals("<key>",
                       map(entry("key", "01234567890123456789")),
                       phBegin,
                       phEnd,
                       readLength,
                       "01234567890123456789");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenPlaceholderObjectValue(String phBegin, String phEnd, int readLength)
         throws IOException {
      assertThatEquals("Some <key> text", map(entry("key", 5L)), phBegin, phEnd, readLength, "Some 5 text");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenUnknownPlaceholder(String phBegin, String phEnd, int readLength)
         throws IOException {
      assertThatEquals("Some <key> text", map(), phBegin, phEnd, readLength, "Some <key> text");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenBadPlaceholderSyntax(String phBegin, String phEnd, int readLength)
         throws IOException {
      assertThatEquals("Some <key text", map(), phBegin, phEnd, readLength, "Some <key text");
      assertThatEquals("Some key> text", map(), phBegin, phEnd, readLength, "Some key> text");
      assertThatEquals("<Some key text", map(), phBegin, phEnd, readLength, "<Some key text");
      assertThatEquals(">Some key text", map(), phBegin, phEnd, readLength, ">Some key text");
      assertThatEquals("Some key text>", map(), phBegin, phEnd, readLength, "Some key text>");
      assertThatEquals("Some key text<", map(), phBegin, phEnd, readLength, "Some key text<");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenEmptyPlaceholder(String phBegin, String phEnd, int readLength)
         throws IOException {
      assertThatEquals("Some <> text", map(), phBegin, phEnd, readLength, "Some <> text");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenRecursivePlaceholder(String phBegin, String phEnd, int readLength)
         throws IOException {
      assertThatEquals("Some <<> text",
                       map(entry("key", "value")),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some <<> text");
      assertThatEquals("Some <>> text",
                       map(entry("key", "value")),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some <>> text");
      assertThatEquals("Some <<>> text",
                       map(entry("key", "value")),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some <<>> text");
      assertThatEquals("Some <key>> text",
                       map(entry("key", "value")),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some value> text");
      assertThatEquals("Some <<key> text",
                       map(entry("key", "value")),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some <<key> text");
      assertThatEquals("Some <<key>> text",
                       map(entry("key", "value")),
                       phBegin,
                       phEnd,
                       readLength,
                       "Some <<key>> text");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenInternalBufferSizeLesserThanReadBufferSize(String phBegin,
                                                                          String phEnd,
                                                                          int readLength) throws IOException {
      assertThat(readerToString(new PlaceholderReplacerReader(new StringReader("Some text"),
                                                              __ -> null,
                                                              phBegin,
                                                              phEnd,
                                                              1000), 2000)).isEqualTo("Some text");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenInternalBufferSizeLesserThanRegularTextSize(String phBegin,
                                                                           String phEnd,
                                                                           int readLength)
         throws IOException {
      assertThatEquals("Some text", map(), phBegin, phEnd, 4, readLength, "Some text");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenInternalBufferSizeLesserThanPlaceholderSize(String phBegin,
                                                                           String phEnd,
                                                                           int readLength)
         throws IOException {
      assertThatEquals("Some <key> text",
                       map(),
                       phBegin,
                       phEnd,
                       phBegin.length() + 3 + phEnd.length(),
                       readLength,
                       "Some <key> text");
      assertThatEquals("Some <key> text",
                       map(entry("key", "01234")),
                       phBegin,
                       phEnd,
                       phBegin.length() + 3 + phEnd.length(),
                       readLength,
                       "Some 01234 text");
      assertThatEquals("Some <key> text",
                       map(),
                       phBegin,
                       phEnd,
                       phBegin.length() + 3 + phEnd.length() - 1,
                       readLength,
                       "Some <key> text");
      assertThatEquals("Some <key> text",
                       map(entry("key", "01234")),
                       phBegin,
                       phEnd,
                       phBegin.length() + 3 + phEnd.length() - 1,
                       readLength,
                       "Some <key> text");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenInternalBufferSizeLesserThanPlaceholderReplacedValueSize(String phBegin,
                                                                                        String phEnd,
                                                                                        int readLength)
         throws IOException {
      assertThatEquals("Some <key> text",
                       map(entry("key", "0123457890123456789")),
                       phBegin,
                       phEnd,
                       phBegin.length() + 3 + phEnd.length(),
                       readLength,
                       "Some <key> text");
   }

   @ParameterizedTest
   @MethodSource("placeholdersSource")
   public void testReplacerWhenInternalBufferSizeIsEqualTo1(String phBegin, String phEnd, int readLength)
         throws IOException {
      assertThatEquals("Some <key> text", map(), phBegin, phEnd, 1, readLength, "Some <key> text");
   }

   @Test
   public void testReplacerWhenPlaceholderBeginDowngradedToRegularText() throws IOException {
      assertThat(readerToString(new PlaceholderReplacerReader(new StringReader("Some $@ $@{key} text"),
                                                              mapModelReplacementFunction(map(entry("key",
                                                                                                    "value"))),
                                                              "$@{",
                                                              "}"))).isEqualTo("Some $@ value text");
   }

   @Test
   public void testReplacerWhenPlaceholderEndDowngradedToRegularText() throws IOException {
      assertThat(readerToString(new PlaceholderReplacerReader(new StringReader("Some {ke}@y}@$ text"),
                                                              mapModelReplacementFunction(map()),
                                                              "{",
                                                              "}@$"))).isEqualTo("Some {ke}@y}@$ text");
      assertThat(readerToString(new PlaceholderReplacerReader(new StringReader("Some {ke}@y}@$ text"),
                                                              mapModelReplacementFunction(map(entry("ke}@y",
                                                                                                    "value"))),
                                                              "{",
                                                              "}@$"))).isEqualTo("Some value text");
   }

   private static Stream<Arguments> placeholdersSource() {
      return stream(Arguments.of("{", "}", DEFAULT_BUFFER_SIZE),
                    Arguments.of("${", "}", DEFAULT_BUFFER_SIZE),
                    Arguments.of("{", "}$", DEFAULT_BUFFER_SIZE),
                    Arguments.of("{{", "}}", DEFAULT_BUFFER_SIZE),
                    Arguments.of("@@", "@@", DEFAULT_BUFFER_SIZE),
                    Arguments.of("{", "}", 4096),
                    Arguments.of("${", "}", 4096),
                    Arguments.of("{", "}$", 4096),
                    Arguments.of("{{", "}}", 4096),
                    Arguments.of("@@", "@@", 4096),
                    Arguments.of("{", "}", 1),
                    Arguments.of("${", "}", 1),
                    Arguments.of("{", "}$", 1),
                    Arguments.of("{{", "}}", 1),
                    Arguments.of("@@", "@@", 1),
                    Arguments.of("{", "}", 2),
                    Arguments.of("${", "}", 2),
                    Arguments.of("{", "}$", 2),
                    Arguments.of("{{", "}}", 2),
                    Arguments.of("@@", "@@", 2),
                    Arguments.of("{", "}", 4),
                    Arguments.of("${", "}", 4),
                    Arguments.of("{", "}$", 4),
                    Arguments.of("{{", "}}", 4),
                    Arguments.of("@@", "@@", 4),
                    Arguments.of("{", "}", -1),
                    Arguments.of("${", "}", -1),
                    Arguments.of("{", "}$", -1),
                    Arguments.of("{{", "}}", -1),
                    Arguments.of("@@", "@@", -1));
   }

   private PlaceholderReplacerReader replacerReader(String string,
                                                    Map<String, Object> model,
                                                    String phBegin,
                                                    String phEnd,
                                                    int bufferSize) {
      return new PlaceholderReplacerReader(new StringReader(string.replace("<", phBegin).replace(">", phEnd)),
                                           mapModelReplacementFunction(model),
                                           phBegin,
                                           phEnd,
                                           bufferSize);
   }

   private PlaceholderReplacerReader replacerReader(String string,
                                                    Map<String, Object> model,
                                                    String phBegin,
                                                    String phEnd) {
      return replacerReader(string, model, phBegin, phEnd, DEFAULT_BUFFER_SIZE);
   }

   private void assertThatEquals(String source,
                                 Map<String, Object> model,
                                 String phBegin,
                                 String phEnd,
                                 int bufferSize,
                                 int readLength,
                                 String expected) throws IOException {
      assertThat(readerToString(replacerReader(source, model, phBegin, phEnd, bufferSize),
                                readLength)).isEqualTo(expected.replace("<", phBegin).replace(">", phEnd));
   }

   private void assertThatEquals(String source,
                                 Map<String, Object> model,
                                 String phBegin,
                                 String phEnd,
                                 int readLength,
                                 String expected) throws IOException {
      assertThatEquals(source, model, phBegin, phEnd, DEFAULT_BUFFER_SIZE, readLength, expected);
   }

   private void assertThatEquals(String source, Map<String, Object> model, String expected)
         throws IOException {
      assertThatEquals(source, model, "${", "}", DEFAULT_BUFFER_SIZE, expected);
   }

}