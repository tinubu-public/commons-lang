/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io;

import static com.tinubu.commons.lang.io.ReplacerTestUtils.DEFAULT_BUFFER_SIZE;
import static com.tinubu.commons.lang.io.ReplacerTestUtils.readerToString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.io.IOException;
import java.io.StringReader;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.io.GeneralReplacerReader.TokenReplacerFactory;

class StringReplacerTest {

   @Test
   public void testStringReaderWhenNominal() throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("Some", "SOME"), "SOME text");
   }

   @Test
   public void testStringReaderWhenBadParameters() throws IOException {
      assertThatNullPointerException()
            .isThrownBy(() -> readerToString(new GeneralReplacerReader(new StringReader("Some text"),
                                                                       bs -> new StringReplacer(null,
                                                                                                "SOME"))))
            .withMessage("'matchString' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> readerToString(new GeneralReplacerReader(new StringReader("Some text"),
                                                                       bs -> new StringReplacer("", "SOME"))))
            .withMessage("'matchString' must not be empty");
   }

   @Test
   public void testStringReaderWhenBlankMatchString() throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer(" ", "SPACE"), "SomeSPACEtext");
   }

   @Test
   public void testStringReaderWhenNullReplacementString() throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("Some", null), "Some text");
   }

   @Test
   public void testStringReaderWhenEmptyReplacementString() throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("Some", ""), " text");
   }

   @Test
   public void testStringReaderWhenCaseSensitive() throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("some", "SOME", false), "Some text");
   }

   @Test
   public void testStringReaderWhenIgnoreCase() throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("some", "SOME", true), "SOME text");
   }

   @Test
   public void testStringReaderWhenMatchingAtStart() throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("Some", "SOME"), "SOME text");
   }

   @Test
   public void testStringReaderWhenMatchingAtEnd() throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("text", "TEXT"), "Some TEXT");
   }

   @Test
   public void testStringReaderWhenMatchingAbortedByEot() throws IOException {
      assertThatEquals("Som", bs -> new StringReplacer("Some", "SOME"), "Som");
   }

   @Test
   public void testReplacerWhenSameReplacementString() throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("So", "So"), "Some text");
   }

   public static void assertThatEquals(String source,
                                       TokenReplacerFactory replacer,
                                       int bufferSize,
                                       int readLength,
                                       String expected) throws IOException {
      assertThat(readerToString(new GeneralReplacerReader(new StringReader(source), replacer, bufferSize),
                                readLength)).isEqualTo(expected);
   }

   public static void assertThatEquals(String source,
                                       TokenReplacerFactory replacer,
                                       int readLength,
                                       String expected) throws IOException {
      assertThatEquals(source, replacer, DEFAULT_BUFFER_SIZE, readLength, expected);
   }

   public static void assertThatEquals(String source, TokenReplacerFactory replacer, String expected)
         throws IOException {
      assertThatEquals(source, replacer, DEFAULT_BUFFER_SIZE, expected);
   }

}