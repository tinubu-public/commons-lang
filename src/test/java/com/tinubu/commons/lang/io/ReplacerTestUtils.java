/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io;

import java.io.IOException;
import java.io.Reader;

public final class ReplacerTestUtils {

   public static final int DEFAULT_BUFFER_SIZE = 8192;

   private ReplacerTestUtils() {
   }

   public static String readerToString(Reader reader, int bufferSize) throws IOException {
      if (bufferSize == -1) {
         StringBuilder returnString = new StringBuilder();
         int c;
         while ((c = reader.read()) != -1) {
            returnString.append((char) c);
         }
         return returnString.toString();
      } else {
         char[] buffer = new char[bufferSize];
         StringBuilder returnString = new StringBuilder();
         int len;
         while ((len = reader.read(buffer, 0, bufferSize)) != -1) {
            returnString.append(buffer, 0, len);
         }
         return returnString.toString();
      }
   }

   public static String readerToString(Reader reader) throws IOException {
      return readerToString(reader, DEFAULT_BUFFER_SIZE);
   }

}