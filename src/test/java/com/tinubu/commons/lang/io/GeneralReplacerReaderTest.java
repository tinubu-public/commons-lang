/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io;

import static com.tinubu.commons.lang.io.ReplacerTestUtils.DEFAULT_BUFFER_SIZE;
import static com.tinubu.commons.lang.io.ReplacerTestUtils.readerToString;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InOrder;

import com.tinubu.commons.lang.io.GeneralReplacerReader.TokenReplacer;
import com.tinubu.commons.lang.io.GeneralReplacerReader.TokenReplacerFactory;

class GeneralReplacerReaderTest {

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenNominal(int readLength) throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("Some", "SOME"), readLength, "SOME text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   @SuppressWarnings("resource")
   public void testReplacerWhenBadParameters(int readLength) {
      assertThatNullPointerException()
            .isThrownBy(() -> new GeneralReplacerReader(null, __ -> null))
            .withMessage("'in' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> new GeneralReplacerReader(new StringReader("test"), null))
            .withMessage("'tokenReplacerFactory' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new GeneralReplacerReader(new StringReader("test"), __ -> null, -5))
            .withMessage("'bufferSize' must be > 0");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenReplacementStringLargerThanToken(int readLength) throws IOException {
      assertThatEquals("Some text",
                       bs -> new StringReplacer("Some", "0123456789"),
                       readLength,
                       "0123456789 text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenReplacementStringIsEmpty(int readLength) throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("Some", null), readLength, "Some text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenRecursiveToken(int readLength) throws IOException {
      assertThatEquals("SoSomemeSomeSome text",
                       bs -> new StringReplacer("Some", "SOME"),
                       readLength,
                       "SoSOMEmeSOMESOME text");
      assertThatEquals("SoSoSoSo text", bs -> new StringReplacer("SoS", "SOS"), readLength, "SOSoSOSo text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenTokenInFirstPosition(int readLength) throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("Some", "SOME"), readLength, "SOME text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenTokenInLastPosition(int readLength) throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("text", "TEXT"), readLength, "Some TEXT");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenTokenContainsPremise(int readLength) throws IOException {
      assertThatEquals("SoS text", bs -> new StringReplacer("SoS", "SOS"), readLength, "SOS text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenInternalBufferSizeLesserThanReadBufferSize(int readLength) throws IOException {
      assertThat(readerToString(new GeneralReplacerReader(new StringReader("Some text"),
                                                          bs -> new StringReplacer("Some", "SOME"),
                                                          1000), 2000)).isEqualTo("SOME text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenInternalBufferSizeLesserThanRegularTextSize(int readLength)
         throws IOException {
      assertThatEquals("A very long text",
                       bs -> new StringReplacer("Some", "SOME"),
                       4,
                       readLength,
                       "A very long text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenInternalBufferSizeLesserThanTokenSize(int readLength) throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("Some", "SOME"), 4, readLength, "SOME text");
      assertThatEquals("Some text", bs -> new StringReplacer("None", "NONE"), 4, readLength, "Some text");
      assertThatEquals("Some text", bs -> new StringReplacer("Some", "SOME"), 3, readLength, "Some text");
      assertThatEquals("Some text", bs -> new StringReplacer("None", "NONE"), 3, readLength, "Some text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenInternalBufferSizeLesserThanTokenReplacementSize(int readLength)
         throws IOException {
      assertThatEquals("Some text",
                       bs -> new StringReplacer("Some", "0123456789"),
                       4,
                       readLength,
                       "Some text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenInternalBufferSizeIsEqualTo1(int readLength) throws IOException {
      assertThatEquals("Some text", bs -> new StringReplacer("Some", "SOME"), 1, readLength, "Some text");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerWhenMatchingSupportsEndOfText(int readLength) throws IOException {
      assertThatEquals("Some text",
                       bs -> new AnyWordReplacer(String::toUpperCase, bs),
                       readLength,
                       "SOME TEXT");
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerToTokenReplacerApiWhenNominal(int readLength) throws IOException {
      SpyTokenReplacerFactory tokenReplacerFactory =
            new SpyTokenReplacerFactory(bs -> new StringReplacer("Some", "SOME"));

      assertThatEquals("Some text", tokenReplacerFactory, readLength, "SOME text");

      InOrder order = tokenReplacerFactory.inOrderReplacers();

      assertThat(tokenReplacerFactory.tokenReplacers()).satisfiesExactly(replacer -> {

         order.verify(replacer).reset();
         order.verify(replacer).isPremise('S');
         order.verify(replacer).append('S');
         order.verify(replacer).matches();
         order.verify(replacer).append('o');
         order.verify(replacer).matches();
         order.verify(replacer).append('m');
         order.verify(replacer).matches();
         order.verify(replacer).append('e');
         order.verify(replacer).matches();
         order.verify(replacer).replacement();

         order.verify(replacer).reset();
         order.verify(replacer, times(2)).isPremise(' ');
         order.verify(replacer).isPremise('t');
         order.verify(replacer).isPremise('e');
         order.verify(replacer).isPremise('x');
         order.verify(replacer).isPremise('t');
         order.verify(replacer).isPremise(-1);

         order.verifyNoMoreInteractions();
      });
   }

   @ParameterizedTest
   @MethodSource("readLengths")
   public void testReplacerToTokenReplacerApiWhenReplacerMatchesEndOfText(int readLength) throws IOException {
      SpyTokenReplacerFactory tokenReplacerFactory =
            new SpyTokenReplacerFactory(bs -> new AnyWordReplacer(String::toUpperCase, bs));

      assertThatEquals("Some text", tokenReplacerFactory, readLength, "SOME TEXT");

      InOrder order = tokenReplacerFactory.inOrderReplacers();

      assertThat(tokenReplacerFactory.tokenReplacers()).satisfiesExactly(replacer -> {

         order.verify(replacer).reset();
         order.verify(replacer).isPremise('S');
         order.verify(replacer).append('S');
         order.verify(replacer).matches();
         order.verify(replacer).append('o');
         order.verify(replacer).matches();
         order.verify(replacer).append('m');
         order.verify(replacer).matches();
         order.verify(replacer).append('e');
         order.verify(replacer).matches();
         order.verify(replacer).append(' ');
         order.verify(replacer).matches();
         order.verify(replacer).replacement();

         order.verify(replacer).reset();
         order.verify(replacer).append('t');
         order.verify(replacer).matches();
         order.verify(replacer).append('e');
         order.verify(replacer).matches();
         order.verify(replacer).append('x');
         order.verify(replacer).matches();
         order.verify(replacer).append('t');
         order.verify(replacer).matches();
         order.verify(replacer).append(-1);
         order.verify(replacer).matches();
         order.verify(replacer).replacement();

         order.verifyNoMoreInteractions();
      });
   }

   private static Stream<Arguments> readLengths() {
      return stream(Arguments.of(DEFAULT_BUFFER_SIZE),
                    Arguments.of(4096),
                    Arguments.of(1),
                    Arguments.of(2),
                    Arguments.of(4),
                    Arguments.of(-1));
   }

   /**
    * Wraps a token replacer factory with a spy and tracks instantiations for later verification.
    */
   private static class SpyTokenReplacerFactory implements TokenReplacerFactory {
      private final TokenReplacerFactory delegate;
      private List<TokenReplacer> tokenReplacers = list();

      /**
       * Constructor.
       *
       * @param delegate token replacer factory to wrap
       */
      public SpyTokenReplacerFactory(TokenReplacerFactory delegate) {
         this.delegate = notNull(delegate, "delegate");
      }

      @Override
      public TokenReplacer instance(int bufferSize) {
         TokenReplacer instance = spy(delegate.instance(bufferSize));

         tokenReplacers.add(instance);

         return instance;
      }

      /**
       * Instantiated token replacers.
       *
       * @return instantiated token replacers
       */
      public List<TokenReplacer> tokenReplacers() {
         return tokenReplacers;
      }

      /**
       * Returns Mockito {@link InOrder} object of instantiated replacers for ordered verifications.
       *
       * @return Mockito {@link InOrder}
       */
      public InOrder inOrderReplacers() {
         return inOrder(tokenReplacers.toArray());
      }
   }

   private static class AnyWordReplacer implements TokenReplacer {
      private static final char WORD_SEPARATOR = ' ';

      private final Function<String, String> wordReplacer;
      private final int bufferSize;

      private char[] buffer;
      private int length = 0;
      private boolean matches = false;
      private boolean endOfText = false;

      public AnyWordReplacer(Function<String, String> wordReplacer, int bufferSize) {
         this.wordReplacer = notNull(wordReplacer);
         this.bufferSize = bufferSize;
         this.buffer = new char[bufferSize];
      }

      @Override
      public void reset() {
         buffer = new char[bufferSize];
         length = 0;
         matches = false;
         endOfText = false;
      }

      @Override
      public boolean isPremise(int c) {
         return c != -1 && c != WORD_SEPARATOR;
      }

      @Override
      public boolean append(int c) {
         if (matches) {
            throw new IllegalStateException();
         }

         endOfText = c == -1;
         matches = endOfText || c == ' ';

         if (endOfText) {
            return true;
         }

         if (length < buffer.length) {
            if (c != ' ') {
               buffer[length] = (char) c;
               length++;
            }

            return true;
         } else {
            return false;
         }
      }

      @Override
      public boolean matches() {
         return matches;
      }

      @Override
      public Optional<char[]> replacement() {
         if (!matches) {
            throw new IllegalStateException();
         }

         return nullable(wordReplacer.apply(new String(buffer, 0, length)))
               .map(r -> r + (!endOfText ? ' ' : ""))
               .map(String::toCharArray);
      }
   }

   private void assertThatEquals(String source,
                                 TokenReplacerFactory replacer,
                                 int bufferSize,
                                 int readLength,
                                 String expected) throws IOException {
      assertThat(readerToString(new GeneralReplacerReader(new StringReader(source), replacer, bufferSize),
                                readLength)).isEqualTo(expected);
   }

   private void assertThatEquals(String source,
                                 TokenReplacerFactory replacer,
                                 int readLength,
                                 String expected) throws IOException {
      assertThatEquals(source, replacer, DEFAULT_BUFFER_SIZE, readLength, expected);
   }

   private void assertThatEquals(String source, TokenReplacerFactory replacer, String expected)
         throws IOException {
      assertThatEquals(source, replacer, DEFAULT_BUFFER_SIZE, expected);
   }

}