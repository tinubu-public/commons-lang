/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.Test;

class NumberVariantTest {

   @Test
   public void testNumberVariantWhenNominal() {
      NumberVariant number = new NumberVariant(3, Integer.class);
      number = new NumberVariant(3L, Long.class);

      assertThat(number).isNotNull();
      assertThat(number.type()).isEqualTo(Long.class);

      Long value = number.value(Long.class);

      assertThat(value).isExactlyInstanceOf(Long.class);
      assertThat(value).isEqualTo(3L);
   }

   @Test
   public void testNumberVariantWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> new NumberVariant(3L, (Class<Long>) null));
   }

   @Test
   public void testNumberVariantWhenNullValue() {
      NumberVariant number = new NumberVariant(null, Long.class);

      assertThat(number).isNotNull();
      assertThat(number.value()).isNull();
      assertThat(number.value(Long.class)).isNull();
      assertThat(number.type()).isEqualTo(Long.class);
   }

   @Test
   public void testValueWhenNullValue() {
      NumberVariant number = new NumberVariant(null, Long.class);

      assertThat(number).isNotNull();

      Long value = number.value(Long.class);

      assertThat(value).isNull();
   }

   @Test
   public void testValueWhenUnsupportedType() {
      assertThat(new NumberVariant(null, AtomicInteger.class).value(AtomicInteger.class)).isNull();
      assertThat(new NumberVariant(new AtomicInteger(3),
                                   AtomicInteger.class).value(AtomicInteger.class)).isExactlyInstanceOf(
            AtomicInteger.class);
      assertThatExceptionOfType(VariantTypeException.class)
            .isThrownBy(() -> new NumberVariant(new AtomicInteger(3),
                                                AtomicInteger.class).value(AtomicLong.class))
            .withMessage(
                  "'java.util.concurrent.atomic.AtomicLong' type is incompatible with 'java.util.concurrent.atomic.AtomicInteger' variant type");
   }

   @Test
   public void testConvertValueWhenLong() {
      assertThat(new NumberVariant(null, Long.class).convertValue(Long.class)).isNull();
      assertThat(new NumberVariant(null, Long.class).longValue()).isNull();

      assertThat(new NumberVariant(3L, Long.class).convertValue(Long.class)).isEqualTo(3L);
      assertThat(new NumberVariant(3L, Long.class).longValue()).isEqualTo(3L);
      assertThat(new NumberVariant(3, Integer.class).convertValue(Long.class)).isEqualTo(3L);
      assertThat(new NumberVariant(3, Integer.class).longValue()).isEqualTo(3L);
      assertThat(new NumberVariant((short) 3, Short.class).convertValue(Long.class)).isEqualTo(3L);
      assertThat(new NumberVariant((short) 3, Short.class).longValue()).isEqualTo(3L);
      assertThat(new NumberVariant((byte) 3, Byte.class).convertValue(Long.class)).isEqualTo(3L);
      assertThat(new NumberVariant((byte) 3, Byte.class).longValue()).isEqualTo(3L);
      assertThat(new NumberVariant(3.0f, Float.class).convertValue(Long.class)).isEqualTo(3L);
      assertThat(new NumberVariant(3.0f, Float.class).longValue()).isEqualTo(3L);
      assertThat(new NumberVariant(3.0d, Double.class).convertValue(Long.class)).isEqualTo(3L);
      assertThat(new NumberVariant(3.0d, Double.class).longValue()).isEqualTo(3L);
      assertThat(new NumberVariant(new BigDecimal("3.0"),
                                   BigDecimal.class).convertValue(Long.class)).isEqualTo(3L);
      assertThat(new NumberVariant(new BigDecimal("3.0"), BigDecimal.class).longValue()).isEqualTo(3L);
      assertThat(new NumberVariant(new BigInteger("3"), BigInteger.class).convertValue(Long.class)).isEqualTo(
            3L);
      assertThat(new NumberVariant(new BigInteger("3"), BigInteger.class).longValue()).isEqualTo(3L);
   }

   @Test
   public void testConvertValueWhenInteger() {
      assertThat(new NumberVariant(null, Integer.class).convertValue(Integer.class)).isNull();
      assertThat(new NumberVariant(null, Integer.class).integerValue()).isNull();

      assertThat(new NumberVariant(3L, Long.class).convertValue(Integer.class)).isEqualTo(3);
      assertThat(new NumberVariant(3L, Long.class).integerValue()).isEqualTo(3);
      assertThat(new NumberVariant(3, Integer.class).convertValue(Integer.class)).isEqualTo(3);
      assertThat(new NumberVariant(3, Integer.class).integerValue()).isEqualTo(3);
      assertThat(new NumberVariant((short) 3, Short.class).convertValue(Integer.class)).isEqualTo(3);
      assertThat(new NumberVariant((short) 3, Short.class).integerValue()).isEqualTo(3);
      assertThat(new NumberVariant((byte) 3, Byte.class).convertValue(Integer.class)).isEqualTo(3);
      assertThat(new NumberVariant((byte) 3, Byte.class).integerValue()).isEqualTo(3);
      assertThat(new NumberVariant(3.0f, Float.class).convertValue(Integer.class)).isEqualTo(3);
      assertThat(new NumberVariant(3.0f, Float.class).integerValue()).isEqualTo(3);
      assertThat(new NumberVariant(3.0d, Double.class).convertValue(Integer.class)).isEqualTo(3);
      assertThat(new NumberVariant(3.0d, Double.class).integerValue()).isEqualTo(3);
      assertThat(new NumberVariant(new BigDecimal("3.0"),
                                   BigDecimal.class).convertValue(Integer.class)).isEqualTo(3);
      assertThat(new NumberVariant(new BigDecimal("3.0"), BigDecimal.class).integerValue()).isEqualTo(3);
      assertThat(new NumberVariant(new BigInteger("3"),
                                   BigInteger.class).convertValue(Integer.class)).isEqualTo(3);
      assertThat(new NumberVariant(new BigInteger("3"), BigInteger.class).integerValue()).isEqualTo(3);
   }

   @Test
   public void testConvertValueWhenShort() {
      assertThat(new NumberVariant(null, Short.class).convertValue(Short.class)).isNull();
      assertThat(new NumberVariant(null, Short.class).shortValue()).isNull();

      assertThat(new NumberVariant(3L, Long.class).convertValue(Short.class)).isEqualTo((short) 3);
      assertThat(new NumberVariant(3L, Long.class).shortValue()).isEqualTo((short) 3);
      assertThat(new NumberVariant(3, Integer.class).convertValue(Short.class)).isEqualTo((short) 3);
      assertThat(new NumberVariant(3, Integer.class).shortValue()).isEqualTo((short) 3);
      assertThat(new NumberVariant((short) 3, Short.class).convertValue(Short.class)).isEqualTo((short) 3);
      assertThat(new NumberVariant((short) 3, Short.class).shortValue()).isEqualTo((short) 3);
      assertThat(new NumberVariant((byte) 3, Byte.class).convertValue(Short.class)).isEqualTo((short) 3);
      assertThat(new NumberVariant((byte) 3, Byte.class).shortValue()).isEqualTo((short) 3);
      assertThat(new NumberVariant(3.0f, Float.class).convertValue(Short.class)).isEqualTo((short) 3);
      assertThat(new NumberVariant(3.0f, Float.class).shortValue()).isEqualTo((short) 3);
      assertThat(new NumberVariant(3.0d, Double.class).convertValue(Short.class)).isEqualTo((short) 3);
      assertThat(new NumberVariant(3.0d, Double.class).shortValue()).isEqualTo((short) 3);
      assertThat(new NumberVariant(new BigDecimal("3.0"),
                                   BigDecimal.class).convertValue(Short.class)).isEqualTo((short) 3);
      assertThat(new NumberVariant(new BigDecimal("3.0"),
                                   BigDecimal.class).shortValue()).isEqualTo((short) 3);
      assertThat(new NumberVariant(new BigInteger("3"),
                                   BigInteger.class).convertValue(Short.class)).isEqualTo((short) 3);
      assertThat(new NumberVariant(new BigInteger("3"), BigInteger.class).shortValue()).isEqualTo((short) 3);
   }

   @Test
   public void testConvertValueWhenByte() {
      assertThat(new NumberVariant(null, Byte.class).convertValue(Byte.class)).isNull();
      assertThat(new NumberVariant(null, Byte.class).byteValue()).isNull();

      assertThat(new NumberVariant(3L, Long.class).convertValue(Byte.class)).isEqualTo((byte) 3);
      assertThat(new NumberVariant(3L, Long.class).byteValue()).isEqualTo((byte) 3);
      assertThat(new NumberVariant(3, Integer.class).convertValue(Byte.class)).isEqualTo((byte) 3);
      assertThat(new NumberVariant(3, Integer.class).byteValue()).isEqualTo((byte) 3);
      assertThat(new NumberVariant((short) 3, Short.class).convertValue(Byte.class)).isEqualTo((byte) 3);
      assertThat(new NumberVariant((short) 3, Short.class).byteValue()).isEqualTo((byte) 3);
      assertThat(new NumberVariant((byte) 3, Byte.class).convertValue(Byte.class)).isEqualTo((byte) 3);
      assertThat(new NumberVariant((byte) 3, Byte.class).byteValue()).isEqualTo((byte) 3);
      assertThat(new NumberVariant(3.0f, Float.class).convertValue(Byte.class)).isEqualTo((byte) 3);
      assertThat(new NumberVariant(3.0f, Float.class).byteValue()).isEqualTo((byte) 3);
      assertThat(new NumberVariant(3.0d, Double.class).convertValue(Byte.class)).isEqualTo((byte) 3);
      assertThat(new NumberVariant(3.0d, Double.class).byteValue()).isEqualTo((byte) 3);
      assertThat(new NumberVariant(new BigDecimal("3.0"),
                                   BigDecimal.class).convertValue(Byte.class)).isEqualTo((byte) 3);
      assertThat(new NumberVariant(new BigDecimal("3.0"), BigDecimal.class).byteValue()).isEqualTo((byte) 3);
      assertThat(new NumberVariant(new BigInteger("3"), BigInteger.class).convertValue(Byte.class)).isEqualTo(
            (byte) 3);
      assertThat(new NumberVariant(new BigInteger("3"), BigInteger.class).byteValue()).isEqualTo((byte) 3);
   }

   @Test
   public void testConvertValueWhenFloat() {
      assertThat(new NumberVariant(null, Float.class).convertValue(Float.class)).isNull();
      assertThat(new NumberVariant(null, Float.class).floatValue()).isNull();

      assertThat(new NumberVariant(3L, Long.class).convertValue(Float.class)).isEqualTo(3.0f);
      assertThat(new NumberVariant(3L, Long.class).floatValue()).isEqualTo(3.0f);
      assertThat(new NumberVariant(3, Integer.class).convertValue(Float.class)).isEqualTo(3.0f);
      assertThat(new NumberVariant(3, Integer.class).floatValue()).isEqualTo(3.0f);
      assertThat(new NumberVariant((short) 3, Short.class).convertValue(Float.class)).isEqualTo(3.0f);
      assertThat(new NumberVariant((short) 3, Short.class).floatValue()).isEqualTo(3.0f);
      assertThat(new NumberVariant((byte) 3, Byte.class).convertValue(Float.class)).isEqualTo(3.0f);
      assertThat(new NumberVariant((byte) 3, Byte.class).floatValue()).isEqualTo(3.0f);
      assertThat(new NumberVariant(3.0f, Float.class).convertValue(Float.class)).isEqualTo(3.0f);
      assertThat(new NumberVariant(3.0f, Float.class).floatValue()).isEqualTo(3.0f);
      assertThat(new NumberVariant(3.0d, Double.class).convertValue(Float.class)).isEqualTo(3.0f);
      assertThat(new NumberVariant(3.0d, Double.class).floatValue()).isEqualTo(3.0f);
      assertThat(new NumberVariant(new BigDecimal("3.0"),
                                   BigDecimal.class).convertValue(Float.class)).isEqualTo(3.0f);
      assertThat(new NumberVariant(new BigDecimal("3.0"), BigDecimal.class).floatValue()).isEqualTo(3.0f);
      assertThat(new NumberVariant(new BigInteger("3"),
                                   BigInteger.class).convertValue(Float.class)).isEqualTo(3.0f);
      assertThat(new NumberVariant(new BigInteger("3"), BigInteger.class).floatValue()).isEqualTo(3.0f);
   }

   @Test
   public void testConvertValueWhenDouble() {
      assertThat(new NumberVariant(null, Double.class).convertValue(Double.class)).isNull();
      assertThat(new NumberVariant(null, Double.class).doubleValue()).isNull();

      assertThat(new NumberVariant(3L, Long.class).convertValue(Double.class)).isEqualTo(3.0d);
      assertThat(new NumberVariant(3L, Long.class).doubleValue()).isEqualTo(3.0d);
      assertThat(new NumberVariant(3, Integer.class).convertValue(Double.class)).isEqualTo(3.0d);
      assertThat(new NumberVariant(3, Integer.class).doubleValue()).isEqualTo(3.0d);
      assertThat(new NumberVariant((short) 3, Short.class).convertValue(Double.class)).isEqualTo(3.0d);
      assertThat(new NumberVariant((short) 3, Short.class).doubleValue()).isEqualTo(3.0d);
      assertThat(new NumberVariant((byte) 3, Byte.class).convertValue(Double.class)).isEqualTo(3.0d);
      assertThat(new NumberVariant((byte) 3, Byte.class).doubleValue()).isEqualTo(3.0d);
      assertThat(new NumberVariant(3.0f, Float.class).convertValue(Double.class)).isEqualTo(3.0d);
      assertThat(new NumberVariant(3.0f, Float.class).doubleValue()).isEqualTo(3.0d);
      assertThat(new NumberVariant(3.0d, Double.class).convertValue(Double.class)).isEqualTo(3.0d);
      assertThat(new NumberVariant(3.0d, Double.class).doubleValue()).isEqualTo(3.0d);
      assertThat(new NumberVariant(new BigDecimal("3.0"),
                                   BigDecimal.class).convertValue(Double.class)).isEqualTo(3.0d);
      assertThat(new NumberVariant(new BigDecimal("3.0"), BigDecimal.class).doubleValue()).isEqualTo(3.0d);
      assertThat(new NumberVariant(new BigInteger("3"),
                                   BigInteger.class).convertValue(Double.class)).isEqualTo(3.0d);
      assertThat(new NumberVariant(new BigInteger("3"), BigInteger.class).doubleValue()).isEqualTo(3.0d);
   }

   @Test
   public void testConvertValueWhenBigDecimal() {
      assertThat(new NumberVariant(null, BigDecimal.class).convertValue(BigDecimal.class)).isNull();
      assertThat(new NumberVariant(null, BigDecimal.class).bigDecimalValue()).isNull();

      assertThat(new NumberVariant(3L, Long.class).convertValue(BigDecimal.class)).isEqualTo(new BigDecimal(
            "3"));
      assertThat(new NumberVariant(3L, Long.class).bigDecimalValue()).isEqualTo(new BigDecimal("3"));
      assertThat(new NumberVariant(3, Integer.class).convertValue(BigDecimal.class)).isEqualTo(new BigDecimal(
            "3"));
      assertThat(new NumberVariant(3, Integer.class).bigDecimalValue()).isEqualTo(new BigDecimal("3"));
      assertThat(new NumberVariant((short) 3,
                                   Short.class).convertValue(BigDecimal.class)).isEqualTo(new BigDecimal("3"));
      assertThat(new NumberVariant((short) 3, Short.class).bigDecimalValue()).isEqualTo(new BigDecimal("3"));
      assertThat(new NumberVariant((byte) 3,
                                   Byte.class).convertValue(BigDecimal.class)).isEqualTo(new BigDecimal("3"));
      assertThat(new NumberVariant((byte) 3, Byte.class).bigDecimalValue()).isEqualTo(new BigDecimal("3"));
      assertThat(new NumberVariant(3.0f,
                                   Float.class).convertValue(BigDecimal.class)).isEqualTo(new BigDecimal("3.0"));
      assertThat(new NumberVariant(3.0f, Float.class).bigDecimalValue()).isEqualTo(new BigDecimal("3.0"));
      assertThat(new NumberVariant(3.0d,
                                   Double.class).convertValue(BigDecimal.class)).isEqualTo(new BigDecimal(
            "3.0"));
      assertThat(new NumberVariant(3.0d, Double.class).bigDecimalValue()).isEqualTo(new BigDecimal("3.0"));
      assertThat(new NumberVariant(new BigDecimal("3.0"),
                                   BigDecimal.class).convertValue(BigDecimal.class)).isEqualTo(new BigDecimal(
            "3.0"));
      assertThat(new NumberVariant(new BigDecimal("3.0"),
                                   BigDecimal.class).bigDecimalValue()).isEqualTo(new BigDecimal("3.0"));
      assertThat(new NumberVariant(new BigInteger("3"),
                                   BigInteger.class).convertValue(BigDecimal.class)).isEqualTo(new BigDecimal(
            "3"));
      assertThat(new NumberVariant(new BigInteger("3"),
                                   BigInteger.class).bigDecimalValue()).isEqualTo(new BigDecimal("3"));
   }

   @Test
   public void testConvertValueWhenBigInteger() {
      assertThat(new NumberVariant(null, BigInteger.class).convertValue(BigInteger.class)).isNull();
      assertThat(new NumberVariant(null, BigInteger.class).bigIntegerValue()).isNull();

      assertThat(new NumberVariant(3L, Long.class).convertValue(BigInteger.class)).isEqualTo(new BigInteger(
            "3"));
      assertThat(new NumberVariant(3L, Long.class).bigIntegerValue()).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant(3, Integer.class).convertValue(BigInteger.class)).isEqualTo(new BigInteger(
            "3"));
      assertThat(new NumberVariant(3, Integer.class).bigIntegerValue()).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant((short) 3,
                                   Short.class).convertValue(BigInteger.class)).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant((short) 3, Short.class).bigIntegerValue()).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant((byte) 3,
                                   Byte.class).convertValue(BigInteger.class)).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant((byte) 3, Byte.class).bigIntegerValue()).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant(3.0f,
                                   Float.class).convertValue(BigInteger.class)).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant(3.0f, Float.class).bigIntegerValue()).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant(3.0d,
                                   Double.class).convertValue(BigInteger.class)).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant(3.0d, Double.class).bigIntegerValue()).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant(new BigDecimal("3"),
                                   BigDecimal.class).convertValue(BigInteger.class)).isEqualTo(new BigInteger(
            "3"));
      assertThat(new NumberVariant(new BigDecimal("3"),
                                   BigDecimal.class).bigIntegerValue()).isEqualTo(new BigInteger("3"));
      assertThat(new NumberVariant(new BigInteger("3"),
                                   BigInteger.class).convertValue(BigInteger.class)).isEqualTo(new BigInteger(
            "3"));
      assertThat(new NumberVariant(new BigInteger("3"),
                                   BigInteger.class).bigIntegerValue()).isEqualTo(new BigInteger("3"));
   }

}
