/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.convert.converters.DefaultConversionService;

class ConverterVariantTest {

   @Test
   public void testConverterVariantWhenNominal() {
      ConverterVariant<String> number =
            new ConverterVariant<>("2", Integer.class, new DefaultConversionService());
      number = new ConverterVariant<>("3", Long.class, new DefaultConversionService());

      assertThat(number).isNotNull();
      assertThat(number.type()).isEqualTo(Long.class);

      Long value = number.value(Long.class);

      assertThat(value).isExactlyInstanceOf(Long.class);
      assertThat(value).isEqualTo(3L);

      String externalValue = number.externalValue();

      assertThat(externalValue).isEqualTo("3");
   }

   @Test
   public void testConverterVariantWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> new ConverterVariant<>("3",
                                                                               null,
                                                                               new DefaultConversionService()));
      assertThatNullPointerException().isThrownBy(() -> new ConverterVariant<>("3", Long.class, null));
   }

   @Test
   public void testConverterVariantWhenNullValue() {
      ConverterVariant<String> number =
            new ConverterVariant<>((String) null, Long.class, new DefaultConversionService());

      assertThat(number).isNotNull();
      assertThat(number.value()).isNull();
      assertThat(number.value(Long.class)).isNull();
      assertThat(number.type()).isEqualTo(Long.class);
   }

   @Test
   public void testConverterVariantWhenDirectValue() {
      ConverterVariant<String> number =
            new ConverterVariant<>(String.class, 3L, Long.class, new DefaultConversionService());

      assertThat(number).isNotNull();
      assertThat(number.type()).isEqualTo(Long.class);

      Long value = number.value(Long.class);

      assertThat(value).isExactlyInstanceOf(Long.class);
      assertThat(value).isEqualTo(3L);

      String externalValue = number.externalValue();

      assertThat(externalValue).isEqualTo("3");
   }

   @Test
   public void testConverterVariantWhenDirectValueAndNoExplicitType() {
      ConverterVariant<String> number =
            new ConverterVariant<>(String.class, 3L, new DefaultConversionService());

      assertThat(number).isNotNull();
      assertThat(number.type()).isEqualTo(Long.class);

      Long value = number.value(Long.class);

      assertThat(value).isExactlyInstanceOf(Long.class);
      assertThat(value).isEqualTo(3L);

      String externalValue = number.externalValue();

      assertThat(externalValue).isEqualTo("3");
   }

   @Test
   public void testConverterVariantWhenDirectValueAndNoExplicitTypeAndNullValue() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ConverterVariant<>(String.class,
                                                     (Long) null,
                                                     new DefaultConversionService()))
            .withMessage("'value' must not be null");
   }

}
