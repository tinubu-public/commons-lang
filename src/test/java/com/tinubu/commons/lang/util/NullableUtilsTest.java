/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

class NullableUtilsTest {

   @Test
   public void testNullable() {
      assertThat(NullableUtils.nullable(null)).isEmpty();
      assertThat(NullableUtils.nullable(32)).isPresent().get().isEqualTo(32);
   }

   @Test
   public void testNullableWhenDefaultValue() {
      assertThat(NullableUtils.nullable(null, 33)).isEqualTo(33);
      assertThat(NullableUtils.nullable(32, 33)).isEqualTo(32);
   }

   @Test
   public void testNullableWhenMapAndDefaultValue() {
      assertThat(NullableUtils.nullable(null, v -> v + 10, 33)).isEqualTo(33);
      assertThat(NullableUtils.nullable(32, v -> v + 10, 33)).isEqualTo(42);
   }

   @Test
   public void testNullableWhenMap() {
      assertThat(NullableUtils.nullable(null, (Function<Integer, Integer>) v -> v + 10)).isNull();
      assertThat(NullableUtils.nullable(32, v -> v + 10)).isEqualTo(42);
   }

   @Test
   public void testNullablePredicateWhenNominal() {
      assertThat(NullableUtils.nullablePredicate(3, v -> v == 3)).hasValue(3);
      assertThat(NullableUtils.nullablePredicate(3, v -> v != 3)).isEmpty();
   }

   @Test
   public void testNullablePredicateWhenNullValue() {
      assertThat(NullableUtils.nullablePredicate((Integer) null, v -> v == 3)).isEmpty();
   }

   @Test
   public void testNullableBooleanWhenNominal() {
      assertThat(NullableUtils.nullableBoolean(true)).hasValue(true);
      assertThat(NullableUtils.nullableBoolean(false)).isEmpty();
   }

   @Test
   public void testNullableBooleanWhenNullValue() {
      assertThat(NullableUtils.nullableBoolean(null)).isEmpty();
   }

   @Test
   public void testNullableInstanceOfSubclassWhenNominal() {
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(), MyClass.class)).isPresent();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(), MyParent.class)).isPresent();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(), MyChild.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(), Long.class)).isEmpty();
   }

   @Test
   public void testNullableInstanceOfSubclassWhenNull() {
      assertThat(NullableUtils.nullableInstanceOf(null, MyClass.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(null, MyParent.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(null, MyChild.class)).isEmpty();

      assertThatNullPointerException()
            .isThrownBy(() -> NullableUtils.nullableInstanceOf(new MyClass(), (Class<MyClass>) null))
            .withMessage("'subclass' must not be null");
   }

   @Test
   public void testNullableInstanceOfSubclassesWhenNominal() {
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(), MyClass.class, String.class)).isPresent();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(), MyParent.class, String.class)).isPresent();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(), MyChild.class, String.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(), Long.class, String.class)).isEmpty();

      assertThat(NullableUtils.nullableInstanceOf(new MyClass(), String.class, MyParent.class)).isPresent();
   }

   @Test
   public void testNullableInstanceOfSubclassesWhenNull() {
      assertThat(NullableUtils.nullableInstanceOf(null, MyClass.class, String.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(null, MyParent.class, String.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(null, MyChild.class, String.class)).isEmpty();

      assertThat(NullableUtils.nullableInstanceOf(null, String.class, MyParent.class)).isEmpty();

      assertThatNullPointerException()
            .isThrownBy(() -> NullableUtils.nullableInstanceOf(new MyClass(), (Class<String>[]) null))
            .withMessage("'subclasses' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> NullableUtils.nullableInstanceOf(new MyClass(), null, (Class<String>) null))
            .withMessage("'subclasses' must not have null elements at index : 0");
   }

   public static class MyParent {}

   public static class MyClass extends MyParent {}

   public static class MyChild extends MyClass {}
}