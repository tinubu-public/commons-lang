/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.function.Consumer;
import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.util.NullableUtilsTest.MyChild;
import com.tinubu.commons.lang.util.NullableUtilsTest.MyClass;
import com.tinubu.commons.lang.util.NullableUtilsTest.MyParent;

class OptionalUtilsTest {

   @Test
   public void testOptionalPredicateWhenNominal() {
      Predicate<Integer> predicate1 = v -> v == 3;
      notNull(predicate1, "predicate");

      assertThat(optional(3).filter(predicate1)).hasValue(3);
      Predicate<Integer> predicate = v -> v != 3;
      notNull(predicate, "predicate");

      assertThat(optional(3).filter(predicate)).isEmpty();
   }

   @Test
   public void testOptionalPredicateWhenNullValue() {
      assertThatNullPointerException().isThrownBy(() -> {
         Predicate<Integer> predicate = v -> v == 3;
         notNull(predicate, "predicate");

         optional((Integer) null).filter(predicate);
      });
   }

   @Test
   public void testOptionalBooleanWhenNominal() {
      assertThat(OptionalUtils.optionalBoolean(true)).hasValue(true);
      assertThat(OptionalUtils.optionalBoolean(false)).isEmpty();
   }

   @Test
   public void testOptionalBooleanWhenNullValue() {
      assertThatNullPointerException().isThrownBy(() -> OptionalUtils.optionalBoolean(null));
   }

   @Test
   public void testOptionalInstanceOfSubclassWhenNominal() {
      assertThat(OptionalUtils.optionalInstanceOf(new MyClass(), MyClass.class)).isPresent();
      assertThat(OptionalUtils.optionalInstanceOf(new MyClass(), MyParent.class)).isPresent();
      assertThat(OptionalUtils.optionalInstanceOf(new MyClass(), MyChild.class)).isEmpty();
      assertThat(OptionalUtils.optionalInstanceOf(new MyClass(), Long.class)).isEmpty();
   }

   @Test
   public void testOptionalInstanceOfSubclassWhenNull() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.optionalInstanceOf(null, MyClass.class))
            .withMessage("'object' must not be null");

      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.optionalInstanceOf(new MyClass(), (Class<MyClass>) null))
            .withMessage("'subclass' must not be null");
   }

   @Test
   public void testOptionalInstanceOfSubclassesWhenNominal() {
      assertThat(OptionalUtils.optionalInstanceOf(new MyClass(), MyClass.class, String.class)).isPresent();
      assertThat(OptionalUtils.optionalInstanceOf(new MyClass(), MyParent.class, String.class)).isPresent();
      assertThat(OptionalUtils.optionalInstanceOf(new MyClass(), MyChild.class, String.class)).isEmpty();
      assertThat(OptionalUtils.optionalInstanceOf(new MyClass(), Long.class, String.class)).isEmpty();

      assertThat(OptionalUtils.optionalInstanceOf(new MyClass(), String.class, MyParent.class)).isPresent();
   }

   @Test
   public void testOptionalInstanceOfSubclassesWhenNull() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.optionalInstanceOf(null, MyClass.class, String.class))
            .withMessage("'object' must not be null");

      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.optionalInstanceOf(new MyClass(), (Class<String>[]) null))
            .withMessage("'subclasses' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> OptionalUtils.optionalInstanceOf(new MyClass(), null, (Class<String>) null))
            .withMessage("'subclasses' must not have null elements at index : 0");
   }

   @Test
   public void testOrWhenNominal() {
      assertThat(OptionalUtils.or(optional(1), () -> optional(2))).hasValue(1);
   }

   @Test
   public void testOrWhenNoSuppliers() {
      assertThat(OptionalUtils.or(optional(1))).hasValue(1);
   }

   @Test
   public void testOrWhenNotPresent() {
      assertThat(OptionalUtils.or(optional(), () -> optional(2))).hasValue(2);
   }

   @Test
   public void testOrWhenNotPresentAndNoSuppliers() {
      assertThat(OptionalUtils.or(optional())).isEmpty();
   }

   @Test
   public void testOrWhenNotPresentWithMultipleSuppliers() {
      assertThat(OptionalUtils.or(optional(), () -> optional(), () -> optional(3))).hasValue(3);
   }

   @Test
   public void testOrWhenNotPresentWithAllEmptySuppliers() {
      assertThat(OptionalUtils.or(optional(), () -> optional(), () -> optional())).isEmpty();
   }

   @Test
   public void testOrWhenNullOptional() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.or(null))
            .withMessage("'optional' must not be null");
   }

   @Test
   public void testOrWhenNullSuppliers() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.or(optional(), null))
            .withMessage("'suppliers' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> OptionalUtils.or(optional(), () -> optional(), null))
            .withMessage("'suppliers' must not have null elements at index : 1");
   }

   @Test
   public void testOrWhenNullSupplierValue() {
      assertThatNullPointerException().isThrownBy(() -> OptionalUtils.or(optional(), () -> null));
   }

   @Test
   public void testPeekWhenNominal() {
      Consumer<Integer> consumer = spy(new Consumer<Integer>() {
         @Override
         public void accept(Integer v) {
            assertThat(v).isEqualTo(1);
         }
      });

      assertThat(OptionalUtils.peek(optional(1), consumer)).hasValue(1);

      verify(consumer).accept(1);
   }

   @Test
   public void testPeekWhenNotPresent() {
      assertThat(OptionalUtils.peek(optional(), v -> {
         throw new IllegalStateException("must not be called");
      })).isEmpty();
   }

   @Test
   public void testPeekWhenNullOptional() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.peek(null, __ -> {}))
            .withMessage("'optional' must not be null");
   }

   @Test
   public void testPeekWhenNullConsumer() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.peek(optional(), null))
            .withMessage("'consumer' must not be null");
   }

   @Test
   public void testPeekEmptyWhenNominal() {
      Runnable runnable = spy(new Runnable() {
         @Override
         public void run() {
         }
      });
      assertThat(OptionalUtils.peekEmpty(optional(), runnable)).isEmpty();

      verify(runnable).run();
   }

   @Test
   public void testPeekEmptyWhenPresent() {
      assertThat(OptionalUtils.peekEmpty(optional(1), () -> {
         throw new IllegalStateException("must not be called");
      })).hasValue(1);
   }

   @Test
   public void testPeekEmptyWhenNullOptional() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.peekEmpty(null, () -> {}))
            .withMessage("'optional' must not be null");
   }

   @Test
   public void testPeekEmptyWhenNullConsumer() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.peekEmpty(optional(), null))
            .withMessage("'runnable' must not be null");
   }

   @Test
   public void testIfPresentOrElseWhenNominal() {
      Runnable runnable = spy(new Runnable() {
         @Override
         public void run() {
         }
      });
      Consumer<Integer> consumer = spy(new Consumer<Integer>() {
         @Override
         public void accept(Integer i) {
            assertThat(i).isEqualTo(1);
         }
      });

      OptionalUtils.ifPresentOrElse(optional(1), consumer, runnable);

      verify(consumer).accept(any());
      verify(runnable, never()).run();
   }

   @Test
   public void testIfPresentOrElseWhenNotPresent() {
      Runnable runnable = spy(new Runnable() {
         @Override
         public void run() {
         }
      });
      Consumer<Integer> consumer = spy(new Consumer<Integer>() {
         @Override
         public void accept(Integer i) {
            assertThat(i).isEqualTo(1);
         }
      });

      OptionalUtils.ifPresentOrElse(optional(), consumer, runnable);

      verify(consumer, never()).accept(1);
      verify(runnable).run();
   }

   @Test
   public void testIfPresentOrElseWhenNullOptional() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.ifPresentOrElse(null, __ -> {}, () -> {}))
            .withMessage("'optional' must not be null");
   }

   @Test
   public void testIfPresentOrElseWhenNullConsumer() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.ifPresentOrElse(optional(), null, () -> {}))
            .withMessage("'consumer' must not be null");
   }

   @Test
   public void testIfPresentOrElseWhenNullRunnable() {
      assertThatNullPointerException()
            .isThrownBy(() -> OptionalUtils.ifPresentOrElse(optional(), __ -> {}, null))
            .withMessage("'runnable' must not be null");
   }

}