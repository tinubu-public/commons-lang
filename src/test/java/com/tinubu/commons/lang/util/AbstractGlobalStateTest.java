/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.stream.IntStream.range;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.junit.jupiter.api.Test;

public abstract class AbstractGlobalStateTest {

   protected abstract <S> GlobalState<S> globalState(S initial);

   protected abstract <S> GlobalState<S> globalState();

   @Test
   public void testWhenNominal() {
      GlobalState<Integer> state = globalState(3);

      assertThat(state.state()).isEqualTo(3);
   }

   @Test
   public void testWhenNoInitialValue() {
      GlobalState<Integer> state = globalState();

      assertThat(state.state()).isNull();
   }

   @Test
   public void testWhenStateAndNoInitialValue() {
      GlobalState<Integer> state = globalState();

      state.state(4);

      assertThat(state.state()).isEqualTo(4);
   }

   @Test
   public void testWhenStateAndInitialValue() {
      GlobalState<Integer> state = globalState(3);

      state.state(4);

      assertThat(state.state()).isEqualTo(4);
   }

   @Test
   public void testWhenNullStateAndInitialValue() {
      GlobalState<Integer> state = globalState(3);

      state.state(null);

      assertThat(state.state()).isEqualTo(3);
   }

   @Test
   public void testWhenNullStateAndNoInitialValue() {
      GlobalState<Integer> state = globalState();

      state.state(null);

      assertThat(state.state()).isNull();
   }

   @Test
   public void testWhenConcurrentThreadUpdates() throws ExecutionException, InterruptedException {
      int nbThreads = 10;
      int nbLoops = 1000;

      GlobalState<Integer> state = globalState(3);

      ExecutorService executor = newFixedThreadPool(nbThreads);
      List<Future<?>> threads = list(range(0, nbThreads).boxed().map(t -> executor.submit(() -> {
         for (int i = 0; i < nbLoops; i++) {
            state.state(t * nbLoops + i);
            assertThat(state.state()).isEqualTo(t * nbLoops + i);
         }
      })));

      for (Future<?> thread : threads) {
         thread.get();
      }
   }

}