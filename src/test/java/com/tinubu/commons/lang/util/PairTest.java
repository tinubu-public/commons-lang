/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.Pair.comparing;
import static com.tinubu.commons.lang.util.Pair.comparingByLeft;
import static com.tinubu.commons.lang.util.Pair.comparingByRight;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class PairTest {

   @Test
   public void testPairWhenNominal() {
      Pair<Integer, Integer> pair = Pair.of(1, 2);

      assertThat(pair.getLeft()).isEqualTo(pair.getKey()).isEqualTo(1);
      assertThat(pair.getRight()).isEqualTo(pair.getValue()).isEqualTo(2);
   }

   @Test
   public void testMapWhenNominal() {
      Pair<Integer, Integer> pair = Pair.of(1, 2);

      Pair<Integer, Integer> mappedPair = pair.map((l, r) -> Pair.of(l + 10, r + 10));

      assertThat(mappedPair.getLeft()).isEqualTo(mappedPair.getKey()).isEqualTo(11);
      assertThat(mappedPair.getRight()).isEqualTo(mappedPair.getValue()).isEqualTo(12);
   }

   @Test
   public void testFilterWhenNominal() {
      Pair<Integer, Integer> pair = Pair.of(1, 2);

      Pair<Integer, Integer> mappedPair = pair.map((l, r) -> Pair.of(l + 10, r + 10));

      assertThat(mappedPair.getLeft()).isEqualTo(mappedPair.getKey()).isEqualTo(11);
      assertThat(mappedPair.getRight()).isEqualTo(mappedPair.getValue()).isEqualTo(12);
   }

   @Test
   public void testForEachWhenNominal() {
      Pair<Integer, Integer> pair = Pair.of(1, 2);

      AtomicInteger mappedLeft = new AtomicInteger();
      AtomicInteger mappedRight = new AtomicInteger();
      pair.forEach((l, r) -> {
         mappedLeft.set(l + 10);
         mappedRight.set(r + 10);
      });

      assertThat(mappedLeft).hasValue(11);
      assertThat(mappedRight).hasValue(12);
   }

   @Test
   public void testSetValueWhenNominal() {
      Pair<Integer, Integer> pair = Pair.of(1, 2);

      assertThatThrownBy(() -> pair.setValue(3)).isInstanceOf(UnsupportedOperationException.class);
   }

   @Test
   public void testEqualsHashCodeWhenNominal() {
      Pair<Integer, Integer> pair1 = Pair.of(1, 2);
      Pair<Integer, Integer> pair1b = Pair.of(1, 2);
      Pair<Integer, Integer> pair2 = Pair.of(10, 20);

      assertThat(pair1).isEqualTo(pair1);
      assertThat(pair1).isEqualTo(pair1b);
      assertThat(pair1).isNotEqualTo("other type");

      Map<Pair<Integer, Integer>, Pair<Integer, Integer>> pairMap = new HashMap<>();
      pairMap.put(pair1, pair1);
      pairMap.put(pair2, pair2);

      Assertions.assertThat(pairMap).containsKeys(pair1, pair2);
      Assertions.assertThat(pairMap).containsValues(Pair.from(pair1), Pair.from(pair2));
   }

   @Test
   public void testToStringWhenNominal() {
      Pair<Integer, Integer> pair = Pair.of(1, 2);

      assertThat(pair.toString()).isEqualTo("(1,2)");
      assertThat(pair.toString("%2$s + %1$s")).isEqualTo("2 + 1");
      assertThat(pair.toString("%s + %s")).isEqualTo("1 + 2");
   }

   @Test
   public void testCompareToWhenNominal() {
      assertThat(Pair.of(1, 1).compareTo(Pair.of(2, 1))).isEqualTo(-1);
      assertThat(Pair.of(1, 1).compareTo(Pair.of(1, 1))).isEqualTo(0);
      assertThat(Pair.of(2, 1).compareTo(Pair.of(1, 1))).isEqualTo(1);
      assertThat(Pair.of(1, 1).compareTo(Pair.of(1, 2))).isEqualTo(-1);
      assertThat(Pair.of(1, 2).compareTo(Pair.of(1, 1))).isEqualTo(1);
   }

   @Test
   public void testCompareToWhenNullValues() {
      assertThat(Pair.of(null, null).compareTo(Pair.of(1, null))).isEqualTo(-1);
      assertThat(Pair.of(null, null).compareTo(Pair.of(null, null))).isEqualTo(0);
      assertThat(Pair.of(1, null).compareTo(Pair.of(null, null))).isEqualTo(1);
      assertThat(Pair.of(null, null).compareTo(Pair.of(null, 1))).isEqualTo(-1);
      assertThat(Pair.of(null, 1).compareTo(Pair.of(null, null))).isEqualTo(1);
   }

   @Test
   public void testCompareToWhenNotComparablesValues() {
      assertThatThrownBy(() -> Pair.of(new Object(), 1).compareTo(Pair.of(new Object(), 1))).isInstanceOf(
            ClassCastException.class);
   }

   @Test
   public void testComparingByLeftWhenNominal() {
      Comparator<Pair<Integer, Integer>> leftComparator = comparingByLeft();

      assertThat(Pair.of(0, 2)).usingComparator(leftComparator).isEqualByComparingTo(Pair.of(0, 2));
      assertThat(Pair.of(0, 2)).usingComparator(leftComparator).isLessThan(Pair.of(1, 2));
      assertThat(Pair.of(1, 2)).usingComparator(leftComparator).isGreaterThan(Pair.of(0, 2));
   }

   @Test
   public void testComparingByLeftWhenNullValues() {
      Comparator<Pair<Integer, Integer>> leftComparator = comparingByLeft();

      assertThat(Pair.of((Integer) null, 2))
            .usingComparator(leftComparator)
            .isEqualByComparingTo(Pair.of(null, 2));
      assertThat(Pair.of((Integer) null, 2)).usingComparator(leftComparator).isLessThan(Pair.of(1, 2));
      assertThat(Pair.of(1, 2)).usingComparator(leftComparator).isGreaterThan(Pair.of(null, 2));
   }

   @Test
   public void testComparingByRightWhenNominal() {
      Comparator<Pair<Integer, Integer>> rightComparator = comparingByRight();

      assertThat(Pair.of(2, 0)).usingComparator(rightComparator).isEqualByComparingTo(Pair.of(2, 0));
      assertThat(Pair.of(2, 0)).usingComparator(rightComparator).isLessThan(Pair.of(2, 1));
      assertThat(Pair.of(2, 1)).usingComparator(rightComparator).isGreaterThan(Pair.of(2, 0));
   }

   @Test
   public void testComparingByRightWhenNullValues() {
      Comparator<Pair<Integer, Integer>> rightComparator = comparingByRight();

      assertThat(Pair.of(2, (Integer) null))
            .usingComparator(rightComparator)
            .isEqualByComparingTo(Pair.of(2, null));
      assertThat(Pair.of(2, (Integer) null)).usingComparator(rightComparator).isLessThan(Pair.of(2, 1));
      assertThat(Pair.of(2, 1)).usingComparator(rightComparator).isGreaterThan(Pair.of(2, null));
   }

   @Test
   public void testComparingWhenNominal() {
      Comparator<Pair<Integer, Integer>> comparator = comparing();

      assertThat(Pair.of(0, 2)).usingComparator(comparator).isEqualByComparingTo(Pair.of(0, 2));
      assertThat(Pair.of(0, 2)).usingComparator(comparator).isLessThan(Pair.of(1, 2));
      assertThat(Pair.of(1, 2)).usingComparator(comparator).isGreaterThan(Pair.of(0, 2));
      assertThat(Pair.of(0, 1)).usingComparator(comparator).isLessThan(Pair.of(0, 2));
      assertThat(Pair.of(0, 2)).usingComparator(comparator).isGreaterThan(Pair.of(0, 1));
   }

   @Test
   public void testComparingWhenNullValues() {
      Comparator<Pair<Integer, Integer>> comparator = comparing();

      assertThat(Pair.of((Integer) null, 2))
            .usingComparator(comparator)
            .isEqualByComparingTo(Pair.of(null, 2));
      assertThat(Pair.of((Integer) null, 2)).usingComparator(comparator).isLessThan(Pair.of(1, 2));
      assertThat(Pair.of(1, 2)).usingComparator(comparator).isGreaterThan(Pair.of(null, 2));
      assertThat(Pair.of((Integer) null, (Integer) null))
            .usingComparator(comparator)
            .isEqualByComparingTo(Pair.of(null, null));
      assertThat(Pair.of((Integer) null, (Integer) null))
            .usingComparator(comparator)
            .isLessThan(Pair.of(null, 2));
      assertThat(Pair.of((Integer) null, 2)).usingComparator(comparator).isGreaterThan(Pair.of(null, null));
   }

}