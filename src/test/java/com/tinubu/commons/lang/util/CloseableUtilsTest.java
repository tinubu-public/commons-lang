/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CloseableUtils.acceptCloseable;
import static com.tinubu.commons.lang.util.CloseableUtils.acceptCloseableUnchecked;
import static com.tinubu.commons.lang.util.CloseableUtils.applyCloseable;
import static com.tinubu.commons.lang.util.CloseableUtils.applyCloseableUnchecked;
import static com.tinubu.commons.lang.util.CloseableUtils.finalAcceptCloseable;
import static com.tinubu.commons.lang.util.CloseableUtils.finalAcceptCloseableUnchecked;
import static com.tinubu.commons.lang.util.CloseableUtils.finalApplyCloseable;
import static com.tinubu.commons.lang.util.CloseableUtils.finalApplyCloseableUnchecked;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class CloseableUtilsTest {

   @Nested
   public class TryApplyAutoCloseable {

      @Test
      public void testTryApplyAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         AutoCloseable closeable = finalApplyCloseable(closeableStream, s -> s);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenFunctionError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
            throw new Error("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenFunctionException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenFunctionRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenFunctionIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> finalApplyCloseable(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenFunctionAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenFunctionAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenFunctionAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableWhenFunctionAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class TryApplyCloseable {

      @Test
      public void testTryApplyCloseableWhenNominal() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         Closeable closeable = finalApplyCloseable(closeableStream, s -> s);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenFunctionError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
            throw new Error("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenFunctionException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenFunctionRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenFunctionIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> finalApplyCloseable(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenFunctionAndCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenFunctionAndCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenFunctionAndCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableWhenFunctionAndCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class TryApplyAutoCloseableUnchecked {

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         AutoCloseable closeable = finalApplyCloseableUnchecked(closeableStream, s -> s);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenFunctionError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new Error("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenFunctionException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenFunctionRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenFunctionIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenFunctionAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenFunctionAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenFunctionAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyAutoCloseableUncheckedWhenFunctionAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class TryApplyCloseableUnchecked {

      @Test
      public void testTryApplyCloseableUncheckedWhenNominal() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         Closeable closeable = finalApplyCloseableUnchecked(closeableStream, s -> s);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenFunctionError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new Error("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenFunctionException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenFunctionRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenFunctionIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws Error {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> s))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenFunctionAndCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenFunctionAndCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenFunctionAndCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryApplyCloseableUncheckedWhenFunctionAndCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalApplyCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class TryAcceptAutoCloseable {

      @Test
      public void testTryAcceptAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         finalAcceptCloseable(closeableStream, s -> {});

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenFunctionError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
            throw new Error("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenFunctionException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenFunctionRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenFunctionIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenFunctionAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenFunctionAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenFunctionAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableWhenFunctionAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class TryAcceptCloseable {

      @Test
      public void testTryAcceptCloseableWhenNominal() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         finalAcceptCloseable(closeableStream, s -> {});

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenFunctionError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
            throw new Error("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenFunctionException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenFunctionRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenFunctionIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenFunctionAndCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenFunctionAndCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenFunctionAndCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableWhenFunctionAndCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class TryAcceptAutoCloseableUnchecked {

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         finalAcceptCloseableUnchecked(closeableStream, s -> {});

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenFunctionError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new Error("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenFunctionException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenFunctionRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenFunctionIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenFunctionAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenFunctionAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenFunctionAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptAutoCloseableUncheckedWhenFunctionAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class TryAcceptCloseableUnchecked {

      @Test
      public void testTryAcceptCloseableUncheckedWhenNominal() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         finalAcceptCloseableUnchecked(closeableStream, s -> {});

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenFunctionError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new Error("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenFunctionException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenFunctionRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenFunctionIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws Error {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {}))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenFunctionAndCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenFunctionAndCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenFunctionAndCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTryAcceptCloseableUncheckedWhenFunctionAndCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> finalAcceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class ApplyAutoCloseable {

      @Test
      public void testApplyAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         AutoCloseable closeable = applyCloseable(closeableStream, s -> s);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenFunctionError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> applyCloseable(closeableStream, s -> {
            throw new Error("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenFunctionException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenFunctionRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenFunctionIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> applyCloseable(closeableStream, s -> {
            throw new IOException("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenFunctionAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenFunctionAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenFunctionAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenFunctionAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class ApplyCloseable {

      @Test
      public void testApplyCloseableWhenNominal() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         Closeable closeable = applyCloseable(closeableStream, s -> s);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableWhenFunctionError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> applyCloseable(closeableStream, s -> {
            throw new Error("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableWhenFunctionException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableWhenFunctionRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableWhenFunctionIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> applyCloseable(closeableStream, s -> {
            throw new IOException("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableWhenCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new Error("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableWhenCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableWhenCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableWhenCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableWhenFunctionAndCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableWhenFunctionAndCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableWhenFunctionAndCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableWhenFunctionAndCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> applyCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class ApplyAutoCloseableUnchecked {

      @Test
      public void testApplyAutoCloseableUncheckedWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         AutoCloseable closeable = applyCloseableUnchecked(closeableStream, s -> s);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenFunctionError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new Error("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenFunctionException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenFunctionRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenFunctionIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenFunctionAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenFunctionAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenFunctionAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableUncheckedWhenFunctionAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class ApplyCloseableUnchecked {

      @Test
      public void testApplyCloseableUncheckedWhenNominal() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         Closeable closeable = applyCloseableUnchecked(closeableStream, s -> s);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenFunctionError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new Error("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenFunctionException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenFunctionRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenFunctionIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws Error {
               throw new Error("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         applyCloseable(closeableStream, s -> s);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenFunctionAndCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenFunctionAndCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenFunctionAndCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyCloseableUncheckedWhenFunctionAndCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class AcceptAutoCloseable {

      @Test
      public void testAcceptAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenFunctionError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> acceptCloseable(closeableStream, s -> {
            throw new Error("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenFunctionException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenFunctionRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenFunctionIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> acceptCloseable(closeableStream, s -> {
            throw new IOException("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenFunctionAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenFunctionAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenFunctionAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenFunctionAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class AcceptCloseable {

      @Test
      public void testAcceptCloseableWhenNominal() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableWhenFunctionError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> acceptCloseable(closeableStream, s -> {
            throw new Error("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableWhenFunctionException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableWhenFunctionRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableWhenFunctionIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> acceptCloseable(closeableStream, s -> {
            throw new IOException("Function error");
         })).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableWhenCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new Error("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableWhenCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableWhenCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableWhenCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableWhenFunctionAndCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableWhenFunctionAndCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableWhenFunctionAndCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableWhenFunctionAndCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseable(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class AcceptAutoCloseableUnchecked {

      @Test
      public void testAcceptAutoCloseableUncheckedWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         acceptCloseableUnchecked(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenFunctionError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new Error("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenFunctionException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenFunctionRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenFunctionIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenFunctionAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenFunctionAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenFunctionAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableUncheckedWhenFunctionAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class AcceptCloseableUnchecked {

      @Test
      public void testAcceptCloseableUncheckedWhenNominal() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         acceptCloseableUnchecked(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenFunctionError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new Error("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenFunctionException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenFunctionRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenFunctionIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws Error {
               throw new Error("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         acceptCloseable(closeableStream, s -> {});

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenFunctionAndCloseError() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new InternalError("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenFunctionAndCloseException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new ClassNotFoundException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenFunctionAndCloseRuntimeException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IllegalStateException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptCloseableUncheckedWhenFunctionAndCloseIOException() throws Exception {
         Closeable closeableStream = spy(new Closeable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> acceptCloseableUnchecked(closeableStream, s -> {
                  throw new IOException("Function error");
               }))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }
}