/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.ExceptionUtils.runtimeThrow;
import static com.tinubu.commons.lang.util.ExceptionUtils.smartFinalize;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UncheckedIOException;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ExceptionUtilsTest {

   @Nested
   public class SneakyThrow {

      @Test
      public void testSneakyThrowWhenNominal() {
         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> sneakyThrow(new ClassNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testSneakyThrowWhenError() {
         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> sneakyThrow(new InternalError("Error")))
               .withMessage("Error");
      }

      @Test
      public void testSneakyThrowWhenExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> sneakyThrow(new Error("Error")))
               .withMessage("Error");
      }

      @Test
      public void testSneakyThrowWhenRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> sneakyThrow(new IllegalStateException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testSneakyThrowWhenExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> sneakyThrow(new RuntimeException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testSneakyThrowWhenException() {
         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> sneakyThrow(new ClassNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testSneakyThrowWhenExactlyException() {
         assertThatExceptionOfType(Exception.class)
               .isThrownBy(() -> sneakyThrow(new Exception("Error")))
               .withMessage("Error");
      }

      @Test
      public void testSneakyThrowWhenIOException() {
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> sneakyThrow(new FileNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testSneakyThrowWhenExactlyIOException() {
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> sneakyThrow(new IOException("Error")))
               .withMessage("Error");
      }
   }

   @Nested
   public class RuntimeThrow {

      @Test
      public void testUncheckedThrowWhenNominal() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> runtimeThrow(new ClassNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testUncheckedThrowWhenError() {
         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> runtimeThrow(new InternalError("Error")))
               .withMessage("Error");
      }

      @Test
      public void testUncheckedThrowWhenExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> runtimeThrow(new Error("Error")))
               .withMessage("Error");
      }

      @Test
      public void testUncheckedThrowWhenRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> runtimeThrow(new IllegalStateException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testUncheckedThrowWhenExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> runtimeThrow(new RuntimeException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testUncheckedThrowWhenException() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> runtimeThrow(new ClassNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testUncheckedThrowWhenExactlyException() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> runtimeThrow(new Exception("Error")))
               .withMessage("Error");
      }

      @Test
      public void testUncheckedThrowWhenIOException() {
         assertThatExceptionOfType(UncheckedIOException.class)
               .isThrownBy(() -> runtimeThrow(new FileNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void testUncheckedThrowWhenExactlyIOException() {
         assertThatExceptionOfType(UncheckedIOException.class)
               .isThrownBy(() -> runtimeThrow(new IOException("Error")))
               .withMessage("Error");
      }
   }

   @Nested
   public class SmartFinalize {

      @Test
      public void testSmartFinalizeWhenNominal() throws Throwable {
         assertThat(smartFinalize(() -> "Value")).isEqualTo("Value");
      }

      @Test
      public void testSmartFinalizeWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> smartFinalize(() -> {
            throw new InternalError("Operation error");
         })).withMessage("Operation error");
      }

      @Test
      public void testSmartFinalizeWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> smartFinalize(() -> {
            throw new Error("Operation error");
         })).withMessage("Operation error");
      }

      @Test
      public void testSmartFinalizeWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> smartFinalize(() -> {
            throw new ClassNotFoundException("Operation error");
         })).withMessage("Operation error");
      }

      @Test
      public void testSmartFinalizeWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> smartFinalize(() -> {
            throw new Exception("Operation error");
         })).withMessage("Operation error");
      }

      @Test
      public void testSmartFinalizeWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> smartFinalize(() -> {
            throw new IllegalStateException("Operation error");
         })).withMessage("Operation error");
      }

      @Test
      public void testSmartFinalizeWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> smartFinalize(() -> {
            throw new RuntimeException("Operation error");
         })).withMessage("Operation error");
      }

      @Test
      public void testSmartFinalizeWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> smartFinalize(() -> {
            throw new FileNotFoundException("Operation error");
         })).withMessage("Operation error");
      }

      @Test
      public void testSmartFinalizeWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> smartFinalize(() -> {
            throw new IOException("Operation error");
         })).withMessage("Operation error");
      }

      @Test
      public void testSmartFinalizeWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(smartFinalize(() -> "Value", finalizer1)).isEqualTo("Value");
         verify(finalizer1).runChecked();
      }

      @Test
      public void testSmartFinalizeWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(smartFinalize(() -> "Value", finalizer1, finalizer2)).isEqualTo("Value");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void testSmartFinalizeWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> ExceptionUtils.<String>smartFinalize(() -> {
                  throw new FileNotFoundException("Operation error");
               }, finalizer1, finalizer2))
               .withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void testSmartFinalizeWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> ExceptionUtils.<String>smartFinalize(() -> {
                  throw new InternalError("Operation error");
               }, finalizer1, finalizer2))
               .withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void testSmartFinalizeWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> smartFinalize(() -> "Value", finalizer1, finalizer2, finalizer3))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void testSmartFinalizeWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> smartFinalize(() -> "Value", finalizer1, finalizer2, finalizer3))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void testSmartFinalizeWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> smartFinalize(() -> {
            throw new IllegalStateException("Function error");
         }, finalizer1, finalizer2)).satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
            assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
            assertThat(s1).hasMessage("Finalizer1 error");
         }, s2 -> {
            assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
            assertThat(s2).hasMessage("Finalizer2 error");
         })).withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

   @Test
   public void testSmartFinalizeWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
         throws Exception {
      CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
         @Override
         public void runChecked() throws FileNotFoundException {
            throw new FileNotFoundException("Finalizer1 error");

         }
      });
      CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
         @Override
         public void runChecked() throws InterruptedIOException {
            throw new InterruptedIOException("Finalizer2 error");
         }
      });
      assertThatExceptionOfType(Error.class).isThrownBy(() -> smartFinalize(() -> {
         throw new Error("Function error");
      }, finalizer1, finalizer2)).satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
         assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
         assertThat(s1).hasMessage("Finalizer1 error");
      }, s2 -> {
         assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
         assertThat(s2).hasMessage("Finalizer2 error");
      })).withMessage("Function error");

      verify(finalizer1).runChecked();
      verify(finalizer2).runChecked();
   }

   @Test
   public void testSmartFinalizeWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
         throws Exception {
      CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
         @Override
         public void runChecked() throws Error {
            throw new Error("Finalizer1 error");

         }
      });
      CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
         @Override
         public void runChecked() throws InterruptedIOException {
            throw new InterruptedIOException("Finalizer2 error");
         }
      });
      assertThatExceptionOfType(Error.class).isThrownBy(() -> smartFinalize(() -> {
         throw new Error("Function error");
      }, finalizer1, finalizer2)).satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
         assertThat(s1).isOfAnyClassIn(Error.class);
         assertThat(s1).hasMessage("Finalizer1 error");
      }, s2 -> {
         assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
         assertThat(s2).hasMessage("Finalizer2 error");
      })).withMessage("Function error");

      verify(finalizer1).runChecked();
      verify(finalizer2).runChecked();
   }

}
