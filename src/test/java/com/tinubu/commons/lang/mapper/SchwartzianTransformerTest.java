/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mapper;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class SchwartzianTransformerTest {

   @Test
   public void testSchwartzianTransformerWhenBadParameters() {
      assertThatNullPointerException()
            .isThrownBy(() -> new SchwartzianTransformer<>("42", null))
            .withMessage("'mapper' must not be null");
   }

   @Test
   public void testEqualsWhenNominal() {
      SchwartzianTransformer<String, Integer> transformer1 =
            new SchwartzianTransformer<>("42", Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer2 =
            new SchwartzianTransformer<>("42", Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("43", Integer::parseInt);

      assertThat(transformer1).isEqualTo(transformer2);
      assertThat(transformer1).isNotEqualTo(transformer3);
      assertThat(transformer2).isEqualTo(transformer1);
      assertThat(transformer3).isNotEqualTo(transformer1);
   }

   @Test
   public void testEqualsWhenNullSource() {
      SchwartzianTransformer<String, Integer> transformer1 =
            new SchwartzianTransformer<>(null, Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer2 =
            new SchwartzianTransformer<>(null, Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("43", Integer::parseInt);

      assertThat(transformer1).isEqualTo(transformer2);
      assertThat(transformer1).isNotEqualTo(transformer3);
      assertThat(transformer2).isEqualTo(transformer1);
      assertThat(transformer3).isNotEqualTo(transformer1);
   }

   @Test
   public void testEqualsWhenNullMapperResult() {
      SchwartzianTransformer<String, Integer> transformer1 = new SchwartzianTransformer<>("42", __ -> null);
      SchwartzianTransformer<String, Integer> transformer2 = new SchwartzianTransformer<>("42", __ -> null);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("42", Integer::parseInt);

      assertThat(transformer1).isEqualTo(transformer2);
      assertThat(transformer1).isNotEqualTo(transformer3);
      assertThat(transformer2).isEqualTo(transformer1);
      assertThat(transformer3).isNotEqualTo(transformer1);
   }

   @Test
   public void testEqualsWhenNullSourceAndMapperResult() {
      SchwartzianTransformer<String, Integer> transformer1 = new SchwartzianTransformer<>(null, __ -> null);
      SchwartzianTransformer<String, Integer> transformer2 = new SchwartzianTransformer<>(null, __ -> null);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("43", Integer::parseInt);

      assertThat(transformer1).isEqualTo(transformer2);
      assertThat(transformer1).isNotEqualTo(transformer3);
      assertThat(transformer2).isEqualTo(transformer1);
      assertThat(transformer3).isNotEqualTo(transformer1);
   }

   @Test
   public void testHashCodeWhenNominal() {
      SchwartzianTransformer<String, Integer> transformer1 =
            new SchwartzianTransformer<>("42", Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer2 =
            new SchwartzianTransformer<>("42", Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("43", Integer::parseInt);

      assertThat(transformer1.hashCode()).isEqualTo(transformer2.hashCode());
      assertThat(transformer1.hashCode()).isNotEqualTo(transformer3.hashCode());
   }

   @Test
   public void testHashCodeWhenNullSource() {
      SchwartzianTransformer<String, Integer> transformer1 =
            new SchwartzianTransformer<>(null, Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer2 =
            new SchwartzianTransformer<>(null, Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("43", Integer::parseInt);

      assertThat(transformer1.hashCode()).isEqualTo(transformer2.hashCode());
      assertThat(transformer1.hashCode()).isNotEqualTo(transformer3.hashCode());
   }

   @Test
   public void testHashCodeWhenNullMapperResult() {
      SchwartzianTransformer<String, Integer> transformer1 = new SchwartzianTransformer<>("42", __ -> null);
      SchwartzianTransformer<String, Integer> transformer2 = new SchwartzianTransformer<>("42", __ -> null);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("42", Integer::parseInt);

      assertThat(transformer1.hashCode()).isEqualTo(transformer2.hashCode());
      assertThat(transformer1.hashCode()).isNotEqualTo(transformer3.hashCode());
   }

   @Test
   public void testHashCodeWhenNullSourceAndMapperResult() {
      SchwartzianTransformer<String, Integer> transformer1 = new SchwartzianTransformer<>(null, __ -> null);
      SchwartzianTransformer<String, Integer> transformer2 = new SchwartzianTransformer<>(null, __ -> null);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("43", Integer::parseInt);

      assertThat(transformer1.hashCode()).isEqualTo(transformer2.hashCode());
      assertThat(transformer1.hashCode()).isNotEqualTo(transformer3.hashCode());
   }

   @Test
   public void testCompareToWhenNominal() {
      SchwartzianTransformer<String, Integer> transformer1 =
            new SchwartzianTransformer<>("42", Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer2 =
            new SchwartzianTransformer<>("42", Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("43", Integer::parseInt);

      assertThat(transformer1).isEqualByComparingTo(transformer2);
      assertThat(transformer1).isLessThan(transformer3);
      assertThat(transformer2).isEqualByComparingTo(transformer1);
      assertThat(transformer3).isGreaterThan(transformer1);
   }

   @Test
   public void testCompareToWhenNullSource() {
      SchwartzianTransformer<String, Integer> transformer1 =
            new SchwartzianTransformer<>(null, Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer2 =
            new SchwartzianTransformer<>(null, Integer::parseInt);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("43", Integer::parseInt);

      assertThat(transformer1).isEqualByComparingTo(transformer2);
      assertThat(transformer1).isLessThan(transformer3);
      assertThat(transformer2).isEqualByComparingTo(transformer1);
      assertThat(transformer3).isGreaterThan(transformer1);
   }

   @Test
   public void testCompareToWhenNullMapperResult() {
      SchwartzianTransformer<String, Integer> transformer1 = new SchwartzianTransformer<>("42", __ -> null);
      SchwartzianTransformer<String, Integer> transformer2 = new SchwartzianTransformer<>("42", __ -> null);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("42", Integer::parseInt);

      assertThat(transformer1).isEqualByComparingTo(transformer2);
      assertThat(transformer1).isLessThan(transformer3);
      assertThat(transformer2).isEqualByComparingTo(transformer1);
      assertThat(transformer3).isGreaterThan(transformer1);
   }

   @Test
   public void testCompareToWhenNullSourceAndMapperResult() {
      SchwartzianTransformer<String, Integer> transformer1 = new SchwartzianTransformer<>(null, __ -> null);
      SchwartzianTransformer<String, Integer> transformer2 = new SchwartzianTransformer<>(null, __ -> null);
      SchwartzianTransformer<String, Integer> transformer3 =
            new SchwartzianTransformer<>("43", Integer::parseInt);

      assertThat(transformer1).isEqualByComparingTo(transformer2);
      assertThat(transformer1).isLessThan(transformer3);
      assertThat(transformer2).isEqualByComparingTo(transformer1);
      assertThat(transformer3).isGreaterThan(transformer1);
   }

   @Test
   public void testSchwartzianTransformerWhenSortWhenNominal() {
      assertThat(Stream
                       .of(2, 1, 3, 5, 4)
                       .map(d -> SchwartzianTransformer.wrap(d, Function.identity()))
                       .sorted()
                       .map(SchwartzianTransformer::unwrap)
                       .collect(toList())).containsExactly(1, 2, 3, 4, 5);
   }

   @Test
   public void testSchwartzianTransformerWhenSortWhenDuplicates() {
      assertThat(Stream
                       .of(2, 3, 1, 3, 5, 4, 1, 1)
                       .map(d -> SchwartzianTransformer.wrap(d, Function.identity()))
                       .sorted()
                       .map(SchwartzianTransformer::unwrap)
                       .collect(toList())).containsExactly(1, 1, 1, 2, 3, 3, 4, 5);
   }

   @Test
   public void testSchwartzianTransformerWhenSortWhenNulls() {
      assertThat(Stream
                       .of(2, null, 1, null, 3, 5, 4)
                       .map(d -> SchwartzianTransformer.wrap(d, Function.identity()))
                       .sorted()
                       .map(SchwartzianTransformer::unwrap)
                       .collect(toList())).containsExactly(null, null, 1, 2, 3, 4, 5);
   }

   @Test
   public void testSchwartzianTransformerWhenDistinctWhenNominal() {
      assertThat(Stream
                       .of(2, 1, 3, 1, 5, 4, 3, 3)
                       .map(d -> SchwartzianTransformer.wrap(d, Function.identity()))
                       .distinct()
                       .map(SchwartzianTransformer::unwrap)
                       .collect(toList())).containsExactly(2, 1, 3, 5, 4);

   }

   @Test
   public void testSchwartzianTransformerWhenDistinctWhenNulls() {
      assertThat(Stream
                       .of(2, 1, null, 3, 1, 5, 4, 3, 3, null)
                       .map(d -> SchwartzianTransformer.wrap(d, Function.identity()))
                       .distinct()
                       .map(SchwartzianTransformer::unwrap)
                       .collect(toList())).containsExactly(2, 1, null, 3, 5, 4);

   }
}