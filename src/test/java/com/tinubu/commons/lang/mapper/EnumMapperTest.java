/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

public class EnumMapperTest {

   @Test
   public void testMapperWhenNominal() {
      EnumMapper<MyEnum, Integer> mapper = new EnumMapper<>(MyEnum.class, MyEnum::value);

      assertThat(mapper.map(1)).isEqualTo(MyEnum.VALUE1);
      assertThat(mapper.map(2)).isEqualTo(MyEnum.VALUE2);
      assertThat(mapper.map(3)).isEqualTo(MyEnum.VALUE3);
   }

   @Test
   public void testMapperWhenNull() {
      EnumMapper<MyEnum, Integer> mapper = new EnumMapper<>(MyEnum.class, MyEnum::value);

      assertThat(mapper.map(null)).isNull();
   }

   @Test
   public void testMapperWhenNonUniqueMappedValues() {
      assertThatThrownBy(() -> new EnumMapper<>(MyEnumWithDuplicates.class, MyEnumWithDuplicates::value))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Mapped values must be unique : [ 1 ]");
   }

   enum MyEnum {

      VALUE1(1), VALUE2(2), VALUE3(3);

      int value;

      MyEnum(int value) {
         this.value = value;
      }

      public int value() {
         return value;
      }
   }

   enum MyEnumWithDuplicates {

      VALUE1(1), VALUE2(1), VALUE3(3), VALUE4(1);

      int value;

      MyEnumWithDuplicates(int value) {
         this.value = value;
      }

      public int value() {
         return value;
      }
   }
}